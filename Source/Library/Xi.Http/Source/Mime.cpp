// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Http/Mime.hpp"

#include <utility>

#include <Xi/String.hpp>

XI_ERROR_CODE_CATEGORY_BEGIN(Xi::Http, Mime)
XI_ERROR_CODE_DESC(IllFormed, "mime type is ill formed")
XI_ERROR_CODE_CATEGORY_END()

namespace Xi {
namespace Http {

const Mime Mime::Any{/* */};

const Mime Mime::Text::Any{"text"};
const Mime Mime::Text::Plain{Mime::Text::Any, "plain"};
const Mime Mime::Text::Css{Mime::Text::Any, "css"};
const Mime Mime::Text::Html{Mime::Text::Any, "html"};
const Mime Mime::Text::Javascript{Mime::Text::Any, "javascript"};
const Mime Mime::Text::Markdown{Mime::Text::Any, "markdown"};

const Mime Mime::Application::Any{"application"};
const Mime Mime::Application::Json{Mime::Application::Any, "json"};
const Mime Mime::Application::Xml{Mime::Application::Any, "xml"};
const Mime Mime::Application::Yaml{Mime::Application::Any, "vnd.yaml"};

Result<Mime> Mime::parse(const std::string &str) {
  XI_SUCCEED_IF(str == "*/*", Mime::Any)
  if (endsWith(str, "/*")) {
    return makeMime(str.substr(0, str.size() - 2));
  }
  const auto splitPos = str.find('/');
  XI_FAIL_IF(splitPos == std::string::npos, MimeError::IllFormed)
  XI_FAIL_IF(str.find('/', splitPos) != std::string::npos, MimeError::IllFormed)
  return makeMime(std::string{str.substr(0, splitPos)}, std::string{str.substr(splitPos)});
}

const std::optional<std::string> &Mime::type() const {
  return m_type;
}

const std::optional<std::string> &Mime::subtype() const {
  return m_subtype;
}

bool Mime::isAny() const {
  return !type();
}

bool Mime::isPartial() const {
  return !subtype();
}

std::string Mime::stringify() const {
  if (!type()) {
    return "*/*";
  } else if (!isPartial()) {
    return toString("{}/*", *type());
  } else {
    return toString("{}/{}", *type(), *subtype());
  }
}

Mime::Mime(const std::string &type_, const std::optional<std::string> &subtype_) : m_type{type_}, m_subtype{subtype_} {
  /* */
}

Mime::Mime(const Mime &parent, const std::string &subtype) : Mime(*parent.type(), subtype) {
  /* */
}

Result<Mime> makeMime(const std::string &type, const std::optional<std::string> &subtype) {
  XI_FAIL_IF(type.find('/') != std::string::npos, MimeError::IllFormed)
  XI_FAIL_IF(type.find(' ') != std::string::npos, MimeError::IllFormed)
  if (subtype) {
    XI_FAIL_IF(subtype->find('/') != std::string::npos, MimeError::IllFormed)
    XI_FAIL_IF(subtype->find(' ') != std::string::npos, MimeError::IllFormed)
  }
  Mime reval{/* */};
  reval.m_type = type;
  reval.m_subtype = subtype;
  XI_SUCCEED(std::move(reval))
}

}  // namespace Http
}  // namespace Xi
