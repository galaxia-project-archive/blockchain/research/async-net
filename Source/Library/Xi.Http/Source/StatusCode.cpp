// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Http/StatusCode.hpp"

#include <Xi/Extern/Push.hh>
#include <boost/beast/http/status.hpp>
#include <Xi/Extern/Pop.hh>

#include <Xi/ErrorModel.hh>

namespace Xi {
namespace Http {

std::string stringify(const StatusCode code) {
  return boost::beast::http::obsolete_reason(static_cast<boost::beast::http::status>(code)).to_string();
}

}  // namespace Http
}  // namespace Xi

static_assert(static_cast<boost::beast::http::status>(Xi::Http::StatusCode::Ok) == boost::beast::http::status::ok,
              "The status code must correspond to the equivalent beast library status code because they internally "
              "converted seamlessly.");
static_assert(static_cast<boost::beast::http::status>(Xi::Http::StatusCode::Created) ==
                  boost::beast::http::status::created,
              "The status code must correspond to the equivalent beast library status code because they internally "
              "converted seamlessly.");
static_assert(static_cast<boost::beast::http::status>(Xi::Http::StatusCode::Accepted) ==
                  boost::beast::http::status::accepted,
              "The status code must correspond to the equivalent beast library status code because they internally "
              "converted seamlessly.");
static_assert(static_cast<boost::beast::http::status>(Xi::Http::StatusCode::NoContent) ==
                  boost::beast::http::status::no_content,
              "The status code must correspond to the equivalent beast library status code because they internally "
              "converted seamlessly.");
static_assert(static_cast<boost::beast::http::status>(Xi::Http::StatusCode::MovedPermanently) ==
                  boost::beast::http::status::moved_permanently,
              "The status code must correspond to the equivalent beast library status code because they internally "
              "converted seamlessly.");
static_assert(static_cast<boost::beast::http::status>(Xi::Http::StatusCode::Found) == boost::beast::http::status::found,
              "The status code must correspond to the equivalent beast library status code because they internally "
              "converted seamlessly.");
static_assert(static_cast<boost::beast::http::status>(Xi::Http::StatusCode::SeeOther) ==
                  boost::beast::http::status::see_other,
              "The status code must correspond to the equivalent beast library status code because they internally "
              "converted seamlessly.");
static_assert(static_cast<boost::beast::http::status>(Xi::Http::StatusCode::TemporaryRedirect) ==
                  boost::beast::http::status::temporary_redirect,
              "The status code must correspond to the equivalent beast library status code because they internally "
              "converted seamlessly.");
static_assert(static_cast<boost::beast::http::status>(Xi::Http::StatusCode::PermanentRedirect) ==
                  boost::beast::http::status::permanent_redirect,
              "The status code must correspond to the equivalent beast library status code because they internally "
              "converted seamlessly.");
static_assert(static_cast<boost::beast::http::status>(Xi::Http::StatusCode::BadRequest) ==
                  boost::beast::http::status::bad_request,
              "The status code must correspond to the equivalent beast library status code because they internally "
              "converted seamlessly.");
static_assert(static_cast<boost::beast::http::status>(Xi::Http::StatusCode::Unauthorized) ==
                  boost::beast::http::status::unauthorized,
              "The status code must correspond to the equivalent beast library status code because they internally "
              "converted seamlessly.");
static_assert(static_cast<boost::beast::http::status>(Xi::Http::StatusCode::Forbidden) ==
                  boost::beast::http::status::forbidden,
              "The status code must correspond to the equivalent beast library status code because they internally "
              "converted seamlessly.");
static_assert(static_cast<boost::beast::http::status>(Xi::Http::StatusCode::NotFound) ==
                  boost::beast::http::status::not_found,
              "The status code must correspond to the equivalent beast library status code because they internally "
              "converted seamlessly.");
static_assert(static_cast<boost::beast::http::status>(Xi::Http::StatusCode::MethodNotAllowed) ==
                  boost::beast::http::status::method_not_allowed,
              "The status code must correspond to the equivalent beast library status code because they internally "
              "converted seamlessly.");
static_assert(static_cast<boost::beast::http::status>(Xi::Http::StatusCode::RequestTimeout) ==
                  boost::beast::http::status::request_timeout,
              "The status code must correspond to the equivalent beast library status code because they internally "
              "converted seamlessly.");
static_assert(static_cast<boost::beast::http::status>(Xi::Http::StatusCode::Gone) == boost::beast::http::status::gone,
              "The status code must correspond to the equivalent beast library status code because they internally "
              "converted seamlessly.");
static_assert(static_cast<boost::beast::http::status>(Xi::Http::StatusCode::LengthRequired) ==
                  boost::beast::http::status::length_required,
              "The status code must correspond to the equivalent beast library status code because they internally "
              "converted seamlessly.");
static_assert(static_cast<boost::beast::http::status>(Xi::Http::StatusCode::UnsupportedMediaType) ==
                  boost::beast::http::status::unsupported_media_type,
              "The status code must correspond to the equivalent beast library status code because they internally "
              "converted seamlessly.");
static_assert(static_cast<boost::beast::http::status>(Xi::Http::StatusCode::TooManyRequests) ==
                  boost::beast::http::status::too_many_requests,
              "The status code must correspond to the equivalent beast library status code because they internally "
              "converted seamlessly.");
static_assert(static_cast<boost::beast::http::status>(Xi::Http::StatusCode::InternalServerError) ==
                  boost::beast::http::status::internal_server_error,
              "The status code must correspond to the equivalent beast library status code because they internally "
              "converted seamlessly.");
static_assert(static_cast<boost::beast::http::status>(Xi::Http::StatusCode::NotImplemented) ==
                  boost::beast::http::status::not_implemented,
              "The status code must correspond to the equivalent beast library status code because they internally "
              "converted seamlessly.");
static_assert(static_cast<boost::beast::http::status>(Xi::Http::StatusCode::BadGateway) ==
                  boost::beast::http::status::bad_gateway,
              "The status code must correspond to the equivalent beast library status code because they internally "
              "converted seamlessly.");
static_assert(static_cast<boost::beast::http::status>(Xi::Http::StatusCode::ServiceUnavailable) ==
                  boost::beast::http::status::service_unavailable,
              "The status code must correspond to the equivalent beast library status code because they internally "
              "converted seamlessly.");
static_assert(static_cast<boost::beast::http::status>(Xi::Http::StatusCode::GatewayTimeout) ==
                  boost::beast::http::status::gateway_timeout,
              "The status code must correspond to the equivalent beast library status code because they internally "
              "converted seamlessly.");
static_assert(static_cast<boost::beast::http::status>(Xi::Http::StatusCode::HttpVersionNotSupported) ==
                  boost::beast::http::status::http_version_not_supported,
              "The status code must correspond to the equivalent beast library status code because they internally "
              "converted seamlessly.");
static_assert(static_cast<boost::beast::http::status>(Xi::Http::StatusCode::NetworkAuthenticationRequired) ==
                  boost::beast::http::status::network_authentication_required,
              "The status code must correspond to the equivalent beast library status code because they internally "
              "converted seamlessly.");
static_assert(static_cast<boost::beast::http::status>(Xi::Http::StatusCode::NetworkConnectTimeout) ==
                  boost::beast::http::status::network_connect_timeout_error,
              "The status code must correspond to the equivalent beast library status code because they internally "
              "converted seamlessly.");
