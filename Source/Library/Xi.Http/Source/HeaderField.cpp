// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Http/HeaderField.hpp"

#include <Xi/Extern/Push.hh>
#include <boost/beast/http/field.hpp>
#include <Xi/Extern/Pop.hh>

#include <Xi/ErrorModel.hh>

namespace Xi {
namespace Http {

std::string stringify(const HeaderField field) {
  return boost::beast::http::to_string(static_cast<boost::beast::http::field>(field)).to_string();
}

}  // namespace Http
}  // namespace Xi

static_assert(
    static_cast<boost::beast::http::field>(Xi::Http::HeaderField::WWWAuthenticate) ==
        boost::beast::http::field::www_authenticate,
    "The casted value must correspond to the enum used by the beast library because they are internally casted.");
static_assert(
    static_cast<boost::beast::http::field>(Xi::Http::HeaderField::Authorization) ==
        boost::beast::http::field::authorization,
    "The casted value must correspond to the enum used by the beast library because they are internally casted.");
static_assert(
    static_cast<boost::beast::http::field>(Xi::Http::HeaderField::Connection) == boost::beast::http::field::connection,
    "The casted value must correspond to the enum used by the beast library because they are internally casted.");
static_assert(
    static_cast<boost::beast::http::field>(Xi::Http::HeaderField::KeepAlive) == boost::beast::http::field::keep_alive,
    "The casted value must correspond to the enum used by the beast library because they are internally casted.");
static_assert(
    static_cast<boost::beast::http::field>(Xi::Http::HeaderField::Accept) == boost::beast::http::field::accept,
    "The casted value must correspond to the enum used by the beast library because they are internally casted.");
static_assert(
    static_cast<boost::beast::http::field>(Xi::Http::HeaderField::AcceptCharset) ==
        boost::beast::http::field::accept_charset,
    "The casted value must correspond to the enum used by the beast library because they are internally casted.");
static_assert(
    static_cast<boost::beast::http::field>(Xi::Http::HeaderField::AcceptEncoding) ==
        boost::beast::http::field::accept_encoding,
    "The casted value must correspond to the enum used by the beast library because they are internally casted.");
static_assert(
    static_cast<boost::beast::http::field>(Xi::Http::HeaderField::ContentType) ==
        boost::beast::http::field::content_type,
    "The casted value must correspond to the enum used by the beast library because they are internally casted.");
static_assert(
    static_cast<boost::beast::http::field>(Xi::Http::HeaderField::ContentEncoding) ==
        boost::beast::http::field::content_encoding,
    "The casted value must correspond to the enum used by the beast library because they are internally casted.");
static_assert(
    static_cast<boost::beast::http::field>(Xi::Http::HeaderField::Location) == boost::beast::http::field::location,
    "The casted value must correspond to the enum used by the beast library because they are internally casted.");
static_assert(
    static_cast<boost::beast::http::field>(Xi::Http::HeaderField::Allow) == boost::beast::http::field::allow,
    "The casted value must correspond to the enum used by the beast library because they are internally casted.");
static_assert(
    static_cast<boost::beast::http::field>(Xi::Http::HeaderField::Server) == boost::beast::http::field::server,
    "The casted value must correspond to the enum used by the beast library because they are internally casted.");
static_assert(
    static_cast<boost::beast::http::field>(Xi::Http::HeaderField::AccessControlAllowOrigin) ==
        boost::beast::http::field::access_control_allow_origin,
    "The casted value must correspond to the enum used by the beast library because they are internally casted.");
static_assert(
    static_cast<boost::beast::http::field>(Xi::Http::HeaderField::AccessControlAllowMethods) ==
        boost::beast::http::field::access_control_allow_methods,
    "The casted value must correspond to the enum used by the beast library because they are internally casted.");
