// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Http/Method.hpp"

XI_ERROR_CODE_CATEGORY_BEGIN(Xi::Http, Method)
XI_ERROR_CODE_DESC(Unknown, "http mehtod not recognized (case sensitive)")
XI_ERROR_CODE_CATEGORY_END()

namespace Xi {
namespace Http {

Result<Method> parse(const std::string &str) {
  if (str == "DELETE") {
    XI_SUCCEED(Method::Delete)
  } else if (str == "GET") {
    XI_SUCCEED(Method::Get)
  } else if (str == "HEAD") {
    XI_SUCCEED(Method::Head)
  } else if (str == "POST") {
    XI_SUCCEED(Method::Post)
  } else if (str == "PUT") {
    XI_SUCCEED(Method::Put)
  } else if (str == "CONNECT") {
    XI_SUCCEED(Method::Connect)
  } else if (str == "OPTIONS") {
    XI_SUCCEED(Method::Options)
  } else if (str == "TRACE") {
    XI_SUCCEED(Method::Trace)
  } else if (str == "PATCH") {
    XI_SUCCEED(Method::Patch)
  } else {
    XI_FAIL(MethodError::Unknown)
  }
}

std::string stringify(const Method method) {
  switch (method) {
    case Http::Method::Delete:
      return "DELETE";
    case Http::Method::Get:
      return "GET";
    case Http::Method::Head:
      return "HEAD";
    case Http::Method::Post:
      return "POST";
    case Http::Method::Put:
      return "PUT";
    case Http::Method::Connect:
      return "CONNECT";
    case Http::Method::Options:
      return "OPTIONS";
    case Http::Method::Trace:
      return "TRACE";
    case Http::Method::Patch:
      return "PATCH";
  }
  XI_EXCEPTIONAL(InvalidEnumValueError)
}

}  // namespace Http
}  // namespace Xi
