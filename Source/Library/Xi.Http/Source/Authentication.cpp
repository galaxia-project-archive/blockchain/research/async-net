// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Http/Authentication.hpp"

#include <Xi/String.hpp>
#include <fmt/ostream.h>

XI_ERROR_CODE_CATEGORY_BEGIN(Xi::Http, Authentication)
XI_ERROR_CODE_DESC(Empty, "authentication is empty")
XI_ERROR_CODE_DESC(UnknownScheme, "authentication scheme not recognized")
XI_ERROR_CODE_CATEGORY_END()

namespace Xi {
namespace Http {

Result<Authentication> Authentication::parse(const std::string &str) {
  XI_FAIL_IF(str.empty(), AuthenticationError::Empty)
  if (startsWith(str, "Basic ")) {
    auto basic = fromString<BasicAuthentication>(str.substr(6));
    XI_ERROR_PROPAGATE(basic)
    XI_SUCCEED(Authentication{basic.take()});
  } else if (startsWith(str, "Bearer ")) {
    auto bearer = fromString<BearerAuthentication>(str.substr(7));
    XI_ERROR_PROPAGATE(bearer)
    XI_SUCCEED(Authentication{bearer.take()});
  } else {
    XI_FAIL(AuthenticationError::UnknownScheme)
  }
}

std::string Authentication::stringify() const {
  if (std::holds_alternative<BasicAuthentication>(*this)) {
    return toString("Basic {}", std::get<BasicAuthentication>(*this).stringify());
  } else if (std::holds_alternative<BearerAuthentication>(*this)) {
    return toString("Bearer {}", std::get<BearerAuthentication>(*this).stringify());
  } else {
    XI_EXCEPTIONAL(InvalidVariantTypeError)
  }
}

}  // namespace Http
}  // namespace Xi
