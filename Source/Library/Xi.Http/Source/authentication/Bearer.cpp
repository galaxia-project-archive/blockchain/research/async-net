// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Http/Authentication/Bearer.hpp"

namespace Xi {
namespace Http {

Result<BearerAuthentication> BearerAuthentication::parse(const std::string &str) {
  return makeBearerAuthentication(str);
}

const std::string &BearerAuthentication::token() const {
  return m_token;
}

std::string BearerAuthentication::stringify() const {
  return token();
}

Result<BearerAuthentication> makeBearerAuthentication(const std::string &token) {
  XI_FAIL_IF(token.empty(), BearerAuthenticationError::TokenIsEmpty);
  XI_FAIL_IF(token.find(' ') != std::string::npos, BearerAuthenticationError::TokenContainsSpaces);
  BearerAuthentication reval{};
  reval.m_token = token;
  XI_SUCCEED(std::move(reval));
}

}  // namespace Http
}  // namespace Xi
