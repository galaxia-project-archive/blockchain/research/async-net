// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <string>

#include <Xi/Global.hh>
#include <Xi/ErrorModel.hh>

namespace Xi {
namespace Http {

XI_ERROR_CODE_BEGIN(Method)
XI_ERROR_CODE_VALUE(Unknown, 0x0001)
XI_ERROR_CODE_END(Method, "Http::MethodError")

enum struct Method {
  Delete = 1,  ///< The DELETE method deletes the specified resource.
  Get = 2,     ///< The GET method requests a representation of the specified resource. Requests using GET should only
  ///< retrieve data.
  Head = 3,  ///< The HEAD method asks for a response identical to that of a GET request, but without the response body.
  Post = 4,  ///< The POST method is used to submit an entity to the specified resource, often causing a change in state
  ///< or side effects on the server.
  Put = 5,  ///< The PUT method replaces all current representations of the target resource with the request payload.
  Connect = 6,  ///< The CONNECT method establishes a tunnel to the server identified by the target resource.
  Options = 7,  ///< The OPTIONS method is used to describe the communication options for the target resource.
  Trace = 8,    ///< The TRACE method performs a message loop-back test along the path to the target resource.
  Patch = 29,   ///< The PATCH method is used to apply partial modifications to a resource.
};

Result<Method> parse(const std::string& str);
std::string stringify(const Method method);

}  // namespace Http
}  // namespace Xi

XI_ERROR_CODE_OVERLOADS(Xi::Http, Method)
