// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <string>

#include <Xi/Global.hh>

namespace Xi {
namespace Http {

/*!
 * HTTP headers allow the client and the server to pass additional information with the request or the response. An
 * HTTP header consists of its case-insensitive name followed by a colon ':', then by its value (without line breaks).
 * Leading white space before the value is ignored.
 *
 * Documentation is taken from https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers
 */
enum struct HeaderField {
  WWWAuthenticate = 329,  ///< The HTTP WWW-Authenticate response header defines the authentication method that should
  ///< be used to gain access to a resource.
  Authorization = 39,  ///< Contains the credentials to authenticate a user agent with a server.
  Connection = 59,     ///< Controls whether the network connection stays open after the current transaction finishes.
  KeepAlive = 160,     ///< Controls how long a persistent connection should stay open.
  Accept = 2,          ///< Informs the server about the types of data that can be sent back. It is MIME-type.
  AcceptCharset = 4,   ///< Informs the server about which character set the client is able to understand.
  AcceptEncoding = 6,  ///< Informs the server about the encoding algorithm, usually a compression algorithm, that can
  ///< be used on the resource sent back.
  ContentType = 78,      ///< Indicates the media type of the resource.
  ContentEncoding = 65,  ///< The Content-Encoding entity header is used to compress the media-type. When present, its
  ///< value indicates which encodings were applied to the entity-body. It lets the client know
  ///< how to decode in order to obtain the media-type referenced by the Content-Type header.
  Location = 176,  ///< Indicates the URL to redirect a page to.
  Allow = 22,      ///< Lists the set of HTTP request methods support by a resource.
  Server = 286,    ///< Contains information about the software used by the origin server to handle the request.
  AccessControlAllowOrigin = 16,  ///< The Access-Control-Allow-Origin response header indicates whether the response
  ///< can be shared with requesting code from the given origin.
  AccessControlAllowMethods =
      15,  ///< The Access-Control-Allow-Methods response header specifies the method or methods allowed when
  ///< accessing the resource in response to a preflight request.
};

std::string stringify(const HeaderField field);

}  // namespace Http
}  // namespace Xi
