// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <string_view>
#include <string>
#include <optional>
#include <map>

#include <Xi/Global.hh>
#include <Xi/ErrorModel.hh>

#include "Xi/Http/HeaderField.hpp"
#include "Xi/Http/Authentication.hpp"

namespace Xi {
namespace Http {

class Header {
  std::optional<Result<Authentication>> authorization() const;
  void setAuthorization(const Authentication& auth);

  [[nodiscard]] bool contains(const HeaderField field) const;

 private:
  std::map<HeaderField, std::string> m_raw;
};

}  // namespace Http
}  // namespace Xi
