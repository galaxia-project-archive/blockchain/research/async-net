// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <Xi/Global.hh>
#include <Xi/ErrorModel.hh>

namespace Xi {
namespace Http {

XI_ERROR_CODE_BEGIN(BasicAuthentication)
XI_ERROR_CODE_VALUE(NoSeperator, 0x0001)
XI_ERROR_CODE_VALUE(MultipleSeperators, 0x0002)
XI_ERROR_CODE_VALUE(InvalidEncoding, 0x0003)
XI_ERROR_CODE_VALUE(EmptyUsername, 0x0004)
XI_ERROR_CODE_VALUE(EmptyPassword, 0x0005)
XI_ERROR_CODE_VALUE(InvalidUsername, 0x0006)
XI_ERROR_CODE_VALUE(InvalidPassword, 0x0007)
XI_ERROR_CODE_END(BasicAuthentication, "Http::BasicAuthenticationError")

class BasicAuthentication {
 public:
  static Result<BasicAuthentication> parse(const std::string& str);

 public:
  XI_DEFAULT_COPY(BasicAuthentication)
  XI_DEFAULT_MOVE(BasicAuthentication)
  ~BasicAuthentication() = default;

  const std::string& username() const;
  const std::string& password() const;

  std::string stringify() const;

 private:
  explicit BasicAuthentication() = default;

  friend Result<BasicAuthentication> makeBasicAuthentication(const std::string&, const std::string&);

 private:
  std::string m_username{/* */};
  std::string m_password{/* */};
};

Result<BasicAuthentication> makeBasicAuthentication(const std::string& username, const std::string& password);

}  // namespace Http
}  // namespace Xi

XI_ERROR_CODE_OVERLOADS(Xi::Http, BasicAuthentication)
