// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <string>
#include <functional>
#include <cinttypes>

#include <Xi/Global.hh>
#include <Xi/ErrorModel.hh>
#include <Xi/Algorithm/Math.hh>

namespace Xi {
namespace Http {

XI_ERROR_CODE_BEGIN(QualityValue)
XI_ERROR_CODE_VALUE(IllFormed, 0x0001)
XI_ERROR_CODE_VALUE(OutOfRange, 0x0002)
XI_ERROR_CODE_END(QualityValue, "Http::QualityValueError")

class QualityValue final {
 public:
  using value_type = uint32_t;

  static constexpr value_type Digits = 3;
  static constexpr value_type Minimum = 0;
  static constexpr value_type Maximum = pow<value_type>(10, Digits);

 public:
  static Result<QualityValue> parse(const std::string& str);

 public:
  explicit QualityValue(value_type value = Maximum);
  explicit QualityValue(float value);
  explicit QualityValue(double value);
  XI_DEFAULT_COPY(QualityValue);
  XI_DELETE_MOVE(QualityValue);
  ~QualityValue() = default;

 public:
  bool operator==(const QualityValue& rhs) const;
  bool operator!=(const QualityValue& rhs) const;

  bool operator<(const QualityValue& rhs) const;
  bool operator<=(const QualityValue& rhs) const;
  bool operator>(const QualityValue& rhs) const;
  bool operator>=(const QualityValue& rhs) const;

 public:
  std::string stringify() const;

 private:
  friend struct std::hash<QualityValue>;

  /// 0.000-1.000 (3 decimals)
  value_type m_value;
};

}  // namespace Http
}  // namespace Xi

XI_ERROR_CODE_OVERLOADS(Xi::Http, QualityValue)

namespace std {
template <>
struct hash<Xi::Http::QualityValue> {
  size_t operator()(const Xi::Http::QualityValue& value) const;
};
}  // namespace std
