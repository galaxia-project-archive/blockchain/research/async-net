// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <string>
#include <optional>
#include <vector>

#include <Xi/Global.hh>
#include <Xi/ErrorModel.hh>

namespace Xi {
namespace Http {

XI_ERROR_CODE_BEGIN(Mime)
XI_ERROR_CODE_VALUE(IllFormed, 0x0001)
XI_ERROR_CODE_END(Mime, "Http::MimeError")

class Mime;
using MimeVector = std::vector<Mime>;

class Mime final {
 public:
  static const Mime Any;

  class Text final {
    Text() = delete;

   public:
    static const Mime Any;
    static const Mime Plain;
    static const Mime Css;
    static const Mime Html;
    static const Mime Javascript;
    static const Mime Markdown;
  };

  class Application final {
    Application() = delete;

   public:
    static const Mime Any;
    static const Mime Json;
    static const Mime Xml;
    static const Mime Yaml;
  };

 public:
  static Result<Mime> parse(const std::string& str);

 public:
  XI_DEFAULT_COPY(Mime)
  XI_DEFAULT_MOVE(Mime)
  ~Mime() = default;

  const std::optional<std::string>& type() const;
  const std::optional<std::string>& subtype() const;

  bool isAny() const;
  bool isPartial() const;

  std::string stringify() const;

 private:
  explicit Mime() = default;
  explicit Mime(const Mime& parent, const std::string& subtype);
  explicit Mime(const std::string& type, const std::optional<std::string>& subtype = std::nullopt);

  friend class MimeType;
  friend Result<Mime> makeMime(const std::string&, const std::optional<std::string>&);

 private:
  std::optional<std::string> m_type{std::nullopt};
  std::optional<std::string> m_subtype{std::nullopt};
};

Result<Mime> makeMime(const std::string& type, const std::optional<std::string>& subtype = std::nullopt);

}  // namespace Http
}  // namespace Xi

XI_ERROR_CODE_OVERLOADS(Xi::Http, Mime)
