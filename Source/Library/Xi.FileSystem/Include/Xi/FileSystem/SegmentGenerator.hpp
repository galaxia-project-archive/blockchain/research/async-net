// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <filesystem>

namespace Xi {
namespace FileSystem {

/*!
 * \brief The SegmentGenerator class encapsulates the concepts of iterating the segments of a filepath.
 */
class SegmentGenerator {
 public:
  using iterator = std::filesystem::path::const_iterator;

  iterator begin() const;
  iterator end() const;
  iterator cbegin() const;
  iterator cend() const;

 private:
  explicit SegmentGenerator(iterator begin, iterator end);

  friend class File;
  friend class Directory;

 private:
  iterator m_begin;
  iterator m_end;
};

}  // namespace FileSystem
}  // namespace Xi
