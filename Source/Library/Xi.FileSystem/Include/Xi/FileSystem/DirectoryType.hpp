// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <string>

#include <Xi/Serialization/Enum.hpp>

namespace Xi {
namespace FileSystem {

enum struct DirectoryType {
  None = 0,
  Regular = 1,
  SymbolicLink = 2,
  Unknown = 3,
};

XI_SERIALIZATION_ENUM(DirectoryType)

std::string stringify(DirectoryType dtype);

}  // namespace FileSystem
}  // namespace Xi

XI_SERIALIZATION_ENUM_RANGE(Xi::FileSystem::DirectoryType, Regular, Unknown)
XI_SERIALIZATION_ENUM_TAG(Xi::FileSystem::DirectoryType, Regular, "regular")
XI_SERIALIZATION_ENUM_TAG(Xi::FileSystem::DirectoryType, SymbolicLink, "symbolic_link")
XI_SERIALIZATION_ENUM_TAG(Xi::FileSystem::DirectoryType, Unknown, "unknown")
