// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <Xi/ErrorModel.hh>

namespace Xi {
namespace FileSystem {

XI_ERROR_CODE_BEGIN(Directory)
XI_ERROR_CODE_VALUE(Internal, 0x0001)
XI_ERROR_CODE_VALUE(IsFile, 0x0002)
XI_ERROR_CODE_VALUE(NotFound, 0x0003)
XI_ERROR_CODE_VALUE(AlreadyExists, 0x0004)
XI_ERROR_CODE_END(Directory, "FileSystem::DirectoryError")

}  // namespace FileSystem
}  // namespace Xi

XI_ERROR_CODE_OVERLOADS(Xi::FileSystem, Directory)
