// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <string>

#include <Xi/Serialization/Enum.hpp>

namespace Xi {
namespace FileSystem {

enum struct FileType {
  None = 0,
  Regular,
  SymbolicLink,
  Block,
  Character,
  Pipe,
  Socket,
  Unknown,
};

XI_SERIALIZATION_ENUM(FileType)

std::string stringify(const FileType ftype);

}  // namespace FileSystem
}  // namespace Xi

XI_SERIALIZATION_ENUM_RANGE(Xi::FileSystem::FileType, Regular, Unknown)
XI_SERIALIZATION_ENUM_TAG(Xi::FileSystem::FileType, Regular, "regular")
XI_SERIALIZATION_ENUM_TAG(Xi::FileSystem::FileType, SymbolicLink, "symbolic_link")
XI_SERIALIZATION_ENUM_TAG(Xi::FileSystem::FileType, Block, "block")
XI_SERIALIZATION_ENUM_TAG(Xi::FileSystem::FileType, Character, "character")
XI_SERIALIZATION_ENUM_TAG(Xi::FileSystem::FileType, Pipe, "pipe")
XI_SERIALIZATION_ENUM_TAG(Xi::FileSystem::FileType, Socket, "socket")
XI_SERIALIZATION_ENUM_TAG(Xi::FileSystem::FileType, Unknown, "unknown")
