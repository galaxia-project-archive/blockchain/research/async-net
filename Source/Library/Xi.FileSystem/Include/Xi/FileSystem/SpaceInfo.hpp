// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <filesystem>

#include <Xi/Memory/Size.hpp>

namespace Xi {
namespace FileSystem {

/// Represents memory available on a drive.
struct SpaceInfo {
  /*!
   * \brief SpaceInfo Constructs a simple space info.
   * \param available_ Number of bytes free to use.
   * \param capacity_ Maximum number of bytes the underlying storage may keep.
   */
  explicit SpaceInfo(Memory::Bytes available_, Memory::Bytes capacity_);
  /*!
   * \brief SpaceInfo Constructs a simple space info.
   * \param available_ Number of bytes not in ues.
   * \param capacity_ Maximum number of bytes the underlying storage may keep.
   * \param free_ Number of bytes free to use.
   *
   * \note On POSIX systems available and free bytes may are not the same due to user permissions.
   */
  explicit SpaceInfo(Memory::Bytes available_, Memory::Bytes capacity_, Memory::Bytes free_);

  /// Number of bytes not occupied.
  Memory::Bytes available() const;
  /// Maximum number of bytes the correpsonding storage may store.
  Memory::Bytes capacity() const;
  /// Number of bytes that can be created on the storage.
  Memory::Bytes free() const;
  /// Number of bytes already used on the storage.
  Memory::Bytes taken() const;

 private:
  Memory::Bytes m_available;
  Memory::Bytes m_capacity;
  Memory::Bytes m_free;
};

/// Constructs a space info from an standard filesystem space info object.
SpaceInfo fromStd(const std::filesystem::space_info& space);

}  // namespace FileSystem
}  // namespace Xi
