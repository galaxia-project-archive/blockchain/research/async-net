// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <Xi/ErrorModel.hh>

namespace Xi {
namespace FileSystem {

XI_ERROR_CODE_BEGIN(FileSystem)
XI_ERROR_CODE_VALUE(NotFound, 0x0001)
XI_ERROR_CODE_END(FileSystem, "FileSystemError")

}  // namespace FileSystem
}  // namespace Xi

XI_ERROR_CODE_OVERLOADS(Xi::FileSystem, FileSystem)
