// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <Xi/ErrorModel.hh>

namespace Xi {
namespace FileSystem {

/// Error occured on a file operation.
XI_ERROR_CODE_BEGIN(File)

/// Internal OS error.
XI_ERROR_CODE_VALUE(Internal, 0x0001)
/// File operation actually encountered a directory.
XI_ERROR_CODE_VALUE(IsDirectory, 0x0002)
/// File operation expected the file does not exist, actually does.
XI_ERROR_CODE_VALUE(AlreadyExists, 0x0003)
/// File operation expected the file exist, actually does not.
XI_ERROR_CODE_VALUE(NotFound, 0x0004)

XI_ERROR_CODE_END(File, "FileSystem::FileError")

}  // namespace FileSystem
}  // namespace Xi

XI_ERROR_CODE_OVERLOADS(Xi::FileSystem, File)
