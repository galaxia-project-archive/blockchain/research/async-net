// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <cinttypes>
#include <filesystem>

#include <Xi/Global.hh>
#include <Xi/ErrorModel.hh>
#include <Xi/Byte.hh>
#include <Xi/Memory/Size.hpp>
#include <Xi/Stream/OutputStream.hpp>
#include <Xi/Stream/InputStream.hpp>
#include <Xi/Serialization/Serialization.hpp>

#include "Xi/FileSystem/Append.hpp"
#include "Xi/FileSystem/Overwrite.hpp"
#include "Xi/FileSystem/Permission.hpp"
#include "Xi/FileSystem/FileType.hpp"
#include "Xi/FileSystem/FileFormat.hpp"
#include "Xi/FileSystem/SegmentGenerator.hpp"

namespace Xi {
namespace FileSystem {

class Directory;

class File {
 public:
  /*!
   * \brief permissions Queries the current file permissions
   * \return  Current file permissions set
   * \attention Continious calls will always result in a new query and thus overhead. File permissions may change
   * between two continious calls and are not cached.
   */
  Result<Permission> permissions() const;

  /*!
   * \brief setPermissions Tries to replace the current file permissions.
   * \param perms The permissions to apply.
   * \return An error if the operation failed.
   */
  Result<void> setPermissions(const Permission perms);

  /*!
   * \brief type Queries the current file type
   * \return Current file type of a resolved file.
   * \attention Continious calls will always result in a new query and thus overhead. A file type may change
   * between two continious calls and are not cached.
   */
  Result<FileType> type() const;

  /*!
   * \brief exists Checks whether the actual filepath currently exists.
   * \return True if the filepath exists and points to a file (not a directory).
   */
  Result<bool> exists() const;

  /*!
   * \brief segments Queries all segments of this file path.
   * \return An iteratable object of all segments.
   */
  SegmentGenerator segments() const;

  /*!
   * \brief baseName Constructs the name of this file without directory or most outer extension
   * \return This file name without directory or most outer extension (ie. C:/KittyProgram/meow.cc -> "meow")
   */
  std::string baseName() const;

  /*!
   * \brief extension Constructs the file extension including the dot.
   * \return This file most outer extension. (ie. C:/KittyProgram/meow.cc -> ".cc")
   */
  std::string extension() const;

  /*!
   * \brief isFormat Checks if this file corresponds to a given file format.
   * \param format The format to check against.
   * \return True if this file extensions corresponds to the given format otherwise false.
   */
  bool isFormat(const FileFormat& format) const;

  /*!
   * \brief toString Constructs a path representation to this file.
   * \return filepath of this file (ie. ../KittyProgram/meow.cc -> "../KittyProgram/meow.cc")
   */
  std::string stringify() const;

  /*!
   * \brief fullName Construct a string representing the full name of this file without directory
   * \return The name of this file including the extensions. (ie. C:/KittyProgram/meow.cc -> "meow.cc")
   */
  std::string fullName() const;

  Result<std::string> absolute() const;

  Directory directory() const;

  /*!
   * \brief size Queries the current sice of this file taken on the harddrive.
   * \return Current file size in bytes.
   */
  Result<Memory::Bytes> size() const;

  /*!
   * \brief resize Resizes this file.
   * \param newSize The new size in bytes this file should take on the harddrive.
   */
  Result<void> resize(Memory::Bytes newSize);

  /*!
   * \brief operator == Checks if both paths will always result into the same file.
   * \param rhs Other file that may points to a different path.
   * \return True if both paths will always point to the same file otherwise false.
   */
  bool operator==(const File& rhs) const;
  bool operator!=(const File& rhs) const;

  bool operator<(const File& rhs) const;
  bool operator<=(const File& rhs) const;
  bool operator>(const File& rhs) const;
  bool operator>=(const File& rhs) const;

  /*!
   * \brief rooted Checks this file against another directory if relative.
   * \param dir The root directory to consider on relative paths.
   * \return If this path is relative prepends the root directory otherwise this file.
   */
  Result<File> rooted(const Directory& dir) const;

  /*!
   * \brief resolvesTo Checks if this filepath resolves to the same physical file pointed to by another filepath.
   * \param other The other filepath to check against.
   * \return True if both files would resolve to the same physical file otherwise false.
   */
  Result<bool> resolvesTo(const File& other) const;

  Result<void> touchIfNotExists();
  Result<void> touchIfNotExists(const Permission perms);
  Result<void> touch(const Permission perms = Permission::OwnerReadWrite);

  Result<void> copyTo(File& other) const;
  Result<void> copyTo(File& other, overwrite_t) const;

  Result<void> copyTo(Directory& dest) const;
  Result<void> copyTo(Directory& dest, overwrite_t) const;

  Result<void> moveTo(File& other);
  Result<void> moveTo(File& other, overwrite_t);

  Result<void> moveTo(Directory& dest);
  Result<void> moveTo(Directory& dest, overwrite_t);

  Result<std::string> readAllText() const;
  Result<Stream::UniqueInputStream> openTextForRead() const;

  Result<void> writeAllText(const std::string& text, append_t);
  Result<void> writeAllText(const std::string& text, overwrite_t);
  Result<Stream::UniqueOutputStream> openTextForWrite();
  Result<Stream::UniqueOutputStream> openTextForWrite(append_t);
  Result<Stream::UniqueOutputStream> openTextForWrite(overwrite_t);

  Result<ByteVector> readAllBinary() const;
  Result<void> readAllBinary(Stream::OutputStream& ostream) const;
  Result<Stream::UniqueInputStream> openBinaryForRead() const;

  /*!
   * \brief remove Erases the file if it exists.
   * \return Success if the file does not exist or was successfully removed, otherwise the error occured on removal.
   */
  Result<void> remove();

 public:
  explicit File() = default;
  XI_DEFAULT_COPY(File);
  XI_DEFAULT_MOVE(File);
  ~File() = default;

 private:
  explicit File(const std::filesystem::path& path);

  Result<std::filesystem::file_status> queryStatus() const;

  friend class Directory;
  friend Result<File> makeFile(const std::filesystem::path&);

 private:
  std::filesystem::path m_path;
};

Result<File> makeFile(const std::filesystem::path& path);
Result<std::string> makeTemporaryFilename();
Result<File> makeTemporaryFile();

Result<void> serialize(File& value, const Serialization::Tag& name, Serialization::Serializer& serializer);

}  // namespace FileSystem
}  // namespace Xi
