// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <Xi/Global.hh>

namespace Xi {
namespace FileSystem {

/// Empty struct for appending type overlaods.
struct append_t {
  /* */
};

/// Static instance to be used for calling typed method overloads.
static inline constexpr append_t append{/* */};

}  // namespace FileSystem
}  // namespace Xi
