// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <iterator>
#include <filesystem>

#include <Xi/Global.hh>
#include <Xi/ErrorModel.hh>

#include "Xi/FileSystem/Entry.hpp"

namespace Xi {
namespace FileSystem {

class EntryIterator final {
 public:
  using iterator_category = std::forward_iterator_tag;
  using value_type = Entry;
  using difference_type = std::ptrdiff_t;
  using pointer = value_type*;
  using reference = value_type&;
  using const_pointer = const value_type*;
  using const_reference = const value_type&;

 public:
  XI_DEFAULT_COPY(EntryIterator);
  XI_DELETE_MOVE(EntryIterator);
  ~EntryIterator() = default;

  value_type operator*() const;
  EntryIterator& operator++();

  bool operator==(const EntryIterator& rhs) const;
  bool operator!=(const EntryIterator& rhs) const;

 private:
  explicit EntryIterator(std::filesystem::directory_iterator iter);
  friend class EntryGenerator;

 private:
  std::filesystem::directory_iterator m_iter;
};

}  // namespace FileSystem
}  // namespace Xi
