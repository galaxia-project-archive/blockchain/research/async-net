// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <Xi/Global.hh>

#include <string>

namespace Xi {
namespace FileSystem {

/// Operating System specific directories.
enum struct SpecialDirectory {
  /// Directory intended to store user specific application data.
  ApplicationData,
  /// Directory intended to store temporary files that may be deleted once the application exits.
  Temporary,
  /// Directory containing the current executable.
  Executable,
};

/*!
 * \brief stringify Human readable repesentation of a special directory.
 * \param dir The os specifc directory entry.
 * \return Human readable name.
 */
std::string stringify(const SpecialDirectory dir);

}  // namespace FileSystem
}  // namespace Xi
