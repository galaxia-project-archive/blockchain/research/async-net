// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <Xi/Global.hh>

namespace Xi {
namespace FileSystem {

/// Empty struct for overwrite type overlaods.
struct overwrite_t {
  /* */
};

/// Static instance to be used for calling typed method overloads.
static inline constexpr overwrite_t overwrite{/* */};

}  // namespace FileSystem
}  // namespace Xi
