// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <filesystem>

#include "Xi/FileSystem/EntryIterator.hpp"

namespace Xi {
namespace FileSystem {

/*!
 * \brief The SegmentGenerator class encapsulates the concepts of iterating the segments of a filepath.
 */
class EntryGenerator {
 public:
  using iterator = EntryIterator;

  iterator begin() const;
  iterator end() const;
  iterator cbegin() const;
  iterator cend() const;

 private:
  explicit EntryGenerator(std::filesystem::directory_iterator begin_, std::filesystem::directory_iterator end_);

  friend class Directory;

 private:
  iterator m_begin;
  iterator m_end;
};

}  // namespace FileSystem
}  // namespace Xi
