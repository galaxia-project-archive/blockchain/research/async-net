﻿#include "PlatformImpl.hpp"

#include <ShlObj.h>
#include <libloaderapi.h>
#include <Xi/Windows/ComError.hpp>

namespace Xi::FileSystem::Impl {

namespace {
Result<std::string> getFolderPath(int csidl) {
  std::array<char, MAX_PATH> buffer{};
  const auto ec = SHGetFolderPathA(nullptr,             // Reserved
                                   csidl,               // Special folder type + create if necessary
                                   nullptr,             // Optional Access Token
                                   SHGFP_TYPE_CURRENT,  // Follow redirected special folders
                                   buffer.data()        // Output buffer (\0 terminated iff successfull)
  );

  XI_SUCCEED_IF(SUCCEEDED(ec), std::string{buffer.data()})
  XI_FAIL(Windows::toComError(ec))
}
}  // namespace

Result<std::string> executablePath() {
  std::string reval{};
  reval.resize(MAX_PATH);
  DWORD ec = ERROR_INSUFFICIENT_BUFFER;
  do {
    ec = GetModuleFileNameA(nullptr, reval.data(), static_cast<DWORD>(reval.size()));
    if (ec == ERROR_INSUFFICIENT_BUFFER) {
      reval.resize(reval.size() * 2);
    }
  } while (ec == ERROR_INSUFFICIENT_BUFFER);
  if (ec == 0) {
    XI_FAIL(Windows::toComError(HRESULT_FROM_WIN32(GetLastError())));
  }
  reval.resize(ec - 1);
  XI_SUCCEED(std::move(reval));
}

Result<std::string> applicationDataDirectory() {
  return getFolderPath(CSIDL_LOCAL_APPDATA | CSIDL_FLAG_CREATE);
}

}  // namespace Xi::FileSystem::Impl
