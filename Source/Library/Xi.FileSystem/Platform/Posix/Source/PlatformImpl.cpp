﻿#include "PlatformImpl.hpp"

#include <unistd.h>

#include "Xi/FileSystem/File.hpp"

namespace Xi::FileSystem::Detail {

Result<std::string> executablePath() {
  std::string reval{};
  reval.resize(256, '\0');
  errno = 0;
  do {
    const ssize_t count = readlink("/proc/self/exe", reval.data(), reval.size());
    if (count < 0) {
      // On error, -1 is returned and errno is set to indicate the error.
      XI_FAIL(static_cast<std::errc>(errno));
    } else if (static_cast<size_t>(count) < reval.size()) {
      // readlink() does not append a null byte to buf.
      reval.resize(static_cast<size_t>(count));
      break;
    } else {
      reval.resize(reval.size() * 2, '\0');
    }
  } while (true);
  XI_SUCCEED(move(reval))
}

Result<std::string> applicationDataDirectory() {
#if defined(__APPLE__) && defined(__MACH__)
  XI_SUCCEED(std::string{"~/Library/Application Support/"});
#else
  XI_SUCCEED(std::string{"~"});
#endif
}

}  // namespace Xi::FileSystem::Detail
