// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/FileSystem/EntryIterator.hpp"

namespace Xi {
namespace FileSystem {

EntryIterator::value_type EntryIterator::operator*() const {
  return Entry{*m_iter};
}

EntryIterator &EntryIterator::operator++() {
  m_iter++;
  return *this;
}

bool EntryIterator::operator==(const EntryIterator &rhs) const {
  return m_iter == rhs.m_iter;
}

bool EntryIterator::operator!=(const EntryIterator &rhs) const {
  return m_iter != rhs.m_iter;
}

EntryIterator::EntryIterator(std::filesystem::directory_iterator iter) : m_iter{iter} {
  /* */
}

}  // namespace FileSystem
}  // namespace Xi
