// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/FileSystem/SymbolicLink.hpp"

#include "Xi/FileSystem/SymbolicLinkError.hpp"

namespace fs = std::filesystem;

namespace Xi {
namespace FileSystem {

Result<fs::path> resolveSymbolicLink(const fs::path &path, size_t redirectionsThreshold) {
  std::error_code ec{/* */};
  const auto exists = fs::exists(path, ec);
  XI_ERROR_CODE_PROPAGATE(ec);
  XI_SUCCEED_IF_NOT(exists, path)
  const auto isSymlink = fs::is_symlink(path, ec);
  XI_ERROR_CODE_PROPAGATE(ec);
  XI_SUCCEED_IF_NOT(isSymlink, path)
  XI_FAIL_IF(redirectionsThreshold == 0, SymbolicLinkError::TooManyRedirections)
  auto redirect = fs::read_symlink(path, ec);
  XI_ERROR_CODE_PROPAGATE(ec);
  return resolveSymbolicLink(redirect, redirectionsThreshold - 1);
}

}  // namespace FileSystem
}  // namespace Xi
