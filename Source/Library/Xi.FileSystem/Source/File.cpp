// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/FileSystem/File.hpp"

#include <array>
#include <cstdio>
#include <fstream>

#include <Xi/Stream/OStream.hpp>
#include <Xi/Stream/IStream.hpp>
#include <Xi/Stream/InMemoryInputStream.hpp>
#include <Xi/Stream/StringOutputStream.hpp>
#include <Xi/Stream/ByteVectorOutputStream.hpp>

#include "Xi/FileSystem/FileError.hpp"
#include "Xi/FileSystem/Directory.hpp"

namespace fs = std::filesystem;

namespace Xi {
namespace FileSystem {

Result<Permission> File::permissions() const {
  XI_ERROR_TRY
  const auto status = queryStatus();
  XI_ERROR_PROPAGATE(status)
  XI_SUCCEED(fromStd(status->permissions()))
  XI_ERROR_CATCH
}

Result<void> File::setPermissions(const Permission perms) {
  std::error_code ec{/* */};
  fs::permissions(m_path, toStd(perms), ec);
  XI_ERROR_CODE_PROPAGATE(ec);
  XI_SUCCEED()
}

Result<FileType> File::type() const {
  XI_ERROR_TRY
  const auto status = queryStatus();
  XI_ERROR_PROPAGATE(status)
  const auto stdtype = status->type();
  switch (stdtype) {
    case fs::file_type::directory:
      XI_FAIL(FileError::IsDirectory)

    case fs::file_type::none:
      XI_SUCCEED(FileType::None)

    case fs::file_type::regular:
      XI_SUCCEED(FileType::Regular)

    case fs::file_type::symlink:
      XI_SUCCEED(FileType::SymbolicLink)

    case fs::file_type::block:
      XI_SUCCEED(FileType::Block)

    case fs::file_type::character:
      XI_SUCCEED(FileType::Character)

    case fs::file_type::fifo:
      XI_SUCCEED(FileType::Pipe)

    case fs::file_type::socket:
      XI_SUCCEED(FileType::Socket)

    default:
      XI_SUCCEED(FileType::Unknown)
  }
  XI_ERROR_CATCH
}

Result<bool> File::exists() const {
  std::error_code ec{/* */};
  const auto stdExists = fs::exists(m_path);
  XI_ERROR_CODE_PROPAGATE(ec);
  XI_SUCCEED_IF_NOT(stdExists, false)
  const auto stdIsDirectory = fs::is_directory(m_path, ec);
  XI_ERROR_CODE_PROPAGATE(ec);
  XI_FAIL_IF(stdIsDirectory, FileError::IsDirectory)
  XI_SUCCEED(true)
}

SegmentGenerator File::segments() const {
  return SegmentGenerator{m_path.begin(), m_path.end()};
}

std::string File::baseName() const {
  return m_path.stem().string();
}

std::string File::extension() const {
  return m_path.extension().string();
}

bool File::isFormat(const FileFormat &format) const {
  return format.hasExtension(extension());
}

std::string File::stringify() const {
  return m_path.string();
}

std::string File::fullName() const {
  return m_path.filename().string();
}

Result<std::string> File::absolute() const {
  std::error_code ec{/* */};
  auto reval = fs::absolute(m_path, ec);
  XI_ERROR_CODE_PROPAGATE(ec)
  XI_SUCCEED(reval.string());
}

Directory File::directory() const {
  return Directory{m_path.parent_path()};
}

Result<Memory::Bytes> File::size() const {
  std::error_code ec{/* */};
  const auto reval = fs::file_size(m_path, ec);
  XI_ERROR_CODE_PROPAGATE(ec)
  XI_SUCCEED(Memory::Bytes{reval})
}

Result<void> File::resize(Memory::Bytes newSize) {
  std::error_code ec{/* */};
  fs::resize_file(m_path, newSize.native());
  XI_ERROR_CODE_PROPAGATE(ec)
  XI_SUCCEED()
}

bool File::operator==(const File &rhs) const {
  return m_path == rhs.m_path;
}

bool File::operator!=(const File &rhs) const {
  return m_path != rhs.m_path;
}

bool File::operator<(const File &rhs) const {
  return this->m_path < rhs.m_path;
}

bool File::operator<=(const File &rhs) const {
  return this->m_path <= rhs.m_path;
}

bool File::operator>(const File &rhs) const {
  return this->m_path > rhs.m_path;
}

bool File::operator>=(const File &rhs) const {
  return this->m_path >= rhs.m_path;
}

Result<File> File::rooted(const Directory &dir) const {
  if (m_path.is_relative()) {
    return dir.relativeFile(m_path);
  } else {
    XI_SUCCEED(*this)
  }
}

Result<bool> File::resolvesTo(const File &other) const {
  std::error_code ec{/* */};
  auto isEqual = fs::equivalent(m_path, other.m_path, ec);
  XI_ERROR_CODE_PROPAGATE(ec)
  XI_SUCCEED(isEqual)
}

Result<void> File::touchIfNotExists() {
  XI_ERROR_TRY
  XI_ERROR_PROPAGATE_CATCH(directory().createRegularIfNotExists())
  auto absolute_ = absolute();
  XI_ERROR_PROPAGATE(absolute_);
  std::ofstream stream{*absolute_, std::ios::app};
  XI_FAIL_IF_NOT(stream.is_open(), FileError::Internal)
  XI_FAIL_IF_NOT(stream.good(), FileError::Internal)
  XI_SUCCEED()
  XI_ERROR_CATCH
}

Result<void> File::touchIfNotExists(const Permission perms) {
  XI_ERROR_PROPAGATE_CATCH(touchIfNotExists());
  XI_ERROR_PROPAGATE_CATCH(setPermissions(perms))
  XI_SUCCEED()
}

Result<void> File::touch(const Permission perms) {
  auto exists_ = exists();
  XI_ERROR_PROPAGATE(exists_)
  XI_FAIL_IF(*exists_, FileError::AlreadyExists)
  return touchIfNotExists(perms);
}

Result<void> File::copyTo(File &dest) const {
  std::error_code ec{/* */};
  fs::copy_file(m_path, dest.m_path, ec);
  XI_ERROR_CODE_PROPAGATE(ec);
  XI_SUCCEED()
}

Result<void> File::copyTo(File &dest, overwrite_t) const {
  std::error_code ec{/* */};
  fs::copy_file(m_path, dest.m_path, fs::copy_options::overwrite_existing, ec);
  XI_ERROR_CODE_PROPAGATE(ec)
  XI_SUCCEED()
}

Result<void> File::moveTo(File &other) {
  std::error_code ec{/* */};
  fs::rename(m_path, other.m_path);
  XI_ERROR_CODE_PROPAGATE(ec)
  XI_SUCCEED()
}

Result<void> File::moveTo(File &other, overwrite_t) {
  const auto exists_ = other.exists();
  XI_ERROR_PROPAGATE(exists_);
  if (*exists_) {
    XI_ERROR_PROPAGATE_CATCH(other.remove())
  }
  return moveTo(other);
}

Result<std::string> File::readAllText() const {
  auto istream = openTextForRead();
  XI_ERROR_PROPAGATE(istream);

  std::string reval{};
  Stream::StringOutputStream ostream{reval};
  XI_ERROR_PROPAGATE_CATCH(istream->readAll(ostream))
  XI_SUCCEED(std::move(reval))
}

Result<void> File::writeAllText(const std::string &text, append_t) {
  Stream::InMemoryInputStream istream{asConstByteSpan(text)};
  auto oStream = openTextForWrite(append);
  XI_ERROR_PROPAGATE(oStream);
  return istream.readAll(**oStream);
}

Result<void> File::writeAllText(const std::string &text, overwrite_t) {
  Stream::InMemoryInputStream istream{asConstByteSpan(text)};
  auto oStream = openTextForWrite(overwrite);
  XI_ERROR_PROPAGATE(oStream);
  return istream.readAll(**oStream);
}

Result<Stream::UniqueInputStream> File::openTextForRead() const {
  XI_ERROR_TRY
  const auto exists_ = exists();
  XI_ERROR_PROPAGATE(exists_)
  XI_FAIL_IF_NOT(*exists_, FileError::NotFound)
  Stream::UniqueInputStream stream =
      std::make_unique<Stream::IStream>(makeValueOwnership<std::istream>(std::ifstream{m_path}));
  XI_SUCCEED(move(stream))
  XI_ERROR_CATCH
}

Result<Stream::UniqueOutputStream> File::openTextForWrite() {
  XI_ERROR_PROPAGATE_CATCH(touch())
  return openTextForWrite(append);
}

Result<Stream::UniqueOutputStream> File::openTextForWrite(append_t) {
  XI_ERROR_TRY
  XI_ERROR_PROPAGATE_CATCH(touchIfNotExists());
  Stream::UniqueOutputStream stream =
      std::make_unique<Stream::OStream>(makeValueOwnership<std::ostream>(std::ofstream{m_path, std::ios::app}));
  XI_SUCCEED(move(stream))
  XI_ERROR_CATCH
}

Result<Stream::UniqueOutputStream> File::openTextForWrite(overwrite_t) {
  XI_ERROR_TRY
  XI_ERROR_PROPAGATE_CATCH(touchIfNotExists());
  Stream::UniqueOutputStream stream =
      std::make_unique<Stream::OStream>(makeValueOwnership<std::ostream>(std::ofstream{m_path, std::ios::trunc}));
  XI_SUCCEED(move(stream));
  XI_ERROR_CATCH
}

Result<ByteVector> File::readAllBinary() const {
  ByteVector reval{};
  Stream::ByteVectorOutputStream stream{reval};
  XI_ERROR_PROPAGATE_CATCH(readAllBinary(stream))
  XI_SUCCEED(std::move(reval))
}

Result<void> File::readAllBinary(Stream::OutputStream &ostream) const {
  auto istream = openBinaryForRead();
  XI_ERROR_PROPAGATE(istream)
  return istream->readAll(ostream);
}

Result<Stream::UniqueInputStream> File::openBinaryForRead() const {
  XI_ERROR_TRY
  const auto exists_ = exists();
  XI_ERROR_PROPAGATE(exists_)
  XI_FAIL_IF_NOT(*exists_, FileError::NotFound)
  Stream::UniqueInputStream stream =
      std::make_unique<Stream::IStream>(makeValueOwnership<std::istream>(std::ifstream{m_path, std::ios::binary}));
  XI_SUCCEED(move(stream))
  XI_ERROR_CATCH
}

Result<void> File::remove() {
  std::error_code ec{/* */};
  fs::remove(m_path, ec);
  XI_ERROR_CODE_PROPAGATE(ec)
  XI_SUCCEED()
}
File::File(const fs::path &path) : m_path{path} {
  /* */
}

Result<fs::file_status> File::queryStatus() const {
  std::error_code ec{/* */};
  auto status = fs::status(m_path, ec);
  XI_ERROR_CODE_PROPAGATE(ec);
  XI_SUCCEED(status)
}

Result<File> makeFile(const fs::path &path) {
  std::error_code ec{};
  [[maybe_unused]] auto _ = fs::absolute(path, ec);
  XI_ERROR_CODE_PROPAGATE(ec)
  XI_SUCCEED(File{path})
}

Result<File> makeTemporaryFile() {
  const auto tempFilename = makeTemporaryFilename();
  XI_ERROR_PROPAGATE(tempFilename)
  const auto tempDir = makeDirectory(SpecialDirectory::Temporary);
  XI_ERROR_PROPAGATE(tempDir);
  return tempDir->relativeFile(*tempFilename);
}

Result<std::string> makeTemporaryFilename() {
  std::array<char, L_tmpnam> buffer{};
  const auto ec = tmpnam(buffer.data());
  XI_FAIL_IF(ec == nullptr, FileError::Internal)
  XI_SUCCEED(std::string{buffer.data()})
}

Result<void> serialize(File &value, const Serialization::Tag &name, Serialization::Serializer &serializer) {
  if (serializer.isInputMode()) {
    std::string str{};
    XI_ERROR_PROPAGATE_CATCH(serializer(str, name));
    auto maybeFile = makeFile(str);
    XI_ERROR_PROPAGATE(maybeFile)
    value = maybeFile.take();
    XI_SUCCEED()
  } else {
    std::string str = toString(value);
    return serializer(str, name);
  }
}

}  // namespace FileSystem
}  // namespace Xi
