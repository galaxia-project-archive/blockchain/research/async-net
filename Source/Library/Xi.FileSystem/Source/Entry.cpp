// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/FileSystem/Entry.hpp"

#include <system_error>

#include "Xi/FileSystem/SymbolicLink.hpp"
#include "Xi/FileSystem/File.hpp"
#include "Xi/FileSystem/Directory.hpp"
#include "Xi/FileSystem/FileSystemError.hpp"

namespace fs = std::filesystem;

namespace Xi {
namespace FileSystem {

Result<Entry::Kind> Entry::kind() const {
  const auto dir = isDirectory();
  XI_ERROR_PROPAGATE(dir);
  XI_SUCCEED(*dir ? Kind::Directory : Kind::File);
}

Result<bool> Entry::isFile() const {
  const auto res = resolveSymbolicLink(m_path);
  XI_ERROR_PROPAGATE(res)
  std::error_code ec{/* */};
  const auto stdIsDirectory = fs::is_directory(*res, ec);
  XI_ERROR_CODE_PROPAGATE(ec)
  XI_SUCCEED(!stdIsDirectory)
}

Result<bool> Entry::isDirectory() const {
  const auto res = resolveSymbolicLink(m_path);
  XI_ERROR_PROPAGATE(res)
  std::error_code ec{/* */};
  const auto stdIsDirectory = fs::is_directory(*res, ec);
  XI_ERROR_CODE_PROPAGATE(ec)
  XI_SUCCEED(stdIsDirectory)
}

Result<File> Entry::asFile() const {
  return makeFile(m_path);
}

Result<Directory> Entry::asDirectory() const {
  return makeDirectory(m_path);
}

std::string Entry::stringify() const {
  return m_path.string();
}

Entry::Entry(const std::filesystem::path &path) : m_path{path} {
  /* */
}

Result<Entry> makeEntry(const std::filesystem::path &path) {
  std::error_code ec{};
  const bool exists = fs::exists(path, ec);
  XI_ERROR_CODE_PROPAGATE(ec);
  XI_FAIL_IF_NOT(exists, FileSystemError::NotFound);
  XI_SUCCEED(Entry{path})
}

}  // namespace FileSystem
}  // namespace Xi
