// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/FileSystem/FileError.hpp"

XI_ERROR_CODE_CATEGORY_BEGIN(Xi::FileSystem, File)
XI_ERROR_CODE_DESC(Internal, "internal os error")
XI_ERROR_CODE_DESC(IsDirectory, "file operation invoked actually points to a directory")
XI_ERROR_CODE_DESC(AlreadyExists, "file already exists")
XI_ERROR_CODE_DESC(NotFound, "file not found")
XI_ERROR_CODE_CATEGORY_END()
