// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/FileSystem/SegmentGenerator.hpp"

namespace Xi {
namespace FileSystem {

SegmentGenerator::iterator SegmentGenerator::begin() const {
  return m_begin;
}

SegmentGenerator::iterator SegmentGenerator::end() const {
  return m_end;
}

SegmentGenerator::iterator SegmentGenerator::cbegin() const {
  return m_begin;
}

SegmentGenerator::iterator SegmentGenerator::cend() const {
  return m_end;
}

SegmentGenerator::SegmentGenerator(SegmentGenerator::iterator begin, SegmentGenerator::iterator end)
    : m_begin{begin}, m_end{end} {
  /* */
}

}  // namespace FileSystem
}  // namespace Xi
