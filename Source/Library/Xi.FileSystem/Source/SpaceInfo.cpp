// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/FileSystem/SpaceInfo.hpp"

namespace Xi {
namespace FileSystem {

SpaceInfo::SpaceInfo(Memory::Bytes available_, Memory::Bytes capacity_) : SpaceInfo(available_, capacity_, available_) {
  /* */
}

SpaceInfo::SpaceInfo(Memory::Bytes available_, Memory::Bytes capacity_, Memory::Bytes free_)
    : m_available{available_}, m_capacity{capacity_}, m_free{free_} {
  /* */
}

Memory::Bytes SpaceInfo::available() const {
  return m_available;
}

Memory::Bytes SpaceInfo::capacity() const {
  return m_capacity;
}

Memory::Bytes SpaceInfo::free() const {
  return m_free;
}

Memory::Bytes SpaceInfo::taken() const {
  return capacity() - free();
}

SpaceInfo fromStd(const std::filesystem::space_info &space) {
  return SpaceInfo{Memory::Bytes{space.available}, Memory::Bytes{space.capacity}, Memory::Bytes{space.free}};
}

}  // namespace FileSystem
}  // namespace Xi
