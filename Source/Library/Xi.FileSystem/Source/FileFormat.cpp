// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/FileSystem/FileFormat.hpp"

#include <Xi/String.hpp>

namespace Xi {
namespace FileSystem {

const FileFormat FileFormat::Csv{Kind::Text, {".csv"}};
const FileFormat FileFormat::Xml{Kind::Text, {".xml"}};
const FileFormat FileFormat::Yaml{Kind::Text, {".yml", ".yaml"}};
const FileFormat FileFormat::Json{Kind::Text, {".json"}};

const FileFormat FileFormat::Bin{Kind::Binary, {".bin", ".dat", ".data", ".raw"}};

const FileFormat FileFormat::Zip{Kind::Archive, {".zip"}};

FileFormat::FileFormat(const FileFormat::Kind kind, const std::set<std::string> &extensions)
    : m_kind{kind}, m_extensions{extensions} {
  /* */
}

FileFormat::Kind FileFormat::kind() const {
  return m_kind;
}

const std::set<std::string> &FileFormat::extensions() const {
  return m_extensions;
}

bool FileFormat::hasExtension(const std::string &ext) const {
  return extensions().find(toLower(ext)) != extensions().end();
}

XI_SERIALIZATION_COMPLEX_EXTERN_BEGIN(FileFormat)
XI_SERIALIZATION_MEMBER(m_kind, 0x0001, "kind")
XI_SERIALIZATION_MEMBER(m_extensions, 0x0002, "extensions")
XI_SERIALIZATION_COMPLEX_EXTERN_END

}  // namespace FileSystem
}  // namespace Xi
