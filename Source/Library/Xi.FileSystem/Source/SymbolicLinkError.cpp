// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/FileSystem/SymbolicLinkError.hpp"

XI_ERROR_CODE_CATEGORY_BEGIN(Xi::FileSystem, SymbolicLink)
XI_ERROR_CODE_DESC(TooManyRedirections, "Symbolic link resolution exceeded maximum redirections threshold.")
XI_ERROR_CODE_CATEGORY_END()
