// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/VersionInfo/Channel.hpp"

#include <Xi/ErrorModel.hh>
#include <Xi/String.hpp>

XI_ERROR_CODE_CATEGORY_BEGIN(Xi::VersionInfo, Channel)
XI_ERROR_CODE_DESC(Unknown, "unknown channel name")
XI_ERROR_CODE_CATEGORY_END()

namespace Xi {
namespace VersionInfo {

std::string stringify(const Channel channel) {
  switch (channel) {
    case Channel::Stable:
      return "stable";
    case Channel::Beta:
      return "beta";
    case Channel::Edge:
      return "edge";
    case Channel::Clutter:
      return "clutter";
  }
  XI_EXCEPTIONAL(InvalidEnumValueError)
}

Result<Channel> fromString(const std::string &str) {
  const auto lowerStr = toLower(str);
  if (lowerStr == stringify(Channel::Stable)) {
    XI_SUCCEED(Channel::Stable)
  } else if (lowerStr == stringify(Channel::Beta)) {
    XI_SUCCEED(Channel::Beta)
  } else if (lowerStr == stringify(Channel::Edge)) {
    XI_SUCCEED(Channel::Edge)
  } else if (lowerStr == stringify(Channel::Clutter)) {
    XI_SUCCEED(Channel::Clutter)
  } else {
    XI_FAIL(ChannelError::Unknown)
  }
}

}  // namespace VersionInfo
}  // namespace Xi
