// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/VersionInfo/VersionInfo.hpp"

namespace Xi {
namespace VersionInfo {

const VersionInfo &VersionInfo::project() {
  static const VersionInfo __singleton = VersionInfoBuilder{/* */}
                                             .withVersion(Version::project())
                                             .withCompiler(Compiler::project())
                                             .withGit(Git::project())
                                             .build();
  return __singleton;
}

const Version &VersionInfo::version() const {
  return m_version;
}

const Compiler &VersionInfo::compiler() const {
  return m_compiler;
}

const std::optional<Git> &VersionInfo::git() const {
  return m_git;
}

XI_SERIALIZATION_COMPLEX_EXTERN_BEGIN(VersionInfo)
XI_SERIALIZATION_MEMBER(m_version, 0x0001, "version")
XI_SERIALIZATION_MEMBER(m_compiler, 0x0001, "compiler")
XI_SERIALIZATION_MEMBER(m_git, 0x0001, "git")
XI_SERIALIZATION_COMPLEX_EXTERN_END

VersionInfoBuilder &VersionInfoBuilder::withVersion(const Version &version) {
  m_info.m_version = version;
  return *this;
}

VersionInfoBuilder &VersionInfoBuilder::withCompiler(const Compiler &compiler) {
  m_info.m_compiler = compiler;
  return *this;
}

VersionInfoBuilder &VersionInfoBuilder::withGit(const std::optional<Git> git) {
  m_info.m_git = git;
  return *this;
}

VersionInfo VersionInfoBuilder::build() const {
  return m_info;
}

}  // namespace VersionInfo
}  // namespace Xi
