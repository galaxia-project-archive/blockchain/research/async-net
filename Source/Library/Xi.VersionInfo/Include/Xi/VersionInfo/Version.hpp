// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <cinttypes>
#include <optional>
#include <string>

#if defined(major)
#undef major
#endif

#if defined(minor)
#undef minor
#endif

#include <Xi/Global.hh>
#include <Xi/Serialization/Serialization.hpp>

#include "Xi/VersionInfo/Channel.hpp"

namespace Xi {
namespace VersionInfo {

// TODO: Semantic Versionsing
// https://semver.org/
class Version {
 public:
  using NumericIdentifier = std::uint64_t;

 public:
  static const Version& project();

 public:
  Version() = default;
  XI_DEFAULT_COPY(Version)
  XI_DEFAULT_MOVE(Version)
  ~Version() = default;

  NumericIdentifier major() const;
  NumericIdentifier minor() const;
  NumericIdentifier patch() const;
  std::optional<NumericIdentifier> build() const;
  std::optional<Channel> channel() const;

  std::string stringify() const;

  XI_SERIALIZATION_COMPLEX_EXTERN()
 private:
  friend class VersionBuilder;

 private:
  NumericIdentifier m_major;
  NumericIdentifier m_minor;
  NumericIdentifier m_patch;
  std::optional<NumericIdentifier> m_build;
  std::optional<Channel> m_channel;
};

class VersionBuilder {
 public:
  VersionBuilder() = default;
  XI_DELETE_COPY(VersionBuilder)
  XI_DELETE_MOVE(VersionBuilder)
  ~VersionBuilder() = default;

  VersionBuilder& withMajor(Version::NumericIdentifier major);
  VersionBuilder& withMinor(Version::NumericIdentifier minor);
  VersionBuilder& withPatch(Version::NumericIdentifier patch);
  VersionBuilder& withBuild(std::optional<Version::NumericIdentifier> build);
  VersionBuilder& withChannel(std::optional<Channel> channel);

  Version build() const;

 private:
  Version m_version;
};

}  // namespace VersionInfo
}  // namespace Xi
