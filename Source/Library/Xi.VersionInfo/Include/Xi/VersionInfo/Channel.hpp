// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <string>

#include <Xi/ErrorModel.hh>
#include <Xi/ErrorModel.hh>
#include <Xi/Serialization/Enum.hpp>

namespace Xi {
namespace VersionInfo {

XI_ERROR_CODE_BEGIN(Channel)
XI_ERROR_CODE_VALUE(Unknown, 0x0001)
XI_ERROR_CODE_END(Channel, "ChannelError")

enum struct Channel {
  Stable = 0x0001,
  Beta = 0x0002,
  Edge = 0x0003,
  Clutter = 0x0004,
};

XI_SERIALIZATION_ENUM(Channel)

std::string stringify(const Channel channel);
Result<Channel> fromString(const std::string& str);

}  // namespace VersionInfo
}  // namespace Xi

XI_ERROR_CODE_OVERLOADS(Xi::VersionInfo, Channel)

XI_SERIALIZATION_ENUM_RANGE(Xi::VersionInfo::Channel, Stable, Clutter)
XI_SERIALIZATION_ENUM_TAG(Xi::VersionInfo::Channel, Stable, "stable")
XI_SERIALIZATION_ENUM_TAG(Xi::VersionInfo::Channel, Beta, "beta")
XI_SERIALIZATION_ENUM_TAG(Xi::VersionInfo::Channel, Edge, "edge")
XI_SERIALIZATION_ENUM_TAG(Xi::VersionInfo::Channel, Clutter, "clutter")
