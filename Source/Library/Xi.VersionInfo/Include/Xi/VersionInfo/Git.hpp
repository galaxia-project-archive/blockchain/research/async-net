// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <string>
#include <optional>

#include <Xi/Global.hh>
#include <Xi/Serialization/Serialization.hpp>

namespace Xi {
namespace VersionInfo {

class Git {
 public:
  static const std::optional<Git>& project();

 public:
  Git() = default;
  XI_DEFAULT_COPY(Git)
  XI_DEFAULT_MOVE(Git)
  ~Git() = default;

  const std::string& commit() const;
  const std::string& author() const;
  const std::string& branch() const;
  const std::string& date() const;

  XI_SERIALIZATION_COMPLEX_EXTERN()
 private:
  friend class GitBuilder;

 private:
  std::string m_commit;
  std::string m_author;
  std::string m_branch;
  std::string m_date;
};

class GitBuilder {
 public:
  GitBuilder() = default;
  XI_DELETE_COPY(GitBuilder)
  XI_DELETE_MOVE(GitBuilder)
  ~GitBuilder() = default;

  GitBuilder& withCommit(const std::string& commit);
  GitBuilder& withAuthor(const std::string& author);
  GitBuilder& withBranch(const std::string& branch);
  GitBuilder& withDate(const std::string& date);

  Git build() const;

 private:
  Git m_info;
};

}  // namespace VersionInfo
}  // namespace Xi
