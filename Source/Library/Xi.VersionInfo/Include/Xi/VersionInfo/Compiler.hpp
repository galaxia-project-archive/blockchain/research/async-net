// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <string>

#include <Xi/Global.hh>
#include <Xi/Serialization/Serialization.hpp>

namespace Xi {
namespace VersionInfo {

class Compiler {
 public:
  static const Compiler& project();

 public:
  Compiler() = default;
  XI_DEFAULT_COPY(Compiler)
  XI_DEFAULT_MOVE(Compiler)
  ~Compiler() = default;

  const std::string& platform() const;
  const std::string& endianess() const;

  const std::string& cFlags() const;
  const std::string& cIdentifier() const;
  const std::string& cVersion() const;

  const std::string& cxxFlags() const;
  const std::string& cxxIdentifier() const;
  const std::string& cxxVersion() const;

  XI_SERIALIZATION_COMPLEX_EXTERN()

 private:
  friend class CompilerBuilder;

 private:
  std::string m_platform;
  std::string m_endianess;

  std::string m_cFlags;
  std::string m_cIdentifier;
  std::string m_cVersion;

  std::string m_cxxFlags;
  std::string m_cxxIdentifier;
  std::string m_cxxVersion;
};

class CompilerBuilder {
 public:
  CompilerBuilder() = default;
  XI_DELETE_COPY(CompilerBuilder)
  XI_DELETE_MOVE(CompilerBuilder)
  ~CompilerBuilder() = default;

  CompilerBuilder& withPlatform(const std::string& platform);
  CompilerBuilder& withEndianess(const std::string& endianess);

  CompilerBuilder& withCFlags(const std::string& flags);
  CompilerBuilder& withCIdentifier(const std::string& identifier);
  CompilerBuilder& withCVersion(const std::string& version);

  CompilerBuilder& withCxxFlags(const std::string& flags);
  CompilerBuilder& withCxxIdentifier(const std::string& identifier);
  CompilerBuilder& withCxxVersion(const std::string& version);

  Compiler build() const;

 private:
  Compiler m_info;
};

}  // namespace VersionInfo
}  // namespace Xi
