// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <optional>

#include <Xi/Global.hh>
#include <Xi/Serialization/Serialization.hpp>

#include "Xi/VersionInfo/Version.hpp"
#include "Xi/VersionInfo/Compiler.hpp"
#include "Xi/VersionInfo/Git.hpp"

namespace Xi {
namespace VersionInfo {

class VersionInfo {
 public:
  static const VersionInfo& project();

 public:
  VersionInfo() = default;
  XI_DEFAULT_COPY(VersionInfo)
  XI_DEFAULT_MOVE(VersionInfo)
  ~VersionInfo() = default;

  const Version& version() const;

  const Compiler& compiler() const;
  const std::optional<Git>& git() const;

  XI_SERIALIZATION_COMPLEX_EXTERN()
 private:
  friend class VersionInfoBuilder;

 private:
  Version m_version;
  Compiler m_compiler;
  std::optional<Git> m_git;
};

class VersionInfoBuilder {
 public:
  VersionInfoBuilder() = default;
  XI_DELETE_COPY(VersionInfoBuilder)
  XI_DELETE_MOVE(VersionInfoBuilder)
  ~VersionInfoBuilder() = default;

  VersionInfoBuilder& withVersion(const Version& version);
  VersionInfoBuilder& withCompiler(const Compiler& compiler);
  VersionInfoBuilder& withGit(const std::optional<Git> git);

  VersionInfo build() const;

 private:
  VersionInfo m_info;
};

}  // namespace VersionInfo
}  // namespace Xi
