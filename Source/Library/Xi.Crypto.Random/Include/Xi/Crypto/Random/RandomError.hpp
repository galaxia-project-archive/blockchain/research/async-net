﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <Xi/ErrorModel.hh>

namespace Xi {
namespace Crypto {
namespace Random {

XI_ERROR_CODE_BEGIN(Random)

/// Generic code for random generation failure.
XI_ERROR_CODE_VALUE(Failed, 0x0001)

XI_ERROR_CODE_END(Random, "random generation error")

}  // namespace Random
}  // namespace Crypto
}  // namespace Xi

XI_ERROR_CODE_OVERLOADS(Xi::Crypto::Random, Random)
