﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Crypto/Random/RandomError.hpp"

XI_ERROR_CODE_CATEGORY_BEGIN(Xi::Crypto::Random, Random)
XI_ERROR_CODE_DESC(Failed, "random generation failed")
XI_ERROR_CODE_CATEGORY_END()
