﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Crypto/Random/Random.hh"

#include <utility>

#include <Xi/Global.hh>
#include <Xi/ErrorModel.hh>

Xi::Result<Xi::ByteVector> Xi::Crypto::Random::generate(size_t count) {
  Xi::ByteVector reval;
  reval.resize(count);
  if (const auto ec = generate(ByteSpan{reval.data(), count}); ec != RandomError::Success) {
    return makeError(ec);
  }
  return makeSuccess(std::move(reval));
}

Xi::Crypto::Random::RandomError Xi::Crypto::Random::generate(Xi::ByteSpan out) {
  XI_RETURN_EC_IF(xi_crypto_random_bytes(out.data(), out.size()) != XI_RETURN_CODE_SUCCESS, RandomError::Failed);
  return RandomError::Success;
}

Xi::Result<Xi::ByteVector> Xi::Crypto::Random::generate(size_t count, ConstByteSpan seed) {
  Xi::ByteVector reval;
  reval.resize(count);
  if (const auto ec = generate(ByteSpan{reval.data(), count}, seed); ec != RandomError::Success) {
    return makeError(ec);
  }
  return makeSuccess(std::move(reval));
}

Xi::Crypto::Random::RandomError Xi::Crypto::Random::generate(ByteSpan out, ConstByteSpan seed) {
  XI_RETURN_EC_IF(
      xi_crypto_random_bytes_determenistic(out.data(), out.size(), seed.data(), seed.size()) != XI_RETURN_CODE_SUCCESS,
      RandomError::Failed);
  return RandomError::Success;
}
