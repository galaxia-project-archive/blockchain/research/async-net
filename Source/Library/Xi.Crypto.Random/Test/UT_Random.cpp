﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include <gmock/gmock.h>

#include <thread>
#include <algorithm>

#include <Xi/Crypto/Random/Random.hh>

#define XI_TEST_SUITE Xi_Crypto_Random

TEST(XI_TEST_SUITE, Generate) {
  using namespace ::testing;
  using namespace Xi::Crypto::Random;

  for (size_t i = 0; i < 40; ++i) {
    Xi::ByteArray<10> bytes;
    ASSERT_EQ(generate(bytes), RandomError::Success);
    EXPECT_THAT(bytes, Contains(Ne(0)));
  }

  {
    Xi::ByteArray<0> bytes;
    ASSERT_EQ(generate(bytes), RandomError::Success);
  }

  for (size_t i = 0; i < 16; ++i) {
    size_t count = 1ull << i;
    auto vecByte = generate(count);
    ASSERT_FALSE(vecByte.isError());
    EXPECT_EQ(vecByte.value().size(), count);
  }

  {
    Xi::ByteArray<512> first, second;
    auto generateArray = [](auto &buffer) { ASSERT_EQ(generate(buffer), Xi::Crypto::Random::RandomError::Success); };
    std::thread t1{std::bind(generateArray, std::ref(first))};
    std::thread t2{std::bind(generateArray, std::ref(second))};
    t1.join();
    t2.join();
    EXPECT_NE(first, second);
  }
}

TEST(XI_TEST_SUITE, GenerateDetermenistic) {
  using namespace ::testing;
  using namespace Xi::Crypto::Random;

  Xi::ByteArray<74> bytes;
  ASSERT_EQ(generate(bytes), RandomError::Success);
  EXPECT_THAT(bytes, Contains(Ne(0)));

  Xi::ByteArray<512> first, second;
  for (size_t i = 0; i < 50; ++i) {
    auto generateArray = [&bytes](auto &buffer) {
      ASSERT_EQ(generate(buffer, bytes), Xi::Crypto::Random::RandomError::Success);
    };
    std::thread t1{std::bind(generateArray, std::ref(first))};
    std::thread t2{std::bind(generateArray, std::ref(second))};
    t1.join();
    t2.join();
    EXPECT_EQ(first, second);
  }
}
