﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Crypto/Hash/Crc.hpp"

#include <utility>

#if defined(_MSC_VER)
#pragma warning(push)
#pragma warning(disable : 4701)
#endif

#include <boost/crc.hpp>

#if defined(_MSC_VER)
#pragma warning(pop)
#endif

#include <boost/endian/conversion.hpp>

#include <Xi/ErrorModel.hh>

XI_CRYPTO_HASH_DECLARE_HASH_IMPLEMENTATION(Xi::Crypto::Hash::Crc::Hash16, 16)
XI_CRYPTO_HASH_DECLARE_HASH_IMPLEMENTATION(Xi::Crypto::Hash::Crc::Hash32, 32)

Xi::Crypto::Hash::HashError Xi::Crypto::Hash::Crc::compute(Xi::ConstByteSpan data, Hash16 &out) {
  static_assert(sizeof(boost::crc_16_type::value_type) == Hash16::bytes(), "invalid hash size for boost crc");
  boost::crc_16_type result{};
  result.process_bytes(data.data(), data.size());
  *reinterpret_cast<boost::crc_16_type::value_type *>(out.data()) = boost::endian::native_to_big(result.checksum());
  XI_RETURN_SC(HashError::Success);
}

Xi::Crypto::Hash::HashError Xi::Crypto::Hash::Crc::compute(Xi::ConstByteSpan data, Hash32 &out) {
  static_assert(sizeof(boost::crc_32_type::value_type) == Hash32::bytes(), "invalid hash size for boost crc");
  boost::crc_32_type result{};
  result.process_bytes(data.data(), data.size());
  *reinterpret_cast<boost::crc_32_type::value_type *>(out.data()) = boost::endian::native_to_big(result.checksum());
  XI_RETURN_SC(HashError::Success);
}

Xi::Result<Xi::Crypto::Hash::Crc::Hash16> Xi::Crypto::Hash::crc16(Xi::ConstByteSpan data) {
  Crc::Hash16 reval;
  const auto ec = compute(data, reval);
  XI_FAIL_IF_NOT(ec == HashError::Success, ec)
  return makeSuccess(std::move(reval));
}

Xi::Result<Xi::Crypto::Hash::Crc::Hash32> Xi::Crypto::Hash::crc32(Xi::ConstByteSpan data) {
  Crc::Hash32 reval;
  const auto ec = compute(data, reval);
  XI_FAIL_IF_NOT(ec == HashError::Success, ec)
  return makeSuccess(std::move(reval));
}
