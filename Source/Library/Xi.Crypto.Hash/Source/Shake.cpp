// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Crypto/Hash/Shake.hh"

#include <Xi/ErrorModel.hh>

XI_CRYPTO_HASH_DECLARE_HASH_IMPLEMENTATION(Xi::Crypto::Hash::Shake::Hash128, 128)
XI_CRYPTO_HASH_DECLARE_HASH_IMPLEMENTATION(Xi::Crypto::Hash::Shake::Hash256, 256)

Xi::Crypto::Hash::HashError Xi::Crypto::Hash::Shake::compute(Xi::ConstByteSpan data, Hash128 &out) {
  XI_RETURN_EC_IF_NOT(
      xi_crypto_hash_shake_128(data.data(), data.size(), out.data()) == XI_RETURN_CODE_SUCCESS,
      HashError::Internal);
  return HashError::Success;
}

Xi::Crypto::Hash::HashError Xi::Crypto::Hash::Shake::compute(Xi::ConstByteSpan data, Hash256 &out) {
  XI_RETURN_EC_IF_NOT(
      xi_crypto_hash_shake_256(data.data(), data.size(), out.data()) == XI_RETURN_CODE_SUCCESS,
      HashError::Internal);
  return HashError::Success;
}
