// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Crypto/Hash/Sha3.hh"

#include <Xi/ErrorModel.hh>

XI_CRYPTO_HASH_DECLARE_HASH_IMPLEMENTATION(Xi::Crypto::Hash::Sha3::Hash224, 224)
XI_CRYPTO_HASH_DECLARE_HASH_IMPLEMENTATION(Xi::Crypto::Hash::Sha3::Hash256, 256)
XI_CRYPTO_HASH_DECLARE_HASH_IMPLEMENTATION(Xi::Crypto::Hash::Sha3::Hash384, 384)
XI_CRYPTO_HASH_DECLARE_HASH_IMPLEMENTATION(Xi::Crypto::Hash::Sha3::Hash512, 512)

Xi::Crypto::Hash::HashError Xi::Crypto::Hash::Sha3::compute(Xi::ConstByteSpan data, Hash224 &out) {
  XI_RETURN_EC_IF_NOT(
      xi_crypto_hash_sha3_224(data.data(), data.size(), out.data()) == XI_RETURN_CODE_SUCCESS,
      HashError::Internal);
  return HashError::Success;
}

Xi::Crypto::Hash::HashError Xi::Crypto::Hash::Sha3::compute(Xi::ConstByteSpan data, Hash256 &out) {
  XI_RETURN_EC_IF_NOT(
      xi_crypto_hash_sha3_256(data.data(), data.size(), out.data()) == XI_RETURN_CODE_SUCCESS,
      HashError::Internal);
  return HashError::Success;
}

Xi::Crypto::Hash::HashError Xi::Crypto::Hash::Sha3::compute(Xi::ConstByteSpan data, Hash384 &out) {
  XI_RETURN_EC_IF_NOT(
      xi_crypto_hash_sha3_384(data.data(), data.size(), out.data()) == XI_RETURN_CODE_SUCCESS,
      HashError::Internal);
  return HashError::Success;
}

Xi::Crypto::Hash::HashError Xi::Crypto::Hash::Sha3::compute(Xi::ConstByteSpan data, Hash512 &out) {
  XI_RETURN_EC_IF_NOT(
      xi_crypto_hash_sha3_512(data.data(), data.size(), out.data()) == XI_RETURN_CODE_SUCCESS,
      HashError::Internal);
  return HashError::Success;
}
