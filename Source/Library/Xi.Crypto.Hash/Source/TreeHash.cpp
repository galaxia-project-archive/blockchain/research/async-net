﻿// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Crypto/Hash/TreeHash.hh"

#include <Xi/ErrorModel.hh>

Xi::Crypto::Hash::HashError Xi::Crypto::Hash::treeHash(const Xi::ConstByteSpan &data, size_t count, Xi::ByteSpan out) {
  XI_RETURN_EC_IF(out.size_bytes() < XI_HASH_FAST_HASH_SIZE, HashError::OutOfMemory);
  XI_RETURN_EC_IF(count == 0, HashError::InsufficientData);
  XI_RETURN_EC_IF_NOT(data.size_bytes() == count * XI_HASH_FAST_HASH_SIZE, HashError::Internal);
  if (const auto ec = xi_crypto_hash_tree_hash(
          reinterpret_cast<const xi_byte_t(*)[XI_HASH_FAST_HASH_SIZE]>(data.data()), count, out.data());
      ec != XI_RETURN_CODE_SUCCESS) {
    return HashError::Internal;
  } else {
    return HashError::Success;
  }
}
