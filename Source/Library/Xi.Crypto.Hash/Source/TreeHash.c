﻿// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Crypto/Hash/TreeHash.hh"

#include <assert.h>
#include <stddef.h>
#include <string.h>

#include <Xi/Memory/Aligned.hh>

#include "Xi/Crypto/Hash/Keccak.hh"

size_t xi_crypto_hash_tree_depth(size_t count) {
  size_t i;
  size_t depth = 0;
  assert(count > 0);
  for (i = sizeof(size_t) << 2; i > 0; i >>= 1) {
    if (count >> i > 0) {
      count >>= i;
      depth += i;
    }
  }
  return depth;
}

int xi_crypto_hash_tree_hash(const xi_byte_t (*hashes)[XI_HASH_FAST_HASH_SIZE], size_t count,
                             xi_crypto_hash_fast rootHash) {
  XI_RETURN_EC_IF_NOT(count > 0, XI_RETURN_CODE_NO_SUCCESS);
  int ec = XI_RETURN_CODE_SUCCESS;
  if (count == 1) {
    memcpy(rootHash, hashes, XI_HASH_FAST_HASH_SIZE);
    return XI_RETURN_CODE_SUCCESS;
  } else if (count == 2) {
    ec = xi_crypto_hash_keccak_256((const xi_byte_t *)hashes, 2 * XI_HASH_FAST_HASH_SIZE, rootHash);
    XI_RETURN_EC_IF_NOT(ec == XI_RETURN_CODE_SUCCESS, ec);
    return XI_RETURN_CODE_SUCCESS;
  } else {
    size_t i, j;
    size_t cnt = count - 1;
    xi_byte_t(*ints)[XI_HASH_FAST_HASH_SIZE];
    for (i = 1; i < 8 * sizeof(size_t); i <<= 1) {
      cnt |= cnt >> i;
    }
    cnt &= ~(cnt >> 1);
    ints = xi_memory_aligned_alloc(cnt * XI_HASH_FAST_HASH_SIZE, XI_HASH_FAST_HASH_SIZE);
    XI_RETURN_EC_IF(ints == NULL, XI_RETURN_CODE_NO_SUCCESS);
    memcpy(ints, hashes, (2 * cnt - count) * XI_HASH_FAST_HASH_SIZE);

    for (i = 2 * cnt - count, j = 2 * cnt - count; j < cnt; i += 2, ++j) {
      ec = xi_crypto_hash_keccak_256(hashes[i], 2 * XI_HASH_FAST_HASH_SIZE, ints[j]);
      if(ec != XI_RETURN_CODE_SUCCESS) goto __label_xi_crypto_hash_tree_hash_return;
    }
    if(ec != XI_RETURN_CODE_SUCCESS) goto __label_xi_crypto_hash_tree_hash_return;
    assert(i == count);
    while (cnt > 2) {
      cnt >>= 1;
      for (i = 0, j = 0; j < cnt; i += 2, ++j) {
        ec = xi_crypto_hash_keccak_256(ints[i], 2 * XI_HASH_FAST_HASH_SIZE, ints[j]);
        if(ec != XI_RETURN_CODE_SUCCESS) goto __label_xi_crypto_hash_tree_hash_return;
      }
    }
    ec = xi_crypto_hash_keccak_256(ints[0], 2 * XI_HASH_FAST_HASH_SIZE, rootHash);

__label_xi_crypto_hash_tree_hash_return:
    xi_memory_aligned_free(ints);
    return ec;
  }
}

int xi_crypto_hash_tree_branch(const xi_byte_t (*hashes)[XI_HASH_FAST_HASH_SIZE], size_t count,
                               xi_byte_t (*branch)[XI_HASH_FAST_HASH_SIZE]) {
  size_t i, j;
  size_t cnt = 1;
  size_t depth = 0;
  xi_byte_t(*ints)[XI_HASH_FAST_HASH_SIZE];
  assert(count > 0);
  for (i = sizeof(size_t) << 2; i > 0; i >>= 1) {
    if (cnt << i <= count) {
      cnt <<= i;
      depth += i;
    }
  }
  XI_RETURN_EC_IF_NOT(cnt == 1ULL << depth, XI_RETURN_CODE_NO_SUCCESS);
  XI_RETURN_EC_IF_NOT(depth == xi_crypto_hash_tree_depth(count), XI_RETURN_CODE_NO_SUCCESS);
  ints = xi_memory_aligned_alloc((cnt - 1) * XI_HASH_FAST_HASH_SIZE, XI_HASH_FAST_HASH_SIZE);
  XI_RETURN_EC_IF(ints == NULL, XI_RETURN_CODE_NO_SUCCESS);

  memcpy(ints, hashes + 1, (2 * cnt - count - 1) * XI_HASH_FAST_HASH_SIZE);
  int ec = XI_RETURN_CODE_SUCCESS;
  for (i = 2 * cnt - count, j = 2 * cnt - count - 1; j < cnt - 1; i += 2, ++j) {
    ec = xi_crypto_hash_keccak_256(hashes[i], 2 * XI_HASH_FAST_HASH_SIZE, ints[j]);
    if(ec != XI_RETURN_CODE_SUCCESS) goto __label_xi_crypto_hash_tree_branch_return;
  }

  if(i != count) goto __label_xi_crypto_hash_tree_branch_return;
  while (depth > 0) {
    if(cnt != (1ULL << depth)) goto __label_xi_crypto_hash_tree_branch_return;
    cnt >>= 1;
    --depth;
    memcpy(branch[depth], ints[0], XI_HASH_FAST_HASH_SIZE);
    for (i = 1, j = 0; j < cnt - 1; i += 2, ++j) {
      ec = xi_crypto_hash_keccak_256(ints[i], 2 * XI_HASH_FAST_HASH_SIZE, ints[j]);
      if(ec != XI_RETURN_CODE_SUCCESS) goto __label_xi_crypto_hash_tree_branch_return;
    }
  }

__label_xi_crypto_hash_tree_branch_return:
  xi_memory_aligned_free(ints);
  return ec;
}

int xi_crypto_hashtree_hash_from_branch(const xi_byte_t (*branch)[XI_HASH_FAST_HASH_SIZE], size_t depth,
                                        const xi_byte_t *leaf, const xi_byte_t *path, xi_byte_t *root_hash) {
  if (depth == 0) {
    memcpy(root_hash, leaf, XI_HASH_FAST_HASH_SIZE);
    return XI_RETURN_CODE_SUCCESS;
  } else {
    int ec = XI_RETURN_CODE_SUCCESS;
    xi_byte_t buffer[2][XI_HASH_FAST_HASH_SIZE];
    int from_leaf = 1;
    xi_byte_t *leaf_path, *branch_path;
    while (depth > 0) {
      --depth;
      if (path && (((const xi_byte_t *)path)[depth >> 3] & (1 << (depth & 7))) != 0) {
        leaf_path = buffer[1];
        branch_path = buffer[0];
      } else {
        leaf_path = buffer[0];
        branch_path = buffer[1];
      }
      if (from_leaf) {
        memcpy(leaf_path, leaf, XI_HASH_FAST_HASH_SIZE);
        from_leaf = 0;
      } else {
        ec = xi_crypto_hash_keccak_256((const xi_byte_t *)buffer, 2 * XI_HASH_FAST_HASH_SIZE, leaf_path);
        XI_RETURN_EC_IF_NOT(ec == XI_RETURN_CODE_SUCCESS, ec);
      }
      memcpy(branch_path, branch[depth], XI_HASH_FAST_HASH_SIZE);
    }
    ec = xi_crypto_hash_keccak_256((const xi_byte_t *)buffer, 2 * XI_HASH_FAST_HASH_SIZE, root_hash);
    XI_RETURN_EC_IF_NOT(ec == XI_RETURN_CODE_SUCCESS, ec);
    return XI_RETURN_CODE_SUCCESS;
  }
}
