// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include "Xi/Crypto/Hash/FastHash.hh"

#if defined(__cplusplus)
extern "C" {
#endif

#include <stdlib.h>

size_t xi_crypto_hash_tree_depth(size_t count);
int xi_crypto_hash_tree_hash(const xi_byte_t (*hashes)[XI_HASH_FAST_HASH_SIZE], size_t count,
                             xi_crypto_hash_fast rootHash);
int xi_crypto_hash_tree_branch(const xi_byte_t (*hashes)[XI_HASH_FAST_HASH_SIZE], size_t count,
                               xi_byte_t (*branch)[XI_HASH_FAST_HASH_SIZE]);
int xi_crypto_hashtree_hash_from_branch(const xi_byte_t (*branch)[XI_HASH_FAST_HASH_SIZE], size_t depth,
                                        const xi_byte_t *leaf, const xi_byte_t *path, xi_byte_t *root_hash);

#if defined(__cplusplus)
}
#endif

#if defined(__cplusplus)

namespace Xi {
namespace Crypto {
namespace Hash {
#include "Xi/Crypto/Hash/HashError.hpp"

HashError treeHash(const ConstByteSpan &data, size_t count, ByteSpan out);

}  // namespace Hash
}  // namespace Crypto
}  // namespace Xi

#endif
