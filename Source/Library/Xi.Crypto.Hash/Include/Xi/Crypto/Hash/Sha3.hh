// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <Xi/Global.hh>
#include <Xi/Byte.hh>

#include "Xi/Crypto/Hash/Hash.hh"

#if defined(__cplusplus)
extern "C" {
#endif

#include <stdio.h>

int xi_crypto_hash_sha3_224(const xi_byte_t *data, size_t length, xi_crypto_hash_224 out);
int xi_crypto_hash_sha3_256(const xi_byte_t *data, size_t length, xi_crypto_hash_256 out);
int xi_crypto_hash_sha3_384(const xi_byte_t *data, size_t length, xi_crypto_hash_384 out);
int xi_crypto_hash_sha3_512(const xi_byte_t *data, size_t length, xi_crypto_hash_512 out);

#if defined(__cplusplus)
}
#endif

#if defined(__cplusplus)

namespace Xi {
namespace Crypto {
namespace Hash {
namespace Sha3 {
XI_CRYPTO_HASH_DECLARE_HASH_TYPE(Hash224, 224);
XI_CRYPTO_HASH_DECLARE_HASH_TYPE(Hash256, 256);
XI_CRYPTO_HASH_DECLARE_HASH_TYPE(Hash384, 384);
XI_CRYPTO_HASH_DECLARE_HASH_TYPE(Hash512, 512);

HashError compute(ConstByteSpan data, Hash224 &out);
HashError compute(ConstByteSpan data, Hash256 &out);
HashError compute(ConstByteSpan data, Hash384 &out);
HashError compute(ConstByteSpan data, Hash512 &out);
}  // namespace Sha3
}  // namespace Hash
}  // namespace Crypto
}  // namespace Xi

#endif
