﻿// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <Xi/Byte.hh>

#include "Xi/Crypto/Hash/Hash.hh"

#if defined(__cplusplus)
extern "C" {
#endif

#include <inttypes.h>
#include <stddef.h>

#define XI_CRYPTO_HASH_KECCAK_HASH_SIZE 32U
#define XI_CRYPTO_HASH_KECCAK_HASH_DATA_AREA 136U

typedef struct xi_crypto_hash_keccak_state {
  /// 1600 bits algorithm hashing state
  uint64_t hash[25];

  /// 1088-bit buffer for leftovers, block size = 136 B for 256-bit keccak
  uint64_t message[17];

  /// count of bytes in the message[] buffer
  size_t rest;
} xi_crypto_hash_keccak_state;

int xi_crypto_hash_keccak(const xi_byte_t *in, size_t inlen, xi_byte_t *md, size_t mdlen);
void xi_crypto_hash_keccakf(uint64_t st[], int rounds);

int xi_crypto_hash_keccak_init(xi_crypto_hash_keccak_state *ctx);
int xi_crypto_hash_keccak_copy(xi_crypto_hash_keccak_state *ctx, const xi_crypto_hash_keccak_state *toCopyCtx);
int xi_crypto_hash_keccak_update(xi_crypto_hash_keccak_state *ctx, const xi_byte_t *in, size_t inlen);
int xi_crypto_hash_keccak_finish(xi_crypto_hash_keccak_state *ctx, xi_byte_t *md);

int xi_crypto_hash_keccak_256(const xi_byte_t *in, size_t inlen, xi_byte_t *md);
int xi_crypto_hash_keccak_1600(const xi_byte_t *in, size_t inlen, xi_byte_t *md);

#if defined(__cplusplus)
}
#endif

#if defined(__cplusplus)

#include <optional>

#include <Xi/ErrorModel.hh>

#include "Xi/Crypto/Hash/HashError.hpp"

namespace Xi {
namespace Crypto {
namespace Hash {
namespace Keccak {

XI_CRYPTO_HASH_DECLARE_HASH_TYPE(Hash256, 256)
XI_CRYPTO_HASH_DECLARE_HASH_TYPE(Hash1600, 1600)

HashError compute(ConstByteSpan data, Hash256 &out);
HashError compute(ConstByteSpan data, Hash1600 &out);

class Hash256Stream {
 public:
  Hash256Stream();
  ~Hash256Stream();

  Hash256Stream &operator<<(ConstByteSpan data);

  Result<Hash256> take();
  Result<Hash256> get() const;

 private:
  xi_crypto_hash_keccak_state m_state;
  std::optional<Error> m_error;
};

}  // namespace Keccak
}  // namespace Hash
}  // namespace Crypto
}  // namespace Xi

#endif
