﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <Xi/Global.hh>
#include <Xi/Byte.hh>
#include <Xi/ErrorModel.hh>

#include "Xi/Crypto/Hash/Hash.hh"
#include "Xi/Crypto/Hash/HashError.hpp"

namespace Xi {
namespace Crypto {
namespace Hash {
namespace Crc {
XI_CRYPTO_HASH_DECLARE_HASH_TYPE(Hash16, 16);
XI_CRYPTO_HASH_DECLARE_HASH_TYPE(Hash32, 32);

HashError compute(ConstByteSpan data, Hash16 &out);
HashError compute(ConstByteSpan data, Hash32 &out);
}  // namespace Crc

Result<Crc::Hash16> crc16(ConstByteSpan data);
Result<Crc::Hash32> crc32(ConstByteSpan data);

}  // namespace Hash
}  // namespace Crypto
}  // namespace Xi
