﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include <gmock/gmock.h>

#include <map>
#include <string>

#include <Xi/Crypto/Hash/Crc.hpp>

#include "OracleTest.hpp"

#define XI_TEST_SUITE Xi_Crypto_Hash_Crc

TEST(XI_TEST_SUITE, Hash16) {
  using Hash = Xi::Crypto::Hash::Crc::Hash16;
  static const std::vector<std::pair<std::string, Hash>> Oracle{{{"", Hash{{0x00, 0x00}}},
                                                                 {"iu6QiD1lpH", Hash{{0xd9, 0x58}}},
                                                                 {"oPY1ZcUoPL", Hash{{0xce, 0xfe}}},
                                                                 {"BXHHILmBXf", Hash{{0x8a, 0xed}}}}};
  XiCryptoTest::genericHashTest<Hash>(Oracle);
}

TEST(XI_TEST_SUITE, Hash32) {
  using Hash = Xi::Crypto::Hash::Crc::Hash32;
  static const std::vector<std::pair<std::string, Hash>> Oracle{{{"", Hash{{0x00, 0x00, 0x00, 0x00}}},
                                                                 {"iu6QiD1lpH", Hash{{0x6e, 0xa6, 0xc4, 0x60}}},
                                                                 {"oPY1ZcUoPL", Hash{{0xfc, 0x47, 0x35, 0x98}}},
                                                                 {"BXHHILmBXf", Hash{{0xe4, 0x66, 0x35, 0x6e}}}}};
  XiCryptoTest::genericHashTest<Hash>(Oracle);
}
