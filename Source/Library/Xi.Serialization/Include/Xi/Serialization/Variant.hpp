// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <variant>
#include <cassert>

#include <Xi/Global.hh>
#include <Xi/ErrorModel.hh>
#include <Xi/ErrorModel.hh>

#include "Xi/Serialization/Tag.hpp"
#include "Xi/Serialization/Serializer.hpp"

namespace Xi {
namespace Serialization {

XI_ERROR_CODE_BEGIN(Variant)
XI_ERROR_CODE_VALUE(UnknownVariant, 0x0001)
XI_ERROR_CODE_VALUE(InvalidTag, 0x0002)
XI_ERROR_CODE_END(Variant, "Serialization::VariantError")

namespace Impl {
inline static const Tag VariantTypeTag{0x0001, "type"};
inline static const Tag VariantValueTag{0x0002, "value"};

template <typename _VariantT, size_t _Index>
inline Tag getVariantTag();

template <size_t _Index, typename _InvariantT, typename... _Ts>
[[nodiscard]] inline Result<void> serializeVariantInput(std::variant<_Ts...> &value, const Tag &tag,
                                                        Serializer &serializer, bool haltOnValue = false) {
  assert(serializer.isInputMode());
  if constexpr (_Index == sizeof...(_Ts)) {
    XI_UNUSED(value)
    XI_UNUSED(tag)
    XI_UNUSED(serializer)
    XI_UNUSED(haltOnValue)
    XI_FAIL(VariantError::UnknownVariant)
  } else {
    using native_t = std::variant_alternative_t<_Index, std::variant<_Ts...>>;
    if (tag == getVariantTag<_InvariantT, _Index>()) {
      native_t nativeValue;
      if (!haltOnValue) {
        XI_ERROR_PROPAGATE_CATCH(serializer(nativeValue, VariantValueTag));
      }
      value = std::move(nativeValue);
      XI_SUCCEED()
    } else {
      return serializeVariantInput<_Index + 1, _InvariantT, _Ts...>(value, tag, serializer, haltOnValue);
    }
  }
}

template <size_t _Index, typename _InvariantT, typename... _Ts>
[[nodiscard]] inline Result<void> serializeVariantOutput(std::variant<_Ts...> &value, Serializer &serializer,
                                                         bool haltOnValue = false) {
  assert(serializer.isOutputMode());
  if constexpr (_Index == sizeof...(_Ts)) {
    XI_UNUSED(value)
    XI_UNUSED(serializer)
    XI_UNUSED(haltOnValue)
    XI_FAIL(VariantError::UnknownVariant)
  } else {
    if (value.index() == _Index) {
      auto &varValue = std::get<_Index>(value);
      Tag tag{getVariantTag<_InvariantT, _Index>()};
      XI_ERROR_PROPAGATE_CATCH(serializer.typeTag(tag, VariantTypeTag));
      if (!haltOnValue) {
        XI_ERROR_PROPAGATE_CATCH(serializer(varValue, VariantValueTag));
      }
      XI_SUCCEED()
    } else {
      return serializeVariantOutput<_Index + 1, _InvariantT, _Ts...>(value, serializer, haltOnValue);
    }
  }
}

}  // namespace Impl

template <typename _InvariantT, typename... _Ts>
[[nodiscard]] inline Result<void> serializeVariant(std::variant<_Ts...> &value, const Tag &name,
                                                   Serializer &serializer) {
  XI_ERROR_PROPAGATE_CATCH(serializer.beginComplex(name));
  if (serializer.isInputMode()) {
    Tag tag{Tag::NoBinaryTag, Tag::NoTextTag};
    XI_ERROR_PROPAGATE_CATCH(serializer.typeTag(tag, Impl::VariantTypeTag));
    XI_FAIL_IF(tag.isNull(), VariantError::InvalidTag);
    const auto ec = Impl::serializeVariantInput<0, _InvariantT, _Ts...>(value, tag, serializer);
    XI_ERROR_PROPAGATE(ec);
  } else {
    assert(serializer.isOutputMode());
    const auto ec = Impl::serializeVariantOutput<0, _InvariantT, _Ts...>(value, serializer, false);
    XI_ERROR_PROPAGATE(ec)
  }
  return serializer.endComplex();
}

template <typename... _Ts>
[[nodiscard]] inline bool serialize(std::variant<_Ts...> &value, const Tag &name, Serializer &serializer) {
  return serializeVariant<std::variant<_Ts...>>(value, name, serializer);
}

}  // namespace Serialization
}  // namespace Xi

XI_ERROR_CODE_OVERLOADS(Xi::Serialization, Variant)

#define XI_SERIALIZATION_VARIANT_TAG(VARIANT_TYPE, VALUE_INDEX, BINARY, TEXT) \
  namespace Xi {                                                              \
  namespace Serialization {                                                   \
  namespace Impl {                                                            \
  template <>                                                                 \
  inline Tag getVariantTag<VARIANT_TYPE, VALUE_INDEX>() {                     \
    return Tag{BINARY, TEXT};                                                 \
  }                                                                           \
  }                                                                           \
  }                                                                           \
  }

#define XI_SERIALIZATION_VARIANT_INVARIANT(VARIANT_TYPE, ...)                                                         \
  struct VARIANT_TYPE : std::variant<__VA_ARGS__> {                                                                   \
    using variant::variant;                                                                                           \
  };                                                                                                                  \
  [[nodiscard]] inline ::Xi::Result<void> serialize(VARIANT_TYPE &value, const ::Xi::Serialization::Tag &name,        \
                                                    ::Xi::Serialization::Serializer &serializer) {                    \
    return ::Xi::Serialization::serializeVariant<VARIANT_TYPE>(static_cast<std::variant<__VA_ARGS__> &>(value), name, \
                                                               serializer);                                           \
  }
