// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <set>

#include <Xi/Global.hh>
#include <Xi/ErrorModel.hh>

#include "Xi/Serialization/Serializer.hpp"

namespace Xi {
namespace Serialization {

XI_ERROR_CODE_BEGIN(Set)
XI_ERROR_CODE_VALUE(Duplicate, 0x0001)
XI_ERROR_CODE_END(Set, "Serialization::SetError")

template <typename _ValueT>
Result<void> serialize(std::set<_ValueT>& value, const Tag& name, Serializer& ser) {
  size_t size = value.size();
  XI_ERROR_PROPAGATE_CATCH(ser.beginVector(size, name))
  if (ser.isOutputMode()) {
    for (const auto& ivalue : value) {
      XI_ERROR_PROPAGATE_CATCH(ser(const_cast<_ValueT&>(ivalue), Tag::Null))
    }
  } else {
    _ValueT ivalue{/* */};
    XI_ERROR_PROPAGATE_CATCH(ser(ivalue, Tag::Null))
    const auto inserted = value.emplace(std::move(ivalue)).second;
    XI_FAIL_IF_NOT(inserted, SetError::Duplicate)
  }
  XI_ERROR_PROPAGATE_CATCH(ser.endVector())
  XI_SUCCEED()
}

}  // namespace Serialization
}  // namespace Xi

XI_ERROR_CODE_OVERLOADS(Xi::Serialization, Set)
