// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <string_view>
#include <cinttypes>
#include <system_error>

#include <Xi/Global.hh>
#include <Xi/ErrorModel.hh>
#include <Xi/Byte.hh>

#include "Xi/Serialization/Format.hpp"
#include "Xi/Serialization/Tag.hpp"

namespace Xi {
namespace Serialization {
class InputSerializer {
 public:
  virtual ~InputSerializer() = default;

  [[nodiscard]] virtual Format format() const = 0;

  [[nodiscard]] virtual Result<void> readInt8(std::int8_t& value, const Tag& nameTag) = 0;
  [[nodiscard]] virtual Result<void> readUInt8(std::uint8_t& value, const Tag& nameTag) = 0;
  [[nodiscard]] virtual Result<void> readInt16(std::int16_t& value, const Tag& nameTag) = 0;
  [[nodiscard]] virtual Result<void> readUInt16(std::uint16_t& value, const Tag& nameTag) = 0;
  [[nodiscard]] virtual Result<void> readInt32(std::int32_t& value, const Tag& nameTag) = 0;
  [[nodiscard]] virtual Result<void> readUInt32(std::uint32_t& value, const Tag& nameTag) = 0;
  [[nodiscard]] virtual Result<void> readInt64(std::int64_t& value, const Tag& nameTag) = 0;
  [[nodiscard]] virtual Result<void> readUInt64(std::uint64_t& value, const Tag& nameTag) = 0;

  [[nodiscard]] virtual Result<void> readBoolean(bool& value, const Tag& nameTag) = 0;

  [[nodiscard]] virtual Result<void> readFloat(float& value, const Tag& nameTag) = 0;
  [[nodiscard]] virtual Result<void> readDouble(double& value, const Tag& nameTag) = 0;

  [[nodiscard]] virtual Result<void> readString(std::string& string, const Tag& nameTag) = 0;
  [[nodiscard]] virtual Result<void> readBinary(ByteVector& blob, const Tag& nameTag) = 0;

  [[nodiscard]] virtual Result<void> readBlob(ByteSpan out, const Tag& nameTag) = 0;

  [[nodiscard]] virtual Result<void> readTag(Tag& value, const Tag& nameTag) = 0;
  [[nodiscard]] virtual Result<void> readFlag(TagVector& value, const Tag& nameTag) = 0;

  [[nodiscard]] virtual Result<void> beginReadComplex(const Tag& nameTag) = 0;
  [[nodiscard]] virtual Result<void> endReadComplex() = 0;

  [[nodiscard]] virtual Result<void> beginReadVector(size_t& size, const Tag& nameTag) = 0;
  [[nodiscard]] virtual Result<void> endReadVector() = 0;

  [[nodiscard]] virtual Result<void> beginReadArray(size_t size, const Tag& nameTag) = 0;
  [[nodiscard]] virtual Result<void> endReadArray() = 0;

  [[nodiscard]] virtual Result<void> checkValue(bool& isNull, const Tag& nameTag) = 0;
};

XI_DECLARE_SMART_POINTER(InputSerializer)

}  // namespace Serialization
}  // namespace Xi
