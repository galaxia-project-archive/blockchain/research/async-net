// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

namespace Xi {
namespace Serialization {
enum struct Format { Binary, HumanReadable };
}
}  // namespace Xi
