// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <type_traits>
#include <cassert>
#include <cinttypes>

#include <Xi/ErrorModel.hh>

#include "Xi/Serialization/Serializer.hpp"
#include "Xi/Serialization/Tag.hpp"

namespace Xi {
namespace Serialization {

XI_ERROR_CODE_BEGIN(Flag)
XI_ERROR_CODE_VALUE(InvalidTag, 0x0001)
XI_ERROR_CODE_VALUE(UnknownFlag, 0x0002)
XI_ERROR_CODE_VALUE(MissingTag, 0x0003)
XI_ERROR_CODE_END(Flag, "Serialization::FlagError")

namespace Impl {
template <typename _FlagT>
struct FlagRange {
  static inline constexpr uint16_t begin() {
    return 0;
  }
  static inline constexpr uint16_t end() {
    return 0;
  }
  static inline constexpr uint8_t beginShift() {
    return 0;
  }
  static inline constexpr uint8_t endShift() {
    return 0;
  }
};

template <uint16_t _IndexV, uint8_t _CurrentV = 0>
inline constexpr uint8_t getFlagShift() {
  static_assert(_IndexV > 0, "0 == (1 << X) has no solution.");
  static_assert(_CurrentV < 14, "only 14 bits ares upported for flags.");
  if constexpr ((_IndexV & (1 << _CurrentV)) > 0) {
    static_assert((_IndexV & (1 << _CurrentV)) == _IndexV, "fags may have exactly one bit set.");
    return _CurrentV;
  } else {
    return getFlagShift<_IndexV, _CurrentV + 1>();
  }
}

template <typename _FlagT, uint16_t _IndexV>
Tag getFlagTag();

template <typename _FlagT, uint16_t _IndexV>
inline constexpr bool hasFlagTag() {
  return false;
}

template <typename _FlagT, uint8_t _ShiftV = FlagRange<_FlagT>::beginShift()>
[[nodiscard]] bool getFlag(_FlagT& flag, const Tag& tag) {
  if constexpr (_ShiftV > FlagRange<_FlagT>::endShift()) {
    XI_UNUSED(flag);
    XI_UNUSED(tag);
    return false;
  } else {
    if constexpr (hasFlagTag<_FlagT, (1 << _ShiftV)>()) {
      if (tag == getFlagTag<_FlagT, (1 << _ShiftV)>()) {
        flag = static_cast<_FlagT>(1 << _ShiftV);
        return true;
      }
    }
    return getFlag<_FlagT, _ShiftV + 1>(flag, tag);
  }
}

template <typename _FlagT, uint8_t _ShiftV = FlagRange<_FlagT>::beginShift()>
[[nodiscard]] bool collectFlatTags(TagVector& tags, const _FlagT flag) {
  if constexpr (_ShiftV > FlagRange<_FlagT>::endShift()) {
    XI_UNUSED(tags);
    XI_UNUSED(flag);
    return static_cast<uint16_t>(flag) < (1 << _ShiftV);
  } else {
    if ((static_cast<uint16_t>(flag) & (1 << _ShiftV)) > 0) {
      if constexpr (!hasFlagTag<_FlagT, (1 << _ShiftV)>()) {
        return false;
      } else {
        tags.push_back(getFlagTag<_FlagT, (1 << _ShiftV)>());
      }
    }
    return collectFlatTags<_FlagT, _ShiftV + 1U>(tags, flag);
  }
}

template <typename _FlagT>
[[nodiscard]] inline Result<void> serializeFlag(_FlagT& value, const Tag& name, Serializer& serializer) {
  if (serializer.isInputMode()) {
    TagVector tags;
    XI_ERROR_PROPAGATE_CATCH(serializer.flag(tags, name));
    _FlagT composition = static_cast<_FlagT>(0);
    for (const auto& tag : tags) {
      _FlagT iFlag = static_cast<_FlagT>(0);
      XI_FAIL_IF_NOT(getFlag<_FlagT>(iFlag, tag), FlagError::InvalidTag);
      XI_FAIL_IF(iFlag == static_cast<_FlagT>(0), FlagError::InvalidTag);
      composition = composition | iFlag;
    }
    value = composition;
    XI_SUCCEED()
  } else {
    TagVector tags;
    XI_FAIL_IF_NOT(collectFlatTags<_FlagT>(tags, value), FlagError::UnknownFlag);
    return serializer.flag(tags, name);
  }
}

}  // namespace Impl

}  // namespace Serialization
}  // namespace Xi

XI_ERROR_CODE_OVERLOADS(Xi::Serialization, Flag)

#define XI_SERIALIZATION_FLAG(FLAG_TYPE)                                                      \
  inline ::Xi::Result<void> serialize(FLAG_TYPE& value, const ::Xi::Serialization::Tag& name, \
                                      ::Xi::Serialization::Serializer& serializer) {          \
    return ::Xi::Serialization::Impl::serializeFlag<FLAG_TYPE>(value, name, serializer);      \
  }

#define XI_SERIALIZATION_FLAG_RANGE(FLAG_TYPE, FLAG_BEGIN, FLAG_END)       \
  namespace Xi {                                                           \
  namespace Serialization {                                                \
  namespace Impl {                                                         \
  template <>                                                              \
  struct FlagRange<FLAG_TYPE> {                                            \
    static inline constexpr uint16_t begin() {                             \
      return static_cast<uint16_t>(FLAG_TYPE::FLAG_BEGIN);                 \
    }                                                                      \
    static inline constexpr uint16_t end() {                               \
      return static_cast<uint16_t>(FLAG_TYPE::FLAG_END);                   \
    }                                                                      \
    static inline constexpr uint8_t beginShift() {                         \
      return getFlagShift<static_cast<uint16_t>(FLAG_TYPE::FLAG_BEGIN)>(); \
    }                                                                      \
    static inline constexpr uint8_t endShift() {                           \
      return getFlagShift<static_cast<uint16_t>(FLAG_TYPE::FLAG_END)>();   \
    }                                                                      \
  };                                                                       \
  }                                                                        \
  }                                                                        \
  }

#define XI_SERIALIZATION_FLAG_TAG(FLAG_TYPE, FLAG_VALUE, TEXT)                                  \
  namespace Xi {                                                                                \
  namespace Serialization {                                                                     \
  namespace Impl {                                                                              \
  template <>                                                                                   \
  inline constexpr bool hasFlagTag<FLAG_TYPE, static_cast<uint16_t>(FLAG_TYPE::FLAG_VALUE)>() { \
    return true;                                                                                \
  }                                                                                             \
  template <>                                                                                   \
  inline Tag getFlagTag<FLAG_TYPE, static_cast<uint16_t>(FLAG_TYPE::FLAG_VALUE)>() {            \
    return Tag{getFlagShift<static_cast<uint16_t>(FLAG_TYPE::FLAG_VALUE)>() + 1U, TEXT};        \
  }                                                                                             \
  }                                                                                             \
  }                                                                                             \
  }
