// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <array>

#include "Xi/Serialization/Serializer.hpp"

namespace Xi {
namespace Serialization {

class Object {
 private:
  struct _Concept {
    virtual ~_Concept() = default;
    virtual Result<void> serializeObject(const Tag& name, Serializer& serializer) = 0;
  };
  template <typename _ValueT>
  struct _Impl final : _Concept {
    _ValueT& ref;

    _Impl(_ValueT& ref_) : ref{ref_} {
      /* */
    }
    ~_Impl() override = default;

    Result<void> serializeObject(const Tag& name, Serializer& serializer) override {
      return serializer(ref, name);
    }
  };

 public:
  Object();
  XI_DELETE_COPY(Object);
  XI_DEFAULT_MOVE(Object);
  ~Object() = default;

  Result<void> serialize(const Tag& name, Serializer& serializer);

 private:
  std::unique_ptr<_Concept> m_concept;

  template <typename _ValueT>
  explicit Object(_ValueT& value) : m_concept{new _Impl<_ValueT>(value)} {
    /* */
  }

  template <typename _ValueT>
  friend Object makeObject(_ValueT&);
};

template <typename _ValueT>
Object makeObject(_ValueT& value) {
  return Object{value};
}

Result<void> serialize(Object& value, const Tag& name, Serializer& serializer);

}  // namespace Serialization
}  // namespace Xi
