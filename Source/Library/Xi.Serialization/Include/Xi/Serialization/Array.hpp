// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <array>

#include "Xi/Serialization/Serializer.hpp"

namespace Xi {
namespace Serialization {

/*!
 * \section Serialization
 *
 * \attention Binary blobs should be serialized using ISerializer::binary, otherwise thiis
 * serialization method may make an array for a blob in its representation. Ie. for Json [ 122, 10 ].
 */
template <typename _ValueT, size_t _SizeV>
Result<void> serialize(std::array<_ValueT, _SizeV>& value, const Tag& name, Serializer& serializer) {
  XI_ERROR_PROPAGATE_CATCH(serializer.beginArray(_SizeV, name));
  for (size_t i = 0; i < _SizeV; ++i) {
    XI_ERROR_PROPAGATE_CATCH(serializer(value[i], Tag::Null));
  }
  XI_ERROR_PROPAGATE_CATCH(serializer.endArray());
  XI_SUCCEED()
}

template <typename _ValueT, size_t _SizeV>
Result<void> serialize(_ValueT value[_SizeV], const Tag& name, Serializer& serializer) {
  XI_ERROR_PROPAGATE_CATCH(serializer.beginArray(_SizeV, name));
  for (size_t i = 0; i < _SizeV; ++i) {
    XI_ERROR_PROPAGATE_CATCH(serializer(value[i], Tag::Null));
  }
  XI_ERROR_PROPAGATE_CATCH(serializer.endArray());
  XI_SUCCEED()
}

}  // namespace Serialization
}  // namespace Xi
