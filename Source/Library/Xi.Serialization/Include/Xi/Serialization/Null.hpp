// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <Xi/Null.hpp>

#include "Xi/Serialization/Serializer.hpp"

namespace Xi {
namespace Serialization {

Result<void> serialize(Null&, const Tag& name, Serializer& serializer);

}  // namespace Serialization
}  // namespace Xi
