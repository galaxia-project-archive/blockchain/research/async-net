// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <optional>
#include <type_traits>
#include <cassert>

#include <Xi/Global.hh>
#include <Xi/ErrorModel.hh>

#include "Xi/Serialization/Serializer.hpp"

namespace Xi {
namespace Serialization {

template <typename _ValueT>
Result<void> serialize(std::optional<_ValueT> &value, const Tag &name, Serializer &serializer) {
  using native_t = typename std::remove_cv_t<_ValueT>;
  static_assert(std::is_default_constructible_v<native_t>,
                "optional serialization expects default constructible types");

  bool hasValue = value.has_value();
  XI_ERROR_PROPAGATE_CATCH(serializer.maybe(hasValue, name));
  if (serializer.isInputMode()) {
    if (hasValue) {
      value.emplace();
      return serializer(*value, name);
    } else {
      value = std::nullopt;
      XI_SUCCEED()
    }
  } else {
    assert(serializer.isOutputMode());
    if (hasValue) {
      return serializer(*value, name);
    } else {
      XI_SUCCEED()
    }
  }
}

}  // namespace Serialization
}  // namespace Xi
