// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <map>
#include <unordered_map>

#include <Xi/Global.hh>
#include <Xi/ErrorModel.hh>
#include <Xi/ErrorModel.hh>
#include <Xi/ErrorModel.hh>

#include "Xi/Serialization/Serializer.hpp"

namespace Xi {
namespace Serialization {

namespace Impl {
extern const Tag __MapKeyTag;
extern const Tag __MapValueTag;
}  // namespace Impl

XI_ERROR_CODE_BEGIN(Map)
XI_ERROR_CODE_VALUE(DuplicateEntry, 0x0001)
XI_ERROR_CODE_END(Map, "Serialization::MapError")

template <typename _KeyT, typename _ValueT>
Result<void> serialize(std::map<_KeyT, _ValueT>& value, const Tag& name, Serializer& serializer) {
  if (serializer.isInputMode()) {
    value.clear();
    size_t size = 0;
    XI_ERROR_PROPAGATE_CATCH(serializer.beginVector(size, name));
    for (size_t i = 0; i < size; ++i) {
      _KeyT ikey{};
      _ValueT ivalue{};

      XI_ERROR_PROPAGATE_CATCH(serializer.beginComplex(Tag::Null));
      XI_ERROR_PROPAGATE_CATCH(serializer(ikey, Impl::__MapKeyTag));
      XI_ERROR_PROPAGATE_CATCH(serializer(ivalue, Impl::__MapValueTag));
      XI_ERROR_PROPAGATE_CATCH(serializer.endComplex());

      XI_FAIL_IF_NOT(value
                         .emplace(std::piecewise_construct, std::forward_as_tuple(std::move(ikey)),
                                  std::forward_as_tuple(std::move(ivalue)))
                         .second,
                     MapError::DuplicateEntry);
    }
    XI_ERROR_PROPAGATE_CATCH(serializer.endVector())
    XI_SUCCEED()
  } else if (serializer.isOutputMode()) {
    size_t size = value.size();
    XI_ERROR_PROPAGATE_CATCH(serializer.beginVector(size, name));
    for (const auto& entry : value) {
      XI_ERROR_PROPAGATE_CATCH(serializer.beginComplex(Tag::Null));
      XI_ERROR_PROPAGATE_CATCH(serializer(const_cast<_KeyT&>(entry.first), Impl::__MapKeyTag));
      XI_ERROR_PROPAGATE_CATCH(serializer(const_cast<_ValueT&>(entry.second), Impl::__MapValueTag));
      XI_ERROR_PROPAGATE_CATCH(serializer.endComplex());
    }
    XI_ERROR_PROPAGATE_CATCH(serializer.endVector())
    XI_SUCCEED()
  } else {
    XI_EXCEPTIONAL(InvalidVariantTypeError)
  }
}

}  // namespace Serialization
}  // namespace Xi

XI_ERROR_CODE_OVERLOADS(Xi::Serialization, Map)
