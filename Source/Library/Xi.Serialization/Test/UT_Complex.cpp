// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include <cinttypes>
#include <limits>

#include <Xi/Serialization/Serialization.hpp>

#include "GenericSerializerTest.hpp"

namespace {
struct Complex {
  std::string string;
  std::uint16_t integer;
  bool boolean;

  XI_SERIALIZATION_COMPLEX_BEGIN()
  XI_SERIALIZATION_MEMBER(string, 0x0001, "string")
  XI_SERIALIZATION_MEMBER(integer, 0x0002, "integer")
  XI_SERIALIZATION_MEMBER(boolean, 0x0003, "boolean")
  XI_SERIALIZATION_COMPLEX_END
};

struct ComplexWithInheritance : Complex {
  float floating;

  XI_SERIALIZATION_COMPLEX_BEGIN()
  XI_SERIALIZATION_BASE(Complex)
  XI_SERIALIZATION_MEMBER(floating, 0x0101, "floating")
  XI_SERIALIZATION_COMPLEX_END
};
}  // namespace

XI_GENERIC_SERIALIZER_TEST(Complex) {
  using namespace Xi::Serialization;
  Complex complex{};
  complex.string = "^._.^";
  complex.integer = 7;
  complex.boolean = true;
  auto _complex = _this.serializeAndDeserialize(complex);
  ASSERT_TRUE(isSuccess(_complex));
  EXPECT_EQ(_complex->string, complex.string);
  EXPECT_EQ(_complex->integer, complex.integer);
  EXPECT_EQ(_complex->boolean, complex.boolean);
}

XI_GENERIC_SERIALIZER_TEST(ComplexWithInheritance) {
  using namespace Xi::Serialization;
  ComplexWithInheritance complex{};
  complex.string = "";
  complex.integer = 0;
  complex.boolean = false;
  complex.floating = -0.74f;
  auto _complex = _this.serializeAndDeserialize(complex);
  ASSERT_TRUE(isSuccess(_complex));
  EXPECT_EQ(_complex->string, complex.string);
  EXPECT_EQ(_complex->integer, complex.integer);
  EXPECT_EQ(_complex->boolean, complex.boolean);
  EXPECT_FLOAT_EQ(_complex->floating, complex.floating);
}
