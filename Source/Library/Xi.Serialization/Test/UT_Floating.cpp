// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include <limits>

#include <Xi/Serialization/Serialization.hpp>

#include "GenericSerializerTest.hpp"

namespace Xi_Serialization_Serializer {
struct FloatingStorage {
  float float_;
  double double_;

  XI_SERIALIZATION_COMPLEX_BEGIN()
  XI_SERIALIZATION_MEMBER(float_, 0x0001, "float")
  XI_SERIALIZATION_MEMBER(double_, 0x0002, "double")
  XI_SERIALIZATION_COMPLEX_END
};
}  // namespace Xi_Serialization_Serializer

XI_GENERIC_SERIALIZER_TEST(Floating) {
  using namespace Xi_Serialization_Serializer;
  using namespace Xi::Serialization;
  FloatingStorage storage{/* */};
  storage.float_ = -12e10f;
  storage.double_ = 1e128;
  auto _storage = _this.serializeAndDeserialize(storage);
  ASSERT_TRUE(isSuccess(_storage));
  EXPECT_EQ(_storage->float_, storage.float_);
  EXPECT_EQ(_storage->double_, storage.double_);
}
