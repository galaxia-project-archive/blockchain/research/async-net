// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include <array>
#include <cinttypes>
#include <limits>

#include <Xi/TypeSafe/Flag.hpp>
#include <Xi/Serialization/Flag.hpp>
#include <Xi/Serialization/Serialization.hpp>

#include "GenericSerializerTest.hpp"

namespace Xi_Serialization_Serializer {
enum struct AnyFlag {
  None = 0,
  First = 1 << 0,
  Second = 1 << 1,
  Both = First | Second,
};
XI_SERIALIZATION_FLAG(AnyFlag)
XI_TYPESAFE_FLAG_MAKE_OPERATIONS(AnyFlag)

struct FlagStorage {
  AnyFlag flag;

  XI_SERIALIZATION_COMPLEX_BEGIN()
  XI_SERIALIZATION_MEMBER(flag, 0x0001, "flag")
  XI_SERIALIZATION_COMPLEX_END
};
}  // namespace Xi_Serialization_Serializer
XI_SERIALIZATION_FLAG_RANGE(Xi_Serialization_Serializer::AnyFlag, First, Second)
XI_SERIALIZATION_FLAG_TAG(Xi_Serialization_Serializer::AnyFlag, First, "first")
XI_SERIALIZATION_FLAG_TAG(Xi_Serialization_Serializer::AnyFlag, Second, "second")

XI_GENERIC_SERIALIZER_TEST(FlagSingle) {
  using namespace Xi_Serialization_Serializer;
  using namespace Xi::Serialization;
  FlagStorage storage{AnyFlag::Second};
  auto _storage = _this.serializeAndDeserialize(storage);
  ASSERT_TRUE(isSuccess(_storage));
  EXPECT_EQ(_storage->flag, storage.flag);
}

XI_GENERIC_SERIALIZER_TEST(FlagCombination) {
  using namespace Xi_Serialization_Serializer;
  using namespace Xi::Serialization;
  FlagStorage storage{AnyFlag::Both};
  auto _storage = _this.serializeAndDeserialize(storage);
  ASSERT_TRUE(isSuccess(_storage));
  EXPECT_EQ(_storage->flag, storage.flag);
}

XI_GENERIC_SERIALIZER_TEST(FlagNone) {
  using namespace Xi_Serialization_Serializer;
  using namespace Xi::Serialization;
  FlagStorage storage{AnyFlag::None};
  auto _storage = _this.serializeAndDeserialize(storage);
  ASSERT_TRUE(isSuccess(_storage));
  EXPECT_EQ(_storage->flag, storage.flag);
}
