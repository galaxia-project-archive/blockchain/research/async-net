// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include <cinttypes>
#include <optional>

#include <Xi/Serialization/Map.hpp>
#include <Xi/Serialization/Serialization.hpp>

#include "GenericSerializerTest.hpp"

XI_GENERIC_SERIALIZER_TEST(EmptyMap) {
  using namespace Xi::Serialization;
  using namespace ::testing;
  std::map<std::string, std::string> map{};
  auto smap = _this.serializeAndDeserialize(map);
  ASSERT_TRUE(isSuccess(smap));
  EXPECT_THAT(*smap, SizeIs(0));
}

XI_GENERIC_SERIALIZER_TEST(FilledMap) {
  using namespace Xi::Serialization;
  using namespace ::testing;
  std::map<std::string, std::string> map{};
  map[""] = "EMPTY";
  map["Some cats"] = "may meow!";
  auto smap = _this.serializeAndDeserialize(map);
  ASSERT_TRUE(isSuccess(smap));
  EXPECT_THAT(*smap, SizeIs(2));
  EXPECT_THAT((*smap)[""], Eq("EMPTY"));
  EXPECT_THAT((*smap)["Some cats"], Eq("may meow!"));
}
