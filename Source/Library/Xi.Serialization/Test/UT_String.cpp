// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include <cinttypes>
#include <string>

#include <Xi/Serialization/Serialization.hpp>

#include "GenericSerializerTest.hpp"

namespace {
struct StringStorage {
  std::string value;

  XI_SERIALIZATION_COMPLEX_BEGIN()
  XI_SERIALIZATION_MEMBER(value, 0x0001, "value")
  XI_SERIALIZATION_COMPLEX_END
};
}  // namespace

XI_GENERIC_SERIALIZER_TEST(StringEmpty) {
  using namespace Xi::Serialization;
  StringStorage maybe{};
  maybe.value = "";
  auto _maybe = _this.serializeAndDeserialize(maybe);
  ASSERT_TRUE(isSuccess(_maybe));
  EXPECT_EQ(_maybe->value, maybe.value);
}

XI_GENERIC_SERIALIZER_TEST(StringCommon) {
  using namespace Xi::Serialization;
  StringStorage maybe{};
  maybe.value = "^.-.^::^*_*^";
  auto _maybe = _this.serializeAndDeserialize(maybe);
  ASSERT_TRUE(isSuccess(_maybe));
  EXPECT_EQ(_maybe->value, maybe.value);
}

XI_GENERIC_SERIALIZER_TEST(StringWithEscapes) {
  using namespace Xi::Serialization;
  StringStorage maybe{};
  maybe.value = R"__(
^.-.^ \t
          \0\n
^*_*^
)__";
  auto _maybe = _this.serializeAndDeserialize(maybe);
  ASSERT_TRUE(isSuccess(_maybe));
  EXPECT_EQ(_maybe->value, maybe.value);
}
