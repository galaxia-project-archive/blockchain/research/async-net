// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <type_traits>
#include <sstream>
#include <string>
#include <cstring>
#include <utility>

#include <gmock/gmock.h>

#include <Xi/Stream/InMemoryStreams.hpp>
#include <Xi/Serialization/Serializer.hpp>
#include <Xi/Serialization/Json/OutputSerializer.hpp>
#include <Xi/Serialization/Json/InputSerializer.hpp>
#include <Xi/Serialization/Binary/OutputSerializer.hpp>
#include <Xi/Serialization/Binary/InputSerializer.hpp>
#include <Xi/Serialization/Yaml/OutputSerializer.hpp>
#include <Xi/Serialization/Yaml/InputSerializer.hpp>

using serialized_type = Xi::ByteVector;

class Xi_Serialization_Serializer_Binary : public ::testing::Test {
 public:
  template <typename _ValueT>
  Xi::Result<serialized_type> valueSerialize(_ValueT& value) {
    using namespace Xi;
    using namespace Xi::Serialization;

    serialized_type reval{};
    Stream::ByteVectorOutputStream stream{reval};
    Binary::OutputSerializer output{stream};
    Serializer serializer{output};

    XI_ERROR_PROPAGATE_CATCH(serialize(value, Tag::Null, serializer));
    XI_SUCCEED(std::move(reval))
  }

  template <typename _ValueT>
  Xi::Result<_ValueT> valueDeserialize(serialized_type& data) {
    using namespace Xi;
    using namespace Xi::Serialization;
    _ValueT reval{};

    Stream::InMemoryInputStream stream{data};
    Binary::InputSerializer input{stream};
    Serializer serializer{input};
    XI_ERROR_PROPAGATE_CATCH(serialize(reval, Tag::Null, serializer))
    XI_SUCCEED(std::move(reval))
  }

  template <typename _ValueT>
  Xi::Result<_ValueT> serializeAndDeserialize(_ValueT& value) {
    auto data = valueSerialize(value);
    XI_ERROR_PROPAGATE(data)
    return valueDeserialize<_ValueT>(*data);
  }
};

class Xi_Serialization_Serializer_Json : public ::testing::Test {
 public:
  template <typename _ValueT>
  Xi::Result<serialized_type> valueSerialize(_ValueT& value) {
    using namespace Xi;
    using namespace Xi::Serialization;

    serialized_type reval{};
    Stream::ByteVectorOutputStream stream{reval};
    Json::OutputSerializer output{stream};
    Serializer serializer{output};
    XI_ERROR_PROPAGATE_CATCH(serialize(value, Tag::Null, serializer));
    XI_SUCCEED(std::move(reval))
  }

  template <typename _ValueT>
  Xi::Result<_ValueT> valueDeserialize(serialized_type& data) {
    using namespace Xi;
    using namespace Xi::Serialization;
    _ValueT reval{};

    Stream::InMemoryInputStream stream{data};
    Json::InputSerializer input{stream};
    Serializer serializer{input};
    XI_ERROR_PROPAGATE_CATCH(serialize(reval, Tag::Null, serializer))
    XI_SUCCEED(std::move(reval))
  }

  template <typename _ValueT>
  Xi::Result<_ValueT> serializeAndDeserialize(_ValueT& value) {
    auto data = valueSerialize(value);
    XI_ERROR_PROPAGATE(data)
    return valueDeserialize<_ValueT>(*data);
  }
};

class Xi_Serialization_Serializer_Yaml : public ::testing::Test {
 public:
  template <typename _ValueT>
  Xi::Result<serialized_type> valueSerialize(_ValueT& value) {
    using namespace Xi;
    using namespace Xi::Serialization;

    std::stringstream stream{};
    Yaml::OutputSerializer output{stream};
    Serializer serializer{output};
    XI_ERROR_PROPAGATE_CATCH(serialize(value, Tag::Null, serializer));

    serialized_type reval{};
    std::string copy = stream.str();
    reval.resize(copy.size());
    std::memcpy(reval.data(), copy.data(), copy.size());

    XI_SUCCEED(std::move(reval))
  }

  template <typename _ValueT>
  Xi::Result<_ValueT> valueDeserialize(serialized_type& data) {
    using namespace Xi;
    using namespace Xi::Serialization;
    _ValueT reval{};

    auto inputEc = Yaml::InputSerializer::parse(std::string{reinterpret_cast<const char*>(data.data()), data.size()});
    XI_ERROR_PROPAGATE(inputEc)
    auto input = inputEc.take();

    Serializer serializer{*input};
    XI_ERROR_PROPAGATE_CATCH(serialize(reval, Tag::Null, serializer))

    XI_SUCCEED(std::move(reval))
  }

  template <typename _ValueT>
  Xi::Result<_ValueT> serializeAndDeserialize(_ValueT& value) {
    auto data = valueSerialize(value);
    XI_ERROR_PROPAGATE(data)
    return valueDeserialize<_ValueT>(*data);
  }
};

#define XI_GENERIC_SERIALIZER_TEST(NAME)                  \
  template <typename _SerializerT>                        \
  void Xi_Serialization_Serializer_##NAME(_SerializerT&); \
  TEST_F(Xi_Serialization_Serializer_Binary, NAME) {      \
    Xi_Serialization_Serializer_##NAME(*this);            \
  }                                                       \
  TEST_F(Xi_Serialization_Serializer_Json, NAME) {        \
    Xi_Serialization_Serializer_##NAME(*this);            \
  }                                                       \
  TEST_F(Xi_Serialization_Serializer_Yaml, NAME) {        \
    Xi_Serialization_Serializer_##NAME(*this);            \
  }                                                       \
  template <typename _SerializerT>                        \
  void Xi_Serialization_Serializer_##NAME(_SerializerT& _this)
