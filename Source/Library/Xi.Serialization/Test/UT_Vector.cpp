// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include <vector>
#include <cinttypes>
#include <limits>

#include <Xi/Serialization/Vector.hpp>

#include "GenericSerializerTest.hpp"

XI_GENERIC_SERIALIZER_TEST(Vector) {
  using namespace Xi::Serialization;
  std::vector<std::int64_t> values{
      {0, std::numeric_limits<std::int64_t>::min(), std::numeric_limits<std::int64_t>::max()}};
  auto _values = _this.serializeAndDeserialize(values);
  ASSERT_TRUE(isSuccess(_values));
  EXPECT_EQ(values, *_values);
}

XI_GENERIC_SERIALIZER_TEST(EmptyVector) {
  using namespace Xi::Serialization;
  std::vector<std::string> values{};
  auto _values = _this.serializeAndDeserialize(values);
  ASSERT_TRUE(isSuccess(_values));
  EXPECT_EQ(values, *_values);
}
