// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include <cinttypes>
#include <string>
#include <variant>

#include <Xi/Serialization/Variant.hpp>
#include <Xi/Serialization/Serialization.hpp>

#include "GenericSerializerTest.hpp"

namespace Xi_Serialization_Serializer {
XI_SERIALIZATION_VARIANT_INVARIANT(TestVariant, int64_t, std::string)

struct VariantStorage {
  TestVariant value;

  XI_SERIALIZATION_COMPLEX_BEGIN()
  XI_SERIALIZATION_MEMBER(value, 0x0001, "value")
  XI_SERIALIZATION_COMPLEX_END
};
}  // namespace Xi_Serialization_Serializer

XI_SERIALIZATION_VARIANT_TAG(Xi_Serialization_Serializer::TestVariant, 0, 0x0001, "integer")
XI_SERIALIZATION_VARIANT_TAG(Xi_Serialization_Serializer::TestVariant, 1, 0x0002, "string")

XI_GENERIC_SERIALIZER_TEST(Variant) {
  using namespace Xi_Serialization_Serializer;
  using namespace Xi::Serialization;
  VariantStorage maybe{};
  maybe.value.emplace<std::string>("^._.^");
  auto _maybe = _this.serializeAndDeserialize(maybe);
  ASSERT_TRUE(isSuccess(_maybe));
  EXPECT_EQ(_maybe->value, maybe.value);
}
