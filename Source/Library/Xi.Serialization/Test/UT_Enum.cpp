// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include <array>
#include <cinttypes>
#include <limits>

#include <Xi/Serialization/Enum.hpp>
#include <Xi/Serialization/Serialization.hpp>

#include "GenericSerializerTest.hpp"

namespace Xi_Serialization_Serializer {
enum struct AnyEnum {
  First = 1,
  Second = 2,
};
XI_SERIALIZATION_ENUM(AnyEnum)
struct EnumStorage {
  AnyEnum enum_;

  XI_SERIALIZATION_COMPLEX_BEGIN()
  XI_SERIALIZATION_MEMBER(enum_, 0x0001, "enum")
  XI_SERIALIZATION_COMPLEX_END
};
}  // namespace Xi_Serialization_Serializer
XI_SERIALIZATION_ENUM_RANGE(Xi_Serialization_Serializer::AnyEnum, First, Second)
XI_SERIALIZATION_ENUM_TAG(Xi_Serialization_Serializer::AnyEnum, First, "first")
XI_SERIALIZATION_ENUM_TAG(Xi_Serialization_Serializer::AnyEnum, Second, "second")

XI_GENERIC_SERIALIZER_TEST(Enum) {
  using namespace Xi_Serialization_Serializer;
  using namespace Xi::Serialization;
  EnumStorage storage{AnyEnum::Second};
  auto _storage = _this.serializeAndDeserialize(storage);
  ASSERT_TRUE(isSuccess(_storage));
  EXPECT_EQ(_storage->enum_, storage.enum_);
}
