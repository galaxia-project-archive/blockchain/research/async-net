// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include <array>
#include <cinttypes>
#include <limits>

#include <Xi/Serialization/Array.hpp>

#include "GenericSerializerTest.hpp"

XI_GENERIC_SERIALIZER_TEST(Array) {
  using namespace Xi::Serialization;
  std::array<std::uint64_t, 3> values{{0, 42, std::numeric_limits<std::uint64_t>::max()}};
  auto _values = _this.serializeAndDeserialize(values);
  ASSERT_TRUE(isSuccess(_values));
  EXPECT_EQ(values, *_values);
}

XI_GENERIC_SERIALIZER_TEST(EmptyArray) {
  using namespace Xi::Serialization;
  std::array<std::string, 0> values{};
  auto _values = _this.serializeAndDeserialize(values);
  ASSERT_TRUE(isSuccess(_values));
  EXPECT_EQ(values, *_values);
}
