// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Serialization/Enum.hpp"

XI_ERROR_CODE_CATEGORY_BEGIN(Xi::Serialization, Enum)
XI_ERROR_CODE_DESC(InvalidTag, "enum type tag is invalid")
XI_ERROR_CODE_DESC(UnknownEnum, "enum value is unknown")
XI_ERROR_CODE_DESC(MissingTag, "enum type tag is missing")
XI_ERROR_CODE_CATEGORY_END()
