// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Serialization/Map.hpp"

XI_ERROR_CODE_CATEGORY_BEGIN(Xi::Serialization, Map)
XI_ERROR_CODE_DESC(DuplicateEntry, "map contains duplicate entries")
XI_ERROR_CODE_CATEGORY_END()

namespace Xi {
namespace Serialization {
const Tag Impl::__MapKeyTag{0x0001, "key"};
const Tag Impl::__MapValueTag{0x0002, "value"};
}  // namespace Serialization
}  // namespace Xi
