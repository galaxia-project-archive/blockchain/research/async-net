// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Serialization/Tag.hpp"

#include <utility>
#include <limits>
#include <cinttypes>

const Xi::Serialization::Tag Xi::Serialization::Tag::Null{NoBinaryTag, NoTextTag};

const Xi::Serialization::Tag::binary_type Xi::Serialization::Tag::NoBinaryTag{
    std::numeric_limits<Xi::Serialization::Tag::binary_type>::min()};

const Xi::Serialization::Tag::text_type Xi::Serialization::Tag::NoTextTag{""};

Xi::Serialization::Tag::Tag(binary_type binary, text_type text_) : m_binary{binary}, m_text{std::move(text_)} {
  /* */
}

Xi::Serialization::Tag::binary_type Xi::Serialization::Tag::binary() const {
  return m_binary;
}

const Xi::Serialization::Tag::text_type &Xi::Serialization::Tag::text() const {
  return m_text;
}

bool Xi::Serialization::Tag::isNull() const {
  return binary() == NoBinaryTag && text() == NoTextTag;
}

bool Xi::Serialization::Tag::operator==(const Xi::Serialization::Tag &rhs) const {
  if (binary() != NoBinaryTag && binary() == rhs.binary()) {
    return true;
  } else if (text() != NoTextTag && text() == rhs.text()) {
    return true;
  } else {
    return false;
  }
}

bool Xi::Serialization::Tag::operator!=(const Xi::Serialization::Tag &rhs) const {
  return !(*this == rhs);
}
