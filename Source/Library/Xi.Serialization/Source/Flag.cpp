// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Serialization/Flag.hpp"

XI_ERROR_CODE_CATEGORY_BEGIN(Xi::Serialization, Flag)
XI_ERROR_CODE_DESC(InvalidTag, "flag type tag is invalid")
XI_ERROR_CODE_DESC(UnknownFlag, "flag value is unknown")
XI_ERROR_CODE_DESC(MissingTag, "flag type tag is missing")
XI_ERROR_CODE_CATEGORY_END()
