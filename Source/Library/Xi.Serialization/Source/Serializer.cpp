// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Serialization/Serializer.hpp"

#include <cstring>

namespace Xi {
namespace Serialization {

namespace {
class ISerializerConcept {
 public:
  virtual ~ISerializerConcept() = default;

  [[nodiscard]] virtual Serializer::Mode mode() const = 0;
  [[nodiscard]] virtual Format format() const = 0;

  [[nodiscard]] virtual Result<void> primitive(std::int8_t& value, const Tag& nameTag) = 0;
  [[nodiscard]] virtual Result<void> primitive(std::uint8_t& value, const Tag& nameTag) = 0;
  [[nodiscard]] virtual Result<void> primitive(std::int16_t& value, const Tag& nameTag) = 0;
  [[nodiscard]] virtual Result<void> primitive(std::uint16_t& value, const Tag& nameTag) = 0;
  [[nodiscard]] virtual Result<void> primitive(std::int32_t& value, const Tag& nameTag) = 0;
  [[nodiscard]] virtual Result<void> primitive(std::uint32_t& value, const Tag& nameTag) = 0;
  [[nodiscard]] virtual Result<void> primitive(std::int64_t& value, const Tag& nameTag) = 0;
  [[nodiscard]] virtual Result<void> primitive(std::uint64_t& value, const Tag& nameTag) = 0;

  [[nodiscard]] virtual Result<void> primitive(bool& value, const Tag& nameTag) = 0;

  [[nodiscard]] virtual Result<void> primitive(float& value, const Tag& nameTag) = 0;
  [[nodiscard]] virtual Result<void> primitive(double& value, const Tag& nameTag) = 0;

  [[nodiscard]] virtual Result<void> string(std::string& value, const Tag& nameTag) = 0;
  [[nodiscard]] virtual Result<void> binary(Xi::ByteVector& value, const Tag& nameTag) = 0;

  [[nodiscard]] virtual Result<void> blob(Xi::ByteSpan value, const Tag& nameTag) = 0;

  [[nodiscard]] virtual Result<void> beginComplex(const Tag& nameTag) = 0;
  [[nodiscard]] virtual Result<void> endComplex() = 0;

  [[nodiscard]] virtual Result<void> maybe(bool& value, const Tag& nameTag) = 0;
  [[nodiscard]] virtual Result<void> typeTag(Tag& value, const Tag& nameTag) = 0;
  [[nodiscard]] virtual Result<void> flag(TagVector& tag, const Tag& nameTag) = 0;

  [[nodiscard]] virtual Result<void> beginVector(size_t& count, const Tag& nameTag) = 0;
  [[nodiscard]] virtual Result<void> endVector() = 0;

  [[nodiscard]] virtual Result<void> beginArray(size_t count, const Tag& nameTag) = 0;
  [[nodiscard]] virtual Result<void> endArray() = 0;
};

class InputSerializerConcept final : public ISerializerConcept {
  InputSerializer& input;

 public:
  InputSerializerConcept(InputSerializer& in) : input{in} {
  }
  ~InputSerializerConcept() override = default;

  [[nodiscard]] Serializer::Mode mode() const override {
    return Serializer::Mode::Input;
  }

  [[nodiscard]] Format format() const override {
    return input.format();
  }

  [[nodiscard]] Result<void> primitive(std::int8_t& value, const Tag& nameTag) override {
    return input.readInt8(value, nameTag);
  }
  [[nodiscard]] Result<void> primitive(std::uint8_t& value, const Tag& nameTag) override {
    return input.readUInt8(value, nameTag);
  }
  [[nodiscard]] Result<void> primitive(std::int16_t& value, const Tag& nameTag) override {
    return input.readInt16(value, nameTag);
  }
  [[nodiscard]] Result<void> primitive(std::uint16_t& value, const Tag& nameTag) override {
    return input.readUInt16(value, nameTag);
  }
  [[nodiscard]] Result<void> primitive(std::int32_t& value, const Tag& nameTag) override {
    return input.readInt32(value, nameTag);
  }
  [[nodiscard]] Result<void> primitive(std::uint32_t& value, const Tag& nameTag) override {
    return input.readUInt32(value, nameTag);
  }
  [[nodiscard]] Result<void> primitive(std::int64_t& value, const Tag& nameTag) override {
    return input.readInt64(value, nameTag);
  }
  [[nodiscard]] Result<void> primitive(std::uint64_t& value, const Tag& nameTag) override {
    return input.readUInt64(value, nameTag);
  }

  [[nodiscard]] Result<void> primitive(bool& value, const Tag& nameTag) override {
    return input.readBoolean(value, nameTag);
  }

  [[nodiscard]] Result<void> primitive(float& value, const Tag& nameTag) override {
    return input.readFloat(value, nameTag);
  }
  [[nodiscard]] Result<void> primitive(double& value, const Tag& nameTag) override {
    return input.readDouble(value, nameTag);
  }

  [[nodiscard]] Result<void> string(std::string& value, const Tag& nameTag) override {
    return input.readString(value, nameTag);
  }
  [[nodiscard]] Result<void> binary(Xi::ByteVector& value, const Tag& nameTag) override {
    return input.readBinary(value, nameTag);
  }

  [[nodiscard]] Result<void> blob(Xi::ByteSpan value, const Tag& nameTag) override {
    return input.readBlob(value, nameTag);
  }

  [[nodiscard]] Result<void> maybe(bool& value, const Tag& nameTag) override {
    return input.checkValue(value, nameTag);
  }
  [[nodiscard]] Result<void> typeTag(Tag& value, const Tag& nameTag) override {
    return input.readTag(value, nameTag);
  }
  [[nodiscard]] Result<void> flag(TagVector& tag, const Tag& nameTag) override {
    return input.readFlag(tag, nameTag);
  }

  [[nodiscard]] Result<void> beginComplex(const Tag& nameTag) override {
    return input.beginReadComplex(nameTag);
  }
  [[nodiscard]] Result<void> endComplex() override {
    return input.endReadComplex();
  }

  [[nodiscard]] Result<void> beginVector(size_t& count, const Tag& nameTag) override {
    return input.beginReadVector(count, nameTag);
  }
  [[nodiscard]] Result<void> endVector() override {
    return input.endReadVector();
  }

  [[nodiscard]] Result<void> beginArray(size_t count, const Tag& nameTag) override {
    return input.beginReadArray(count, nameTag);
  }
  [[nodiscard]] Result<void> endArray() override {
    return input.endReadArray();
  }
};

class OutputSerializerConcept final : public ISerializerConcept {
  OutputSerializer& output;

 public:
  OutputSerializerConcept(OutputSerializer& out) : output{out} {
    /* */
  }
  ~OutputSerializerConcept() override = default;

  [[nodiscard]] Serializer::Mode mode() const override {
    return Serializer::Mode::Output;
  }

  [[nodiscard]] Format format() const override {
    return output.format();
  }

  [[nodiscard]] Result<void> primitive(std::int8_t& value, const Tag& nameTag) override {
    return output.writeInt8(value, nameTag);
  }
  [[nodiscard]] Result<void> primitive(std::uint8_t& value, const Tag& nameTag) override {
    return output.writeUInt8(value, nameTag);
  }
  [[nodiscard]] Result<void> primitive(std::int16_t& value, const Tag& nameTag) override {
    return output.writeInt16(value, nameTag);
  }
  [[nodiscard]] Result<void> primitive(std::uint16_t& value, const Tag& nameTag) override {
    return output.writeUInt16(value, nameTag);
  }
  [[nodiscard]] Result<void> primitive(std::int32_t& value, const Tag& nameTag) override {
    return output.writeInt32(value, nameTag);
  }
  [[nodiscard]] Result<void> primitive(std::uint32_t& value, const Tag& nameTag) override {
    return output.writeUInt32(value, nameTag);
  }
  [[nodiscard]] Result<void> primitive(std::int64_t& value, const Tag& nameTag) override {
    return output.writeInt64(value, nameTag);
  }
  [[nodiscard]] Result<void> primitive(std::uint64_t& value, const Tag& nameTag) override {
    return output.writeUInt64(value, nameTag);
  }

  [[nodiscard]] Result<void> primitive(bool& value, const Tag& nameTag) override {
    return output.writeBoolean(value, nameTag);
  }

  [[nodiscard]] Result<void> primitive(float& value, const Tag& nameTag) override {
    return output.writeFloat(value, nameTag);
  }
  [[nodiscard]] Result<void> primitive(double& value, const Tag& nameTag) override {
    return output.writeDouble(value, nameTag);
  }

  [[nodiscard]] Result<void> string(std::string& value, const Tag& nameTag) override {
    return output.writeString(value, nameTag);
  }
  [[nodiscard]] Result<void> binary(Xi::ByteVector& value, const Tag& nameTag) override {
    return output.writeBinary(value, nameTag);
  }

  [[nodiscard]] Result<void> blob(Xi::ByteSpan value, const Tag& nameTag) override {
    return output.writeBlob(value, nameTag);
  }

  [[nodiscard]] Result<void> maybe(bool& value, const Tag& nameTag) override {
    if (value) {
      return output.writeNotNull(nameTag);
    } else {
      return output.writeNull(nameTag);
    }
  }

  [[nodiscard]] Result<void> typeTag(Tag& value, const Tag& nameTag) override {
    return output.writeTag(value, nameTag);
  }

  [[nodiscard]] Result<void> flag(TagVector& tag, const Tag& nameTag) override {
    return output.writeFlag(tag, nameTag);
  }

  [[nodiscard]] Result<void> beginComplex(const Tag& nameTag) override {
    return output.beginWriteComplex(nameTag);
  }
  [[nodiscard]] Result<void> endComplex() override {
    return output.endWriteComplex();
  }

  [[nodiscard]] Result<void> beginVector(size_t& count, const Tag& nameTag) override {
    return output.beginWriteVector(count, nameTag);
  }
  [[nodiscard]] Result<void> endVector() override {
    return output.endWriteVector();
  }

  [[nodiscard]] Result<void> beginArray(size_t count, const Tag& nameTag) override {
    return output.beginWriteArray(count, nameTag);
  }
  [[nodiscard]] Result<void> endArray() override {
    return output.endWriteArray();
  }
};
}  // namespace

struct Serializer::_Impl {
  std::unique_ptr<ISerializerConcept> inner;

  explicit _Impl(InputSerializer& input) : inner{new InputSerializerConcept{input}} {
    /* */
  }
  explicit _Impl(OutputSerializer& output) : inner{new OutputSerializerConcept{output}} {
    /* */
  }
};

Serializer::Serializer(InputSerializer& ser) : m_impl{new _Impl{ser}} {
}

Serializer::Serializer(OutputSerializer& ser) : m_impl{new _Impl{ser}} {
}

Serializer::~Serializer() {
}

Serializer::Mode Serializer::mode() const {
  return m_impl->inner->mode();
}

bool Serializer::isInputMode() const {
  return mode() == Mode::Input;
}

bool Serializer::isOutputMode() const {
  return mode() == Mode::Output;
}

Format Serializer::format() const {
  return m_impl->inner->format();
}

bool Serializer::isBinaryFormat() const {
  return format() == Format::Binary;
}

bool Serializer::isHumanReadableFromat() const {
  return format() == Format::HumanReadable;
}

Result<void> Serializer::primitive(int8_t& value, const Tag& nameTag) {
  return m_impl->inner->primitive(value, nameTag);
}

Result<void> Serializer::primitive(uint8_t& value, const Tag& nameTag) {
  return m_impl->inner->primitive(value, nameTag);
}

Result<void> Serializer::primitive(int16_t& value, const Tag& nameTag) {
  return m_impl->inner->primitive(value, nameTag);
}

Result<void> Serializer::primitive(uint16_t& value, const Tag& nameTag) {
  return m_impl->inner->primitive(value, nameTag);
}

Result<void> Serializer::primitive(int32_t& value, const Tag& nameTag) {
  return m_impl->inner->primitive(value, nameTag);
}

Result<void> Serializer::primitive(uint32_t& value, const Tag& nameTag) {
  return m_impl->inner->primitive(value, nameTag);
}

Result<void> Serializer::primitive(int64_t& value, const Tag& nameTag) {
  return m_impl->inner->primitive(value, nameTag);
}

Result<void> Serializer::primitive(uint64_t& value, const Tag& nameTag) {
  return m_impl->inner->primitive(value, nameTag);
}

Result<void> Serializer::primitive(bool& value, const Tag& nameTag) {
  return m_impl->inner->primitive(value, nameTag);
}

Result<void> Serializer::primitive(float& value, const Tag& nameTag) {
  return m_impl->inner->primitive(value, nameTag);
}

Result<void> Serializer::primitive(double& value, const Tag& nameTag) {
  return m_impl->inner->primitive(value, nameTag);
}

Result<void> Serializer::string(std::string& value, const Tag& nameTag) {
  return m_impl->inner->string(value, nameTag);
}

Result<void> Serializer::binary(Xi::ByteVector& value, const Tag& nameTag) {
  return m_impl->inner->binary(value, nameTag);
}

Result<void> Serializer::blob(ByteSpan value, const Tag& nameTag) {
  return m_impl->inner->blob(value, nameTag);
}

Result<void> Serializer::maybe(bool& value, const Tag& nameTag) {
  return m_impl->inner->maybe(value, nameTag);
}

Result<void> Serializer::typeTag(Tag& tag, const Tag& nameTag) {
  return m_impl->inner->typeTag(tag, nameTag);
}

Result<void> Serializer::flag(TagVector& tag, const Tag& nameTag) {
  return m_impl->inner->flag(tag, nameTag);
}

Result<void> Serializer::beginComplex(const Tag& nameTag) {
  return m_impl->inner->beginComplex(nameTag);
}

Result<void> Serializer::endComplex() {
  return m_impl->inner->endComplex();
}

Result<void> Serializer::beginArray(size_t count, const Tag& nameTag) {
  return m_impl->inner->beginArray(count, nameTag);
}

Result<void> Serializer::endArray() {
  return m_impl->inner->endArray();
}

Result<void> Serializer::beginVector(size_t& count, const Tag& nameTag) {
  return m_impl->inner->beginVector(count, nameTag);
}

Result<void> Serializer::endVector() {
  return m_impl->inner->endArray();
}

}  // namespace Serialization
}  // namespace Xi
