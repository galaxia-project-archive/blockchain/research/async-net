// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Serialization/Variant.hpp"

XI_ERROR_CODE_CATEGORY_BEGIN(Xi::Serialization, Variant)
XI_ERROR_CODE_DESC(UnknownVariant, "variant index is unknown")
XI_ERROR_CODE_DESC(InvalidTag, "type tag is invalid for this variant")
XI_ERROR_CODE_CATEGORY_END()
