// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Serialization/Null.hpp"

namespace Xi {
namespace Serialization {

Result<void> serialize(Null&, const Tag&, Serializer&) {
  XI_SUCCEED()
}

}  // namespace Serialization
}  // namespace Xi
