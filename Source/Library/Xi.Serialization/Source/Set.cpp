// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Serialization/Set.hpp"

XI_ERROR_CODE_CATEGORY_BEGIN(Xi::Serialization, Set)
XI_ERROR_CODE_DESC(Duplicate, "set contains a duplicate value")
XI_ERROR_CODE_CATEGORY_END()
