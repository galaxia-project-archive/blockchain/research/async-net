// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <vector>
#include <initializer_list>

#include "Xi/Log/ILogger.hpp"

namespace Xi {
namespace Log {

class Compound final : public ILogger {
 public:
  explicit Compound();
  explicit Compound(std::initializer_list<SharedILogger> logger);
  virtual ~Compound() override = default;

  bool isFiltered(const Level level) const override;
  void print(Context context, std::string_view message) override;

 private:
  std::vector<SharedILogger> m_logger;
};

}  // namespace Log
}  // namespace Xi
