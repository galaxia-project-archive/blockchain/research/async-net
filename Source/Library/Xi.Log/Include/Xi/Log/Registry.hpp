﻿// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <map>

#include <Xi/Global.hh>
#include <Xi/Async/Mutex.hpp>

#include "Xi/Log/Category.hpp"
#include "Xi/Log/Handle.hpp"

namespace Xi {
namespace Log {

class Registry final {
 private:
  Registry();
  friend Registry &registry();

 public:
  ~Registry();

  static Handle get(std::string_view category);
  static void put(SharedILogger logger);

 private:
  Handle doGet(std::string_view category);
  void doPut(SharedILogger logger);

 private:
  SharedCategory getCategory(const std::string &category, std::string_view path);

 private:
  XI_DECLARE_SMART_POINTER_CLASS(LoggerWrapper)

  std::map<std::string, SharedCategory> m_categories;
  SharedLoggerWrapper m_logger;
  Async::Mutex m_guard;
};

Registry &registry();

}  // namespace Log
}  // namespace Xi
