// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <Xi/Extern/Push.hh>
#include <fmt/format.h>
#include <Xi/Extern/Pop.hh>

#include <Xi/Global.hh>
#include <Xi/String.hpp>

#include "Xi/Log/ILogger.hpp"
#include "Xi/Log/Category.hpp"
#include "Xi/Log/Context.hpp"

namespace Xi {
namespace Log {

class Handle final {
 private:
  void print(const Level level, std::string_view message);

 public:
  Handle(WeakCategory category, WeakILogger logger);
  XI_DEFAULT_COPY(Handle)
  ~Handle() = default;

  bool isFiltered(const Level level) const;

  void log(const Level level, std::string_view message);
  void log(const Level level, std::string_view message, fmt::format_args args);

  template <Level _LevelV, typename... _ArgsT>
  void log(std::string_view message, _ArgsT&&... args) {
    if constexpr (sizeof...(_ArgsT) == 0) {
      log(_LevelV, message);
    } else {
      this->log(_LevelV, message, fmt::make_format_args(std::forward<_ArgsT>(args)...));
    }
  }

#define XI_LOG_FORWARD(LEVEL, NAME)                                             \
  template <typename... _ArgsT>                                                 \
  void NAME(std::string_view message, _ArgsT&&... args) {                       \
    this->log<Level::LEVEL, _ArgsT...>(message, std::forward<_ArgsT>(args)...); \
  }

  XI_LOG_FORWARD(Fatal, fatal)
  XI_LOG_FORWARD(Error, error)
  XI_LOG_FORWARD(Warn, warn)
  XI_LOG_FORWARD(Info, info)
  XI_LOG_FORWARD(Debug, debug)
  XI_LOG_FORWARD(Trace, trace)

 private:
  WeakCategory m_category;
  WeakILogger m_logger;
};

}  // namespace Log
}  // namespace Xi
