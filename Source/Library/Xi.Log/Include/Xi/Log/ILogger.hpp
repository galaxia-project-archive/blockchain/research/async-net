// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <Xi/Global.hh>

#include "Xi/Log/Level.hpp"
#include "Xi/Log/Context.hpp"

namespace Xi {
namespace Log {

class ILogger {
 public:
  virtual ~ILogger() = default;

  virtual bool isFiltered(const Level level) const = 0;
  virtual void print(Context context, std::string_view message) = 0;
};

XI_DECLARE_SMART_POINTER(ILogger)

}  // namespace Log
}  // namespace Xi
