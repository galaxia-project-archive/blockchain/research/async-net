// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <chrono>
#include <thread>

#include <Xi/Global.hh>

#include "Xi/Log/Level.hpp"
#include "Xi/Log/Category.hpp"

namespace Xi {
namespace Log {

class Context final {
 public:
  using clock_type = std::chrono::system_clock;
  using time_point_type = clock_type::time_point;
  using thread_id_type = std::thread::id;

 public:
  static Context current(const Level level, WeakCategory category = WeakCategory{});

 private:
  Context();

 public:
  XI_DEFAULT_COPY(Context)
  XI_DEFAULT_MOVE(Context)
  ~Context() = default;

  time_point_type timestamp() const;
  thread_id_type thread() const;
  Level level();
  WeakCategory category() const;

 private:
  time_point_type m_timestamp;
  thread_id_type m_thread;
  Level m_level;
  WeakCategory m_category;
};

}  // namespace Log
}  // namespace Xi
