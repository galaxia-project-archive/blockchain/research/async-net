// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <Xi/Global.hh>

#include "Xi/Log/Wrapper.hpp"

namespace Xi {
namespace Log {

class ConsoleLogger final : public Wrapper {
 public:
  ~ConsoleLogger() = default;

 private:
  using Wrapper::Wrapper;
  friend class ConsoleLoggerBuilder;
};

class ConsoleLoggerBuilder final {
 public:
  ConsoleLoggerBuilder() = default;
  XI_DELETE_COPY(ConsoleLoggerBuilder)
  XI_DELETE_MOVE(ConsoleLoggerBuilder)
  ~ConsoleLoggerBuilder() = default;

  ConsoleLoggerBuilder& withColoring();
  ConsoleLoggerBuilder& withoutColoring();
  ConsoleLoggerBuilder& withErrorStream();
  ConsoleLoggerBuilder& withStandardStream();

  SharedILogger build();

 private:
  bool m_colored = false;
  bool m_errorStream = false;
};

}  // namespace Log
}  // namespace Xi
