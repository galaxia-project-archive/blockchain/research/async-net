// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <atomic>

#include "Xi/Log/Wrapper.hpp"

namespace Xi {
namespace Log {

class Filter : public Wrapper {
 public:
  static const Level DefaultLevel;

 public:
  explicit Filter(SharedILogger inner);
  explicit Filter(SharedILogger inner, Level minLevel);
  virtual ~Filter() override = default;

  bool isFiltered(const Level level) const override;

  Level level() const;
  void setLevel(Level level);

 private:
  std::atomic<Level> m_level;
};

}  // namespace Log
}  // namespace Xi
