// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Log/Handle.hpp"

#include <cstdio>

Xi::Log::Handle::Handle(WeakCategory category, Xi::Log::WeakILogger logger) : m_category{category}, m_logger{logger} {
}

void Xi::Log::Handle::log(const Xi::Log::Level level, std::string_view message) {
  if (!isFiltered(level)) {
    print(level, message);
  }
}

void Xi::Log::Handle::log(const Xi::Log::Level level, std::string_view message, fmt::format_args args) {
  if (!isFiltered(level)) {
    try {
      std::string _message{message};
      print(level, fmt::vformat(_message, args));
    } catch (const std::exception& e) {
      fprintf(stderr, "Logger print threw: %s", e.what());
      fflush(stderr);
    } catch (...) {
      fprintf(stderr, "Logger print threw: UNKNOWN");
      fflush(stderr);
    }
  }
}

bool Xi::Log::Handle::isFiltered(const Xi::Log::Level level) const {
  if (auto locked = m_logger.lock()) {
    return locked->isFiltered(level);
  } else {
    return true;
  }
}

void Xi::Log::Handle::print(const Level level, std::string_view message) {
  if (auto locked = m_logger.lock()) {
    return locked->print(Context::current(level, m_category), message);
  }
}
