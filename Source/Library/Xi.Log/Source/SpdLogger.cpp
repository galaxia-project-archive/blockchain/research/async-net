// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "SpdLogger.hpp"

#include <Xi/Extern/Push.hh>
#include <spdlog/sinks/sink.h>
#include <Xi/Extern/Pop.hh>

#include <Xi/ErrorModel.hh>

namespace Xi {
namespace Log {

SpdLogger::SpdLogger(spdlog::sink_ptr sink) : m_sink{sink} {
  XI_EXCEPTIONAL_IF_NOT(NullArgumentError, sink)
  sink->set_pattern("[%Y-%m-%d %T.%e] %^[%8l]%$ <%n> %v");
}

bool SpdLogger::isFiltered(const Level level) const {
  XI_UNUSED(level)
  return false;
}

void SpdLogger::print(Context context, std::string_view message) {
  const std::string* categoryName = nullptr;
  const auto category = context.category().lock();
  if (category) {
    categoryName = std::addressof(category->name());
  }

  try {
    m_sink->log(spdlog::details::log_msg(categoryName, toSpdLog(context.level()), message));
  } catch (...) {
    /* swallow */
  }
}

spdlog::level::level_enum toSpdLog(const Level level) {
  using spdlog::level::level_enum;

  switch (level) {
    case Level::None:
      return level_enum::off;
    case Level::Fatal:
      return level_enum::critical;
    case Level::Error:
      return level_enum::err;
    case Level::Warn:
      return level_enum::warn;
    case Level::Info:
      return level_enum::info;
    case Level::Debug:
      return level_enum::debug;
    case Level::Trace:
      return level_enum::trace;
  }
  XI_EXCEPTIONAL(InvalidEnumValueError)
}

}  // namespace Log
}  // namespace Xi
