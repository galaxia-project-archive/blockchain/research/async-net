// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <Xi/Global.hh>

#include <Xi/Extern/Push.hh>
#include <spdlog/spdlog.h>
#include <Xi/Extern/Pop.hh>

#include "Xi/Log/ILogger.hpp"

namespace Xi {
namespace Log {

class SpdLogger final : public ILogger {
 public:
  explicit SpdLogger(spdlog::sink_ptr sink);

  bool isFiltered(const Level level) const override;
  void print(Context context, std::string_view message) override;

 private:
  spdlog::sink_ptr m_sink;
};

spdlog::level::level_enum toSpdLog(const Level level);

}  // namespace Log
}  // namespace Xi
