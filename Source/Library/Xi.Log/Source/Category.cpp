// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Log/Category.hpp"

#include "Xi/Log/Registry.hpp"

Xi::Log::Category::Category(std::string_view name, std::string_view fullName)
    : m_name{name}, m_fullName{fullName} {
}

const std::string &Xi::Log::Category::name() const {
  return m_name;
}

const std::string &Xi::Log::Category::fullName() const {
  return m_fullName;
}

Xi::Log::WeakCategory Xi::Log::Category::parent() const {
  return m_parent;
}

const std::vector<Xi::Log::WeakCategory> &Xi::Log::Category::children() const {
  return m_children;
}
