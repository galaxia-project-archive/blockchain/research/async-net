// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Log/Context.hpp"

Xi::Log::Context::Context()
    : m_timestamp{clock_type::now()},
      m_thread{std::this_thread::get_id()},
      m_level{Level::None},
      m_category{} {
}

Xi::Log::Context Xi::Log::Context::current(const Xi::Log::Level level,
                                           Xi::Log::WeakCategory category) {
  Context context{};
  context.m_level = level;
  context.m_category = category;
  return context;
}

Xi::Log::Context::time_point_type Xi::Log::Context::timestamp() const {
  return m_timestamp;
}

Xi::Log::Context::thread_id_type Xi::Log::Context::thread() const {
  return m_thread;
}

Xi::Log::Level Xi::Log::Context::level() {
  return m_level;
}

Xi::Log::WeakCategory Xi::Log::Context::category() const {
  return m_category;
}
