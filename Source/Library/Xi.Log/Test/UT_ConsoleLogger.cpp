// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include <Xi/Extern/Push.hh>
#include <gmock/gmock.h>
#include <Xi/Extern/Pop.hh>

#include <iostream>

#include <Xi/Log/ConsoleLogger.hpp>
#include <Xi/Log/Log.hpp>
#include <Xi/Log/Registry.hpp>

#define XI_TEST_SUITE Xi_Log_ConsoleLogger

TEST(XI_TEST_SUITE, StdOutSink) {
  using namespace ::testing;
  using namespace Xi::Log;

  Registry::put(ConsoleLoggerBuilder{}.withColoring().withStandardStream().build());
  auto handle = Registry::get("StdOutSink");
  handle.debug("The answer is {}. Just took {:.2f} seconds to find out.", 42, 1.23);
}

XI_LOGGER("Xi/Log/Test")

TEST(XI_TEST_SUITE, HandleLog) {
  XI_LOG_INFO("Some cats may meow! ^-.-^");
}

TEST(XI_TEST_SUITE, Unicode) {
  XI_LOG_INFO("Testing unicode -- English -- Ελληνικά -- Español param = {}", "Русский");
}
