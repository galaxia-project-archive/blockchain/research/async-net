// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include "Xi/Memory/Blob.hpp"
#include "Xi/Memory/Clear.hh"

namespace Xi {
namespace Memory {

template <typename _StructT, size_t _SizeV>
struct EnableSecureBlobFromThis : Memory::EnableBlobFromThis<_StructT, _SizeV> {
  using EnableBlobFromThis<_StructT, _SizeV>::EnableBlobFromThis;

  void clear() {
    ::Xi::Memory::secureClear(this->mutableSpan());
  }

  ~EnableSecureBlobFromThis() {
    this->clear();
  }
};

}  // namespace Memory
}  // namespace Xi
