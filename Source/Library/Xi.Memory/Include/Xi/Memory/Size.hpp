// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <cinttypes>
#include <type_traits>

#include <Xi/Algorithm/Math.hh>
#include <Xi/TypeSafe/Integral.hpp>

namespace Xi {
namespace Memory {

template <std::uintmax_t _UnitPrefix>
struct Size : TypeSafe::EnableIntegralFromThis<std::uintmax_t, Size<_UnitPrefix>>,
              TypeSafe::EnableIntegralAdditionFromThis<Size<_UnitPrefix>>,
              TypeSafe::EnableIntegralSubtractionFromThis<Size<_UnitPrefix>> {
  static inline constexpr auto unitPrefix() {
    return _UnitPrefix;
  }

  static_assert(isPowerOf2(unitPrefix()), "Unit prefix of memory size must be a multiple of 2 (or 1)");

  using TypeSafe::EnableIntegralFromThis<std::uintmax_t, Size<_UnitPrefix>>::EnableIntegralFromThis;

  template <std::uintmax_t _OtherUnitPrefix>
  operator Size<_OtherUnitPrefix>() const {
    static_assert(_OtherUnitPrefix <= unitPrefix(), "Unit prefix conversion reduces accuracy, use size_cast.");
    return Size<_OtherUnitPrefix>{this->native() * (_OtherUnitPrefix / unitPrefix())};
  }
};

template <typename _OtherSizeT, typename _ThisSizeT>
_OtherSizeT size_cast(_ThisSizeT toCast) {
  return _OtherSizeT{(toCast.native() * _ThisSizeT::unitPrefix()) / _OtherSizeT::unitPrefix()};
}

using Bytes = Size<1>;
using KiloBytes = Size<1024>;
using MegaBytes = Size<1048576>;
using GigaBytes = Size<1073741824>;
using TeraBytes = Size<1099511627776>;
using PetaBytes = Size<1125899906842624>;

}  // namespace Memory
}  // namespace Xi
