// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <cstring>

#include "Xi/Span.hpp"
#include "Xi/Byte.hh"

namespace Xi {
namespace Memory {

template <typename _T, size_t _Bytes>
struct EnableBlobFromThis : protected ByteArray<_Bytes> {
  using value_type = std::remove_cv_t<_T>;
  using array_type = ByteArray<_Bytes>;

  static constexpr bool is_const_v = std::is_const_v<_T>;
  static constexpr bool is_mutable_v = !is_const_v;

  static inline constexpr size_t bytes() {
    return _Bytes;
  }

  EnableBlobFromThis() {
    this->fill(0);
  }
  explicit EnableBlobFromThis(array_type raw) : array_type(std::move(raw)) {
  }

  const Byte *data() const {
    return this->ByteArray<_Bytes>::data();
  }
  std::conditional_t<is_const_v, const Byte *, Byte *> data() {
    return this->ByteArray<_Bytes>::data();
  }

  using array_type::fill;
  using array_type::size;
  using array_type::operator[];

  std::conditional_t<is_const_v, ConstByteSpan, ByteSpan> span() {
    return std::conditional_t<is_const_v, ConstByteSpan, ByteSpan>{this->data(), this->size()};
  }
  ConstByteSpan span() const {
    return ConstByteSpan{this->data(), this->size()};
  }

  // TODO Move me to other, secure blob need constants time comparison
  inline bool operator==(const value_type &rhs) {
    return ::std::memcmp(this->data(), rhs.data(), bytes()) == 0;
  }
  inline bool operator!=(const value_type &rhs) {
    return ::std::memcmp(this->data(), rhs.data(), bytes()) != 0;
  }

 protected:
  Byte *mutableData() {
    return this->ByteArray<_Bytes>::data();
  }
  ByteSpan mutableSpan() {
    return ByteSpan{this->mutableData(), this->size()};
  }
};

}  // namespace Memory
}  // namespace Xi
