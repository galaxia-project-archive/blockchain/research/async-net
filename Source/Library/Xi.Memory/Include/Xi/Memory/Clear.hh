// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <Xi/Byte.hh>

#if defined(__cplusplus)
extern "C" {
#endif

#include <stddef.h>

void xi_memory_secure_clear(xi_byte_t* data, size_t length);

#if defined(__cplusplus)
}
#endif

#if defined(__cplusplus)
namespace Xi {
namespace Memory {

void secureClear(ByteSpan data);

}
}  // namespace Xi
#endif
