// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <Xi/Global.hh>
#include <Xi/Byte.hh>

#if defined(__cplusplus)
extern "C" {
#endif

#include <stdlib.h>

bool xi_memory_secure_compare(const xi_byte_t* lhs, const xi_byte_t* rhs, size_t length);

#if defined(__cplusplus)
}
#endif

#if defined(__cplusplus)
namespace Xi {
namespace Memory {

[[nodiscard]] bool secureCompare(ConstByteSpan lhs, ConstByteSpan rhs);

}
}  // namespace Xi
#endif
