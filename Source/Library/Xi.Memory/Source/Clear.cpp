// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Memory/Clear.hh"

void Xi::Memory::secureClear(Xi::ByteSpan data) {
  xi_memory_secure_clear(data.data(), data.size_bytes());
}
