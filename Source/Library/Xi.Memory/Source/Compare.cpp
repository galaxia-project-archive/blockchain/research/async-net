// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Memory/Compare.hh"

#include <algorithm>

#include <Xi/Global.hh>
#include <Xi/ErrorModel.hh>

bool Xi::Memory::secureCompare(Xi::ConstByteSpan lhs, Xi::ConstByteSpan rhs) {
  XI_EXCEPTIONAL_IF_NOT(InvalidSizeError, lhs.size() == rhs.size());
  return xi_memory_secure_compare(lhs.data(), rhs.data(), lhs.size());
}
