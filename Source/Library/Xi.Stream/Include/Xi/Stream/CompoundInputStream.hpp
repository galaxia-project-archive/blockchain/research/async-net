// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <initializer_list>
#include <deque>

#include <Xi/Global.hh>

#include "Xi/Stream/InputStream.hpp"

namespace Xi {
namespace Stream {

class CompoundInputStream final : public InputStream {
 public:
  explicit CompoundInputStream(std::vector<UniqueInputStream> sources);
  XI_DELETE_COPY(CompoundInputStream);
  XI_DEFAULT_MOVE(CompoundInputStream);
  ~CompoundInputStream() override = default;

  [[nodiscard]] Result<size_t> read(ByteSpan buffer) override;
  [[nodiscard]] Result<bool> isEndOfStream() const override;
  [[nodiscard]] Result<size_t> tell() const override;
  [[nodiscard]] Result<Byte> peek() const override;
  [[nodiscard]] Result<Byte> take() override;

 private:
  Result<void> popEndOfStreamSources();

 private:
  std::deque<UniqueInputStream> m_sources;
  size_t m_pos;
};

XI_DECLARE_SMART_POINTER(CompoundInputStream)

}  // namespace Stream
}  // namespace Xi
