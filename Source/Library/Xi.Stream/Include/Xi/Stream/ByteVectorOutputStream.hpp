// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <Xi/Global.hh>

#include "Xi/Stream/OutputStream.hpp"

namespace Xi {
namespace Stream {
class ByteVectorOutputStream final : public OutputStream {
 public:
  explicit ByteVectorOutputStream(ByteVector &dest);
  XI_DELETE_COPY(ByteVectorOutputStream);
  XI_DEFAULT_MOVE(ByteVectorOutputStream);
  ~ByteVectorOutputStream() override = default;

  [[nodiscard]] Result<size_t> write(ConstByteSpan buffer) override;
  [[nodiscard]] Result<void> flush() override;

 private:
  ByteVector &m_dest;
};

XI_DECLARE_SMART_POINTER(ByteVectorOutputStream)
}  // namespace Stream
}  // namespace Xi
