// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <Xi/Global.hh>
#include <Xi/Byte.hh>

#include "Xi/Stream/OutputStream.hpp"

namespace Xi {
namespace Stream {
class ByteArrayOutputStream final : public OutputStream {
 public:
  explicit ByteArrayOutputStream(ByteSpan dest);
  XI_DELETE_COPY(ByteArrayOutputStream);
  XI_DEFAULT_MOVE(ByteArrayOutputStream);
  ~ByteArrayOutputStream() override = default;

  [[nodiscard]] Result<size_t> write(ConstByteSpan buffer) override;
  [[nodiscard]] Result<void> flush() override;

  ByteSpan written() const;

 private:
  ByteSpan m_dest;
  ByteSpan m_written;
};

XI_DECLARE_SMART_POINTER(ByteArrayOutputStream)
}  // namespace Stream
}  // namespace Xi
