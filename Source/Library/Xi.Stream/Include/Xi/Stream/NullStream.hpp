﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include "Xi/Stream/InputStream.hpp"
#include "Xi/Stream/OutputStream.hpp"

namespace Xi::Stream {

/*!
 *
 * \brief The NullStream class works as a proxy for streams.
 *
 * As an input stream it works like an end of file stream.
 * As an output stream it works like a bottle without a bottom, writing everything to void.
 *
 */
class NullStream final : public InputStream, public OutputStream {
 public:
  ~NullStream() override = default;

  // InputStream
  Result<size_t> read(ByteSpan buffer) override;
  Result<bool> isEndOfStream() const override;
  Result<size_t> tell() const override;
  Result<Byte> peek() const override;
  Result<Byte> take() override;

  // OutputStream
  Result<size_t> write(ConstByteSpan buffer) override;
  Result<void> flush() override;
};

std::unique_ptr<NullStream> makeNullStream();

}  // namespace Xi::Stream
