// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include "Xi/Stream/ByteArrayOutputStream.hpp"
#include "Xi/Stream/ByteVectorOutputStream.hpp"
#include "Xi/Stream/CompoundInputStream.hpp"
#include "Xi/Stream/CompoundOutputStream.hpp"
#include "Xi/Stream/InMemoryInputStream.hpp"
#include "Xi/Stream/InMemoryStreams.hpp"
#include "Xi/Stream/InputStream.hpp"
#include "Xi/Stream/IStream.hpp"
#include "Xi/Stream/OStream.hpp"
#include "Xi/Stream/OutputStream.hpp"
#include "Xi/Stream/StreamError.hpp"
#include "Xi/Stream/StringOutputStream.hpp"
