// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <deque>
#include <initializer_list>

#include <Xi/Global.hh>

#include "Xi/Stream/OutputStream.hpp"

namespace Xi {
namespace Stream {

class CompoundOutputStream final : OutputStream {
 public:
  explicit CompoundOutputStream(std::vector<UniqueOutputStream> dests);
  XI_DELETE_COPY(CompoundOutputStream);
  XI_DEFAULT_MOVE(CompoundOutputStream);
  ~CompoundOutputStream() override = default;

  [[nodiscard]] Result<size_t> write(ConstByteSpan buffer) override;
  [[nodiscard]] Result<void> flush() override;

 private:
  std::deque<UniqueOutputStream> m_dests;
};

XI_DECLARE_SMART_POINTER(CompoundOutputStream)

}  // namespace Stream
}  // namespace Xi
