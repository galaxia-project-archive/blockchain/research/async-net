// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <Xi/ErrorModel.hh>

namespace Xi {
namespace Stream {

XI_ERROR_CODE_BEGIN(Stream)
XI_ERROR_CODE_VALUE(EndOfStream, 0x0001)
XI_ERROR_CODE_VALUE(BadState, 0x0002)
XI_ERROR_CODE_VALUE(Unsupported, 0x0003)
XI_ERROR_CODE_END(Stream, "stream operation failure")

}  // namespace Stream
}  // namespace Xi

XI_ERROR_CODE_OVERLOADS(Xi::Stream, Stream)
