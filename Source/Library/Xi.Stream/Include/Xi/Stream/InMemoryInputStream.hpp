// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <Xi/Global.hh>

#include "Xi/Stream/InputStream.hpp"

namespace Xi {
namespace Stream {
class InMemoryInputStream final : public InputStream {
 public:
  explicit InMemoryInputStream(ConstByteSpan source);
  XI_DELETE_COPY(InMemoryInputStream);
  XI_DEFAULT_MOVE(InMemoryInputStream);
  ~InMemoryInputStream() override = default;

  [[nodiscard]] Result<size_t> read(ByteSpan buffer) override;
  [[nodiscard]] Result<bool> isEndOfStream() const override;
  [[nodiscard]] Result<size_t> tell() const override;
  [[nodiscard]] Result<Byte> peek() const override;
  [[nodiscard]] Result<Byte> take() override;

 private:
  ConstByteSpan m_source;
  size_t m_pos;
};

XI_DECLARE_SMART_POINTER(InMemoryInputStream)
}  // namespace Stream
}  // namespace Xi
