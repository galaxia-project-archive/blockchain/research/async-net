// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <string>
#include <string_view>

#include "Xi/Stream/InMemoryInputStream.hpp"
#include "Xi/Stream/ByteVectorOutputStream.hpp"
#include "Xi/Stream/ByteArrayOutputStream.hpp"
#include "Xi/Stream/StringOutputStream.hpp"

namespace Xi {
namespace Stream {
UniqueInMemoryInputStream asInputStream(const std::string_view source);
UniqueInMemoryInputStream asInputStream(ConstByteSpan source);

UniqueStringOutputStream asOutputStream(std::string &dest);
UniqueByteVectorOutputStream asOutputStream(ByteVector &dest);
UniqueByteArrayOutputStream asOutputStream(ByteSpan dest);
}  // namespace Stream
}  // namespace Xi
