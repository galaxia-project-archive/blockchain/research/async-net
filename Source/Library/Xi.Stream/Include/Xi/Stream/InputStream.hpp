﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <Xi/Global.hh>
#include <Xi/Byte.hh>
#include <Xi/ErrorModel.hh>

namespace Xi {
namespace Stream {

/*!
 *
 * \brief The IInputStream interface declares all necessary interaction to read from an arbritrary
 * source of bytes.
 *
 * InputStreams should not hold any data. They rather wrap a source of data and provide an interface
 * to read from it.
 *
 */
class InputStream {
 public:
  virtual ~InputStream() = default;

  /*!
   *
   * \brief read reads bytes up till end of stream is reach of the provided buffer is filled.
   * \param buffer An output buffer to copy bytes read to.
   * \return A failure or actually bytes read.
   * \return zero on end of stream otherwise actually bytes read.
   *
   * \attention Always check the actual bytes read, its is not guaranteed to be fully filled, if you
   * require exaclty the number of bytes of the buffer size use readStrict.
   *
   */
  [[nodiscard]] virtual Result<size_t> read(ByteSpan buffer) = 0;

  /*!
   *
   * \brief isEndOfStream checks if the source stream is exhausted.
   * \return true if no byte can be read anymore, otherwise false.
   *
   * \note The caller of tell and peek has to check this first, otherwise behavious is undefined.
   *
   */
  [[nodiscard]] virtual Result<bool> isEndOfStream() const = 0;

  /*!
   *
   * \brief tell current position in the stream
   * \return index based offset of the current stream position, relative to its beginning, (size) if
   * endOfStream.
   *
   * \attention in case of compund streams this position may be invalid for the current buffer
   *
   */
  [[nodiscard]] virtual Result<size_t> tell() const = 0;

  /*!
   *
   * \brief take reads the next byte without moving the stream.
   * \return byte read
   *
   * \attention It is up to the caller to guarantee the stream can perform this action otherwise an
   * excpetions is thrown.
   *
   */
  [[nodiscard]] virtual Result<Byte> peek() const = 0;

  /*!
   *
   * \brief take reads the next byte and advances the stream.
   * \return byte read
   *
   * \attention It is up to the caller to guarantee the stream can perform this action otherwise an
   * excpetions is thrown.
   *
   */
  [[nodiscard]] virtual Result<Byte> take() = 0;

  /*!
   *
   * \brief readStrict repeates read operations until buffer is filled
   * \param buffer to fill
   * \return true on success otherwise false
   *
   * \note providing an empty buffer is well defined and will return true, reading no byte at all.
   *
   */
  Result<void> readStrict(ByteSpan buffer);

  /*!
   * \brief readAll reads the entire stream and forwards it to an output stream.
   * \param stream The stream to write to.
   * \return An internal input or output stream error on failure.
   */
  Result<void> readAll(class OutputStream& stream);

  /*!
   * \brief readAvailable reads the available input of the stream and forwards it to an output stream.
   * \param stream The stream to write to.
   * \return An internal input or output stream error on failure.
   */
  Result<void> readAvailable(class OutputStream& stream);
};

XI_DECLARE_SMART_POINTER(InputStream)

}  // namespace Stream
}  // namespace Xi
