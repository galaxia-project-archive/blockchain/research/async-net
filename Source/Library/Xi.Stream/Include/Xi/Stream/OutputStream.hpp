// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <Xi/Global.hh>
#include <Xi/ErrorModel.hh>
#include <Xi/Byte.hh>

namespace Xi {
namespace Stream {
class OutputStream {
 public:
  virtual ~OutputStream() = default;

  virtual Result<size_t> write(ConstByteSpan buffer) = 0;
  virtual Result<void> flush() = 0;

  Result<void> writeStrict(ConstByteSpan buffer);
};

XI_DECLARE_SMART_POINTER(OutputStream)

}  // namespace Stream
}  // namespace Xi
