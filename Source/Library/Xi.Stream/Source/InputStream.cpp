﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Stream/InputStream.hpp"

#include <Xi/Async/Async.hpp>

#include "Xi/Stream/StreamError.hpp"
#include "Xi/Stream/OutputStream.hpp"

namespace Xi {
namespace Stream {

Result<void> InputStream::readStrict(ByteSpan buffer) {
  while (!buffer.empty()) {
    const auto nRead = read(buffer);
    XI_ERROR_PROPAGATE(nRead)
    XI_FAIL_IF(*nRead == 0ULL, StreamError::EndOfStream);
    buffer = buffer.slice(*nRead);
  }
  XI_FAIL_IF_NOT(buffer.empty(), StreamError::EndOfStream);
  XI_SUCCEED();
}

Result<void> InputStream::readAll(OutputStream &stream) {
  auto isEos = isEndOfStream();
  XI_ERROR_PROPAGATE(isEos)
  while (!*isEos) {
    ByteArray<1024> buffer{};
    auto size = read(buffer);
    XI_ERROR_PROPAGATE(size)

    if (*size > 0) {
      XI_ERROR_PROPAGATE_CATCH(stream.writeStrict(asConstByteSpan(buffer.data(), *size)));
    }

    isEos = isEndOfStream();
    XI_ERROR_PROPAGATE(isEos)

    if (*size == 0) {
      boost::this_fiber::yield();
    }
  }
  XI_SUCCEED()
}

Result<void> InputStream::readAvailable(OutputStream &stream) {
  auto isEos = isEndOfStream();
  XI_ERROR_PROPAGATE(isEos)
  while (!*isEos) {
    ByteArray<1024> buffer{};
    auto size = read(buffer);
    XI_ERROR_PROPAGATE(size)
    XI_SUCCEED_IF(*size == 0)

    XI_ERROR_PROPAGATE_CATCH(stream.writeStrict(asConstByteSpan(buffer.data(), *size)));
    isEos = isEndOfStream();
    XI_ERROR_PROPAGATE(isEos)
  }
  XI_SUCCEED()
}

}  // namespace Stream
}  // namespace Xi
