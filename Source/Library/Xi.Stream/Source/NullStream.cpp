﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Stream/NullStream.hpp"

#include "Xi/Stream/StreamError.hpp"

namespace Xi::Stream {
Result<size_t> NullStream::read(ByteSpan) {
  XI_FAIL(StreamError::EndOfStream);
}

Result<bool> NullStream::isEndOfStream() const {
  XI_SUCCEED(true);
}

Result<size_t> NullStream::tell() const {
  XI_SUCCEED(0);
}

Result<Byte> NullStream::peek() const {
  XI_FAIL(StreamError::EndOfStream);
}

Result<Byte> NullStream::take() {
  XI_FAIL(StreamError::EndOfStream);
}

Result<size_t> NullStream::write(ConstByteSpan buffer) {
  XI_SUCCEED(buffer.size_bytes());
}

Result<void> NullStream::flush() {
  XI_SUCCEED();
}

std::unique_ptr<NullStream> makeNullStream() {
  return std::make_unique<NullStream>();
}

}  // namespace Xi::Stream
