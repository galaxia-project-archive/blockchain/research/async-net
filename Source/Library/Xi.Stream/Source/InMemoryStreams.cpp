// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Stream/InMemoryStreams.hpp"

Xi::Stream::UniqueInMemoryInputStream Xi::Stream::asInputStream(const std::string_view source) {
  return std::make_unique<InMemoryInputStream>(
      ConstByteSpan{reinterpret_cast<const Byte *>(source.data()), source.size()});
}

Xi::Stream::UniqueInMemoryInputStream Xi::Stream::asInputStream(ConstByteSpan source) {
  return std::make_unique<InMemoryInputStream>(source);
}

Xi::Stream::UniqueStringOutputStream Xi::Stream::asOutputStream(std::string &dest) {
  return std::make_unique<StringOutputStream>(dest);
}

Xi::Stream::UniqueByteVectorOutputStream Xi::Stream::asOutputStream(Xi::ByteVector &dest) {
  return std::make_unique<ByteVectorOutputStream>(dest);
}

Xi::Stream::UniqueByteArrayOutputStream Xi::Stream::asOutputStream(Xi::ByteSpan dest) {
  return std::make_unique<ByteArrayOutputStream>(dest);
}
