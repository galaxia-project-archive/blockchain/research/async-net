// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Stream/StreamError.hpp"

XI_ERROR_CODE_CATEGORY_BEGIN(Xi::Stream, Stream)
XI_ERROR_CODE_DESC(EndOfStream, "unexpected end of stream")
XI_ERROR_CODE_DESC(BadState, "the internal managed stream reached a bad state")
XI_ERROR_CODE_DESC(Unsupported, "stream operation is not supported")
XI_ERROR_CODE_CATEGORY_END()
