// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Stream/CompoundInputStream.hpp"

#include <utility>

#include "Xi/Stream/StreamError.hpp"

namespace Xi {
namespace Stream {

CompoundInputStream::CompoundInputStream(std::vector<UniqueInputStream> sources) : m_pos{0} {
  for (auto& source : sources) {
    if (source) {
      m_sources.emplace_back(std::move(source));
    }
  }
}

Result<size_t> CompoundInputStream::read(Xi::ByteSpan buffer) {
  XI_ERROR_PROPAGATE(popEndOfStreamSources());
  {
    const auto ec = isEndOfStream();
    XI_ERROR_PROPAGATE(ec)
    XI_FAIL_IF(*ec, StreamError::EndOfStream)
  }
  return m_sources.front()->read(buffer);
}

Result<bool> CompoundInputStream::isEndOfStream() const {
  for (const auto& iSource : m_sources) {
    const auto ec = iSource->isEndOfStream();
    XI_ERROR_PROPAGATE(ec)
    XI_SUCCEED_IF(*ec, false)
  }
  XI_SUCCEED(true)
}

Result<size_t> CompoundInputStream::tell() const {XI_SUCCEED(m_pos)}

Result<Byte> CompoundInputStream::peek() const {
  const auto ec = isEndOfStream();
  XI_ERROR_PROPAGATE(ec)
  XI_FAIL_IF(*ec, StreamError::EndOfStream)
  for (const auto& iSource : m_sources) {
    const auto iec = iSource->isEndOfStream();
    XI_ERROR_PROPAGATE(iec)
    if (!*iec) {
      return iSource->peek();
    }
  }
  XI_FAIL(StreamError::EndOfStream)
}

Result<Byte> CompoundInputStream::take() {
  XI_ERROR_PROPAGATE(popEndOfStreamSources())
  const auto ec = isEndOfStream();
  XI_ERROR_PROPAGATE(ec);
  XI_FAIL_IF(*ec, StreamError::EndOfStream)
  const auto reval = m_sources.front()->take();
  m_pos += 1;
  return reval;
}

Result<void> CompoundInputStream::popEndOfStreamSources() {
  while (!m_sources.empty()) {
    const auto ec = m_sources.front()->isEndOfStream();
    XI_ERROR_PROPAGATE(ec)
    if (*ec) {
      m_sources.pop_front();
    }
    if (const auto iec = m_sources.front()->isEndOfStream(); *iec == true) {
      m_sources.pop_front();
    } else {
      break;
    }
  }
  return makeSuccess();
}

}  // namespace Stream
}  // namespace Xi
