// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Stream/OStream.hpp"

#include <utility>

#include <Xi/ErrorModel.hh>

#include "Xi/Stream/StreamError.hpp"

namespace Xi {
namespace Stream {

OStream::OStream(Ownership<std::ostream> dest) : m_dest{std::move(dest)} {
  /* */
}

OStream::~OStream() {
  /* */
}

Result<size_t> OStream::write(ConstByteSpan buffer) {
  XI_ERROR_TRY
  m_dest.get().write(reinterpret_cast<const char*>(buffer.data()), static_cast<std::streamsize>(buffer.size()));
  return makeSuccess(buffer.size());
  XI_ERROR_CATCH
}

Result<void> OStream::flush() {
  XI_ERROR_TRY
  m_dest.get().flush();
  return makeSuccess();
  XI_ERROR_CATCH
}

}  // namespace Stream
}  // namespace Xi
