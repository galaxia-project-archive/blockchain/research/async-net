// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Stream/ByteArrayOutputStream.hpp"

#include <cstring>
#include <algorithm>

Xi::Stream::ByteArrayOutputStream::ByteArrayOutputStream(Xi::ByteSpan dest) : m_dest{dest} {
  /* */
}

Xi::Result<size_t> Xi::Stream::ByteArrayOutputStream::write(Xi::ConstByteSpan buffer) {
  XI_SUCCEED_IF(m_dest.empty(), (size_t)0);
  const auto nWrite = std::min(buffer.size(), m_dest.size());
  std::memcpy(m_dest.data(), buffer.data(), nWrite);
  m_dest = m_dest.slice(buffer.size());
  m_written = ByteSpan{m_written.data(), m_written.size() + buffer.size()};
  return makeSuccess(nWrite);
}

Xi::Result<void> Xi::Stream::ByteArrayOutputStream::flush() {
  return makeSuccess();
}

Xi::ByteSpan Xi::Stream::ByteArrayOutputStream::written() const {
  return m_written;
}
