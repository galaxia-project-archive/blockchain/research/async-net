// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Stream/OutputStream.hpp"

#include "Xi/Stream/StreamError.hpp"

Xi::Result<void> Xi::Stream::OutputStream::writeStrict(Xi::ConstByteSpan buffer) {
  while (!buffer.empty()) {
    const auto nWritten = write(buffer);
    XI_ERROR_PROPAGATE(nWritten)
    XI_FAIL_IF(*nWritten == 0, StreamError::EndOfStream)
    buffer = buffer.slice(*nWritten);
  }
  return makeSuccess();
}
