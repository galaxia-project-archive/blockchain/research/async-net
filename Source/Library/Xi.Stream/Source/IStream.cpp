// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Stream/IStream.hpp"

#include <utility>

#include "Xi/Stream/StreamError.hpp"

namespace Xi {
namespace Stream {

IStream::IStream(Ownership<std::istream> source) : m_source{std::move(source)} {
  /* */
}

Result<size_t> IStream::read(ByteSpan buffer) {
  const auto isEos = isEndOfStream();
  XI_ERROR_PROPAGATE(isEos)
  if (*isEos) {
    return makeSuccess<size_t>(0U);
  }
  m_source.get().read(reinterpret_cast<char *>(buffer.data()), static_cast<std::streamsize>(buffer.size()));
  auto read = m_source.get().gcount();
  if (read <= 0) {
    XI_FAIL_IF(m_source.get().fail(), StreamError::BadState)
    return makeSuccess<size_t>(0U);
  } else {
    return makeSuccess(static_cast<size_t>(read));
  }
}

Result<bool> IStream::isEndOfStream() const {
  XI_SUCCEED_IF(m_source.get().eof(), true);
  XI_FAIL_IF(m_source.get().fail(), StreamError::BadState);
  XI_SUCCEED(false);
}

Result<size_t> IStream::tell() const {
  const auto pos = m_source.get().tellg();
  XI_FAIL_IF(pos < 0, StreamError::BadState)
  return makeSuccess(static_cast<size_t>(pos));
}

Result<Byte> IStream::peek() const {
  const auto isEos = isEndOfStream();
  XI_ERROR_PROPAGATE(isEos)
  XI_FAIL_IF(*isEos, StreamError::EndOfStream)
  const auto byte = static_cast<char>(m_source.get().peek());
  return makeSuccess(static_cast<Byte>(byte));
}

Result<Byte> IStream::take() {
  const auto isEos = isEndOfStream();
  XI_ERROR_PROPAGATE(isEos)
  XI_FAIL_IF(*isEos, StreamError::EndOfStream)
  const auto byte = static_cast<char>(m_source.get().get());
  return makeSuccess(static_cast<Byte>(byte));
}

}  // namespace Stream
}  // namespace Xi
