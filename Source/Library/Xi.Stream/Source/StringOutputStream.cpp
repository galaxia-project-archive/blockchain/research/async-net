// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Stream/StringOutputStream.hpp"

#include <Xi/ErrorModel.hh>

Xi::Stream::StringOutputStream::StringOutputStream(std::string &dest) : m_dest{dest} {
}

Xi::Result<size_t> Xi::Stream::StringOutputStream::write(ConstByteSpan buffer) {
  m_dest.insert(m_dest.end(), reinterpret_cast<const std::string::value_type *>(buffer.data()),
                reinterpret_cast<const std::string::value_type *>(buffer.data() + buffer.size()));
  return makeSuccess(buffer.size());
}

Xi::Result<void> Xi::Stream::StringOutputStream::flush() {
  return makeSuccess();
}
