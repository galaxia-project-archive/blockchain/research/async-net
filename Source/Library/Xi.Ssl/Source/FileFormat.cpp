// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Ssl/FileFormat.hpp"

#include <Xi/String.hpp>

XI_ERROR_CODE_CATEGORY_BEGIN(Xi::Ssl, FileFormat)
XI_ERROR_CODE_DESC(IllFormed, "ssl file format string representation is ill formed")
XI_ERROR_CODE_CATEGORY_END()

namespace Xi {
namespace Ssl {

const FileSystem::FileFormat Asn1FileFormat{FileSystem::FileFormat::Kind::Text, {".asn1"}};
const FileSystem::FileFormat PemFileFormat{FileSystem::FileFormat::Kind::Text, {".pem"}};

std::string stringify(const FileFormat format) {
  switch (format) {
    case FileFormat::Asn1:
      return "asn1";
    case FileFormat::Pem:
      return "pem";
  }
  XI_EXCEPTIONAL(InvalidEnumValueError)
}

Result<void> parse(const std::string &str, FileFormat &out) {
  auto relaxed = relax(str);
  if (str == toString(FileFormat::Asn1)) {
    out = FileFormat::Asn1;
    XI_SUCCEED();
  } else if (str == toString(FileFormat::Pem)) {
    out = FileFormat::Pem;
    XI_SUCCEED();
  } else {
    XI_FAIL(FileFormatError::IllFormed)
  }
}

boost::asio::ssl::context::file_format toBoost(const FileFormat format) {
  switch (format) {
    case FileFormat::Asn1:
      return boost::asio::ssl::context::file_format::asn1;
    case FileFormat::Pem:
      return boost::asio::ssl::context::file_format::pem;
  }
  XI_EXCEPTIONAL(InvalidEnumValueError)
}

}  // namespace Ssl
}  // namespace Xi
