// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Ssl/Protocol.hpp"

#include <vector>

#include <Xi/String.hpp>

#include <Xi/Log/Log.hpp>
XI_LOGGER("Ssl/Protocol")

XI_ERROR_CODE_CATEGORY_BEGIN(Xi::Ssl, Protocol)
XI_ERROR_CODE_DESC(IllFormed, "protocol string representation is ill formed")
XI_ERROR_CODE_CATEGORY_END()

namespace Xi {
namespace Ssl {

bool hasSslSupport(const Protocol protocol) {
  return hasAnyFlag(protocol, Protocol::Ssl);
}

bool hasTlsSupport(const Protocol protocol) {
  return hasAnyFlag(protocol, Protocol::Tls);
}

bool hasDeprectaedSupport(const Protocol protocol) {
  return hasAnyFlag(protocol, Protocol::Deprecated);
}

bool hasInsecureSupport(const Protocol protocol) {
  return hasAnyFlag(protocol, Protocol::Insecure);
}

std::string stringify(const Protocol protocol) {
  if (protocol == Protocol::None) {
    return "none";
  }

  std::vector<std::string> flags{};
  flags.reserve(6);

  if (hasFlag(protocol, Protocol::Ssl_v2)) {
    flags.emplace_back("ssl_v2");
  }
  if (hasFlag(protocol, Protocol::Ssl_v3)) {
    flags.emplace_back("ssl_v3");
  }
  if (hasFlag(protocol, Protocol::Tls_v1_0)) {
    flags.emplace_back("tls_v1_0");
  }
  if (hasFlag(protocol, Protocol::Tls_v1_1)) {
    flags.emplace_back("tls_v1_1");
  }
  if (hasFlag(protocol, Protocol::Tls_v1_2)) {
    flags.emplace_back("tls_v1_2");
  }
  if (hasFlag(protocol, Protocol::Tls_v1_3)) {
    flags.emplace_back("tls_v1_3");
  }

  return join(flags, ",");
}

Result<void> parse(const std::string &str, Protocol &out) {
  auto relaxed = relax(str);
  auto flags = split(relaxed, ",");

  if (flags.size() == 1 && flags[0] == toString(Protocol::None)) {
    out = Protocol::None;
    XI_SUCCEED();
  }

  Protocol result = Protocol::None;
  for (const auto &flag : flags) {
    if (flag == toString(Protocol::Ssl_v2)) {
      result |= Protocol::Ssl_v2;
    } else if (flag == toString(Protocol::Ssl_v3)) {
      result |= Protocol::Ssl_v3;
    } else if (flag == toString(Protocol::Tls_v1_0)) {
      result |= Protocol::Tls_v1_0;
    } else if (flag == toString(Protocol::Tls_v1_1)) {
      result |= Protocol::Tls_v1_1;
    } else if (flag == toString(Protocol::Tls_v1_2)) {
      result |= Protocol::Tls_v1_2;
    } else if (flag == toString(Protocol::Tls_v1_3)) {
      result |= Protocol::Tls_v1_3;
    } else if (flag == "ssl") {
      result |= Protocol::Ssl;
    } else if (flag == "tls") {
      result |= Protocol::Tls;
    } else if (flag == "default") {
      result |= Protocol::Default;
    } else if (flag == "deprecated") {
      result |= Protocol::Deprecated;
    } else {
      XI_LOG_ERROR("Unknown ssl protocole '{}'", flag);
      XI_FAIL(ProtocolError::IllFormed)
    }
  }
  out = result;
  XI_SUCCEED();
}

}  // namespace Ssl
}  // namespace Xi
