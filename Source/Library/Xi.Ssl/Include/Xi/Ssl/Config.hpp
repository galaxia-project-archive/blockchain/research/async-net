// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <set>
#include <string>

#include <Xi/Global.hh>
#include <Xi/ErrorModel.hh>
#include <Xi/FileSystem/FileSystem.hpp>
#include <Xi/Serialization/Serialization.hpp>

#include "Xi/Ssl/Protocol.hpp"
#include "Xi/Ssl/VerifyMode.hpp"
#include "Xi/Ssl/Context.hpp"

namespace Xi {
namespace Ssl {

class Config {
 public:
  explicit Config() = default;
  XI_DEFAULT_COPY(Config)
  XI_DEFAULT_MOVE(Config)
  ~Config() = default;

  Protocol protocol() const;
  void setProtocol(const Protocol protocol_);

  VerifyMode verifyMode() const;
  void setVerifyMode(const VerifyMode verifyMode_);

  const std::set<FileSystem::File>& verifyFiles() const;
  std::set<FileSystem::File>& verifyFiles();

  const std::set<FileSystem::Directory>& verifyPaths() const;
  std::set<FileSystem::Directory>& verifyPaths();

  std::optional<int> verifyDepth() const;
  void setVerifyDepth(const std::optional<int>& depth);

  const std::optional<std::string>& privateKeyPassword() const;
  void setPrivateKeyPassword(const std::optional<std::string>& privateKeyPassword_);

  const std::optional<FileSystem::File>& privateKey() const;
  void setPrivateKey(const std::optional<FileSystem::File>& file);

  const std::optional<FileSystem::File>& certificate() const;
  void setCertificate(const std::optional<FileSystem::File>& file);

  const std::optional<FileSystem::File>& certificateChain() const;
  void setCertificateChain(const std::optional<FileSystem::File>& file);

  const std::optional<FileSystem::File>& dhParam() const;
  void setDhParam(const std::optional<FileSystem::File>& file);

  /// Should root certificates be loaded as verified.
  bool rootStore() const;
  /// Sets whether root certificates should be trusted.
  void setRootStore(bool isEnabled);

  ///
  /// \brief configure Emplaces this configuration into an existing ssl context.
  /// \param ctx context to configure
  /// \return An error iff unssuccessful
  /// \attention The context may be configured partially incomplete on error.
  ///
  Result<void> configure(Context& ctx) const;

  ///
  /// \brief construct Creates a new ssl context according to this configuration.
  /// \return The configured context or an error.
  ///
  Result<Context> construct() const;

  XI_SERIALIZATION_COMPLEX_EXTERN()

 private:
  Protocol m_protocol{Protocol::Default};
  VerifyMode m_verifyMode{VerifyMode::None};
  std::set<FileSystem::File> m_verifyFiles{/* */};
  std::set<FileSystem::Directory> m_verifyPaths{/* */};
  std::optional<int> m_verifyDepth{std::nullopt};
  std::optional<std::string> m_privateKeyPassword{std::nullopt};
  std::optional<FileSystem::File> m_privateKey{std::nullopt};
  std::optional<FileSystem::File> m_certificate{std::nullopt};
  std::optional<FileSystem::File> m_certificateChain{std::nullopt};
  std::optional<FileSystem::File> m_dhParam{std::nullopt};
  bool m_rootStore{true};
};

}  // namespace Ssl
}  // namespace Xi
