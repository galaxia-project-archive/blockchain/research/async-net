// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <string>

#include <Xi/Extern/Push.hh>
#include <boost/asio/ssl.hpp>
#include <Xi/Extern/Pop.hh>

#include <Xi/Global.hh>
#include <Xi/ErrorModel.hh>
#include <Xi/Serialization/Enum.hpp>
#include <Xi/FileSystem/FileFormat.hpp>

namespace Xi {
namespace Ssl {

XI_ERROR_CODE_BEGIN(FileFormat)
XI_ERROR_CODE_VALUE(IllFormed, 0x0001)
XI_ERROR_CODE_END(FileFormat, "Ssl::FileFormatError")

enum struct FileFormat {
  Asn1 = 0x0001,
  Pem = 0x0002,
};

extern const FileSystem::FileFormat Asn1FileFormat;
extern const FileSystem::FileFormat PemFileFormat;

XI_SERIALIZATION_ENUM(FileFormat)

std::string stringify(const FileFormat format);
Result<void> parse(const std::string& str, FileFormat& out);

boost::asio::ssl::context::file_format toBoost(const FileFormat format);

}  // namespace Ssl
}  // namespace Xi

XI_SERIALIZATION_ENUM_RANGE(Xi::Ssl::FileFormat, Asn1, Pem)
XI_SERIALIZATION_ENUM_TAG(Xi::Ssl::FileFormat, Asn1, "asn1")
XI_SERIALIZATION_ENUM_TAG(Xi::Ssl::FileFormat, Pem, "pem")

XI_ERROR_CODE_OVERLOADS(Xi::Ssl, FileFormat)
