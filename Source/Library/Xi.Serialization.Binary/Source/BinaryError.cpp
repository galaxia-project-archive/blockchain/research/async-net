// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Serialization/Binary/BinaryError.hpp"

XI_ERROR_CODE_CATEGORY_BEGIN(Xi::Serialization::Binary, Binary)
XI_ERROR_CODE_DESC(InvalidBooleanRepresentation, "binary representation of boolean is invalid")
XI_ERROR_CODE_DESC(NullTag, "binary type tag is null")
XI_ERROR_CODE_DESC(FlagOverflow, "flag representation exceeded limit")
XI_ERROR_CODE_CATEGORY_END()
