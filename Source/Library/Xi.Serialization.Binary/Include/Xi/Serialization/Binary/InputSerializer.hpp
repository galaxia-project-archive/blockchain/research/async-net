// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <utility>

#include <Xi/ErrorModel.hh>
#include <Xi/Stream/InputStream.hpp>
#include <Xi/Stream/InMemoryInputStream.hpp>
#include <Xi/Serialization/Serializer.hpp>
#include <Xi/Serialization/InputSerializer.hpp>

#include "Xi/Serialization/Binary/Options.hpp"

namespace Xi {
namespace Serialization {
namespace Binary {

XI_ERROR_CODE_BEGIN(InputSerializer)
XI_ERROR_CODE_VALUE(TrailingBytes, 0x0001)
XI_ERROR_CODE_END(InputSerializer, "Binary::InputSerializerError")

class InputSerializer final : public Serialization::InputSerializer {
 public:
  explicit InputSerializer(Stream::InputStream& stream, Options options = Options{});
  ~InputSerializer() override = default;

  Format format() const override;

  Result<void> readInt8(std::int8_t& value, const Tag& nameTag) override;
  Result<void> readUInt8(std::uint8_t& value, const Tag& nameTag) override;
  Result<void> readInt16(std::int16_t& value, const Tag& nameTag) override;
  Result<void> readUInt16(std::uint16_t& value, const Tag& nameTag) override;
  Result<void> readInt32(std::int32_t& value, const Tag& nameTag) override;
  Result<void> readUInt32(std::uint32_t& value, const Tag& nameTag) override;
  Result<void> readInt64(std::int64_t& value, const Tag& nameTag) override;
  Result<void> readUInt64(std::uint64_t& value, const Tag& nameTag) override;

  Result<void> readBoolean(bool& value, const Tag& nameTag) override;

  Result<void> readFloat(float& value, const Tag& nameTag) override;
  Result<void> readDouble(double& value, const Tag& nameTag) override;

  Result<void> readTag(Tag& value, const Tag& nameTag) override;
  Result<void> readFlag(TagVector& value, const Tag& nameTag) override;

  Result<void> readString(std::string& value, const Tag& nameTag) override;
  Result<void> readBinary(ByteVector& out, const Tag& nameTag) override;

  Result<void> readBlob(ByteSpan out, const Tag& nameTag) override;

  Result<void> beginReadComplex(const Tag& nameTag) override;
  Result<void> endReadComplex() override;

  Result<void> beginReadVector(size_t& size, const Tag& nameTag) override;
  Result<void> endReadVector() override;

  Result<void> beginReadArray(size_t size, const Tag& nameTag) override;
  Result<void> endReadArray() override;

  Result<void> checkValue(bool& value, const Tag& nameTag) override;

 private:
  Stream::InputStream& m_stream;
  Options m_options;
};

}  // namespace Binary

template <typename _ValueT>
Result<_ValueT> fromBinary(Stream::InputStream& stream, Binary::Options options = Binary::Options{}) {
  Binary::InputSerializer input{stream, options};
  Serializer serializer{input};
  _ValueT reval{};
  XI_ERROR_PROPAGATE_CATCH(serializer(reval, Tag::Null));
  XI_SUCCEED(std::move(reval));
}

template <typename _ValueT>
Result<_ValueT> fromBinary(ConstByteSpan data, Binary::Options options = Binary::Options{}) {
  Stream::InMemoryInputStream stream{data};
  auto bin = fromBinary<_ValueT>(stream, options);
  XI_ERROR_PROPAGATE(bin)
  auto isEos = stream.isEndOfStream();
  XI_ERROR_PROPAGATE(isEos)
  XI_FAIL_IF_NOT(*isEos, Binary::InputSerializerError::TrailingBytes);
  return bin;
}

template <typename _ValueT>
Result<_ValueT> fromBinary(const ByteVector& data, Binary::Options options = Binary::Options{}) {
  return fromBinary<_ValueT>(asConstByteSpan(data.data(), data.size()), options);
}

}  // namespace Serialization
}  // namespace Xi

XI_ERROR_CODE_OVERLOADS(Xi::Serialization::Binary, InputSerializer)
