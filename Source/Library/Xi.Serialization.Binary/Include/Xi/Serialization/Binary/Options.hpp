// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <Xi/Endianess/Endianess.hh>
#include <Xi/TypeSafe/Flag.hpp>

namespace Xi {
namespace Serialization {
namespace Binary {

enum struct VarIntUsage {
  None = 0,
  Int16 = 1 << 0,
  UInt16 = 1 << 1,
  Int32 = 1 << 2,
  UInt32 = 1 << 3,
  Int64 = 1 << 4,
  UInt64 = 1 << 5,

  Signed = Int16 | Int32 | Int64,
  Unsigned = UInt16 | UInt32 | UInt64,
  All = Signed | Unsigned,

  Bits16 = Int16 | UInt16,
  Bits32 = Int32 | UInt32,
  Bits64 = Int64 | UInt64,

  GreaterBits16Signed = Int32 | Int64,
  GreaterBits32Signed = Int64,

  GreaterBits16Unsigned = UInt32 | UInt64,
  GreaterBits32Unsigned = UInt64,

  GreaterBits16 = GreaterBits16Signed | GreaterBits16Unsigned,
  GreaterBits32 = GreaterBits32Signed | GreaterBits32Unsigned,

};

XI_TYPESAFE_FLAG_MAKE_OPERATIONS(VarIntUsage)

struct Options {
  VarIntUsage varInt{VarIntUsage::All};
  Endianess::Type endianess{Endianess::Type::Little};
};

}  // namespace Binary
}  // namespace Serialization
}  // namespace Xi
