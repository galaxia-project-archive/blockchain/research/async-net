// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <stack>
#include <utility>

#include <Xi/Stream/OutputStream.hpp>
#include <Xi/Stream/ByteVectorOutputStream.hpp>
#include <Xi/Serialization/Serializer.hpp>
#include <Xi/Serialization/OutputSerializer.hpp>

#include "Xi/Serialization/Binary/Options.hpp"

namespace Xi {
namespace Serialization {
namespace Binary {
class OutputSerializer final : public Serialization::OutputSerializer {
 public:
  OutputSerializer(Stream::OutputStream& stream, Options options = Options{});
  ~OutputSerializer() override = default;

  Format format() const override;

  Result<void> writeInt8(std::int8_t value, const Tag& nameTag) override;
  Result<void> writeUInt8(std::uint8_t value, const Tag& nameTag) override;
  Result<void> writeInt16(std::int16_t value, const Tag& nameTag) override;
  Result<void> writeUInt16(std::uint16_t value, const Tag& nameTag) override;
  Result<void> writeInt32(std::int32_t value, const Tag& nameTag) override;
  Result<void> writeUInt32(std::uint32_t value, const Tag& nameTag) override;
  Result<void> writeInt64(std::int64_t value, const Tag& nameTag) override;
  Result<void> writeUInt64(std::uint64_t value, const Tag& nameTag) override;

  Result<void> writeBoolean(bool value, const Tag& nameTag) override;

  Result<void> writeFloat(float value, const Tag& nameTag) override;
  Result<void> writeDouble(double value, const Tag& nameTag) override;

  Result<void> writeTag(const Tag& value, const Tag& nameTag) override;
  Result<void> writeFlag(const TagVector& flag, const Tag& nameTag) override;

  Result<void> writeString(const std::string_view value, const Tag& nameTag) override;
  Result<void> writeBinary(ConstByteSpan value, const Tag& nameTag) override;

  Result<void> writeBlob(ConstByteSpan value, const Tag& nameTag) override;

  Result<void> beginWriteComplex(const Tag& nameTag) override;
  Result<void> endWriteComplex() override;

  Result<void> beginWriteVector(size_t size, const Tag& nameTag) override;
  Result<void> endWriteVector() override;

  Result<void> beginWriteArray(size_t size, const Tag& nameTag) override;
  Result<void> endWriteArray() override;

  Result<void> writeNull(const Tag& nameTag) override;
  Result<void> writeNotNull(const Tag& nameTag) override;

 private:
  Stream::OutputStream& m_stream;
  Options m_options;
};

}  // namespace Binary

template <typename _ValueT>
Result<void> toBinary(Stream::OutputStream& stream, const _ValueT& value, Binary::Options options = Binary::Options{}) {
  Binary::OutputSerializer output{stream, options};
  Serializer serializer{output};
  return serializer(const_cast<_ValueT&>(value), Tag::Null);
}

template <typename _ValueT>
Result<ByteVector> toBinary(const _ValueT& value, Binary::Options options = Binary::Options{}) {
  ByteVector reval{};
  Stream::ByteVectorOutputStream stream{reval};
  XI_ERROR_PROPAGATE_CATCH(toBinary(stream, value, options));
  XI_SUCCEED(std::move(reval));
}

}  // namespace Serialization
}  // namespace Xi
