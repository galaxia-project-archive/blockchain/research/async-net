// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <Xi/ErrorModel.hh>

namespace Xi {
namespace Serialization {
namespace Binary {

XI_ERROR_CODE_BEGIN(Binary)
XI_ERROR_CODE_VALUE(InvalidBooleanRepresentation, 0x0001)
XI_ERROR_CODE_VALUE(NullTag, 0x0002)
XI_ERROR_CODE_VALUE(FlagOverflow, 0x0003)
XI_ERROR_CODE_END(Binary, "Serialization::BinaryError")

}  // namespace Binary
}  // namespace Serialization
}  // namespace Xi

XI_ERROR_CODE_OVERLOADS(Xi::Serialization::Binary, Binary)
