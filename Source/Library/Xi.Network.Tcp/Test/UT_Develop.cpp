﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include <gmock/gmock.h>

#include <Xi/Testing/Result.hpp>
#include <Xi/Async/Async.hpp>
#include <Xi/Network/Tcp/Listener.hpp>
#include <Xi/Network/Tcp/Acceptor.hpp>
#include <Xi/Network/Tcp/AcceptorBuilder.hpp>

#define XI_TEST_SUITE Xi_Network_Tcp_Develop

TEST(XI_TEST_SUITE, RunServer) {
  using namespace Xi;
  using namespace Xi::Async;
  using namespace Xi::Network;
  using namespace Xi::Network::Tcp;
  using namespace Xi::Testing;

  auto listener = makeListener();
  auto plainV4 = AcceptorBuilder{/* */}.withEndpoint(Endpoint{IpAddress::v4Loopback, Port{22868}}).build();
  ASSERT_TRUE(isSuccess(plainV4));
  ASSERT_THAT(listener->bind(*plainV4), IsSuccess());
  auto plainV6 = AcceptorBuilder{/* */}.withEndpoint(Endpoint{IpAddress::v6Loopback, Port{22968}}).build();
  ASSERT_TRUE(isSuccess(plainV6));
  ASSERT_THAT(listener->bind(*plainV6), IsSuccess());

  Ssl::Config ssl{};
  ssl.setPrivateKey(FileSystem::makeFile("./key.pem").take());
  ssl.setPrivateKeyPassword("test");
  ssl.setCertificate(FileSystem::makeFile("./cert.pem").take());
  auto sslV4 = AcceptorBuilder{/* */}.withEndpoint(Endpoint{IpAddress::v4Loopback, Port{22869}}).withSsl(ssl).build();
  ASSERT_TRUE(isSuccess(sslV4));
  ASSERT_THAT(listener->bind(*sslV4), IsSuccess());
  auto sslV6 = AcceptorBuilder{/* */}.withEndpoint(Endpoint{IpAddress::v6Loopback, Port{22869}}).withSsl(ssl).build();
  ASSERT_TRUE(isSuccess(sslV6));
  ASSERT_THAT(listener->bind(*sslV6), IsSuccess());

  auto listenerRun = Async::invoke(&Listener::run, listener);
  listenerRun.wait();
}
