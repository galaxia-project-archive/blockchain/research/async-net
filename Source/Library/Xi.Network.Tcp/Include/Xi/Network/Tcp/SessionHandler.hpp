// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <Xi/Global.hh>

namespace Xi {
namespace Network {
namespace Tcp {

class Session;

class SessionHandler {
 public:
  virtual ~SessionHandler() = default;

  virtual void onNewSession(Session&& session) = 0;
};

XI_DECLARE_SMART_POINTER(SessionHandler)

}  // namespace Tcp
}  // namespace Network
}  // namespace Xi
