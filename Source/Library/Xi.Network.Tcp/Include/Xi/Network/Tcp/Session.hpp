﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <string>

#include <Xi/Global.hh>
#include <Xi/ErrorModel.hh>
#include <Xi/Stream/InputStream.hpp>
#include <Xi/Stream/OutputStream.hpp>
#include <Xi/Serialization/Serialization.hpp>

#include "Xi/Network/Tcp/SessionDirection.hpp"
#include "Xi/Network/Tcp/SessionConcept.hpp"

namespace Xi {
namespace Network {
namespace Tcp {

XI_DECLARE_SMART_POINTER_CLASS(SessionManager)

class Session final {
 private:
  explicit Session(SharedSessionManager manager, SessionDirection dir, UniqueSessionConcept concept_);
  friend class SessionManager;

 public:
  XI_DELETE_COPY(Session)
  ~Session();
  Session(Session&& other);
  Session& operator=(Session&& other);

  SessionEncryption encryption() const;
  SessionDirection direction() const;
  Result<Endpoint> localEndpoint() const;
  Result<Endpoint> remoteEndpoint() const;

  /// Initializes the connection performing optional initialization steps.
  Result<void> setup();
  /// Performs optional tear down steps, but may not closes the socket.
  Result<void> teardown();
  /// Gracefully teards down the session and closes it after.
  Result<void> shutdown();
  /// Forcefully closes the session.
  Result<void> close();

  Stream::InputStream& input();
  Stream::OutputStream& output();

  /// Small representation of this session, for logging purposes.
  std::string stringify() const;

  /// Use with care.
  SessionConcept& implementation();
  /// Use with care.
  const SessionConcept& implementation() const;

 private:
  void emplacePlainStreams();
  void emplaceSslStreams();
  void release();
  void emplace(Session&& other);

 private:
  WeakSessionManager m_manager;
  SessionDirection m_direction;
  UniqueSessionConcept m_concept;
  Stream::UniqueInputStream m_input;
  Stream::UniqueOutputStream m_output;

  mutable Result<Endpoint> m_localEndpoint;
  mutable Result<Endpoint> m_remoteEndoint;
};

}  // namespace Tcp
}  // namespace Network
}  // namespace Xi
