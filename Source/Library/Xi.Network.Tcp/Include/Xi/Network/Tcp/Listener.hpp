﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <memory>
#include <set>
#include <map>
#include <vector>
#include <atomic>
#include <string>

#include <Xi/Global.hh>
#include <Xi/ErrorModel.hh>
#include <Xi/Async/Async.hpp>

#include "Xi/Network/Tcp/Acceptor.hpp"
#include "Xi/Network/Tcp/SessionManager.hpp"

namespace Xi {
namespace Network {
namespace Tcp {

XI_ERROR_CODE_BEGIN(Listener)
XI_ERROR_CODE_VALUE(NotRunning, 0x0001)
XI_ERROR_CODE_VALUE(IsShuttingDown, 0x0002)
XI_ERROR_CODE_VALUE(AlreadyRunning, 0x0003)
XI_ERROR_CODE_VALUE(AcceptorNotFound, 0x0101)
XI_ERROR_CODE_VALUE(AcceptorAlreadyInUse, 0x0102)
XI_ERROR_CODE_END(Listener, "Network::Tcp::ListenerError")

XI_DECLARE_SMART_POINTER_CLASS(Listener)

/*!
 *
 * A Listener handles incoming connections from different network interfaces managed by acceptors. Therefore it informs
 * the network manager about the desired session action and handles the session accordingly (ie. waiting for open slots
 * or close the connection due to too many waiting sessions). If a session is accepted successfully the session
 *
 */
class Listener final : public std::enable_shared_from_this<Listener> {
 public:
  enum struct State {
    Closed = 1,
    Running = 2,
    ShuttingDown = 3,
  };

 public:
  ~Listener();

  Result<void> run();
  void shutdown();

  [[nodiscard]] State state() const;
  [[nodiscard]] bool isRunning() const;
  [[nodiscard]] bool isClosed() const;

 public:
  Result<void> bind(SharedAcceptor acceptor);
  Result<void> unbind(SharedAcceptor acceptor);

 private:
  /// Called by acceptor in a detached fiber, operations wont block the acceptor.
  void onIncomingConnection(UniqueSessionConcept socket, SharedAcceptor acceptor);
  friend class Acceptor;

  void setState(const State state_);

  /// Ensures a session manager is used, a default static one is used if none is configured.
  SessionManager& sessionManager();

 private:
  Listener();
  friend SharedListener makeListener();

 private:
  Async::Mutex m_guard;

  std::atomic<State> m_state{State::Closed};
  Async::ConditionVariable m_stateGate{};

  std::map<SharedAcceptor, Async::FutureResult<void>> m_acceptorsRunning;
  std::set<SharedAcceptor> m_acceptorsToStart;

  SharedSessionManager m_sessionManager{};
};

SharedListener makeListener();

std::string stringify(const Listener::State state);

}  // namespace Tcp
}  // namespace Network
}  // namespace Xi

XI_ERROR_CODE_OVERLOADS(Xi::Network::Tcp, Listener)
