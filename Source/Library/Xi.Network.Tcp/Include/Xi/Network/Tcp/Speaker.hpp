﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <Xi/Global.hh>
#include <Xi/ErrorModel.hh>

#include "Xi/Network/Tcp/Endpoint.hpp"
#include "Xi/Network/Tcp/Session.hpp"
#include "Xi/Network/Tcp/SessionManager.hpp"
#include "Xi/Network/Tcp/Connector.hpp"

namespace Xi {
namespace Network {
namespace Tcp {

XI_ERROR_CODE_BEGIN(Speaker)
XI_ERROR_CODE_VALUE(InvalidPort, 0x0001)
XI_ERROR_CODE_END(Speaker, "Tcp::SpeakerError")

/*!
 *
 * The speaker abstracts establishment of new connections. Therefore it manages multiple configurations to connect
 * to various ips/hosts stored in connectors and picks them according to some ip resolution using ip/mask or host
 * wildcards.
 *
 */
class Speaker {
 public:
  Result<Session> connect(const Endpoint& ep);

  SessionManager& sessionManager();

 private:
  SharedSessionManager m_sessionManager;
  SharedConnector m_defaultConnector;
};

}  // namespace Tcp
}  // namespace Network
}  // namespace Xi

XI_ERROR_CODE_OVERLOADS(Xi::Network::Tcp, Speaker)
