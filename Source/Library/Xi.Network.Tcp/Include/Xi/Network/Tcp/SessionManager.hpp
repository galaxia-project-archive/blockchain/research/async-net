// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <cinttypes>

#include <Xi/Global.hh>
#include <Xi/ErrorModel.hh>
#include <Xi/Async/Async.hpp>

#include "Xi/Network/Tcp/Session.hpp"
#include "Xi/Network/Tcp/SessionConcept.hpp"

namespace Xi::Network::Tcp {

XI_DECLARE_SMART_POINTER_CLASS(SessionManager)

/*!
 *
 * SessionManager controls the number of open Session and whether they can be opened at all, like a tiny firewall,
 * if desired. Every connection opened either way requires a SessionSlot which may only be constructed by the
 * SessionManager forcing the programmer to acquire a slot first.
 *
 */
class SessionManager final : public std::enable_shared_from_this<SessionManager> {
 private:
  /// Stores the current usage of a specific direction.
  struct _DirectionalData {
    Async::Mutex guard{/* */};
    Async::ConditionVariable gate{/* */};
    size_t currentOpen{0};
    size_t maxOpen{0};

    _DirectionalData(size_t max);
  };

 private:
  SessionManager();

  friend SharedSessionManager makeSessionManager();

 public:
  XI_DELETE_COPY(SessionManager)
  XI_DELETE_MOVE(SessionManager)
  ~SessionManager();

 public:
  /// Accepts as a server iff the slot is available.
  Result<Session> accept(UniqueSessionConcept session);

  /// Connects as a client iff a slot is avialable.
  Result<Session> connect(UniqueSessionConcept session);

  /// Called by session slot to release the connection.
  void release(const Session& session);

 private:
  Result<void> acquireSlot(SessionDirection direction, _DirectionalData& data);

  void release(SessionDirection direction, _DirectionalData& data);

 private:
  _DirectionalData m_incoming{32};
  _DirectionalData m_outgoing{4};
};

SharedSessionManager makeSessionManager();

}  // namespace Xi::Network::Tcp
