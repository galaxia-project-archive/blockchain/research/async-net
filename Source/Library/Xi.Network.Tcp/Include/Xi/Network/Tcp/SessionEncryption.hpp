// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <string>

#include <Xi/Global.hh>
#include <Xi/ErrorModel.hh>
#include <Xi/Serialization/Enum.hpp>

namespace Xi {
namespace Network {
namespace Tcp {

enum struct SessionEncryption {
  None = 1,
  Ssl = 2,
};

XI_SERIALIZATION_ENUM(SessionEncryption)

std::string stringify(const SessionEncryption enc);

}  // namespace Tcp
}  // namespace Network
}  // namespace Xi

XI_SERIALIZATION_ENUM_RANGE(Xi::Network::Tcp::SessionEncryption, None, Ssl)
XI_SERIALIZATION_ENUM_TAG(Xi::Network::Tcp::SessionEncryption, None, "none")
XI_SERIALIZATION_ENUM_TAG(Xi::Network::Tcp::SessionEncryption, Ssl, "ssl")
