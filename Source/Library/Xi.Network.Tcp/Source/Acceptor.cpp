﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Network/Tcp/Acceptor.hpp"

#include <utility>

#include <Xi/Extern/Push.hh>
#include <boost/asio.hpp>
#include <boost/asio/error.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <Xi/Extern/Pop.hh>

#include <Xi/Async/Async.hpp>
#include <Xi/ScopeExit.hpp>

#include <Xi/Log/Log.hpp>
XI_LOGGER("Network/Tcp/Acceptor")

#include "Xi/Network/Tcp/Listener.hpp"

#include "PlainSession.hpp"
#include "SslSession.hpp"

using boost_acceptor = boost::asio::ip::tcp::acceptor;
using boost_error_code = boost::system::error_code;

XI_ERROR_CODE_CATEGORY_BEGIN(Xi::Network::Tcp, Acceptor)
XI_ERROR_CODE_DESC(AlreadyOpen, "acceptor is already running")
XI_ERROR_CODE_CATEGORY_END()

namespace Xi {
namespace Network {
namespace Tcp {

Result<void> Acceptor::run() {
  XI_ERROR_PROPAGATE_CATCH(doStartUp());
  XI_SUCCEED();
}

Result<void> Acceptor::shutdown() {
  XI_ERROR_PROPAGATE_CATCH(doTearDown());
  XI_SUCCEED();
}

bool Acceptor::isOpen() const {
  return m_acceptor.is_open();
}

void Acceptor::onAccept(UniqueSessionConcept session, Boost::ErrorCode ec) {
  auto& socket = session->socket();
  try {
    if (ec) {
      // TODO consider threshold doAccept if too many errors occur.
      XI_LOG_ERROR_IF(ec != boost::asio::error::operation_aborted, "Error accepting new connection: {}",
                      makeFailure(ec));
      return;
    } else {
      auto lockedHandler = m_handler.lock();
      if (lockedHandler) {
        Async::invoke(Async::Launch::Post, &Listener::onIncomingConnection, lockedHandler, std::move(session),
                      shared_from_this());
        return;
      } else {
        XI_LOG_ERROR("Could not accept incoming connection due to departed listener.");
      }
    }
  } catch (const std::exception& e) {
    XI_LOG_ERROR("Accept routine threw: {}", e.what());
  } catch (...) {
    XI_LOG_ERROR("Accept routine threw: UNKNOWN");
  }
  {
    Boost::ErrorCode iec{/* */};
    socket.cancel(iec);
    XI_LOG_ERROR_IF(iec, "Socket cancellation failed: {}", makeFailure(iec))
  }
  {
    Boost::ErrorCode iec{/* */};
    socket.close(iec);
    XI_LOG_ERROR_IF(iec, "Closing socket failed: {}", makeFailure(iec))
  }
}

Result<void> Acceptor::doStartUp() {
  XI_ERROR_TRY;
  XI_FAIL_IF(isOpen(), AcceptorError::AlreadyOpen)

  XI_LOG_TRACE("Open acceptor at '{}'", m_endpoint);

  Boost::ErrorCode ec{/* */};
  Boost::Endpoint nativeEndpoint = m_endpoint;
  m_acceptor.open(nativeEndpoint.protocol(), ec);
  if (ec) {
    XI_LOG_ERROR("Open failed: {}", makeFailure(ec));
    return makeFailure(ec);
  }

  m_acceptor.set_option(boost::asio::socket_base::reuse_address{m_reuseAddress}, ec);
  if (ec) {
    XI_LOG_ERROR("Failed setting resuse address option: {}", makeFailure(ec));
    return makeFailure(ec);
  }

  m_acceptor.bind(m_endpoint, ec);
  if (ec) {
    XI_LOG_ERROR("Binding failed: {}", makeFailure(ec));
    return makeFailure(ec);
  }

  m_acceptor.listen(boost::asio::socket_base::max_listen_connections, ec);
  if (ec) {
    XI_LOG_ERROR("Listen failed: {}", makeFailure(ec));
    return makeFailure(ec);
  }

  nativeEndpoint = m_acceptor.local_endpoint(ec);
  if (ec) {
    XI_LOG_ERROR("Failed to retrieve endpoint: {}", makeFailure(ec));
    return makeFailure(ec);
  }
  m_endpoint = nativeEndpoint;
  XI_LOG_INFO("Acceptor started at {}", m_endpoint);

  while (isOpen()) {
    UniqueSessionConcept session = makeConcept();
    Boost::ErrorCode iec{/* */};
    m_acceptor.async_accept(session->socket(), Async::yield(iec));
    onAccept(std::move(session), iec);
  }

  XI_SUCCEED();
  XI_ERROR_CATCH;
}

Result<void> Acceptor::doTearDown() {
  XI_ERROR_TRY;
  boost_error_code ec{};
  m_acceptor.cancel(ec);
  if (ec.failed()) {
    XI_LOG_ERROR("Acceptor cancellation failed: '{}'", makeFailure(ec))
    return makeFailure(ec);
  }
  m_acceptor.close(ec);
  if (ec.failed()) {
    XI_LOG_ERROR("Closing acceptor failed: '{}'", makeFailure(ec))
    return makeFailure(ec);
  }
  XI_LOG_TRACE("Acceptor ({}) closed.", m_endpoint)
  XI_SUCCEED();
  XI_ERROR_CATCH;
}

Acceptor::Acceptor() : m_reuseAddress {
#if defined(NDEBUG)
  false
#else
  true
#endif
}
, m_acceptor{Async::this_executor::sharedIo()}, m_ssl{std::nullopt} {
}

Acceptor::~Acceptor() {
  /* */
}

UniqueSessionConcept Acceptor::makeConcept() {
  if (m_ssl) {
    return std::make_unique<SslSession>(*m_ssl);
  } else {
    return std::make_unique<PlainSession>();
  }
}

}  // namespace Tcp
}  // namespace Network
}  // namespace Xi
