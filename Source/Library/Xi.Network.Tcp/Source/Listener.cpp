// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Network/Tcp/Listener.hpp"

#include <map>

#include <Xi/ScopeExit.hpp>
#include <Xi/Async/Async.hpp>
#include <Xi/Stream/OStream.hpp>

#include <Xi/Log/Log.hpp>
XI_LOGGER("Network/Tcp/Listener")

#include "PlainSession.hpp"
#include "SslSession.hpp"

XI_ERROR_CODE_CATEGORY_BEGIN(Xi::Network::Tcp, Listener)
XI_ERROR_CODE_DESC(NotRunning, "action requires a running listener, actually its not running")
XI_ERROR_CODE_DESC(IsShuttingDown, "listener is already shutting down")
XI_ERROR_CODE_DESC(AlreadyRunning, "listener is already running")
XI_ERROR_CODE_DESC(AcceptorNotFound, "acceptor not found")
XI_ERROR_CODE_DESC(AcceptorAlreadyInUse, "acceptor is already in use")
XI_ERROR_CODE_CATEGORY_END()

namespace Xi {
namespace Network {
namespace Tcp {

Listener::Listener() {
  /* */
}

Listener::~Listener() {
  /* */
}

SharedListener makeListener() {
  return SharedListener{new Listener};
}

Result<void> Listener::run() {
  XI_FAIL_IF(state() != State::Closed, ListenerError::AlreadyRunning);
  XI_ASYNC_CANCELLATION_HANDLER([this]() { shutdown(); })
  [[maybe_unused]] auto lock = Async::lock(Async::unique, m_guard);

  setState(State::Running);
  XI_SCOPE_EXIT([this]() { setState(State::Closed); })

  do {
    for (auto& acceptor : m_acceptorsToStart) {
      m_acceptorsRunning.emplace(std::piecewise_construct, std::forward_as_tuple(acceptor),
                                 std::forward_as_tuple(Async::invoke(&Acceptor::run, acceptor)));
    }
    m_acceptorsToStart.clear();

    m_stateGate.wait(lock);
  } while (state() == State::Running);

  std::vector<Async::FutureResult<void>> shutdowns{};
  shutdowns.reserve(m_acceptorsRunning.size());
  for (const auto& runningAcceptor : m_acceptorsRunning) {
    shutdowns.emplace_back(Async::invoke(&Acceptor::shutdown, runningAcceptor.first));
  }

  for (auto& shutdown : shutdowns) {
    if (auto ec = shutdown.get(); ec.isError()) {
      XI_LOG_ERROR("Error shutting down listener: {}", ec.error())
    }
  }

  for (auto& acceptor : m_acceptorsRunning) {
    const auto ec = acceptor.second.get();
    XI_LOG_ERROR_IF(ec.isError(), "Acceptor closed with error: {}", ec);
  }
  m_acceptorsRunning.clear();

  XI_SUCCEED();
}

Listener::State Listener::state() const {
  return m_state.load(std::memory_order_consume);
}

bool Listener::isRunning() const {
  return !isClosed();
}

bool Listener::isClosed() const {
  return state() == State::Closed;
}

Result<void> Listener::bind(SharedAcceptor acceptor) {
  XI_EXCEPTIONAL_IF_NOT(NullArgumentError, acceptor)

  XI_FAIL_IF(acceptor->isOpen(), ListenerError::AcceptorAlreadyInUse);
  [[maybe_unused]] auto lock = Async::lock(m_guard);
  {
    auto search = m_acceptorsRunning.find(acceptor);
    XI_FAIL_IF_NOT(search == m_acceptorsRunning.end(), ListenerError::AcceptorAlreadyInUse)
  }
  {
    auto search = m_acceptorsToStart.find(acceptor);
    XI_FAIL_IF_NOT(search == m_acceptorsToStart.end(), ListenerError::AcceptorAlreadyInUse)
  }
  XI_FAIL_IF(acceptor->m_handler.lock(), ListenerError::AcceptorAlreadyInUse);
  {
    acceptor->m_handler = shared_from_this();
    m_acceptorsToStart.emplace(acceptor);
  }
  m_stateGate.notify_one();
  XI_SUCCEED()
}

Result<void> Listener::unbind(SharedAcceptor acceptor) {
  XI_EXCEPTIONAL_IF_NOT(NullArgumentError, acceptor);
  [[maybe_unused]] auto lock = Async::lock(m_guard);
  {
    auto search = m_acceptorsRunning.find(acceptor);
    if (search != m_acceptorsRunning.end()) {
      XI_ERROR_PROPAGATE_CATCH(acceptor->shutdown())
      auto runEc = std::move(search->second);
      m_acceptorsRunning.erase(search);
      return runEc.get();
    }
  }
  {
    auto search = m_acceptorsToStart.find(acceptor);
    if (search != m_acceptorsToStart.end()) {
      m_acceptorsToStart.erase(search);
      XI_SUCCEED()
    } else {
      XI_FAIL(ListenerError::AcceptorNotFound);
    }
  }
}

void Listener::setState(const State state_) {
  XI_LOG_TRACE_IF(state() != state_, "State switch: '{}' -> '{}'", state(), state_);
  m_state.store(state_, std::memory_order_release);
  m_stateGate.notify_one();
}

std::string stringify(const Listener::State state) {
  switch (state) {
    case Listener::State::Closed:
      return "closed";
    case Listener::State::Running:
      return "running";
    case Listener::State::ShuttingDown:
      return "shutting_down";
  }
  XI_EXCEPTIONAL(InvalidEnumValueError)
}

void Listener::shutdown() {
  if (m_state.load(std::memory_order_consume) == State::Running) {
    setState(State::ShuttingDown);
  }
}

void Listener::onIncomingConnection(UniqueSessionConcept socket, SharedAcceptor source) {
  XI_EXCEPTIONAL_IF_NOT(NullArgumentError, source)
  // TODO Timeout
  auto session = sessionManager().accept(std::move(socket));
  if (session.isError()) {
    XI_LOG_ERROR("Unable to open incoming connection: {}.", session.error())
    return;
  }

  Async::sleepFor(std::chrono::milliseconds{2000});

  if (const auto ec = session->teardown(); ec.isError()) {
    XI_LOG_ERROR(ec.error().message());
  }
}

SessionManager& Listener::sessionManager() {
  static SharedSessionManager __Fallback = makeSessionManager();
  if (!m_sessionManager) {
    XI_LOG_WARN("Default fallback session manager is in use.")
    m_sessionManager = __Fallback;
  }
  return *m_sessionManager;
}

}  // namespace Tcp
}  // namespace Network
}  // namespace Xi
