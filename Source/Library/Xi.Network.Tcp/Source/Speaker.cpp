// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Network/Tcp/Speaker.hpp"

#include <Xi/Network/Uri.hpp>

#include <Xi/Log/Log.hpp>
XI_LOGGER("Network/Tcp/Speaker")

XI_ERROR_CODE_CATEGORY_BEGIN(Xi::Network::Tcp, Speaker)
XI_ERROR_CODE_DESC(InvalidPort, "invalid port (any)")
XI_ERROR_CODE_CATEGORY_END()

namespace Xi {
namespace Network {
namespace Tcp {

Result<Session> Speaker::connect(const Endpoint &ep) {
  XI_FAIL_IF(ep.port().isAny(), SpeakerError::InvalidPort);
  XI_FAIL(SpeakerError::InvalidPort);
}

SessionManager &Speaker::sessionManager() {
  static SharedSessionManager __Fallback = makeSessionManager();
  if (!m_sessionManager) {
    m_sessionManager = __Fallback;
    XI_LOG_WARN("Default fallback session manager is in use.")
  }
  return *m_sessionManager;
}

}  // namespace Tcp
}  // namespace Network
}  // namespace Xi
