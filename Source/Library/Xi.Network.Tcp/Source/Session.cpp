﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Network/Tcp/Session.hpp"

#include <utility>

#include <Xi/String.hpp>
#include <Xi/Stream/NullStream.hpp>

#include "boost/InputStream.hpp"
#include "boost/OutputStream.hpp"

#include <Xi/Log/Log.hpp>
XI_LOGGER("Network/Tcp/Session")

#include "Xi/Network/Tcp/SessionManager.hpp"

namespace Xi {
namespace Network {
namespace Tcp {

Session::Session(SharedSessionManager manager, SessionDirection dir, UniqueSessionConcept concept_)
    : m_manager{manager},
      m_direction{dir},
      m_concept{std::move(concept_)},
      m_localEndpoint{makeError(GlitchError::NotInitialized)},
      m_remoteEndoint{makeError(GlitchError::NotInitialized)} {
  XI_EXCEPTIONAL_IF_NOT(NullArgumentError, manager);
  XI_EXCEPTIONAL_IF_NOT(NullArgumentError, m_concept);

  switch (implementation().encryption()) {
    case SessionEncryption::None:
      emplacePlainStreams();
      break;
    case SessionEncryption::Ssl:
      emplaceSslStreams();
      break;
  }

  XI_EXCEPTIONAL_IF_NOT(InvalidVariantTypeError, m_input)
  XI_EXCEPTIONAL_IF_NOT(InvalidVariantTypeError, m_output)
}

Session::~Session() {
  release();
}

Session::Session(Session &&other) {
  emplace(std::move(other));
}

Session &Session::operator=(Session &&other) {
  emplace(std::move(other));
  return *this;
}

SessionEncryption Session::encryption() const {
  return implementation().encryption();
}

SessionDirection Session::direction() const {
  return m_direction;
}

Result<Endpoint> Session::localEndpoint() const {
  if (m_localEndpoint.isError()) {
    m_localEndpoint = implementation().localEndpoint();
  }
  return m_localEndpoint;
}

Result<Endpoint> Session::remoteEndpoint() const {
  if (m_remoteEndoint.isError()) {
    m_remoteEndoint = implementation().remoteEndpoint();
  }
  return m_remoteEndoint;
}

Result<void> Session::setup() {
  switch (direction()) {
    case SessionDirection::Incoming:
      return implementation().setupIncoming();
    case SessionDirection::Outgoing:
      return implementation().setupOutgoing();
  }
  XI_EXCEPTIONAL(InvalidEnumValueError)
}

Result<void> Session::teardown() {
  switch (direction()) {
    case SessionDirection::Incoming:
      return implementation().teardownIncoming();
    case SessionDirection::Outgoing:
      return implementation().teardownOutgoing();
  }
  XI_EXCEPTIONAL(InvalidEnumValueError)
}

Result<void> Session::shutdown() {
  if (auto ec = teardown(); ec.isError()) {
    XI_LOG_WARN("Error gracefully shutting down connection: '{}'.", ec.error())
  }
  return close();
}

Result<void> Session::close() {
  return implementation().close();
}

Stream::InputStream &Session::input() {
  return *m_input;
}

Stream::OutputStream &Session::output() {
  return *m_output;
}

std::string Session::stringify() const {
  return toString("{} {} {}", localEndpoint(), direction() == SessionDirection::Incoming ? "<-" : "->",
                  remoteEndpoint());
}

SessionConcept &Session::implementation() {
  return *m_concept;
}

const SessionConcept &Session::implementation() const {
  return *m_concept;
}

void Session::emplacePlainStreams() {
  m_input = std::make_unique<Boost::InputStream<Boost::Stream>>(implementation().plainStream());
  m_output = std::make_unique<Boost::OutputStream<Boost::Stream>>(implementation().plainStream());
}

void Session::emplaceSslStreams() {
  m_input = std::make_unique<Boost::InputStream<Boost::SslStream>>(implementation().sslStream());
  m_output = std::make_unique<Boost::OutputStream<Boost::SslStream>>(implementation().sslStream());
}

void Session::release() {
  if (m_concept) {
    if (const auto manager = m_manager.lock()) {
      manager->release(*this);
    } else {
      XI_LOG_WARN("Departed session manager, cannot release active session.")
    }

    m_concept.release();
  }
}

void Session::emplace(Session &&other) {
  release();

  m_manager = other.m_manager;
  other.m_manager = WeakSessionManager{/* */};

  m_direction = other.m_direction;

  m_concept = std::move(other.m_concept);
  other.m_concept.reset(nullptr);

  m_input = std::move(other.m_input);
  other.m_input.reset(nullptr);

  m_output = std::move(other.m_output);
  other.m_output.reset(nullptr);

  m_localEndpoint = std::move(other.m_localEndpoint);
  m_remoteEndoint = std::move(other.m_remoteEndoint);
}

}  // namespace Tcp
}  // namespace Network
}  // namespace Xi
