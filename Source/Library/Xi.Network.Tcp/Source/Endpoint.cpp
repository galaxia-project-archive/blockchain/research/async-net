// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Network/Tcp/Endpoint.hpp"

#include <Xi/String.hpp>
#include <algorithm>

XI_ERROR_CODE_CATEGORY_BEGIN(Xi::Network::Tcp, Endpoint)
XI_ERROR_CODE_DESC(IllFormed, "endpoint string representation is ill formed.")
XI_ERROR_CODE_CATEGORY_END()

namespace Xi {
namespace Network {
namespace Tcp {

Result<Endpoint> Endpoint::parse(const std::string &str) {
  auto seperator = str.find_first_of(':');
  XI_FAIL_IF(seperator == std::string::npos, EndpointError::IllFormed)
  XI_FAIL_IF_NOT(seperator < str.size(), EndpointError::IllFormed)
  auto address = fromString<IpAddress>(std::string{str.data(), seperator});
  XI_ERROR_PROPAGATE(address)
  auto port = fromString<Port>(std::string{str.data() + seperator + 1, str.size() - seperator - 1});
  XI_ERROR_PROPAGATE(port)
  return makeSuccess<Endpoint>(*address, *port);
}

Endpoint::Endpoint(const IpAddress &address_, Port port_) : m_address{address_}, m_port{port_} {
  /* */
}

Endpoint::Endpoint(const Endpoint::native_type &native) : m_address{native.address()}, m_port{native.port()} {
  /* */
}

Xi::Network::Tcp::Endpoint::operator native_type() const {
  return native_type{address(), port().native()};
}

const IpAddress &Endpoint::address() const {
  return m_address;
}

void Endpoint::setAddress(const IpAddress &address_) {
  m_address = address_;
}

Port Endpoint::port() const {
  return m_port;
}

void Endpoint::setPort(Port port_) {
  m_port = port_;
}

std::string Endpoint::stringify() const {
  return toString("{}:{}", address(), port());
}

Result<void> serialize(Endpoint &value, const Serialization::Tag &name, Serialization::Serializer &serializer) {
  static const Serialization::Tag AddressTag{0x0001, "address"};
  static const Serialization::Tag PortTag{0x0002, "port"};

  if (serializer.isHumanReadableFromat()) {
    std::string str;
    if (serializer.isInputMode()) {
      XI_ERROR_PROPAGATE_CATCH(serializer(str, name))
      auto parsed = fromString<Endpoint>(str);
      XI_ERROR_PROPAGATE(parsed)
      value = parsed.take();
      XI_SUCCEED()
    } else {
      str = toString(value);
      return serializer(str, name);
    }
  } else {
    XI_ERROR_PROPAGATE_CATCH(serializer.beginComplex(name))
    XI_ERROR_PROPAGATE_CATCH(serializer(value.m_address, AddressTag))
    XI_ERROR_PROPAGATE_CATCH(serializer(value.m_port, PortTag))
    return serializer.endComplex();
  }
}

}  // namespace Tcp
}  // namespace Network
}  // namespace Xi
