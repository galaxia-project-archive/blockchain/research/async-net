// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Network/Tcp/SessionDirection.hpp"

#include <Xi/String.hpp>
#include <Xi/ErrorModel.hh>

XI_ERROR_CODE_CATEGORY_BEGIN(Xi::Network::Tcp, SessionDirection)
XI_ERROR_CODE_DESC(IllFormed, "session direction string representation is invalid or unknown")
XI_ERROR_CODE_CATEGORY_END()

namespace Xi {
namespace Network {
namespace Tcp {

std::string stringify(const SessionDirection direction) {
  switch (direction) {
    case SessionDirection::Incoming:
      return "incoming";
    case SessionDirection::Outgoing:
      return "outgoing";
  }
  XI_EXCEPTIONAL(InvalidEnumValueError)
}

Result<void> parse(const std::string &str, SessionDirection &out) {
  const auto relaxed = relax(str);
  if (relaxed == toString(SessionDirection::Incoming)) {
    out = SessionDirection::Incoming;
    XI_SUCCEED()
  } else if (relaxed == toString(SessionDirection::Outgoing)) {
    out = SessionDirection::Outgoing;
    XI_SUCCEED()
  } else {
    XI_FAIL(SessionDirectionError::IllFormed)
  }
}

}  // namespace Tcp
}  // namespace Network
}  // namespace Xi
