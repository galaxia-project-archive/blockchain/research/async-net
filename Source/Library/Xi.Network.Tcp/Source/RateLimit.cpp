// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Network/Tcp/RateLimit.hpp"

namespace Xi {
namespace Network {
namespace Tcp {

Memory::Bytes RateLimit::read() const {
  return m_read;
}

void RateLimit::setRead(const Memory::Bytes limit) {
  m_read = limit;
}

Memory::Bytes RateLimit::write() const {
  return m_write;
}

void RateLimit::setWrite(const Memory::Bytes limit) {
  m_write = limit;
}

XI_SERIALIZATION_COMPLEX_EXTERN_BEGIN(RateLimit)
XI_SERIALIZATION_MEMBER(m_read, 0x0001, "read")
XI_SERIALIZATION_MEMBER(m_write, 0x0002, "write")
XI_SERIALIZATION_COMPLEX_EXTERN_END

}  // namespace Tcp
}  // namespace Network
}  // namespace Xi
