// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <Xi/Global.hh>
#include <Xi/ErrorModel.hh>
#include <Xi/Async/Async.hpp>
#include <Xi/Stream/OutputStream.hpp>
#include <Xi/Stream/StreamError.hpp>

#include "Xi/Network/Tcp/Boost/Boost.hpp"

namespace Xi {
namespace Network {
namespace Tcp {
namespace Boost {

template <typename _StreamT>
class OutputStream final : public Xi::Stream::OutputStream {
 private:
  _StreamT& m_stream;

 public:
  OutputStream(_StreamT& stream) : m_stream{stream} {
    /* */
  }
  ~OutputStream() override = default;

  inline Result<size_t> write(ConstByteSpan buffer) override {
    Boost::ErrorCode ec{/* */};
    size_t transfered = 0;
    XI_ASYNC_CANCELLATION_HANDLER([this]() {
      try {
        Boost::get_lowest_layer(m_stream).cancel();
      } catch (...) {
        /* Swallow */
      }
    })
    m_stream.async_write_some(boost::asio::const_buffer{buffer.data(), buffer.size()}, Async::yield(ec, transfered));
    XI_ERROR_CODE_PROPAGATE(ec);
    XI_SUCCEED(transfered);
  }

  inline Result<void> flush() override {
    XI_SUCCEED();
  }
};

}  // namespace Boost
}  // namespace Tcp
}  // namespace Network
}  // namespace Xi
