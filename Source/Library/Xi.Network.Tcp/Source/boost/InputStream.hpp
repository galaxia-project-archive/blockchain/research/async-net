﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <Xi/Global.hh>
#include <Xi/ErrorModel.hh>
#include <Xi/Async/Async.hpp>
#include <Xi/Stream/InputStream.hpp>
#include <Xi/Stream/StreamError.hpp>

#include "Xi/Network/Tcp/Boost/Boost.hpp"

namespace Xi {
namespace Network {
namespace Tcp {
namespace Boost {

template <typename _StreamT>
class InputStream final : public Xi::Stream::InputStream {
 private:
  _StreamT& m_stream;

 public:
  InputStream(_StreamT& stream) : m_stream{stream} {
    /* */
  }
  ~InputStream() override = default;

  [[nodiscard]] inline Result<size_t> read(ByteSpan buffer) override {
    Boost::ErrorCode ec{/* */};
    size_t transfered = 0;
    XI_ASYNC_CANCELLATION_HANDLER([this]() {
      try {
        Boost::get_lowest_layer(m_stream).cancel();
      } catch (...) {
        /* Swallow */
      }
    })
    m_stream.async_read_some(boost::asio::mutable_buffer{buffer.data(), buffer.size()}, Async::yield(ec, transfered));
    if (ec.failed()) {
      if (ec == boost::asio::error::eof) {
        XI_SUCCEED(0U);
      } else {
        return ::Xi::makeError(ec);
      }
    } else {
      XI_SUCCEED(transfered);
    }
  }

  [[nodiscard]] inline Result<bool> isEndOfStream() const override {
    XI_SUCCEED(!get_lowest_layer(m_stream).socket().is_open());
  }

  [[nodiscard]] inline Result<size_t> tell() const override {
    XI_FAIL(Xi::Stream::StreamError::Unsupported);
  }

  [[nodiscard]] inline Result<Byte> peek() const override {
    XI_FAIL(Xi::Stream::StreamError::Unsupported);
  }

  [[nodiscard]] inline Result<Byte> take() override {
    XI_FAIL(Xi::Stream::StreamError::Unsupported);
  }
};

}  // namespace Boost
}  // namespace Tcp
}  // namespace Network
}  // namespace Xi
