﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Network/Tcp/AcceptorBuilder.hpp"

#include <utility>

namespace Xi {
namespace Network {
namespace Tcp {

AcceptorBuilder::AcceptorBuilder() {
  /* */
}

AcceptorBuilder &AcceptorBuilder::withEndpoint(const Endpoint &endpoint) {
  m_endpoint = endpoint;
  return *this;
}

AcceptorBuilder &AcceptorBuilder::withIpAddress(const IpAddress ip) {
  m_endpoint.setAddress(ip);
  return *this;
}

AcceptorBuilder &AcceptorBuilder::withPort(const Port port) {
  m_endpoint.setPort(port);
  return *this;
}

AcceptorBuilder &AcceptorBuilder::withAddressReuse(bool reuseAddress) {
  m_addressReuse = reuseAddress;
  return *this;
}

AcceptorBuilder &AcceptorBuilder::withSsl(const std::optional<Ssl::Config> &ssl) {
  m_ssl = ssl;
  return *this;
}

AcceptorBuilder &Tcp::AcceptorBuilder::withoutSsl() {
  return withSsl(std::nullopt);
}

Result<SharedAcceptor> AcceptorBuilder::build() {
  SharedAcceptor reval{new Acceptor{/* */}};
  reval->m_endpoint = m_endpoint;
  if (m_addressReuse) {
    reval->m_reuseAddress = *m_addressReuse;
  }
  if (m_ssl) {
    auto ssl = m_ssl->construct();
    XI_ERROR_PROPAGATE(ssl);
    reval->m_ssl.emplace(ssl.take());
  }
  return makeSuccess(std::move(reval));
}

}  // namespace Tcp
}  // namespace Network
}  // namespace Xi
