// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Network/Tcp/SessionManager.hpp"

#include <Xi/Log/Log.hpp>
XI_LOGGER("Tcp/SessionManager")

namespace Xi::Network::Tcp {

SessionManager::_DirectionalData::_DirectionalData(size_t max) : maxOpen{max} {
  /* */
}

SessionManager::SessionManager() {
  /* */
}

SessionManager::~SessionManager() {
  /* */
}

Result<Session> SessionManager::accept(UniqueSessionConcept session) {
  auto slot = acquireSlot(SessionDirection::Incoming, m_incoming);
  XI_ERROR_PROPAGATE(slot)
  Session reval{shared_from_this(), SessionDirection::Incoming, std::move(session)};
  XI_ERROR_PROPAGATE_CATCH(reval.setup());
  XI_SUCCEED(std::move(reval))
}

Result<Session> SessionManager::connect(UniqueSessionConcept session) {
  auto slot = acquireSlot(SessionDirection::Outgoing, m_outgoing);
  XI_ERROR_PROPAGATE(slot)
  Session reval{shared_from_this(), SessionDirection::Outgoing, std::move(session)};
  XI_ERROR_PROPAGATE_CATCH(reval.setup());
  XI_SUCCEED(std::move(reval))
}

void SessionManager::release(const Session &session) {
  switch (session.direction()) {
    case SessionDirection::Incoming:
      release(SessionDirection::Incoming, m_incoming);
      return;
    case SessionDirection::Outgoing:
      release(SessionDirection::Outgoing, m_outgoing);
      return;
  }
}

Result<void> SessionManager::acquireSlot(SessionDirection direction, SessionManager::_DirectionalData &data) {
  Async::UniqueLock<Async::Mutex> lck{data.guard};
  while (data.currentOpen >= data.maxOpen) {
    XI_LOG_TRACE("Waiting for {} connection slot.", direction)
    data.gate.wait(lck);
  }
  // TODO tear down operation
  data.currentOpen++;
  XI_LOG_TRACE("{} session slot acquired (current {}/{}).", direction, data.currentOpen, data.maxOpen)
  XI_SUCCEED()
}

void SessionManager::release(SessionDirection direction, SessionManager::_DirectionalData &data) {
  XI_EXCEPTIONAL_IF(OutOfRangeError, data.currentOpen == 0)
  data.currentOpen -= 1;
  data.gate.notify_one();
  XI_LOG_TRACE("{} session slot released (current {}/{}).", direction, data.currentOpen, data.maxOpen)
}

SharedSessionManager makeSessionManager() {
  return SharedSessionManager{new SessionManager};
}

}  // namespace Xi::Network::Tcp
