// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Crypto/EllipticCurve/SignatureError.hpp"

XI_ERROR_CODE_CATEGORY_BEGIN(Xi::Crypto::EllipticCurve, Signature)
XI_ERROR_CODE_DESC(HashComputationFailed, "unable to compute message hash")
XI_ERROR_CODE_DESC(InvalidPoint, "provided elliptic curve point is invalid")
XI_ERROR_CODE_DESC(RandomGenerationFailed, "random generator failed to provide random bytes")
XI_ERROR_CODE_DESC(KeyDerivationFailed, "derivation of an elliptic curve structure from a hash/message failed")
XI_ERROR_CODE_CATEGORY_END();
