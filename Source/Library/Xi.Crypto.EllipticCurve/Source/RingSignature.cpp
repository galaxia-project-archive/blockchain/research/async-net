// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Crypto/EllipticCurve/RingSignature.hpp"

#include "Xi/Crypto/EllipticCurve/Scalar.hpp"
#include "bernstein/Bernstein.hh"

#include <Xi/ErrorModel.hh>

#define XI_LOG_VERBOSE_ENABLE
#include <Xi/Log/Log.hpp>

XI_LOGGER("Crypto/EllipticCurve/RingSignature")

Xi::Result<Xi::Crypto::EllipticCurve::RingSignature> Xi::Crypto::EllipticCurve::RingSignature::sign(
    const Xi::Crypto::EllipticCurve::Hash& messageHash, const Xi::Crypto::EllipticCurve::Point& image,
    Xi::Crypto::EllipticCurve::ConstPointSpan publicKeys, const Scalar& secretKey, const size_t secretKeyIndex) {
  XI_EXCEPTIONAL_IF_NOT(OutOfRangeError, secretKeyIndex < publicKeys.size());
  XI_EXCEPTIONAL_IF(InvalidArgumentError, secretKey.toPoint() != secretKey.toPoint());

  ge_p3 image_unp;
  auto ec = ge_frombytes_vartime(&image_unp, image.data());
  XI_RETURN_EC_IF(ec != XI_RETURN_CODE_SUCCESS, makeFailure(SignatureError::InvalidPoint));
  ge_dsmp image_pre;
  ge_dsm_precomp(image_pre, &image_unp);

  RingSignature reval{};
  reval.m_image = std::move(image);
  reval.m_signatures.resize(publicKeys.size());

  Scalar sum{zero()};

  xi_crypto_elliptic_curve_hash_state state;
  ec = xi_crypto_elliptic_curve_hash_init(&state);
  XI_RETURN_EC_IF(ec != XI_RETURN_CODE_SUCCESS, makeFailure(SignatureError::HashComputationFailed));
  ec = xi_crypto_elliptic_curve_hash_update(&state, messageHash.data(), messageHash.size());
  XI_RETURN_EC_IF(ec != XI_RETURN_CODE_SUCCESS, makeFailure(SignatureError::HashComputationFailed));

  Scalar k;
  xi_byte_t buffer[XI_CRYPTO_ELLIPTIC_CURVE_POINT_SIZE];
  for (size_t i = 0; i < publicKeys.size(); ++i) {
    ge_p2 tmp2;
    ge_p3 tmp3;

    const auto& iPublicKey = publicKeys[i];

    if (i == secretKeyIndex) {
      XI_RETURN_EC_IF_NOT(random(k), makeFailure(SignatureError::RandomGenerationFailed));
      ge_scalarmult_base(&tmp3, k.data());
      ge_p3_tobytes(buffer, &tmp3);
      ec = xi_crypto_elliptic_curve_hash_update(&state, buffer, XI_CRYPTO_ELLIPTIC_CURVE_POINT_SIZE);
      XI_RETURN_EC_IF(ec != XI_RETURN_CODE_SUCCESS, makeFailure(SignatureError::HashComputationFailed));

      ec = ge_p3_from_message(&tmp3, iPublicKey.data(), iPublicKey.size());
      XI_RETURN_EC_IF(ec != XI_RETURN_CODE_SUCCESS, makeFailure(SignatureError::HashComputationFailed));
      ge_scalarmult(&tmp2, k.data(), &tmp3);
      ge_tobytes(buffer, &tmp2);
      ec = xi_crypto_elliptic_curve_hash_update(&state, buffer, XI_CRYPTO_ELLIPTIC_CURVE_POINT_SIZE);
      XI_RETURN_EC_IF(ec != XI_RETURN_CODE_SUCCESS, makeFailure(SignatureError::HashComputationFailed));
    } else {
      auto& iSignature = reval.m_signatures[i];
      ec = sc_random(iSignature.mutableFirst());
      XI_RETURN_EC_IF(ec != XI_RETURN_CODE_SUCCESS, makeFailure(SignatureError::RandomGenerationFailed));
      ec = sc_random(iSignature.mutableSecond());
      XI_RETURN_EC_IF(ec != XI_RETURN_CODE_SUCCESS, makeFailure(SignatureError::RandomGenerationFailed));
      ec = ge_frombytes_vartime(&tmp3, iPublicKey.data());
      XI_RETURN_EC_IF(ec != XI_RETURN_CODE_SUCCESS, makeFailure(SignatureError::InvalidPoint));
      ge_double_scalarmult_base_vartime(&tmp2, iSignature.first(), &tmp3, iSignature.second());
      ge_tobytes(buffer, &tmp2);
      ec = xi_crypto_elliptic_curve_hash_update(&state, buffer, XI_CRYPTO_ELLIPTIC_CURVE_POINT_SIZE);
      XI_RETURN_EC_IF(ec != XI_RETURN_CODE_SUCCESS, makeFailure(SignatureError::HashComputationFailed));
      ec = ge_p3_from_message(&tmp3, iPublicKey.data(), iPublicKey.size());
      XI_RETURN_EC_IF(ec != XI_RETURN_CODE_SUCCESS, makeFailure(SignatureError::KeyDerivationFailed));
      ge_double_scalarmult_precomp_vartime(&tmp2, iSignature.second(), &tmp3, iSignature.first(), image_pre);
      ge_tobytes(buffer, &tmp2);
      ec = xi_crypto_elliptic_curve_hash_update(&state, buffer, XI_CRYPTO_ELLIPTIC_CURVE_POINT_SIZE);
      XI_RETURN_EC_IF(ec != XI_RETURN_CODE_SUCCESS, makeFailure(SignatureError::HashComputationFailed));
      sc_add(sum.mutableData(), sum.data(), iSignature.first());
    }
  }

  ec = xi_crypto_elliptic_curve_hash_finish(&state, buffer);
  XI_RETURN_EC_IF(ec != XI_RETURN_CODE_SUCCESS, makeFailure(SignatureError::HashComputationFailed));
  sc_reduce32(buffer);
  auto& secSignature = reval.m_signatures[secretKeyIndex];
  sc_sub(secSignature.mutableFirst(), buffer, sum.data());
  sc_mulsub(secSignature.mutableSecond(), secSignature.first(), secretKey.data(), k.data());

  return makeSuccess(std::move(reval));
}

Xi::Result<Xi::Crypto::EllipticCurve::RingSignature> Xi::Crypto::EllipticCurve::RingSignature::sign(
    Xi::ConstByteSpan message, const Xi::Crypto::EllipticCurve::Point& image,
    Xi::Crypto::EllipticCurve::ConstPointSpan publicKeys, const Scalar& secretKey, const size_t secretKeyIndex) {
  Hash messageHash;
  if (const auto ec = compute(message, messageHash); ec != Crypto::Hash::HashError::Success) {
    return makeFailure(ec);
  }
  return sign(messageHash, image, publicKeys, secretKey, secretKeyIndex);
}

bool Xi::Crypto::EllipticCurve::RingSignature::validate(const Xi::Crypto::EllipticCurve::Hash& messageHash,
                                                        ConstPointSpan publicKeys) const {
  XI_RETURN_EC_IF(publicKeys.size() != m_signatures.size(), false);

  ge_p3 image_unp;
  XI_RETURN_EC_IF(ge_frombytes_vartime(&image_unp, m_image.data()) != XI_RETURN_CODE_SUCCESS, false);
  ge_dsmp image_pre;
  ge_dsm_precomp(image_pre, &image_unp);
  XI_RETURN_EC_IF(ge_check_subgroup_precomp_vartime(image_pre) != 0, false);

  Scalar sum{zero()};

  xi_crypto_elliptic_curve_hash_state state;
  if (const auto ec = xi_crypto_elliptic_curve_hash_init(&state); ec != XI_RETURN_CODE_SUCCESS) {
    XI_LOG_VERBOSE("Hash state initialization failed: {}", ec);
    return false;
  }
  if (const auto ec = xi_crypto_elliptic_curve_hash_update(&state, messageHash.data(), messageHash.size());
      ec != XI_RETURN_CODE_SUCCESS) {
    XI_LOG_VERBOSE("Hash state update failed: {}", ec);
    return false;
  }

  Hash buffer;
  static_assert(Hash::bytes() == Point::bytes(), "hash and point size must match.");

  for (size_t i = 0; i < m_signatures.size(); ++i) {
    const auto& iSignature = m_signatures[i];
    const auto& iPublicKey = publicKeys[i];

    ge_p2 tmp2;
    ge_p3 tmp3;
    ge_p2 tmp4;
    ge_p1p1 tmp5;

    XI_RETURN_EC_IF(sc_check(iSignature.first()) != 0, false);
    XI_RETURN_EC_IF(sc_check(iSignature.second()) != 0, false);
    XI_RETURN_EC_IF(ge_frombytes_vartime(&tmp3, iPublicKey.data()) != XI_RETURN_CODE_SUCCESS, false);

    ge_double_scalarmult_base_vartime(&tmp2, iSignature.first(), &tmp3, iSignature.second());
    ge_tobytes(buffer.data(), &tmp2);
    if (const auto ec = xi_crypto_elliptic_curve_hash_update(&state, buffer.data(), buffer.size());
        ec != XI_RETURN_CODE_SUCCESS) {
      XI_LOG_VERBOSE("Hash state update failed: {}", ec);
      return false;
    }

    if (const auto ec = compute(iPublicKey.span(), buffer); ec != Crypto::Hash::HashError::Success) {
      XI_LOG_VERBOSE("Hash computation failed: {}", ec);
      return false;
    }
    ge_fromfe_frombytes_vartime(&tmp4, buffer.data());
    ge_mul8(&tmp5, &tmp4);
    ge_p1p1_to_p3(&tmp3, &tmp5);

    ge_double_scalarmult_precomp_vartime(&tmp2, iSignature.second(), &tmp3, iSignature.first(), image_pre);
    ge_tobytes(buffer.data(), &tmp2);
    if (const auto ec = xi_crypto_elliptic_curve_hash_update(&state, buffer.data(), buffer.size());
        ec != XI_RETURN_CODE_SUCCESS) {
      XI_LOG_VERBOSE("Hash state update failed: {}", ec);
      return false;
    }
    sc_add(sum.mutableData(), sum.data(), iSignature.first());
  }

  if (const auto ec = xi_crypto_elliptic_curve_hash_finish(&state, buffer.data()); ec != XI_RETURN_CODE_SUCCESS) {
    XI_LOG_VERBOSE("Hash finalization failed: {}", ec);
    return false;
  }
  sc_reduce32(buffer.data());
  sc_sub(buffer.data(), buffer.data(), sum.data());
  return sc_isnonzero(buffer.data()) == 0;
}

bool Xi::Crypto::EllipticCurve::RingSignature::validate(Xi::ConstByteSpan message, ConstPointSpan publicKeys) const {
  Hash messageHash;
  if (const auto ec = compute(message, messageHash); ec != Crypto::Hash::HashError::Success) {
    XI_LOG_VERBOSE("Signature message hash computations failed: {}", ec);
    XI_RETURN_EC(false);
  }
  return validate(messageHash, publicKeys);
}
