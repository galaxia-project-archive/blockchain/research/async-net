// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Crypto/EllipticCurve/KeyDerivationError.hpp"

XI_ERROR_CODE_CATEGORY_BEGIN(Xi::Crypto::EllipticCurve, KeyDerivation)
XI_ERROR_CODE_DESC(HashComputationFailed, "unable to compute message hash")
XI_ERROR_CODE_DESC(InvalidDerivation, "key was derived but is invalid")
XI_ERROR_CODE_DESC(RandomGenerationFailed, "random generator failed to produce")
XI_ERROR_CODE_CATEGORY_END();
