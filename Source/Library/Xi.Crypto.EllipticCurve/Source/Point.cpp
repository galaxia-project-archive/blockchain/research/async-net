// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Crypto/EllipticCurve/Point.hpp"

#include <utility>

#include <Xi/ErrorModel.hh>

#include "Xi/Crypto/EllipticCurve/Scalar.hpp"
#include "Xi/Crypto/EllipticCurve/KeyDerivationError.hpp"
#include "bernstein/Bernstein.hh"

const Xi::Crypto::EllipticCurve::Point Xi::Crypto::EllipticCurve::Point::Null{
    {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
     0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}};

Xi::Result<Xi::Crypto::EllipticCurve::Point> Xi::Crypto::EllipticCurve::Point::generateKeyImage(
    const Xi::Crypto::EllipticCurve::Point &publicKey, const Scalar &secretKey) {
  XI_EXCEPTIONAL_IF_NOT(InvalidArgumentError, publicKey.isValid());
  XI_EXCEPTIONAL_IF_NOT(InvalidArgumentError, secretKey.isValid());

  ge_p3 point;
  ge_p2 point2;

  Point reval;
  if (ge_p3_from_message(&point, publicKey.data(), publicKey.size()) != XI_RETURN_CODE_SUCCESS) {
    return makeFailure(KeyDerivationError::HashComputationFailed);
  }
  ge_scalarmult(&point2, secretKey.data(), &point);
  ge_tobytes(reval.mutableData(), &point2);
  return makeSuccess(std::move(reval));
}

Xi::Result<Xi::Crypto::EllipticCurve::Point> Xi::Crypto::EllipticCurve::Point::generateKeyDerivation(
    const Xi::Crypto::EllipticCurve::Point &publicKey, const Scalar &secretKey) {
  XI_EXCEPTIONAL_IF_NOT(InvalidArgumentError, publicKey.isValid());
  XI_EXCEPTIONAL_IF_NOT(InvalidArgumentError, secretKey.isValid());

  ge_p3 point;
  ge_p2 point2;
  ge_p1p1 point3;

  XI_RETURN_EC_IF(ge_frombytes_vartime(&point, publicKey.data()) != XI_RETURN_CODE_SUCCESS,
                  makeFailure(KeyDerivationError::InvalidDerivation));
  ge_scalarmult(&point2, secretKey.data(), &point);
  ge_mul8(&point3, &point2);
  ge_p1p1_to_p2(&point2, &point3);

  Point reval;
  ge_tobytes(reval.mutableData(), &point2);
  return makeSuccess(std::move(reval));
}

Xi::Result<Xi::Crypto::EllipticCurve::Point> Xi::Crypto::EllipticCurve::Point::fromKeyDerivation(
    const Xi::Crypto::EllipticCurve::Point &derivation, uint64_t output_index,
    const Xi::Crypto::EllipticCurve::Point &base) {
  XI_EXCEPTIONAL_IF_NOT(InvalidArgumentError, derivation.isValid());
  XI_EXCEPTIONAL_IF_NOT(InvalidArgumentError, base.isValid());

  ge_p3 point1;
  ge_p3 point2;
  ge_cached point3;
  ge_p1p1 point4;
  ge_p2 point5;

  if (ge_frombytes_vartime(&point1, base.data()) != XI_RETURN_CODE_SUCCESS) {
    return makeFailure(KeyDerivationError::InvalidDerivation);
  }
  auto scalar = Scalar::fromKeyDerivation(derivation, output_index);
  XI_RETURN_EC_IF(scalar.isError(), scalar.error());
  ge_scalarmult_base(&point2, scalar->data());
  ge_p3_to_cached(&point3, &point2);
  ge_add(&point4, &point1, &point3);
  ge_p1p1_to_p2(&point5, &point4);

  Point reval;
  ge_tobytes(reval.mutableData(), &point5);
  return makeSuccess(std::move(reval));
}

bool Xi::Crypto::EllipticCurve::Point::isValid() const {
  ge_p3 _;
  XI_RETURN_EC_IF_NOT(ge_frombytes_vartime(&_, data()) == XI_RETURN_CODE_SUCCESS, false);
  XI_RETURN_EC_IF_NOT(ge_p3_is_point_at_infinity(&_) == false, false);
  return true;
}

bool Xi::Crypto::EllipticCurve::Point::operator!() const {
  return !isValid();
}

Xi::Crypto::EllipticCurve::Point::operator bool() const {
  return isValid();
}

Xi::Crypto::EllipticCurve::Point Xi::Crypto::EllipticCurve::Point::operator+(
    const Xi::Crypto::EllipticCurve::Point &rhs) const {
  Point reval = *this;
  reval += rhs;
  return reval;
}

Xi::Crypto::EllipticCurve::Point &Xi::Crypto::EllipticCurve::Point::operator+=(
    const Xi::Crypto::EllipticCurve::Point &rhs) {
  ge_p3 pthis, prhs;
  XI_EXCEPTIONAL_IF_NOT(InvalidArgumentError, ge_frombytes_vartime(&pthis, data()) == XI_RETURN_CODE_SUCCESS);
  XI_EXCEPTIONAL_IF_NOT(InvalidArgumentError, ge_frombytes_vartime(&prhs, rhs.data()) == XI_RETURN_CODE_SUCCESS);
  ge_cached tmp2;
  ge_p3_to_cached(&tmp2, &prhs);
  ge_p1p1 tmp3;
  ge_add(&tmp3, &pthis, &tmp2);
  ge_p1p1_to_p3(&pthis, &tmp3);
  ge_p3_tobytes(mutableData(), &pthis);
  return *this;
}
