// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "bernstein/HashDerivation.hh"

#include <Xi/Global.hh>
#include <Xi/ErrorModel.hh>

#include "Xi/Crypto/EllipticCurve/Hash.hh"

void ge_p3_from_hash(ge_p3* out, const xi_byte_t* hash) {
  ge_p2 point;
  ge_p1p1 point2;
  ge_fromfe_frombytes_vartime(&point, hash);
  ge_mul8(&point2, &point);
  ge_p1p1_to_p3(out, &point2);
}

int ge_p3_from_message(ge_p3* out, const xi_byte_t* message, size_t length) {
  xi_byte_t messageHash[XI_CRYPTO_ELLIPTIC_CURVE_HASH_SIZE];
  const int ec = xi_crypto_elliptic_curve_hash(message, length, messageHash);
  XI_RETURN_EC_IF(ec != XI_RETURN_CODE_SUCCESS, ec);
  ge_p3_from_hash(out, messageHash);
  return XI_RETURN_CODE_SUCCESS;
}
