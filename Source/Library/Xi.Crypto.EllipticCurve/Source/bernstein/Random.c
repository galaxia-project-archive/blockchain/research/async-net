// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "bernstein/Random.hh"

#include <Xi/Global.hh>
#include <Xi/ErrorModel.hh>

#include "Xi/Crypto/EllipticCurve/Constants.hh"
#include "bernstein/Operations.hh"

int sc_random(xi_byte_t *out) {
  int ec = XI_RETURN_CODE_SUCCESS;
  do {
    ec = xi_crypto_random_bytes(out, XI_CRYPTO_ELLIPTIC_CURVE_SCALAR_SIZE);
    XI_RETURN_EC_IF_NOT(ec == XI_RETURN_CODE_SUCCESS, ec);
  } while (!sc_isnonzero(out) && !sc_less_32(out, xi_crypto_elliptic_curve_constants_curveOrder));
  sc_reduce32(out);
  return XI_RETURN_CODE_SUCCESS;
}

int sc_random_deterministic(xi_byte_t *out, const xi_byte_t *seed, size_t length) {
  xi_crypto_random_state *rng = xi_crypto_random_state_create();
  XI_RETURN_EC_IF(rng == NULL, XI_RETURN_CODE_NO_SUCCESS);
  int ec = xi_crypto_random_state_init_deterministic(rng, seed, length);
  if (ec != XI_RETURN_CODE_SUCCESS) {
    xi_crypto_random_state_destroy(rng);
    return XI_RETURN_CODE_NO_SUCCESS;
  }

  do {
    ec = xi_crypto_random_bytes_from_state_deterministic(out, XI_CRYPTO_ELLIPTIC_CURVE_SCALAR_SIZE,
                                                         rng);
    if (ec != XI_RETURN_CODE_SUCCESS) {
      xi_crypto_random_state_destroy(rng);
      return XI_RETURN_CODE_NO_SUCCESS;
    }
  } while (!sc_isnonzero(out) && !sc_less_32(out, xi_crypto_elliptic_curve_constants_curveOrder));
  sc_reduce32(out);
  xi_crypto_random_state_destroy(rng);
  return XI_RETURN_CODE_SUCCESS;
}
