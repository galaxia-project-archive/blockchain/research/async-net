// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include "Xi/Crypto/Random/Random.hh"

#if defined(__cplusplus)
extern "C" {
#endif

#include <stdlib.h>

int sc_random(xi_byte_t* out);
int sc_random_deterministic(xi_byte_t* out, const xi_byte_t* seed, size_t length);

#if defined(__cplusplus)
}
#endif
