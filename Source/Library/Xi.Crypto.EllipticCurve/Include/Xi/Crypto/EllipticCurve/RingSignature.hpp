// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <Xi/Global.hh>
#include <Xi/ErrorModel.hh>

#include "Xi/Crypto/EllipticCurve/Hash.hh"
#include "Xi/Crypto/EllipticCurve/Point.hpp"
#include "Xi/Crypto/EllipticCurve/Signature.hpp"

namespace Xi {
namespace Crypto {
namespace EllipticCurve {

struct RingSignature {
 public:
  static Result<RingSignature> sign(const Hash& messageHash, const Point& image,
                                    ConstPointSpan publicKeys, const Scalar& secretKey,
                                    const size_t secretKeyIndex);
  static Result<RingSignature> sign(ConstByteSpan message, const Point& image,
                                    ConstPointSpan publicKeys, const Scalar& secretKey,
                                    const size_t secretKeyIndex);

 public:
  [[nodiscard]] bool validate(const Hash& messageHash, ConstPointSpan publicKeys) const;
  [[nodiscard]] bool validate(ConstByteSpan message, ConstPointSpan publicKeys) const;

 private:
  Point m_image;
  SignatureVector m_signatures;
};

}  // namespace EllipticCurve
}  // namespace Crypto
}  // namespace Xi
