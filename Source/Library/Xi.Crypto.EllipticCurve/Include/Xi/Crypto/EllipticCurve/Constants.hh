// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <Xi/Byte.hh>

#if defined(__cplusplus)
extern "C" {
#endif

#define XI_CRYPTO_ELLIPTIC_CURVE_POINT_SIZE (32U)
#define XI_CRYPTO_ELLIPTIC_CURVE_SCALAR_SIZE (32U)
#define XI_CRYPTO_ELLIPTIC_CURVE_SIGNATURE_SIZE (64U)

extern const xi_byte_t xi_crypto_elliptic_curve_constants_identity[32];
extern const xi_byte_t xi_crypto_elliptic_curve_constants_basePoint[32];
extern const xi_byte_t xi_crypto_elliptic_curve_constants_curveOrder[32];
extern const xi_byte_t xi_crypto_elliptic_curve_constants_commitment[32];
extern const xi_byte_t xi_crypto_elliptic_curve_constants_one[32];
extern const xi_byte_t xi_crypto_elliptic_curve_constants_minusOne[32];
extern const xi_byte_t xi_crypto_elliptic_curve_constants_zero[32];
extern const xi_byte_t xi_crypto_elliptic_curve_constants_two[32];
extern const xi_byte_t xi_crypto_elliptic_curve_constants_eight[32];
extern const xi_byte_t xi_crypto_elliptic_curve_constants_eightInverse[32];
extern const xi_byte_t xi_crypto_elliptic_curve_constants_minusEightInverse[32];

#if defined(__cplusplus)
}
#endif

#if defined(__cplusplus)

#include <cinttypes>

namespace Xi {
namespace Crypto {
namespace EllipticCurve {

inline constexpr size_t pointSize() {
  return XI_CRYPTO_ELLIPTIC_CURVE_POINT_SIZE;
}
inline constexpr size_t scalarSize() {
  return XI_CRYPTO_ELLIPTIC_CURVE_SCALAR_SIZE;
}
inline constexpr size_t signatureSize() {
  return XI_CRYPTO_ELLIPTIC_CURVE_SIGNATURE_SIZE;
}

#define XI_CRYPTO_ELLIPTIC_CURVE_CONSTANT_DEFINE(NAME)  \
  struct NAME##_t {                                     \
   public:                                              \
    const Xi::Byte *data() const {                      \
      return xi_crypto_elliptic_curve_constants_##NAME; \
    }                                                   \
  };                                                    \
  inline NAME##_t NAME() {                              \
    return NAME##_t{};                                  \
  }

XI_CRYPTO_ELLIPTIC_CURVE_CONSTANT_DEFINE(identity)
XI_CRYPTO_ELLIPTIC_CURVE_CONSTANT_DEFINE(basePoint)
XI_CRYPTO_ELLIPTIC_CURVE_CONSTANT_DEFINE(curveOrder)
XI_CRYPTO_ELLIPTIC_CURVE_CONSTANT_DEFINE(commitment)
XI_CRYPTO_ELLIPTIC_CURVE_CONSTANT_DEFINE(one)
XI_CRYPTO_ELLIPTIC_CURVE_CONSTANT_DEFINE(minusOne)
XI_CRYPTO_ELLIPTIC_CURVE_CONSTANT_DEFINE(zero)
XI_CRYPTO_ELLIPTIC_CURVE_CONSTANT_DEFINE(two)
XI_CRYPTO_ELLIPTIC_CURVE_CONSTANT_DEFINE(eight)
XI_CRYPTO_ELLIPTIC_CURVE_CONSTANT_DEFINE(eightInverse)
XI_CRYPTO_ELLIPTIC_CURVE_CONSTANT_DEFINE(minusEightInverse)

#undef XI_CRYPTO_ELLIPTIC_CURVE_CONSTANT_DEFINE

}  // namespace EllipticCurve
}  // namespace Crypto
}  // namespace Xi

#endif
