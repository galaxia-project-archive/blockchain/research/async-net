// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <Xi/ErrorModel.hh>

namespace Xi {
namespace Crypto {
namespace EllipticCurve {

XI_ERROR_CODE_BEGIN(KeyDerivation)

/// Hash computation failed.
XI_ERROR_CODE_VALUE(HashComputationFailed, 0x0001)

/// Reduction succeeded but the result is invalid.
XI_ERROR_CODE_VALUE(InvalidDerivation, 0x0002)

/// The random generator used for the derivation failed.
XI_ERROR_CODE_VALUE(RandomGenerationFailed, 0x0003)

XI_ERROR_CODE_END(KeyDerivation, "key derivation operation failed")

}  // namespace EllipticCurve
}  // namespace Crypto
}  // namespace Xi

XI_ERROR_CODE_OVERLOADS(Xi::Crypto::EllipticCurve, KeyDerivation)
