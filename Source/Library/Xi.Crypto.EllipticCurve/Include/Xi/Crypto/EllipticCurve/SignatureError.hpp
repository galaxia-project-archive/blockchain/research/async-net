// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <Xi/ErrorModel.hh>

namespace Xi {
namespace Crypto {
namespace EllipticCurve {

XI_ERROR_CODE_BEGIN(Signature)

/// Hash computation failed.
XI_ERROR_CODE_VALUE(HashComputationFailed, 0x0001)

/// Invalid elliptic curve point provided
XI_ERROR_CODE_VALUE(InvalidPoint, 0x0003)

/// Random generator failed to provide random data
XI_ERROR_CODE_VALUE(RandomGenerationFailed, 0x0004)

/// Hash to key operation failed
XI_ERROR_CODE_VALUE(KeyDerivationFailed, 0x0005)

XI_ERROR_CODE_END(Signature, "signature operation failed")

}  // namespace EllipticCurve
}  // namespace Crypto
}  // namespace Xi

XI_ERROR_CODE_OVERLOADS(Xi::Crypto::EllipticCurve, Signature)
