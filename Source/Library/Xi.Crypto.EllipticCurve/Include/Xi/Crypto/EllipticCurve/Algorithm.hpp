// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include "Xi/Crypto/EllipticCurve/Point.hpp"
#include "Xi/Crypto/EllipticCurve/Scalar.hpp"

namespace Xi {
namespace Crypto {
namespace EllipticCurve {

ScalarVector powers(const Scalar& base, size_t maxExponent);
Scalar powersSum(Scalar base, size_t maxExponent);

Scalar innerProduct(ConstScalarSpan lhs, ConstScalarSpan rhs);

ScalarVector hadamardProduct(ConstScalarSpan lhs, ConstScalarSpan rhs);

}  // namespace EllipticCurve
}  // namespace Crypto
}  // namespace Xi
