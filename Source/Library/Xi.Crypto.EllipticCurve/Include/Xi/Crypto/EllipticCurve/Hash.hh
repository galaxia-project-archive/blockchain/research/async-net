// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include "Xi/Crypto/Hash/Keccak.hh"

#if defined(__cplusplus)
extern "C" {
#endif

#define XI_CRYPTO_ELLIPTIC_CURVE_HASH_SIZE XI_CRYPTO_HASH_KECCAK_HASH_SIZE

#define xi_crypto_elliptic_curve_hash xi_crypto_hash_keccak_256
#define xi_crypto_elliptic_curve_hash_state xi_crypto_hash_keccak_state
#define xi_crypto_elliptic_curve_hash_copy xi_crypto_hash_keccak_copy
#define xi_crypto_elliptic_curve_hash_init xi_crypto_hash_keccak_init
#define xi_crypto_elliptic_curve_hash_update xi_crypto_hash_keccak_update
#define xi_crypto_elliptic_curve_hash_finish xi_crypto_hash_keccak_finish

#if defined(__cplusplus)
}
#endif

#if defined(__cplusplus)

namespace Xi {
namespace Crypto {
namespace EllipticCurve {

using Hash = Xi::Crypto::Hash::Keccak::Hash256;
using HashStream = Xi::Crypto::Hash::Keccak::Hash256Stream;

}  // namespace EllipticCurve
}  // namespace Crypto
}  // namespace Xi

#endif
