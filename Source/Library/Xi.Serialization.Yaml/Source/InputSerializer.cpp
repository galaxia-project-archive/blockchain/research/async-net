﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Serialization/Yaml/InputSerializer.hpp"

#include <stack>
#include <unordered_set>

#include <Xi/Extern/Push.hh>
#include <yaml-cpp/yaml.h>
#include <Xi/Extern/Pop.hh>

#include <Xi/String.hpp>
#include <Xi/Encoding/Base64.hh>

#include "Xi/Serialization/Yaml/YamlError.hpp"

namespace Xi {
namespace Serialization {
namespace Yaml {

struct InputSerializer::_Impl {
  std::optional<YAML::Node> root{std::nullopt};
  std::stack<YAML::Node> nodes;
  std::stack<uint32_t> indices;

  const YAML::Node* top() {
    XI_RETURN_SC_IF(nodes.empty(), nullptr);
    XI_RETURN_SC(std::addressof(nodes.top()));
  }

  [[nodiscard]] Result<void> push(YAML::Node node) {
    XI_FAIL_IF_NOT(node.IsMap() || node.IsSequence(), YamlError::TypeMissmatchContainer);
    nodes.push(std::move(node));
    indices.push(0);
    XI_SUCCEED()
  }

  [[nodiscard]] Result<void> pop() {
    XI_FAIL_IF(nodes.empty(), YamlError::NoValue);
    XI_FAIL_IF(indices.empty(), YamlError::NoValue);
    nodes.pop();
    indices.pop();
    XI_SUCCEED()
  }

  [[nodiscard]] std::optional<YAML::Node> child(const Tag& nameTag) {
    auto current = top();
    if (current == nullptr) {
      XI_RETURN_EC_IF_NOT(root, std::nullopt);
      auto _root = root;
      root = std::nullopt;
      XI_RETURN_SC(_root);
    } else {
      if (current->IsMap()) {
        YAML::Node value = (*current)[nameTag.text()];
        XI_RETURN_EC_IF_NOT(value, std::nullopt);
        XI_RETURN_SC(value);
      } else if (current->IsSequence()) {
        const auto index = indices.top();
        XI_RETURN_EC_IF_NOT(index < current->size(), std::nullopt);
        auto value = (*current)[index];
        XI_RETURN_EC_IF_NOT(value, std::nullopt);
        indices.top() += 1;
        XI_RETURN_SC(value);
      } else {
        XI_RETURN_EC(std::nullopt);
      }
    }
  }

  [[nodiscard]] Result<void> child(const Tag& nameTag, std::string& out) {
    XI_ERROR_TRY
    auto value = child(nameTag);
    XI_FAIL_IF_NOT(value, YamlError::NoValue);
    XI_FAIL_IF_NOT(value->IsScalar(), YamlError::TypeMissmatchScalar);
    out = value->Scalar();
    XI_SUCCEED()
    XI_ERROR_CATCH
  }

  template <typename _IntegerT, std::enable_if_t<std::is_integral_v<_IntegerT>, int> = 0>
  [[nodiscard]] Result<void> child(const Tag& nameTag, _IntegerT& out) {
    XI_ERROR_TRY
    std::string scalar{};
    XI_ERROR_PROPAGATE_CATCH(child(nameTag, scalar));
    trim(scalar, in_place);

    auto number = fromString<_IntegerT>(scalar);
    XI_ERROR_PROPAGATE(number)
    out = *number;

    XI_SUCCEED()
    XI_ERROR_CATCH
  }
};

InputSerializer::InputSerializer() : m_impl{new _Impl} {
  /* */
}

Result<std::unique_ptr<InputSerializer> > InputSerializer::parse(const std::string& content) {
  XI_ERROR_TRY
  auto node = YAML::Load(content);
  std::unique_ptr<InputSerializer> reval{new InputSerializer};
  reval->m_impl->root = node;
  XI_SUCCEED(move(reval));
  XI_ERROR_CATCH
}

InputSerializer::~InputSerializer() {
  /* */
}

Format InputSerializer::format() const {
  return Format::HumanReadable;
}

Result<void> InputSerializer::readInt8(int8_t& value, const Tag& nameTag) {
  XI_ERROR_TRY
  XI_ERROR_PROPAGATE_CATCH(m_impl->child(nameTag, value));
  XI_SUCCEED()
  XI_ERROR_CATCH
}

Result<void> InputSerializer::readUInt8(uint8_t& value, const Tag& nameTag) {
  XI_ERROR_TRY
  XI_ERROR_PROPAGATE_CATCH(m_impl->child(nameTag, value));
  XI_SUCCEED()
  XI_ERROR_CATCH
}

Result<void> InputSerializer::readInt16(int16_t& value, const Tag& nameTag) {
  XI_ERROR_TRY
  XI_ERROR_PROPAGATE_CATCH(m_impl->child(nameTag, value));
  XI_SUCCEED()
  XI_ERROR_CATCH
}

Result<void> InputSerializer::readUInt16(uint16_t& value, const Tag& nameTag) {
  XI_ERROR_TRY
  XI_ERROR_PROPAGATE_CATCH(m_impl->child(nameTag, value));
  XI_SUCCEED()
  XI_ERROR_CATCH
}

Result<void> InputSerializer::readInt32(int32_t& value, const Tag& nameTag) {
  XI_ERROR_TRY
  XI_ERROR_PROPAGATE_CATCH(m_impl->child(nameTag, value));
  XI_SUCCEED()
  XI_ERROR_CATCH
}

Result<void> InputSerializer::readUInt32(uint32_t& value, const Tag& nameTag) {
  XI_ERROR_TRY
  XI_ERROR_PROPAGATE_CATCH(m_impl->child(nameTag, value));
  XI_SUCCEED()
  XI_ERROR_CATCH
}

Result<void> InputSerializer::readInt64(int64_t& value, const Tag& nameTag) {
  XI_ERROR_TRY
  XI_ERROR_PROPAGATE_CATCH(m_impl->child(nameTag, value));
  XI_SUCCEED()
  XI_ERROR_CATCH
}

Result<void> InputSerializer::readUInt64(uint64_t& value, const Tag& nameTag) {
  XI_ERROR_TRY
  XI_ERROR_PROPAGATE_CATCH(m_impl->child(nameTag, value));
  XI_SUCCEED()
  XI_ERROR_CATCH
}

Result<void> InputSerializer::readBoolean(bool& value, const Tag& nameTag) {
  XI_ERROR_TRY
  auto node = m_impl->child(nameTag);
  XI_FAIL_IF_NOT(node, YamlError::NoValue)
  value = node->as<bool>();
  XI_SUCCEED()
  XI_ERROR_CATCH
}

Result<void> InputSerializer::readFloat(float& value, const Tag& nameTag) {
  XI_ERROR_TRY
  auto node = m_impl->child(nameTag);
  XI_FAIL_IF_NOT(node, YamlError::NoValue)
  value = node->as<float>();
  XI_SUCCEED()
  XI_ERROR_CATCH
}

Result<void> InputSerializer::readDouble(double& value, const Tag& nameTag) {
  XI_ERROR_TRY
  auto node = m_impl->child(nameTag);
  XI_FAIL_IF_NOT(node, YamlError::NoValue)
  value = node->as<double>();
  XI_SUCCEED()
  XI_ERROR_CATCH
}

Result<void> InputSerializer::readTag(Tag& value, const Tag& nameTag) {
  XI_ERROR_TRY
  Tag::text_type tTag{Tag::NoTextTag};
  XI_ERROR_PROPAGATE_CATCH(readString(tTag, nameTag));
  XI_FAIL_IF(tTag == Tag::NoTextTag, YamlError::NullTag);
  value = Tag{Tag::NoBinaryTag, tTag};
  XI_SUCCEED()
  XI_ERROR_CATCH
}

Result<void> InputSerializer::readFlag(TagVector& value, const Tag& nameTag) {
  XI_ERROR_TRY
  value.clear();
  bool hasFlag = false;
  XI_ERROR_PROPAGATE_CATCH(checkValue(hasFlag, nameTag))
  XI_SUCCEED_IF_NOT(hasFlag)

  size_t size = 0;
  XI_ERROR_PROPAGATE_CATCH(beginReadVector(size, nameTag))
  XI_FAIL_IF(size > 14, YamlError::FlagOverflow);

  std::unordered_set<std::string> processedFlags{/* */};
  for (size_t i = 0; i < size; ++i) {
    Tag iTag = Tag::Null;
    XI_ERROR_PROPAGATE_CATCH(readTag(iTag, Tag::Null))
    XI_FAIL_IF_NOT(processedFlags.insert(iTag.text()).second, YamlError::DuplicateTag)
    value.push_back(iTag);
  }
  XI_ERROR_PROPAGATE_CATCH(endReadVector())
  XI_SUCCEED()
  XI_ERROR_CATCH
}

Result<void> InputSerializer::readString(std::string& value, const Tag& nameTag) {
  XI_ERROR_TRY
  XI_ERROR_PROPAGATE_CATCH(m_impl->child(nameTag, value));
  XI_SUCCEED();
  XI_ERROR_CATCH
}

Result<void> InputSerializer::readBinary(ByteVector& out, const Tag& nameTag) {
  XI_ERROR_TRY
  std::string encoded{};
  XI_ERROR_PROPAGATE_CATCH(readString(encoded, nameTag))
  auto size = Encoding::Base64::decodeSize(encoded);
  XI_ERROR_PROPAGATE(size)
  out.resize(*size);
  XI_ERROR_PROPAGATE_CATCH(Encoding::Base64::decodeStrict(encoded, out))
  XI_SUCCEED()
  XI_ERROR_CATCH
}

Result<void> InputSerializer::readBlob(ByteSpan out, const Tag& nameTag) {
  XI_ERROR_TRY
  std::string encoded{};
  XI_ERROR_PROPAGATE_CATCH(readString(encoded, nameTag))
  XI_ERROR_PROPAGATE_CATCH(Encoding::Base64::decodeStrict(encoded, out))
  XI_SUCCEED()
  XI_ERROR_CATCH
}

Result<void> InputSerializer::beginReadComplex(const Tag& nameTag) {
  XI_ERROR_TRY
  auto value = m_impl->child(nameTag);
  XI_FAIL_IF_NOT(value, YamlError::NoValue);
  XI_FAIL_IF_NOT(value->IsMap(), YamlError::TypeMissmatchObject);
  XI_ERROR_PROPAGATE_CATCH(m_impl->push(*value));
  XI_SUCCEED()
  XI_ERROR_CATCH
}

Result<void> InputSerializer::endReadComplex() {
  XI_ERROR_TRY
  auto top = m_impl->top();
  XI_FAIL_IF_NOT(top != nullptr, YamlError::Internal);
  XI_FAIL_IF_NOT(top->IsMap(), YamlError::TypeMissmatchObject);
  XI_ERROR_PROPAGATE_CATCH(m_impl->pop());
  XI_SUCCEED()
  XI_ERROR_CATCH
}

Result<void> InputSerializer::beginReadVector(size_t& size, const Tag& nameTag) {
  XI_ERROR_TRY
  auto value = m_impl->child(nameTag);
  XI_FAIL_IF_NOT(value, YamlError::NoValue);
  XI_FAIL_IF_NOT(value->IsSequence(), YamlError::TypeMissmatchArray);
  size = value->size();
  XI_ERROR_PROPAGATE_CATCH(m_impl->push(*value));
  XI_SUCCEED()
  XI_ERROR_CATCH
}

Result<void> InputSerializer::endReadVector() {
  XI_ERROR_TRY
  auto top = m_impl->top();
  XI_FAIL_IF_NOT(top != nullptr, YamlError::NoValue);
  XI_FAIL_IF_NOT(top->IsSequence(), YamlError::TypeMissmatchArray);
  XI_ERROR_PROPAGATE_CATCH(m_impl->pop());
  XI_SUCCEED()
  XI_ERROR_CATCH
}

Result<void> InputSerializer::beginReadArray(size_t size, const Tag& nameTag) {
  XI_ERROR_TRY
  auto value = m_impl->child(nameTag);
  XI_FAIL_IF_NOT(value, YamlError::NoValue);
  XI_FAIL_IF_NOT(value->IsSequence(), YamlError::TypeMissmatchArray);
  XI_FAIL_IF_NOT(value->size() == size, YamlError::SizeMissmatch);
  XI_ERROR_PROPAGATE_CATCH(m_impl->push(*value));
  XI_SUCCEED()
  XI_ERROR_CATCH
}

Result<void> InputSerializer::endReadArray() {
  XI_ERROR_TRY
  auto top = m_impl->top();
  XI_FAIL_IF_NOT(top != nullptr, YamlError::NoValue);
  XI_FAIL_IF_NOT(top->IsSequence(), YamlError::TypeMissmatchArray);
  XI_ERROR_PROPAGATE_CATCH(m_impl->pop());
  XI_SUCCEED()
  XI_ERROR_CATCH
}

Result<void> InputSerializer::checkValue(bool& value, const Tag& nameTag) {
  XI_ERROR_TRY
  auto node = m_impl->child(nameTag);
  if (!node) {
    value = false;
    XI_SUCCEED()
  } else {
    value = !node->IsNull();
    XI_SUCCEED()
  }
  XI_ERROR_CATCH
}

}  // namespace Yaml
}  // namespace Serialization
}  // namespace Xi
