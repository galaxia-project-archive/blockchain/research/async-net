// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <memory>
#include <string>
#include <utility>

#include <Xi/Stream/InputStream.hpp>
#include <Xi/Serialization/Serializer.hpp>
#include <Xi/Serialization/InputSerializer.hpp>

namespace Xi {
namespace Serialization {
namespace Yaml {

class InputSerializer final : public Serialization::InputSerializer {
 public:
  static Result<std::unique_ptr<InputSerializer>> parse(const std::string& content);

 public:
  ~InputSerializer() override;

  Format format() const override;

  Result<void> readInt8(std::int8_t& value, const Tag& nameTag) override;
  Result<void> readUInt8(std::uint8_t& value, const Tag& nameTag) override;
  Result<void> readInt16(std::int16_t& value, const Tag& nameTag) override;
  Result<void> readUInt16(std::uint16_t& value, const Tag& nameTag) override;
  Result<void> readInt32(std::int32_t& value, const Tag& nameTag) override;
  Result<void> readUInt32(std::uint32_t& value, const Tag& nameTag) override;
  Result<void> readInt64(std::int64_t& value, const Tag& nameTag) override;
  Result<void> readUInt64(std::uint64_t& value, const Tag& nameTag) override;

  Result<void> readBoolean(bool& value, const Tag& nameTag) override;

  Result<void> readFloat(float& value, const Tag& nameTag) override;
  Result<void> readDouble(double& value, const Tag& nameTag) override;

  Result<void> readTag(Tag& value, const Tag& nameTag) override;
  Result<void> readFlag(TagVector& value, const Tag& nameTag) override;

  Result<void> readString(std::string& value, const Tag& nameTag) override;
  Result<void> readBinary(ByteVector& out, const Tag& nameTag) override;

  Result<void> readBlob(ByteSpan out, const Tag& nameTag) override;

  Result<void> beginReadComplex(const Tag& nameTag) override;
  Result<void> endReadComplex() override;

  Result<void> beginReadVector(size_t& size, const Tag& nameTag) override;
  Result<void> endReadVector() override;

  Result<void> beginReadArray(size_t size, const Tag& nameTag) override;
  Result<void> endReadArray() override;

  Result<void> checkValue(bool& value, const Tag& nameTag) override;

 private:
  explicit InputSerializer();

 private:
  struct _Impl;
  std::unique_ptr<_Impl> m_impl;
};

}  // namespace Yaml

template <typename _ValueT>
Result<_ValueT> fromYaml(const std::string& content) {
  _ValueT reval{};
  auto input = Yaml::InputSerializer::parse(content);
  XI_ERROR_PROPAGATE(input)
  Serialization::Serializer ser{**input};
  XI_ERROR_PROPAGATE_CATCH(ser(reval, Tag::Null))
  XI_SUCCEED(std::move(reval))
}

}  // namespace Serialization
}  // namespace Xi
