﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#if defined(__cplusplus)
extern "C" {
#endif

/// Specifies an endianess type at runtime.
enum xi_endianess_type {
  /// Indicates no endianess conversion should be applied.
  Xi_Endianess_Type_Native = 1,
  /// Little endiannes conversion is applied.
  Xi_Endianess_Type_Little = 2,
  /// Big endiannes conversion is applied.
  Xi_Endianess_Type_Big = 3,
};

#if defined(__cplusplus)
}
#endif

#if defined(__cplusplus)

namespace Xi {
namespace Endianess {

/// Specifies an endianess type at runtime.
enum struct Type {
  /// Indicates no endianess conversion should be applied.
  Native = Xi_Endianess_Type_Native,
  /// Little endiannes conversion is applied.
  Little = Xi_Endianess_Type_Little,
  /// Big endiannes conversion is applied.
  Big = Xi_Endianess_Type_Big,
};

}  // namespace Endianess
}  // namespace Xi

#endif
