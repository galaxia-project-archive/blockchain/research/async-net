﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#if defined(__cplusplus)
extern "C" {
#endif

#include <inttypes.h>
#include <stdlib.h>

static inline void xi_endianess_identity_u16_inplace(uint16_t* value) {
  (void)value;
}

/*!
 * \brief Returns the value given.
 * \param value Value to return.
 * \return The value given.
 */
static inline uint16_t xi_endianess_identity_u16(const uint16_t value) {
  return value;
}

static inline void xi_endianess_identity_u16_vector(uint16_t* value, const size_t count) {
  (void)value;
  (void)count;
}

static inline void xi_endianess_identity_u32_inplace(uint32_t* value) {
  (void)value;
}

/*!
 * \brief Returns the value given.
 * \param value Value to return.
 * \return The value given.
 */
static inline uint32_t xi_endianess_identity_u32(const uint32_t value) {
  return value;
}

static inline void xi_endianess_identity_u32_vector(uint32_t* value, const size_t count) {
  (void)value;
  (void)count;
}

static inline void xi_endianess_identity_u64_inplace(uint64_t* value) {
  (void)value;
}

/*!
 * \brief Returns the value given.
 * \param value Value to return.
 * \return The value given.
 */
static inline uint64_t xi_endianess_identity_u64(const uint64_t value) {
  return value;
}

static inline void xi_endianess_identity_u64_vector(uint64_t* value, const size_t count) {
  (void)value;
  (void)count;
}

static inline void xi_endianess_identity_16_inplace(int16_t* value) {
  (void)value;
}

/*!
 * \brief Returns the value given.
 * \param value Value to return.
 * \return The value given.
 */
static inline int16_t xi_endianess_identity_16(const int16_t value) {
  return value;
}

static inline void xi_endianess_identity_16_vector(int16_t* value, const size_t count) {
  (void)value;
  (void)count;
}

static inline void xi_endianess_identity_32_inplace(int32_t* value) {
  (void)value;
}

/*!
 * \brief Returns the value given.
 * \param value Value to return.
 * \return The value given.
 */
static inline int32_t xi_endianess_identity_32(const int32_t value) {
  return value;
}

static inline void xi_endianess_identity_32_vector(int32_t* value, const size_t count) {
  (void)value;
  (void)count;
}

static inline void xi_endianess_identity_64_inplace(int64_t* value) {
  (void)value;
}

/*!
 * \brief Returns the value given.
 * \param value Value to return.
 * \return The value given.
 */
static inline int64_t xi_endianess_identity_64(const int64_t value) {
  return value;
}

static inline void xi_endianess_identity_64_vector(int64_t* value, const size_t count) {
  (void)value;
  (void)count;
}

#if defined(__cplusplus)
}
#endif

#if defined(__cplusplus)

namespace Xi {
namespace Endianess {

/*!
 * \brief Returns the value given.
 * \param value Value to return.
 * \return The value given.
 */
[[nodiscard]] static inline constexpr uint16_t identity(uint16_t value) {
  return value;
}

/*!
 * \brief Returns the value given.
 * \param value Value to return.
 * \return The value given.
 */
[[nodiscard]] static inline constexpr uint32_t identity(uint32_t value) {
  return value;
}

/*!
 * \brief Returns the value given.
 * \param value Value to return.
 * \return The value given.
 */
[[nodiscard]] static inline constexpr uint64_t identity(uint64_t value) {
  return value;
}

/*!
 * \brief Returns the value given.
 * \param value Value to return.
 * \return The value given.
 */
[[nodiscard]] static inline constexpr int16_t identity(int16_t value) {
  return value;
}

/*!
 * \brief Returns the value given.
 * \param value Value to return.
 * \return The value given.
 */
[[nodiscard]] static inline constexpr int32_t identity(int32_t value) {
  return value;
}

/*!
 * \brief Returns the value given.
 * \param value Value to return.
 * \return The value given.
 */
[[nodiscard]] static inline constexpr int64_t identity(int64_t value) {
  return value;
}

}  // namespace Endianess
}  // namespace Xi

#endif
