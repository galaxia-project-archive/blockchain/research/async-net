// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <iostream>
#include <string>
#include <memory>

#include <Xi/Stream/OutputStream.hpp>
#include <Xi/Serialization/Serializer.hpp>
#include <Xi/Serialization/OutputSerializer.hpp>

namespace Xi {
namespace Serialization {
namespace Console {

class OutputSerializer final : public Serialization::OutputSerializer {
 public:
  OutputSerializer(std::ostream& stream);
  ~OutputSerializer() override;

  Format format() const override;

  Result<void> writeInt8(std::int8_t value, const Tag& nameTag) override;
  Result<void> writeUInt8(std::uint8_t value, const Tag& nameTag) override;
  Result<void> writeInt16(std::int16_t value, const Tag& nameTag) override;
  Result<void> writeUInt16(std::uint16_t value, const Tag& nameTag) override;
  Result<void> writeInt32(std::int32_t value, const Tag& nameTag) override;
  Result<void> writeUInt32(std::uint32_t value, const Tag& nameTag) override;
  Result<void> writeInt64(std::int64_t value, const Tag& nameTag) override;
  Result<void> writeUInt64(std::uint64_t value, const Tag& nameTag) override;

  Result<void> writeBoolean(bool value, const Tag& nameTag) override;

  Result<void> writeFloat(float value, const Tag& nameTag) override;
  Result<void> writeDouble(double value, const Tag& nameTag) override;

  Result<void> writeTag(const Tag& value, const Tag& nameTag) override;
  Result<void> writeFlag(const TagVector& flag, const Tag& nameTag) override;

  Result<void> writeString(const std::string_view value, const Tag& nameTag) override;
  Result<void> writeBinary(ConstByteSpan value, const Tag& nameTag) override;

  Result<void> writeBlob(ConstByteSpan value, const Tag& nameTag) override;

  Result<void> beginWriteComplex(const Tag& nameTag) override;
  Result<void> endWriteComplex() override;

  Result<void> beginWriteVector(size_t size, const Tag& nameTag) override;
  Result<void> endWriteVector() override;

  Result<void> beginWriteArray(size_t size, const Tag& nameTag) override;
  Result<void> endWriteArray() override;

  Result<void> writeNull(const Tag& nameTag) override;
  Result<void> writeNotNull(const Tag& nameTag) override;

 private:
  struct _Impl;
  std::unique_ptr<_Impl> m_impl;
};

}  // namespace Console

template <typename _ValueT>
Result<void> toConsole(const _ValueT& value, const Tag::text_type& name, std::ostream& ostream) {
  Console::OutputSerializer output{ostream};
  Serialization::Serializer ser{output};
  if (!name.empty()) {
    XI_ERROR_PROPAGATE_CATCH(ser.beginComplex(Tag::Null));
  }
  XI_ERROR_PROPAGATE_CATCH(ser(const_cast<_ValueT&>(value), Tag{Tag::NoBinaryTag, name}))
  if (!name.empty()) {
    XI_ERROR_PROPAGATE_CATCH(ser.endComplex());
  }
  XI_SUCCEED()
}

template <typename _ValueT>
Result<void> toConsole(const _ValueT& value, const Tag::text_type& name = "") {
  return toConsole(value, name, std::cout);
}

template <typename _ValueT>
Result<void> toConsole(const _ValueT& value, std::ostream& ostream) {
  return toConsole(value, "", ostream);
}

}  // namespace Serialization
}  // namespace Xi
