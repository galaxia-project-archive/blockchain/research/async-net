// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <Xi/Global.hh>

#include "Xi/Serialization/Console/ConsoleError.hpp"
#include "Xi/Serialization/Console/OutputSerializer.hpp"
