// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Serialization/Console/OutputSerializer.hpp"

#include <stack>

#include <Xi/Extern/Push.hh>
#include <yaml-cpp/yaml.h>
#include <rang.hpp>
#include <Xi/Extern/Pop.hh>

#include <Xi/Encoding/Base64.hh>

#include "Xi/Serialization/Console/ConsoleError.hpp"

namespace Xi {
namespace Serialization {
namespace Console {

struct OutputSerializer::_Impl {
  YAML::Emitter emitter;
  std::ostream& stream;

  enum EntityType {
    OBJECT,
    ARRAY,
  };
  std::stack<EntityType> stack{};

  explicit _Impl(std::ostream& stream_) : emitter{stream_}, stream{stream_} {
    /* */
  }

  template <typename _T>
  Result<void> emit(_T& value, const Tag& nameTag, rang::fg fg, rang::style st) {
    XI_ERROR_TRY
    XI_FAIL_IF(stack.empty(), ConsoleError::Internal);
    if (stack.top() == _Impl::OBJECT) {
      stream << rang::fg::reset << rang::style::reset;
      stream << rang::fg::green;
      emitter << YAML::Key << nameTag.text();
      stream << rang::fg::reset << rang::style::reset;
      emitter << YAML::Value;
      stream << fg << st;
      emitter << value;
      stream << rang::fg::reset << rang::style::reset;
      XI_SUCCEED()
    } else if (stack.top() == _Impl::ARRAY) {
      emitter << value;
      XI_SUCCEED()
    } else {
      XI_SUCCEED()
    }
    XI_ERROR_CATCH
  }
};

OutputSerializer::OutputSerializer(std::ostream& stream) : m_impl{new _Impl{stream}} {
  /* */
}

OutputSerializer::~OutputSerializer() {
  /* */
}

Format OutputSerializer::format() const {
  return Format::HumanReadable;
}

Result<void> OutputSerializer::writeInt8(int8_t value, const Tag& nameTag) {
  int16_t _ = value;
  return m_impl->emit(_, nameTag, rang::fg::gray, value < 0 ? rang::style::italic : rang::style::reset);
}

Result<void> OutputSerializer::writeUInt8(uint8_t value, const Tag& nameTag) {
  uint16_t _ = value;
  return m_impl->emit(_, nameTag, rang::fg::gray, rang::style::reset);
}

Result<void> OutputSerializer::writeInt16(int16_t value, const Tag& nameTag) {
  return m_impl->emit(value, nameTag, rang::fg::gray, value < 0 ? rang::style::italic : rang::style::reset);
}

Result<void> OutputSerializer::writeUInt16(uint16_t value, const Tag& nameTag) {
  return m_impl->emit(value, nameTag, rang::fg::gray, rang::style::reset);
}

Result<void> OutputSerializer::writeInt32(int32_t value, const Tag& nameTag) {
  return m_impl->emit(value, nameTag, rang::fg::gray, value < 0 ? rang::style::italic : rang::style::reset);
}

Result<void> OutputSerializer::writeUInt32(uint32_t value, const Tag& nameTag) {
  return m_impl->emit(value, nameTag, rang::fg::gray, rang::style::reset);
}

Result<void> OutputSerializer::writeInt64(int64_t value, const Tag& nameTag) {
  return m_impl->emit(value, nameTag, rang::fg::gray, value < 0 ? rang::style::italic : rang::style::reset);
}

Result<void> OutputSerializer::writeUInt64(uint64_t value, const Tag& nameTag) {
  return m_impl->emit(value, nameTag, rang::fg::gray, rang::style::reset);
}

Result<void> OutputSerializer::writeBoolean(bool value, const Tag& nameTag) {
  return m_impl->emit(value, nameTag, value ? rang::fg::green : rang::fg::red, rang::style::reset);
}

Result<void> OutputSerializer::writeFloat(float value, const Tag& nameTag) {
  return m_impl->emit(value, nameTag, rang::fg::gray, value < 0 ? rang::style::italic : rang::style::reset);
}

Result<void> OutputSerializer::writeDouble(double value, const Tag& nameTag) {
  return m_impl->emit(value, nameTag, rang::fg::gray, value < 0 ? rang::style::italic : rang::style::reset);
}

Result<void> OutputSerializer::writeTag(const Tag& value, const Tag& nameTag) {
  XI_FAIL_IF(value.text() == Tag::NoTextTag, ConsoleError::NullTag)
  return m_impl->emit(value.text(), nameTag, rang::fg::cyan, rang::style::bold);
}

Result<void> OutputSerializer::writeFlag(const TagVector& flag, const Tag& nameTag) {
  auto size = flag.size();
  XI_ERROR_PROPAGATE_CATCH(beginWriteVector(size, nameTag));
  for (auto& iFlag : flag) {
    XI_FAIL_IF(iFlag.text() == Tag::NoTextTag, ConsoleError::NullTag);
    XI_ERROR_PROPAGATE_CATCH(m_impl->emit(iFlag.text(), Tag::Null, rang::fg::cyan, rang::style::bold));
  }
  XI_ERROR_PROPAGATE_CATCH(endWriteVector());
  XI_SUCCEED()
}

Result<void> OutputSerializer::writeString(const std::string_view value, const Tag& nameTag) {
  std::string copy{value.data(), value.size()};
  return m_impl->emit(copy, nameTag, rang::fg::reset, rang::style::reset);
}

Result<void> OutputSerializer::writeBinary(ConstByteSpan value, const Tag& nameTag) {
  const auto encoded = Encoding::Base64::encode(value);
  return m_impl->emit(encoded, nameTag, rang::fg::yellow, rang::style::italic);
}

Result<void> OutputSerializer::writeBlob(ConstByteSpan value, const Tag& nameTag) {
  const auto encoded = Encoding::Base64::encode(value);
  return m_impl->emit(encoded, nameTag, rang::fg::yellow, rang::style::italic);
}

Result<void> OutputSerializer::beginWriteComplex(const Tag& nameTag) {
  XI_ERROR_TRY
  if (m_impl->stack.empty()) {
    m_impl->emitter << YAML::BeginMap;
    m_impl->stack.push(_Impl::OBJECT);
    XI_SUCCEED()
  } else if (m_impl->stack.top() == _Impl::OBJECT) {
    m_impl->emitter << YAML::Key;
    m_impl->stream << rang::fg::green;
    m_impl->emitter << nameTag.text();
    m_impl->stream << rang::fg::reset;
    m_impl->emitter << YAML::Value << YAML::BeginMap;
    m_impl->stack.push(_Impl::OBJECT);
    XI_SUCCEED()
  } else if (m_impl->stack.top() == _Impl::ARRAY) {
    m_impl->emitter << YAML::BeginMap;
    m_impl->stack.push(_Impl::OBJECT);
    XI_SUCCEED()
  } else {
    XI_FAIL(ConsoleError::TypeMissmatchContainer)
  }
  XI_ERROR_CATCH
}

Result<void> OutputSerializer::endWriteComplex() {
  XI_ERROR_TRY
  XI_FAIL_IF(m_impl->stack.empty(), ConsoleError::NoValue);
  XI_FAIL_IF_NOT(m_impl->stack.top() == _Impl::OBJECT, ConsoleError::TypeMissmatchObject);
  m_impl->stack.pop();
  m_impl->emitter << YAML::EndMap;
  XI_SUCCEED()
  XI_ERROR_CATCH
}

Result<void> OutputSerializer::beginWriteVector(size_t size, const Tag& nameTag) {
  XI_UNUSED(size)
  XI_ERROR_TRY
  if (m_impl->stack.empty()) {
    m_impl->emitter << YAML::BeginSeq;
    m_impl->stack.push(_Impl::ARRAY);
    XI_SUCCEED()
  } else if (m_impl->stack.top() == _Impl::OBJECT) {
    m_impl->emitter << YAML::Key;
    m_impl->stream << rang::fg::green;
    m_impl->emitter << nameTag.text();
    m_impl->stream << rang::fg::reset;
    m_impl->emitter << YAML::Value << YAML::BeginSeq;
    m_impl->stack.push(_Impl::ARRAY);
    XI_SUCCEED()
  } else if (m_impl->stack.top() == _Impl::ARRAY) {
    m_impl->emitter << YAML::BeginSeq;
    m_impl->stack.push(_Impl::ARRAY);
    XI_SUCCEED()
  } else {
    XI_FAIL(ConsoleError::TypeMissmatchContainer)
  }
  XI_ERROR_CATCH
}

Result<void> OutputSerializer::endWriteVector() {
  XI_ERROR_TRY
  XI_FAIL_IF(m_impl->stack.empty(), ConsoleError::NoValue);
  XI_FAIL_IF_NOT(m_impl->stack.top() == _Impl::ARRAY, ConsoleError::TypeMissmatchArray);
  m_impl->stack.pop();
  m_impl->emitter << YAML::EndSeq;
  XI_SUCCEED()
  XI_ERROR_CATCH
}

Result<void> OutputSerializer::beginWriteArray(size_t size, const Tag& nameTag) {
  return beginWriteVector(size, nameTag);
}

Result<void> OutputSerializer::endWriteArray() {
  return endWriteVector();
}

Result<void> OutputSerializer::writeNull(const Tag& nameTag) {
  XI_FAIL_IF(m_impl->stack.empty(), ConsoleError::NoValue);
  return m_impl->emit(YAML::Null, nameTag, rang::fg::red, rang::style::reset);
}

Result<void> OutputSerializer::writeNotNull(const Tag& nameTag) {
  XI_UNUSED(nameTag)
  XI_SUCCEED()
}

}  // namespace Console
}  // namespace Serialization
}  // namespace Xi
