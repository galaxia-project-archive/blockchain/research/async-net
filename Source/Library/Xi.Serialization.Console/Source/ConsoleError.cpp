// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Serialization/Console/ConsoleError.hpp"

XI_ERROR_CODE_CATEGORY_BEGIN(Xi::Serialization::Console, Console)
XI_ERROR_CODE_DESC(NoValue, "value not found")
XI_ERROR_CODE_DESC(Internal, "internal software error")
XI_ERROR_CODE_DESC(NullTag, "null type tag")

XI_ERROR_CODE_DESC(TypeMissmatchScalar, "type missmatch, expected scalar")
XI_ERROR_CODE_DESC(TypeMissmatchBoolean, "type missmatch, expected boolean")
XI_ERROR_CODE_DESC(TypeMissmatchFloating, "type missmatch, expected floating")
XI_ERROR_CODE_DESC(TypeMissmatchContainer, "type missmatch, expected container")
XI_ERROR_CODE_DESC(TypeMissmatchObject, "type missmatch, expected object")
XI_ERROR_CODE_DESC(TypeMissmatchArray, "type missmatch, expected array")
XI_ERROR_CODE_CATEGORY_END()
