﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <type_traits>

namespace Xi {
namespace TypeSafe {

template <typename _ValueT, typename _CompType>
struct EnableIntegralFromThis {
 public:
  static_assert(std::is_arithmetic_v<_ValueT>, "Arithmetic can only wrap arithmetic types");

  using value_type = _ValueT;
  using compatible_t = _CompType;

 public:
  inline constexpr compatible_t &this_compatible() {
    return *static_cast<compatible_t *>(this);
  }
  inline constexpr const compatible_t &this_compatible() const {
    return *static_cast<const compatible_t *>(this);
  }

 protected:
  value_type value;  ///< stores the actual value representation

 public:
  explicit constexpr EnableIntegralFromThis() : value{} {
    /* */
  }
  explicit constexpr EnableIntegralFromThis(value_type _value) : value{_value} {
    /* */
  }
  explicit constexpr EnableIntegralFromThis(const EnableIntegralFromThis &_value) : value{_value.value} {
    /* */
  }
  constexpr compatible_t &operator=(compatible_t _value) {
    this->value = _value;
    return this_compatible();
  }

  constexpr value_type native() const {
    return this->value;
  }

  constexpr bool operator==(const compatible_t &rhs) const {
    return this->value == rhs.value;
  }
  constexpr bool operator!=(const compatible_t &rhs) const {
    return this->value != rhs.value;
  }

  constexpr bool operator<(const compatible_t &rhs) const {
    return this->value < rhs.value;
  }
  constexpr bool operator<=(const compatible_t &rhs) const {
    return this->value <= rhs.value;
  }
  constexpr bool operator>(const compatible_t &rhs) const {
    return this->value > rhs.value;
  }
  constexpr bool operator>=(const compatible_t &rhs) const {
    return this->value >= rhs.value;
  }
};

template <typename _CompType>
struct EnableIntegralAdditionFromThis {
  using compatible_t = _CompType;

  constexpr compatible_t operator+(compatible_t rhs) const {
    return compatible_t{static_cast<const compatible_t *>(this)->native() + rhs.native()};
  }
  constexpr compatible_t &operator+=(compatible_t rhs) {
    return static_cast<compatible_t &>(*this) = ((*this) + rhs);
  }
};

template <typename _CompType>
struct EnableIntegralSubtractionFromThis {
  using compatible_t = _CompType;

  constexpr compatible_t operator-(compatible_t rhs) const {
    return compatible_t{static_cast<const compatible_t *>(this)->native() - rhs.native()};
  }
  constexpr compatible_t &operator-=(compatible_t rhs) {
    return static_cast<compatible_t &>(*this) = ((*this) - rhs);
  }
};

}  // namespace TypeSafe
}  // namespace Xi
