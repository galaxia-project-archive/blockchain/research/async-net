// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include "Xi/TypeSafe/Flag.hpp"
#include "Xi/TypeSafe/Integral.hpp"
