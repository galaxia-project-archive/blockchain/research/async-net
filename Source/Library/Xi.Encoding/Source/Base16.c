// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Encoding/Base16.hh"

#include <inttypes.h>

#include <Xi/Global.hh>
#include <Xi/ErrorModel.hh>

size_t xi_encoding_base16_encode_length(size_t sourceLength)
{
  return sourceLength * 2;
}

static const char xi_encoding_base16_encode_lookup[] = "0123456789ABCDEF";

int xi_encoding_base16_encode(char *encoded, const xi_byte_t *data, size_t dataLength)
{
  XI_RETURN_SC_IF(dataLength == 0, XI_RETURN_CODE_SUCCESS);
  XI_RETURN_EC_IF(encoded == NULL, XI_BASE16_ENCODE_NULL_ARGUMENT);
  XI_RETURN_EC_IF(data == NULL, XI_BASE16_ENCODE_NULL_ARGUMENT);

  for(size_t i = 0; i < dataLength; ++i) {
    *encoded++ = xi_encoding_base16_encode_lookup[(data[i] & 0xF0) >> 4];
    *encoded++ = xi_encoding_base16_encode_lookup[(data[i] & 0x0F) >> 0];
  }

  XI_RETURN_SC(XI_RETURN_CODE_SUCCESS);
}

static const xi_byte_t xi_encoding_base16_decode_lookup[256] =
{
  16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16,
  16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16,
  16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16,
   0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 16, 16, 16, 16, 16, 16,
  16, 10, 11, 12, 13, 14, 15, 16, 16, 16, 16, 16, 16, 16, 16, 16,
  16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16,
  16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16,
  16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16,
  16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16,
  16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16,
  16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16,
  16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16,
  16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16,
  16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16,
  16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16,
  16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16,
};

int xi_encoding_base16_decode_length(size_t encodedLength, size_t* out)
{
  XI_RETURN_EC_IF((encodedLength & 1) > 0, XI_BASE16_DECODE_INVALID_SIZE);
  *out = encodedLength / 2;
  XI_RETURN_SC(XI_RETURN_CODE_SUCCESS);
}

int xi_encoding_base16_decode(xi_byte_t *decoded, const char *data, size_t dataLength)
{
  XI_RETURN_SC_IF(dataLength == 0, XI_RETURN_CODE_SUCCESS);
  XI_RETURN_EC_IF(decoded == NULL, XI_BASE16_DECODE_NULL_ARGUMENT);
  XI_RETURN_EC_IF(data == NULL, XI_BASE16_DECODE_NULL_ARGUMENT);
  XI_RETURN_EC_IF((dataLength & 1) > 0, XI_BASE16_DECODE_INVALID_SIZE);

  for(size_t i = 0; i < dataLength; i += 2)
  {
    const xi_byte_t high = xi_encoding_base16_decode_lookup[((const xi_byte_t*)data)[i]];
    XI_RETURN_EC_IF(high > 15, XI_BASE16_DECODE_INVALID_CHAR);
    const xi_byte_t low = xi_encoding_base16_decode_lookup[((const xi_byte_t*)data)[i + 1]];
    XI_RETURN_EC_IF(low > 15, XI_BASE16_DECODE_INVALID_CHAR);
    *decoded++ = (xi_byte_t)((high << 4) | low);
  }

  XI_RETURN_SC(XI_RETURN_CODE_SUCCESS);
}
