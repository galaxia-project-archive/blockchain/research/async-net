﻿// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include <Xi/ErrorModel.hh>

#include "Xi/Encoding/Base64.hh"

#include <utility>

XI_ERROR_CODE_CATEGORY_BEGIN(Xi::Encoding::Base64, Decode)
XI_ERROR_CODE_DESC(NullArgument, "a null argument was provided to the inner implementation")
XI_ERROR_CODE_DESC(InvalidCharacter, "base64 encoding contains an illegal character")
XI_ERROR_CODE_DESC(InvalidSize, "base64 encoding size is not dividable by 4")
XI_ERROR_CODE_DESC(OutOfMemory, "base64 exceeded maximum storage capacity")
XI_ERROR_CODE_DESC(SizeMissmatch, "base64 encoding size is not expected size")
XI_ERROR_CODE_CATEGORY_END()

namespace Xi {
namespace Encoding {
namespace Base64 {

std::string encode(ConstByteSpan raw) {
  std::string reval{};
  reval.resize(xi_encoding_base64_encode_length(raw.size()));
  const auto ec = xi_encoding_base64_encode(reval.data(), raw.data(), raw.size());
  XI_EXCEPTIONAL_IF(NullArgumentError, ec == XI_BASE64_ENCODE_NULL_ARGUMENT)
  return reval;
}

size_t encodeSize(const size_t sourceSize) {
  return xi_encoding_base64_encode_length(sourceSize);
}

Result<size_t> decodeSize(const std::string_view raw) {
  size_t reval = 0;
  const auto ec = xi_encoding_base64_decode_length(raw.data(), raw.size(), std::addressof(reval));
  XI_FAIL_IF(ec == XI_BASE64_DECODE_NULL_ARGUMENT, DecodeError::NullArgument)
  XI_FAIL_IF(ec == XI_BASE64_DECODE_INVALID_SIZE, DecodeError::InvalidSize)
  XI_FAIL_IF(ec == XI_BASE64_DECODE_INVALID_CHAR, DecodeError::InvalidCharacter)
  XI_SUCCEED(reval)
}

Result<ByteVector> decode(const std::string_view raw) {
  ByteVector reval{};
  size_t revalSize = 0;
  {
    const auto ec = xi_encoding_base64_decode_length(raw.data(), raw.size(), std::addressof(revalSize));
    XI_FAIL_IF(ec == XI_BASE64_DECODE_NULL_ARGUMENT, DecodeError::NullArgument)
    XI_FAIL_IF(ec == XI_BASE64_DECODE_INVALID_SIZE, DecodeError::InvalidSize)
  }
  reval.resize(revalSize);
  {
    const auto ec = xi_encoding_base64_decode(reval.data(), raw.data(), raw.size());
    XI_FAIL_IF(ec == XI_BASE64_DECODE_NULL_ARGUMENT, DecodeError::NullArgument)
    XI_FAIL_IF(ec == XI_BASE64_DECODE_INVALID_SIZE, DecodeError::InvalidSize)
    XI_FAIL_IF(ec == XI_BASE64_DECODE_INVALID_CHAR, DecodeError::InvalidCharacter)
  }
  return makeSuccess(move(reval));
}

Result<size_t> decode(const std::string_view raw, ByteSpan out) {
  size_t revalSize = 0;
  {
    const auto ec = xi_encoding_base64_decode_length(raw.data(), raw.size(), std::addressof(revalSize));
    XI_FAIL_IF(ec == XI_BASE64_DECODE_NULL_ARGUMENT, DecodeError::NullArgument)
    XI_FAIL_IF(ec == XI_BASE64_DECODE_INVALID_SIZE, DecodeError::InvalidSize)
  }
  XI_FAIL_IF(revalSize > out.size(), DecodeError::OutOfMemory) {
    const auto ec = xi_encoding_base64_decode(out.data(), raw.data(), raw.size());
    XI_FAIL_IF(ec == XI_BASE64_DECODE_NULL_ARGUMENT, DecodeError::NullArgument)
    XI_FAIL_IF(ec == XI_BASE64_DECODE_INVALID_SIZE, DecodeError::InvalidSize)
    XI_FAIL_IF(ec == XI_BASE64_DECODE_INVALID_CHAR, DecodeError::InvalidCharacter)
  }
  return makeSuccess(revalSize);
}

Result<void> decodeStrict(const std::string_view raw, ByteSpan out) {
  size_t revalSize = 0;
  {
    const auto ec = xi_encoding_base64_decode_length(raw.data(), raw.size(), std::addressof(revalSize));
    XI_FAIL_IF(ec == XI_BASE64_DECODE_NULL_ARGUMENT, DecodeError::NullArgument)
    XI_FAIL_IF(ec == XI_BASE64_DECODE_INVALID_SIZE, DecodeError::InvalidSize)
  }
  XI_FAIL_IF_NOT(revalSize == out.size(), DecodeError::InvalidSize) {
    const auto ec = xi_encoding_base64_decode(out.data(), raw.data(), raw.size());
    XI_FAIL_IF(ec == XI_BASE64_DECODE_NULL_ARGUMENT, DecodeError::NullArgument)
    XI_FAIL_IF(ec == XI_BASE64_DECODE_INVALID_SIZE, DecodeError::InvalidSize)
    XI_FAIL_IF(ec == XI_BASE64_DECODE_INVALID_CHAR, DecodeError::InvalidCharacter)
  }
  return makeSuccess();
}

}  // namespace Base64
}  // namespace Encoding
}  // namespace Xi
