// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <Xi/Global.hh>
#include <Xi/Byte.hh>

#if defined(__cplusplus)
extern "C" {
#endif

#include <stdlib.h>

#define XI_BASE58_DECODE_INVALID_CHAR -1
#define XI_BASE58_DECODE_INVALID_SIZE -2
#define XI_BASE58_DECODE_NONE_CANONICAL -3
#define XI_BASE58_DECODE_NULL_ARGUMENT -4
#define XI_BASE58_ENCODE_NULL_ARGUMENT -1

size_t xi_encoding_base58_encode_max_length(size_t sourceLength);

int xi_encoding_base58_encode(char* encoded, size_t* encodedSize, const xi_byte_t* data, size_t dataLength);

size_t xi_encoding_base58_decode_max_length(size_t encodedLength);

int xi_encoding_base58_decode(xi_byte_t* decoded, size_t* decodedSize, const char* data, size_t dataLength);

#if defined(__cplusplus)
}
#endif

#if defined(__cplusplus)
#include <string>

#include <Xi/ErrorModel.hh>
#include <Xi/ErrorModel.hh>

namespace Xi {
namespace Encoding {
namespace Base58 {

std::string encode(ConstByteSpan raw);
Result<ByteVector> decode(const std::string& raw);
Result<void> decodeStrict(const std::string& raw, ByteSpan out);

XI_ERROR_CODE_BEGIN(Decode)
XI_ERROR_CODE_VALUE(NullArgument, 0x0001)
XI_ERROR_CODE_VALUE(InvalidSize, 0x0002)
XI_ERROR_CODE_VALUE(InvalidCharacter, 0x0003)
XI_ERROR_CODE_VALUE(NoneCanonical, 0x0004)
XI_ERROR_CODE_VALUE(SizeMissmatch, 0x0005)
XI_ERROR_CODE_VALUE(Internal, 0x0006)
XI_ERROR_CODE_END(Decode, "Encoding::Base58::Decode")

}  // namespace Base58
}  // namespace Encoding
}  // namespace Xi

XI_ERROR_CODE_OVERLOADS(Xi::Encoding::Base58, Decode)

#endif
