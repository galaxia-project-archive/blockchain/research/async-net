// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include <gmock/gmock.h>
#include <Xi/Encoding/Base16.hh>

#include <string>

#define XI_TESTSUITE T_Xi_Encoding_Base16

namespace {
const std::vector<std::pair<std::string, std::string>> Oracle = {{
    {"", ""},
    {"f", "66"},
    {"fo", "666F"},
    {"foo", "666F6F"},
    {"foob", "666F6F62"},
    {"fooba", "666F6F6261"},
    {"foobar", "666F6F626172"},
}};
}  // namespace

TEST(XI_TESTSUITE, Encode) {
  using namespace ::testing;
  using namespace Xi::Encoding::Base16;

  for (const auto& oracle : Oracle) {
    const auto encoded = encode(Xi::asConstByteSpan(oracle.first));
    EXPECT_EQ(encoded, oracle.second);
  }
}

TEST(XI_TESTSUITE, Decode) {
  using namespace ::testing;
  using namespace Xi::Encoding::Base16;

  for (const auto& oracle : Oracle) {
    const auto decoded = decode(oracle.second);
    ASSERT_FALSE(decoded.isError());
    const std::string decodedStr{reinterpret_cast<const char*>(decoded->data()), decoded->size()};
    EXPECT_EQ(decodedStr, oracle.first);
  }
}
