﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include <gmock/gmock.h>
#include <Xi/Encoding/Base64.hh>

#include <string>

#define XI_TESTSUITE T_Xi_Encoding_Base64

namespace {
const std::vector<std::pair<std::string, std::string>> Oracle = {{
    {"", ""},
    {"f", "Zg=="},
    {"fo", "Zm8="},
    {"foo", "Zm9v"},
    {"foob", "Zm9vYg=="},
    {"fooba", "Zm9vYmE="},
    {"foobar", "Zm9vYmFy"},
    {"This is a test string for Xi.", "VGhpcyBpcyBhIHRlc3Qgc3RyaW5nIGZvciBYaS4="},
}};
}  // namespace

TEST(XI_TESTSUITE, Encode) {
  using namespace ::testing;
  using namespace Xi::Encoding::Base64;

  for (const auto& oracle : Oracle) {
    const auto encoded = encode(Xi::asConstByteSpan(oracle.first));
    EXPECT_EQ(encoded, oracle.second);
  }
}

TEST(XI_TESTSUITE, Decode) {
  using namespace ::testing;
  using namespace Xi::Encoding::Base64;

  for (const auto& oracle : Oracle) {
    const auto decoded = decode(oracle.second);
    ASSERT_FALSE(decoded.isError());
    const std::string decodedStr{reinterpret_cast<const char*>(decoded->data()), decoded->size()};
    EXPECT_EQ(decodedStr, oracle.first);
  }
}
