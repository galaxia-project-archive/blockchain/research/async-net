// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <cinttypes>
#include <string>

#include <Xi/Global.hh>
#include <Xi/ErrorModel.hh>
#include <Xi/TypeSafe/Integral.hpp>
#include <Xi/Serialization/Serialization.hpp>

#include "Xi/Network/Protocol.hpp"

namespace Xi {
namespace Network {

XI_ERROR_CODE_BEGIN(Port)
XI_ERROR_CODE_VALUE(IsAny, 0x0001)
XI_ERROR_CODE_VALUE(NoDefaultPort, 0x0002)
XI_ERROR_CODE_END(Port, "Network::PortError")

struct Port : TypeSafe::EnableIntegralFromThis<uint16_t, Port> {
  static const Port Any;

  static Result<Port> parse(const std::string& str);
  static Result<Port> fromProtocol(const Protocol protocol);

  using EnableIntegralFromThis::EnableIntegralFromThis;

  bool isAny() const;
  std::string stringify() const;

  Port orDefault(Port def) const;
  Result<Port> orDefault(const Protocol protocol) const;
};

Result<void> serialize(Port& value, const Serialization::Tag& name, Serialization::Serializer& serializer);

}  // namespace Network
}  // namespace Xi

XI_ERROR_CODE_OVERLOADS(Xi::Network, Port)
