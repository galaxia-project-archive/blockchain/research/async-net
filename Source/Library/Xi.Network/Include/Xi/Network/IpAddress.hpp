// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <variant>
#include <vector>
#include <string>
#include <cinttypes>
#include <optional>

#include <Xi/Extern/Push.hh>
#include <boost/asio/ip/address.hpp>
#include <Xi/Extern/Pop.hh>

#include <Xi/Global.hh>
#include <Xi/ErrorModel.hh>
#include <Xi/Byte.hh>
#include <Xi/Serialization/Serialization.hpp>

#include "Xi/Network/Port.hpp"

namespace Xi {
namespace Network {

XI_ERROR_CODE_BEGIN(IpAddress)
XI_ERROR_CODE_VALUE(IllFormed, 0x0001)
XI_ERROR_CODE_VALUE(NotFound, 0x0002)
XI_ERROR_CODE_END(IpAddress, "Network::IpAddressError")

class IpAddress;
using IpAddressVector = std::vector<IpAddress>;

class IpAddress {
 public:
  using native_type = boost::asio::ip::address;
  using v4_storage = ByteArray<4>;
  using v6_storage = ByteArray<16>;

  enum struct Type {
    v4 = 1,
    v6 = 2,
  };

 public:
  static const IpAddress v4Any;
  static const IpAddress v4Loopback;
  static const IpAddress v6Any;
  static const IpAddress v6Loopback;

  static const IpAddress& any(const Type type);
  static const IpAddress& loopback(const Type type);

 public:
  static Result<IpAddress> parse(const std::string& str);
  static Result<IpAddressVector> resolve(const std::string& host);
  static Result<IpAddressVector> resolve(const std::string& host, const Port port);
  static Result<IpAddress> resolveAny(const std::string& host);
  static Result<IpAddress> resolveAny(const std::string& host, const Type type);

 public:
  explicit IpAddress();
  explicit IpAddress(const Type type);
  XI_DEFAULT_COPY(IpAddress)
  XI_DEFAULT_MOVE(IpAddress)

  /* implict */ IpAddress(const native_type& native);
  operator native_type() const;

  Type type() const;

  const v4_storage& v4() const;
  const v6_storage& v6() const;

  const Byte* data() const;
  size_t size() const;

  std::string stringify() const;

 private:
  std::variant<v4_storage, v6_storage> m_data;

  XI_SERIALIZATION_FRIEND(IpAddress)
};

Result<void> serialize(IpAddress& value, const Serialization::Tag& name, Serialization::Serializer& serializer);

XI_SERIALIZATION_ENUM(IpAddress::Type)

}  // namespace Network
}  // namespace Xi

XI_ERROR_CODE_OVERLOADS(Xi::Network, IpAddress)

XI_SERIALIZATION_ENUM_RANGE(Xi::Network::IpAddress::Type, v4, v6)
XI_SERIALIZATION_ENUM_TAG(Xi::Network::IpAddress::Type, v4, "v4")
XI_SERIALIZATION_ENUM_TAG(Xi::Network::IpAddress::Type, v6, "v6")
