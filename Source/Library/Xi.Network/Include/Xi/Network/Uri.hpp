﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <memory>
#include <string>
#include <string_view>
#include <optional>
#include <cinttypes>

#include <Xi/Global.hh>
#include <Xi/ErrorModel.hh>

#include "Xi/Network/Port.hpp"
#include "Xi/Network/Protocol.hpp"

namespace Xi {
namespace Network {

XI_ERROR_CODE_BEGIN(Uri)
XI_ERROR_CODE_VALUE(IllFormed, 0x0001)
XI_ERROR_CODE_END(Uri, "Network::UriError")

class Uri {
 public:
  static Result<Uri> parse(const std::string& str);

 private:
  explicit Uri();

 public:
  Uri(const Uri& other);
  Uri& operator=(const Uri& other);

  Uri(Uri&& other);
  Uri& operator=(Uri&& other);

  ~Uri();

  const std::string& scheme() const;
  Result<Protocol> protocol() const;
  const std::string& host() const;

  /// The uri port, any iff not specified.
  Port port() const;

  const std::string& path() const;
  const std::string& query() const;
  const std::string& fragment() const;
  const std::string& target() const;

  std::string stringify() const;

 private:
  struct _Impl;
  std::unique_ptr<_Impl> m_impl;
};

bool isUri(const std::string& str);

}  // namespace Network
}  // namespace Xi

XI_ERROR_CODE_OVERLOADS(Xi::Network, Uri)
