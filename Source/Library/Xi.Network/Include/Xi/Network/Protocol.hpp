﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <string>

#include <Xi/Global.hh>
#include <Xi/ErrorModel.hh>

namespace Xi {
namespace Network {

XI_ERROR_CODE_BEGIN(Protocol)
XI_ERROR_CODE_VALUE(Unknown, 0x0001)
XI_ERROR_CODE_END(Protocol, "Network::ProtocolError")

enum struct Protocol {
  Http,   ///< Hypertext Transfer Protocol
  Https,  ///< Hypertext Transfer Protocol Secure
  Ws,     ///< Web Socket
  Wss,    ///< Web Socket Secure
  Tcp,
  Udp,

  // None standard, but for convenience
  Xip,   ///< P2p protocol
  Xips,  ///< P2p protocol with encryption
};

Result<void> parse(const std::string& str, Protocol& out);
std::string stringify(const Protocol protocol);

bool isHttpBased(const Protocol protocol);
bool isHttp(const Protocol protocol);
bool isHttps(const Protocol protocol);

}  // namespace Network
}  // namespace Xi

XI_ERROR_CODE_OVERLOADS(Xi::Network, Protocol)
