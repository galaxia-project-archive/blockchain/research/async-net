// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Network/Protocol.hpp"

#include <Xi/ErrorModel.hh>

#include <Xi/Log/Log.hpp>
XI_LOGGER("Network/Protocol")

XI_ERROR_CODE_CATEGORY_BEGIN(Xi::Network, Protocol)
XI_ERROR_CODE_DESC(Unknown, "network protocol is unknown")
XI_ERROR_CODE_CATEGORY_END()

namespace Xi {
namespace Network {

Result<void> parse(const std::string &str, Protocol &out) {
  XI_ERROR_TRY
  if (str == "http") {
    out = Protocol::Http;
    XI_SUCCEED();
  } else if (str == "https") {
    out = Protocol::Https;
    XI_SUCCEED();
  } else if (str == "ws") {
    out = Protocol::Ws;
    XI_SUCCEED();
  } else if (str == "wss") {
    out = Protocol::Wss;
    XI_SUCCEED();
  } else if (str == "tcp") {
    out = Protocol::Tcp;
    XI_SUCCEED();
  } else if (str == "udp") {
    out = Protocol::Udp;
    XI_SUCCEED();
  } else if (str == "xip") {
    out = Protocol::Xip;
    XI_SUCCEED();
  } else if (str == "xips") {
    out = Protocol::Xips;
    XI_SUCCEED();
  } else {
    XI_LOG_ERROR("Unknown protocol name '{}'.", str)
    XI_FAIL(ProtocolError::Unknown);
  }
  XI_ERROR_CATCH
}

std::string stringify(const Protocol protocol) {
  switch (protocol) {
    case Protocol::Http:
      return "http";
    case Protocol::Https:
      return "https";
    case Protocol::Ws:
      return "ws";
    case Protocol::Wss:
      return "wss";
    case Protocol::Tcp:
      return "tcp";
    case Protocol::Udp:
      return "udp";
    case Protocol::Xip:
      return "xip";
    case Protocol::Xips:
      return "xips";
  }
  XI_EXCEPTIONAL(InvalidEnumValueError, "Unrecognized network protocol");
}

bool isHttpBased(const Protocol protocol) {
  switch (protocol) {
    case Protocol::Http:
    case Protocol::Https:
      return true;

    default:
      return false;
  }
}

bool isHttp(const Protocol protocol) {
  switch (protocol) {
    case Protocol::Http:
      return true;

    default:
      return false;
  }
}

bool isHttps(const Protocol protocol) {
  switch (protocol) {
    case Protocol::Https:
      return true;

    default:
      return false;
  }
}

}  // namespace Network
}  // namespace Xi
