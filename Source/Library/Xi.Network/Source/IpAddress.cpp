﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Network/IpAddress.hpp"

#include <cstring>
#include <utility>
#include <algorithm>

#include <Xi/Extern/Push.hh>
#include <boost/asio.hpp>
#include <boost/asio/ip/address.hpp>
#include <Xi/Extern/Pop.hh>

#include <Xi/String.hpp>
#include <Xi/ErrorModel.hh>
#include <Xi/Async/Async.hpp>

#include <Xi/Log/Log.hpp>
XI_LOGGER("Network/Ip")

XI_ERROR_CODE_CATEGORY_BEGIN(Xi::Network, IpAddress)
XI_ERROR_CODE_DESC(IllFormed, "ip address string representation is invalid")
XI_ERROR_CODE_DESC(NotFound, "resolving host did not return any suitable ip address")
XI_ERROR_CODE_CATEGORY_END()

namespace Xi {
namespace Network {

const IpAddress IpAddress::v4Any{Type::v4};
const IpAddress IpAddress::v4Loopback{IpAddress::parse("127.0.0.1").take()};
const IpAddress IpAddress::v6Any{Type::v6};
const IpAddress IpAddress::v6Loopback{IpAddress::parse("::1").take()};

const IpAddress& IpAddress::any(const IpAddress::Type type) {
  switch (type) {
    case Type::v4:
      return v4Any;
    case Type::v6:
      return v6Any;
  }
  XI_EXCEPTIONAL(InvalidEnumValueError)
}

const IpAddress& IpAddress::loopback(const IpAddress::Type type) {
  switch (type) {
    case Type::v4:
      return v4Loopback;
    case Type::v6:
      return v6Loopback;
  }
  XI_EXCEPTIONAL(InvalidEnumValueError)
}

Result<IpAddress> IpAddress::parse(const std::string& str) {
  XI_ERROR_TRY
  IpAddress reval{};
  boost::system::error_code ec{};
  const auto ip = boost::asio::ip::address::from_string(str, ec);
  XI_ERROR_CODE_PROPAGATE(ec);
  if (ip.is_v4()) {
    const auto ip4 = ip.to_v4().to_bytes();
    v4_storage storage{};
    std::memcpy(storage.data(), ip4.data(), ip4.size());
    reval.m_data = storage;
  } else if (ip.is_v6()) {
    const auto ip6 = ip.to_v6().to_bytes();
    v6_storage storage{};
    std::memcpy(storage.data(), ip6.data(), ip6.size());
    reval.m_data = storage;
  } else {
    XI_EXCEPTIONAL(InvalidVariantTypeError);
  }
  return makeSuccess(reval);
  XI_ERROR_CATCH
}

Result<IpAddressVector> IpAddress::resolve(const std::string& host) {
  return resolve(host, Port::Any);
}

Result<IpAddressVector> IpAddress::resolve(const std::string& host, const Port port) {
  XI_ERROR_TRY
  if (const auto plain = parse(host); plain.isValue()) {
    return makeSuccess(IpAddressVector{{plain.value()}});
  }
  boost::asio::ip::tcp::resolver::query query{host, toString(port.native())};
  boost::asio::ip::tcp::resolver resolver{Async::this_executor::sharedIo()};
  boost::system::error_code ec{};
  boost::asio::ip::tcp::resolver::results_type begin;
  resolver.async_resolve(query, Async::yield(ec, begin));
  if (ec.failed()) {
    XI_LOG_ERROR("Host resolution for '{}' failed: {}", host, ec.message());
    return makeFailure(ec);
  }
  boost::asio::ip::tcp::resolver::iterator end{};

  IpAddressVector reval{};
  for (; begin != end; ++begin) {
    reval.emplace_back(begin->endpoint().address());
  }
  return makeSuccess(reval);
  XI_ERROR_CATCH
}

Result<IpAddress> IpAddress::resolveAny(const std::string& host) {
  XI_ERROR_TRY
  const auto addresses = resolve(host);
  XI_ERROR_PROPAGATE(addresses)
  XI_FAIL_IF(addresses->empty(), IpAddressError::NotFound)
  // TODO, consider to pick a random entry, some provider use this for load balancing.
  return makeSuccess(addresses->front());
  XI_ERROR_CATCH
}

Result<IpAddress> IpAddress::resolveAny(const std::string& host, const IpAddress::Type type) {
  XI_ERROR_TRY
  const auto addresses = resolve(host);
  XI_ERROR_PROPAGATE(addresses)
  const auto search =
      std::find_if(addresses->begin(), addresses->end(), [type](const auto& i) { return i.type() == type; });
  XI_FAIL_IF(search == addresses->end(), IpAddressError::NotFound)
  return makeSuccess(*search);
  XI_ERROR_CATCH
}

IpAddress::Type IpAddress::type() const {
  if (std::holds_alternative<v4_storage>(m_data)) {
    return Type::v4;
  } else if (std::holds_alternative<v6_storage>(m_data)) {
    return Type::v6;
  } else {
    XI_EXCEPTIONAL(InvalidVariantTypeError);
  }
}

const IpAddress::v4_storage& IpAddress::v4() const {
  if (std::holds_alternative<v4_storage>(m_data)) {
    return std::get<v4_storage>(m_data);
  } else {
    XI_EXCEPTIONAL(InvalidVariantTypeError);
  }
}

const IpAddress::v6_storage& IpAddress::v6() const {
  if (std::holds_alternative<v6_storage>(m_data)) {
    return std::get<v6_storage>(m_data);
  } else {
    XI_EXCEPTIONAL(InvalidVariantTypeError);
  }
}

const Byte* IpAddress::data() const {
  if (std::holds_alternative<v4_storage>(m_data)) {
    return std::get<v4_storage>(m_data).data();
  } else if (std::holds_alternative<v6_storage>(m_data)) {
    return std::get<v6_storage>(m_data).data();
  } else {
    XI_EXCEPTIONAL(InvalidVariantTypeError);
  }
}

size_t IpAddress::size() const {
  if (std::holds_alternative<v4_storage>(m_data)) {
    return 4;
  } else if (std::holds_alternative<v6_storage>(m_data)) {
    return 16;
  } else {
    XI_EXCEPTIONAL(InvalidVariantTypeError);
  }
}

std::string IpAddress::stringify() const {
  if (std::holds_alternative<v4_storage>(m_data)) {
    const auto& storage = std::get<v4_storage>(m_data);
    return boost::asio::ip::make_address_v4(storage).to_string();
  } else if (std::holds_alternative<v6_storage>(m_data)) {
    const auto& storage = std::get<v6_storage>(m_data);
    return boost::asio::ip::make_address_v6(storage).to_string();
  } else {
    XI_EXCEPTIONAL(InvalidVariantTypeError);
  }
}

IpAddress::IpAddress() {
  /* */
}

IpAddress::IpAddress(const IpAddress::Type type) {
  if (type == Type::v4) {
    m_data = v4_storage{};
  } else if (type == Type::v6) {
    m_data = v6_storage{};
  } else {
    XI_EXCEPTIONAL(InvalidEnumValueError)
  }
}

IpAddress::IpAddress(const IpAddress::native_type& native) {
  if (native.is_v4()) {
    m_data = native.to_v4().to_bytes();
  } else if (native.is_v6()) {
    m_data = native.to_v6().to_bytes();
  } else {
    XI_EXCEPTIONAL(InvalidVariantTypeError);
  }
}

Xi::Network::IpAddress::operator native_type() const {
  switch (type()) {
    case IpAddress::Type::v4:
      return native_type{boost::asio::ip::address_v4{v4()}};
    case IpAddress::Type::v6:
      return native_type{boost::asio::ip::address_v6{v6()}};
  }
  XI_EXCEPTIONAL(InvalidEnumValueError)
}

Result<void> serialize(IpAddress& value, const Serialization::Tag& name, Serialization::Serializer& serializer) {
  static const Serialization::Tag TypeTag{0x0001, "type"};
  static const Serialization::Tag DataTag{0x0002, "data"};

  if (serializer.isHumanReadableFromat()) {
    std::string str;
    if (serializer.isInputMode()) {
      XI_ERROR_PROPAGATE_CATCH(serializer(str, name))
      auto parsed = fromString<IpAddress>(str);
      XI_ERROR_PROPAGATE(parsed)
      value = parsed.take();
      XI_SUCCEED()
    } else {
      str = toString(value);
      return serializer(str, name);
    }
  } else {
    IpAddress::Type type;
    if (serializer.isOutputMode()) {
      type = value.type();
    }

    XI_ERROR_PROPAGATE_CATCH(serializer.beginComplex(name))
    XI_ERROR_PROPAGATE_CATCH(serializer(type, TypeTag))

    if (serializer.isInputMode()) {
      if (type == IpAddress::Type::v4) {
        value.m_data = IpAddress::v4_storage{/* */};
      } else if (type == IpAddress::Type::v6) {
        value.m_data = IpAddress::v6_storage{/* */};
      } else {
        XI_EXCEPTIONAL(InvalidEnumValueError)
      }
    }

    if (type == IpAddress::Type::v4) {
      XI_ERROR_PROPAGATE_CATCH(serializer.blob(std::get<IpAddress::v4_storage>(value.m_data), DataTag));
    } else if (type == IpAddress::Type::v6) {
      XI_ERROR_PROPAGATE_CATCH(serializer.blob(std::get<IpAddress::v6_storage>(value.m_data), DataTag));
      value.m_data = IpAddress::v6_storage{/* */};
    } else {
      XI_EXCEPTIONAL(InvalidEnumValueError)
    }

    return serializer.endComplex();
  }
}

}  // namespace Network
}  // namespace Xi
