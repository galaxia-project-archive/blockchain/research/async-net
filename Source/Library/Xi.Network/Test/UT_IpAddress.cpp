// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include <gmock/gmock.h>

#include <string>

#include <Xi/Testing/Result.hpp>
#include <Xi/Network/IpAddress.hpp>

#define XI_TESTSUITE T_Xi_Network_IpAddress

TEST(XI_TESTSUITE, Resolve) {
  using namespace ::testing;
  using namespace ::Xi::Network;
  using namespace ::Xi::Testing;

  {
    const auto ip = IpAddress::resolveAny("google.com", IpAddress::Type::v4);
    EXPECT_THAT(ip, IsSuccess());
  }

  {
    const auto ip = IpAddress::resolveAny("172.217.22.78", IpAddress::Type::v4);
    EXPECT_THAT(ip, IsSuccess());
  }
}
