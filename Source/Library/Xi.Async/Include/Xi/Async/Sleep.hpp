// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <chrono>

#include <Xi/Extern/Push.hh>
#include <boost/fiber/operations.hpp>
#include <Xi/Extern/Pop.hh>

namespace Xi {
namespace Async {

template <typename _ClockT, typename _DurationT>
inline void sleepFor(const std::chrono::duration<_ClockT, _DurationT>& duration) {
  boost::this_fiber::sleep_for(duration);
}

template <typename _RepT, typename _ClockT>
inline void sleepUntil(const std::chrono::time_point<_RepT, _ClockT>& limit) {
  boost::this_fiber::sleep_until(limit);
}

}  // namespace Async
}  // namespace Xi
