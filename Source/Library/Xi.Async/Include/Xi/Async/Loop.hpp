﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <memory>

#include <Xi/Global.hh>

namespace Xi {
namespace Async {

class Loop final {
 public:
  static Loop& current();

 public:
  explicit Loop();
  XI_DELETE_COPY(Loop)
  XI_DELETE_MOVE(Loop)
  ~Loop();

 public:
  ///
  /// \brief run Executes Tasks in the execution Loop using a number of threads.
  /// \param numThreads The number of threads allocated to exectue shared tasks.
  ///
  /// Note for 0 threads the hardware concurrency is used and the caller of this method will always block until this
  /// loop shuts down.
  ///
  void run(std::uint32_t numThreads = 1);
  void shutdown();

 private:
  std::unique_ptr<class LoopGuard> m_guard;
};

}  // namespace Async
}  // namespace Xi
