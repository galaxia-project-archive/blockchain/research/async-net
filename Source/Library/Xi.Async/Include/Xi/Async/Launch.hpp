// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <type_traits>

#include <Xi/Global.hh>

#include "Xi/Async/Boost.hpp"

namespace Xi {
namespace Async {

enum Launch : std::underlying_type_t<Boost::Policy> {
  Dispatch = static_cast<std::underlying_type_t<Boost::Policy>>(Boost::Policy::dispatch),
  Post = static_cast<std::underlying_type_t<Boost::Policy>>(Boost::Policy::post)
};

template <typename _T>
struct is_launch : std::false_type {};

template <>
struct is_launch<Launch> : std::true_type {};

template <typename _T>
constexpr bool is_launch_v = is_launch<_T>::value;

}  // namespace Async
}  // namespace Xi
