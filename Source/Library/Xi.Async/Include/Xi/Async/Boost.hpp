﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <mutex>

#include <Xi/Extern/Push.hh>
#include <boost/asio.hpp>
#include <boost/asio/io_context.hpp>
#include <boost/fiber/fiber.hpp>
#include <boost/fiber/mutex.hpp>
#include <boost/fiber/condition_variable.hpp>
#include <boost/fiber/future.hpp>
#include <boost/fiber/policy.hpp>
#include <boost/fiber/algo/algorithm.hpp>
#include <Xi/Extern/Pop.hh>

#include <Xi/Global.hh>

namespace Xi {
namespace Async {
namespace Boost {

using ErrorCode = boost::system::error_code;
using IoContext = boost::asio::io_context;
XI_DECLARE_SMART_POINTER(IoContext)

using SteadyTimer = boost::asio::steady_timer;
using DeadlineTimer = boost::asio::deadline_timer;
using ReadyQueue = boost::fibers::scheduler::ready_queue_type;

using Mutex = boost::fibers::mutex;

template <typename _MutexT>
using UniqueLock = std::unique_lock<_MutexT>;

using Fiber = boost::fibers::fiber;
using FiberContext = boost::fibers::context;
using FiberProperties = boost::fibers::fiber_properties;
using FiberType = boost::fibers::type;
using ConditionVariable = boost::fibers::condition_variable_any;
using Policy = boost::fibers::launch;

template <typename _ValueT>
using Future = boost::fibers::future<_ValueT>;

template <typename _ValueT>
using SharedFuture = boost::fibers::shared_future<_ValueT>;

template <typename _ValueT>
using Promise = boost::fibers::promise<_ValueT>;

template <typename _Signature>
using PackagedTask = boost::fibers::packaged_task<_Signature>;

using ConditionVariableStatus = boost::fibers::cv_status;

template <typename _PropertyT>
using Algorithm = boost::fibers::algo::algorithm_with_properties<_PropertyT>;

}  // namespace Boost
}  // namespace Async
}  // namespace Xi
