// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <Xi/ErrorModel.hh>

#include "Xi/Async/Boost.hpp"

namespace Xi {
namespace Async {

template <typename _ValueT>
using Future = Boost::Future<_ValueT>;

template <typename _ValueT>
using FutureResult = Boost::Future<Result<_ValueT>>;

template <typename _ValueT>
using SharedFuture = Boost::SharedFuture<_ValueT>;

template <typename _ValueT>
using SharedFutureResult = Boost::SharedFuture<Result<_ValueT>>;

}  // namespace Async
}  // namespace Xi
