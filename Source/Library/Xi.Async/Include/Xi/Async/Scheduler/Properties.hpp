﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <memory>

#include <Xi/Global.hh>

#include "Xi/Async/Boost.hpp"
#include "Xi/Async/Event.hpp"

namespace Xi::Async {

namespace Scheduler {
XI_DECLARE_SMART_POINTER_CLASS(Executor)

/*!
 * \brief The Properties class wraps scheduler relevant data for every executed fiber.
 *
 * This includes the executor that shall run this fiber (if not detached) as well as the cancellation context, ie. a
 * timeout.
 */
class Properties : public Boost::FiberProperties {
 public:
  static Properties& current();

 public:
  explicit Properties(Boost::FiberContext* context);
  explicit Properties(Boost::FiberContext* context, WeakExecutor executor);
  XI_DELETE_COPY(Properties)
  XI_DEFAULT_MOVE(Properties)
  ~Properties();

  /// Queries the executor this fiber is attached to.
  WeakExecutor executor();
  /// Queries the executor this fiber is attached to.
  WeakConstExecutor executor() const;

  /// Queries the cancellation event that is notified once the composed operation should be cancelled.
  Event& cancellation();
  /// Queries the cancellation event that is notified once the composed operation should be cancelled.
  const Event& cancellation() const;

  void schedule(Boost::FiberContext* context);

 private:
  WeakExecutor m_executor;
  Event m_cancellation;
};

}  // namespace Scheduler

namespace this_fiber {

Event cancellation();
Scheduler::WeakExecutor executor();

}  // namespace this_fiber

}  // namespace Xi::Async

#define XI_ASYNC_CANCELLATION_HANDLER(CB) \
  [[maybe_unused]] auto __CancellationHandler = ::Xi::Async::this_fiber::cancellation().onNotify(CB);
