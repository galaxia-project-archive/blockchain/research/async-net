// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <chrono>

namespace Xi {
namespace Async {

/// Using alias for the clock being used for async routines internally.
using Clock = std::chrono::steady_clock;

/// Using alias for time points of the clock being used.
using TimePoint = Clock::time_point;

/// Most granular duration declarative.
using Duration = Clock::duration;

}  // namespace Async
}  // namespace Xi
