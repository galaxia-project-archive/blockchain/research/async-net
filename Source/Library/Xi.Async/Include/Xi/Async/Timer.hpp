﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <memory>
#include <functional>

#include <Xi/Global.hh>
#include <Xi/Chrono.hpp>

#include "Xi/Async/Clock.hpp"
#include "Xi/Async/Boost.hpp"

namespace Xi::Async {

/*!
 * \brief The Timer class creates timed events based on a timed schedule.
 *
 * The timer will be invoked asynchroniously after a certain duration has been passed.  The timer
 * may be invoked again if it is scheduled to repeat itself.
 */
class Timer : public std::enable_shared_from_this<Timer> {
 public:
  /*!
   * \brief The State enum reflects the current state of the timer.
   */
  enum struct State { Cancelled, Paused, Running, Finished };

  /*!
   * \brief Functor signature called once the timer ellapses.
   */
  using Callback = std::function<void()>;

 public:
  /*!
   * \brief once Creates a new timer that will be invoked exactly once.
   * \param duration The duration to wait before the callback gets invoked.
   * \param callback The callback to be invoked once the timer triggers.
   * \return A shared reference to the created timer.
   */
  [[nodiscard]] static std::shared_ptr<Timer> once(const std::chrono::milliseconds& duration, Callback callback);

  /*!
   * \brief repeat Creates a new timer taht will be invoked repeatiedly after a ceratin duration.
   * \param duration The duration to wait before the callback gets invoked.
   * \param callback The callback to be invoked once the timer triggers.
   * \return A shared reference to the created timer.
   *
   * \note To cancel/stop the timer you may call cancel. Note that timer may be invoked during the
   * cancellation due to concurrent evaluation.
   */
  [[nodiscard]] static std::shared_ptr<Timer> repeat(const std::chrono::milliseconds& duration, Callback callback);

 public:
  XI_DELETE_COPY(Timer)
  XI_DELETE_MOVE(Timer)
  ~Timer();

  /*!
   * \brief state Current state of the timer.
   */
  State state() const;

  /*!
   * \brief cancel Stops the timer, the callback will not further be invoked.
   */
  void cancel();

  /*!
   * \brief pause The timer will remain paused and will not invoke the callback.
   */
  void pause();

  /*!
   * \brief resume Reenables the timer. Resets the duration.
   */
  void resume();

 private:
  /*!
   * \brief Timer Creates a timer with given callback and expiration.
   * \param cb The callback to invoke once the timer triggers.
   * \param dur The duration to be waited before invoking the callback.
   */
  explicit Timer(Callback cb, const Duration& dur);

  /// Invokes the callback if the timer state is Running.
  static void invoke(std::weak_ptr<Timer>, Boost::ErrorCode ec);

  /// Enqueues the timer to the event loop.
  void start();

 private:
  Callback m_callback;
  Duration m_duration;
  Boost::DeadlineTimer m_timer;
  std::atomic<State> m_state;
  bool m_repeat;
};

XI_DECLARE_SMART_POINTER(Timer)

}  // namespace Xi::Async
