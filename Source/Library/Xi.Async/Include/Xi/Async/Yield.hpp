﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <type_traits>
#include <tuple>
#include <utility>
#include <memory>
#include <mutex>
#include <atomic>
#include <cassert>

#include "Xi/Async/Boost.hpp"
#include "Xi/Async/Scheduler/Executor.hpp"
#include "Xi/Async/Scheduler/Properties.hpp"

namespace Xi {
namespace Async {

/// Implmentation details for the async library, this is not part of the interface.
namespace Detail {

template <typename _T>
using yield_argument_t = std::add_lvalue_reference_t<std::decay_t<_T>>;

template <typename _T>
using yield_argument_storage_t = std::add_pointer_t<std::decay_t<_T>>;

/*!
 *
 * The yield storage stores pointers to the references passed to the callback, for the coroutine.
 *
 */
template <typename... _ArgsT>
struct yield_storage {
  using storage_t = std::tuple<_ArgsT...>;

  storage_t arguments;

  yield_storage(_ArgsT... args) : arguments{args...} {
    /* */
  }

  template <size_t _IndexV, typename _AssignArgT, typename... _AssignArgsT>
  void _assign(_AssignArgT &&arg, _AssignArgsT... args) {
    *std::get<_IndexV>(this->arguments) = std::forward<_AssignArgT>(arg);
    if constexpr (sizeof...(args) > 0) {
      _assign<_IndexV + 1, _AssignArgsT...>(std::forward<_AssignArgsT>(args)...);
    }
  }

  template <typename... _AssignArgsT>
  void assign(_AssignArgsT &&... args) {
    static_assert(sizeof...(_AssignArgsT) == sizeof...(_ArgsT), "");
    if constexpr (sizeof...(_AssignArgsT) > 0) {
      this->_assign<0, _AssignArgsT...>(std::forward<_AssignArgsT>(args)...);
    }
  }
};

template <typename... _ArgsT>
using yield_storage_t = yield_storage<yield_argument_storage_t<_ArgsT>...>;

template <typename... _ArgsT>
inline auto yield(_ArgsT &... args) {
  return Detail::yield_storage<Detail::yield_argument_storage_t<_ArgsT>...>{std::addressof<_ArgsT>(args)...};
}

struct yield_completion {
  enum state_t { init, waiting, complete };

  typedef boost::fibers::detail::spinlock mutex_t;
  typedef std::unique_lock<mutex_t> lock_t;
  typedef boost::intrusive_ptr<yield_completion> ptr_t;

  std::atomic<std::size_t> useCount{0};
  mutex_t mutex{};
  state_t state{init};

  void wait() {
    lock_t lck{mutex};
    if (state != complete) {
      state = waiting;
      boost::fibers::context::active()->suspend(lck);
    }
  }

  friend void intrusive_ptr_add_ref(yield_completion *yc) noexcept {
    BOOST_ASSERT(nullptr != yc);
    yc->useCount.fetch_add(1, std::memory_order_relaxed);
  }

  friend void intrusive_ptr_release(yield_completion *yc) noexcept {
    BOOST_ASSERT(nullptr != yc);
    if (1 == yc->useCount.fetch_sub(1, std::memory_order_release)) {
      std::atomic_thread_fence(std::memory_order_acquire);
      delete yc;
    }
  }
};

template <typename... _ArgsT>
struct yield_handler {
  using storage_t = yield_storage<Detail::yield_argument_storage_t<_ArgsT>...>;

  boost::fibers::context *fiber_context;
  storage_t storage;
  yield_completion::ptr_t completion;

  yield_handler(const storage_t &_storage) : fiber_context{boost::fibers::context::active()}, storage{_storage} {
    /* */
  }

  template <typename... _CallArgsT>
  void operator()(_CallArgsT &&... args) {
    yield_completion::lock_t lk{completion->mutex};
    yield_completion::state_t state = completion->state;

    completion->state = yield_completion::state_t::complete;
    storage.assign(std::forward<_CallArgsT>(args)...);

    if (state == yield_completion::state_t::waiting) {
      // MUST be called on the FiberProperty to ensure localIo threads are used on resume.
      boost::this_fiber::properties<Scheduler::Properties>().schedule(fiber_context);
    }
  }
};

}  // namespace Detail

template <typename... _ArgsT>
inline auto yield(_ArgsT &... args) {
  return Detail::yield<_ArgsT...>(args...);
}

}  // namespace Async
}  // namespace Xi

namespace boost {
namespace asio {

template <class Result, class... Args>
class async_result<Xi::Async::Detail::yield_storage_t<Args...>, Result(Args...)> {
 public:
  typedef Xi::Async::Detail::yield_handler<Args...> completion_handler_type;
  typedef void return_type;

  using completion_t = Xi::Async::Detail::yield_completion;

  completion_t::ptr_t completion;
  completion_handler_type &handler;

  explicit async_result(completion_handler_type &h) : completion{new completion_t}, handler{h} {
    this->handler.completion = this->completion;
  }

  async_result(const async_result &) = delete;
  async_result &operator=(const async_result &) = delete;

  return_type get() {
    assert(this->completion != nullptr);
    this->completion->wait();
  }
};

}  // namespace asio
}  // namespace boost
