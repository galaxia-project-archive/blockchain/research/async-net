// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <mutex>
#include <type_traits>

#include <Xi/Extern/Push.hh>
#include <boost/thread/locks.hpp>
#include <Xi/Extern/Pop.hh>

#include "Xi/Async/Mutex/Mutex.hpp"
#include "Xi/Async/Mutex/SharedMutex.hpp"
#include "Xi/Async/Mutex/SharedUpgradeMutex.hpp"

namespace Xi {
namespace Async {

template <typename _MutexT>
using GuardedLock = std::lock_guard<_MutexT>;

template <typename _MutexT>
using UniqueLock = std::unique_lock<_MutexT>;

template <typename _MutexT>
using SharedLock = boost::shared_lock<_MutexT>;

template <typename _MutexT>
using UpgradeLock = boost::upgrade_lock<_MutexT>;

template <typename _MutexT>
using UpgradeToUniqueLock = boost::upgrade_to_unique_lock<_MutexT>;

template <typename _MutexT>
inline GuardedLock<_MutexT> lockGuarded(_MutexT& mutex) {
  return GuardedLock<_MutexT>{mutex};
}

template <typename _MutexT>
inline UniqueLock<_MutexT> lockUnique(_MutexT& mutex) {
  return UniqueLock<_MutexT>{mutex};
}

struct GuardedOverload {
  /* */
};
constexpr GuardedOverload guarded{/* */};

template <typename _MutexT>
inline GuardedLock<_MutexT> lock(GuardedOverload, _MutexT& mutex) {
  return lockGuarded(mutex);
}

struct UniqueOverload {
  /* */
};
constexpr UniqueOverload unique{/* */};

template <typename _MutexT>
inline UniqueLock<_MutexT> lock(UniqueOverload, _MutexT& mutex) {
  return lockUnique(mutex);
}

template <typename _MutexT>
inline GuardedLock<_MutexT> lock(_MutexT& mutex) {
  return lock(guarded, mutex);
}

struct SharedOverload {
  /* */
};
constexpr SharedOverload shared{/* */};

template <typename _MutexT>
inline SharedLock<_MutexT> lock(SharedOverload, _MutexT& mutex) {
  return SharedLock<_MutexT>{mutex};
}

struct UpgradeOverload {
  /* */
};
constexpr UpgradeOverload upgrade{/* */};

template <typename _MutexT>
inline UpgradeLock<_MutexT> lock(UpgradeOverload, _MutexT& mutex) {
  return UpgradeLock<_MutexT>{mutex};
}

struct UpgradeToUniqueOverload {
  /* */
};
constexpr UpgradeToUniqueOverload acquire{/* */};
constexpr UpgradeToUniqueOverload upgrade_to_unique{/* */};

template <typename _MutexT>
inline UpgradeToUniqueLock<_MutexT> lock(UpgradeToUniqueOverload, UpgradeLock<_MutexT>& shared) {
  return UpgradeToUniqueLock<_MutexT>{shared};
}

template <typename _MutexT>
inline UpgradeToUniqueLock<_MutexT> lock(UpgradeLock<_MutexT>& shared) {
  return lock(upgrade_to_unique, shared);
}

}  // namespace Async
}  // namespace Xi
