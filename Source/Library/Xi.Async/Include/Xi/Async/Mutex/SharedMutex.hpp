// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <mutex>
#include <cinttypes>
#include <shared_mutex>
#include <chrono>
#include <cassert>

#include <Xi/Extern/Push.hh>
#include <boost/fiber/timed_mutex.hpp>
#include <boost/fiber/condition_variable.hpp>
#include <Xi/Extern/Pop.hh>

namespace Xi {
namespace Async {

class SharedMutex {
 private:
  using condition_variable = boost::fibers::condition_variable_any;
  using timed_mutex = boost::fibers::timed_mutex;
  using unique_lock = std::unique_lock<timed_mutex>;
  using readers_count = uint32_t;

  static inline constexpr readers_count write_entered_mask = 1U << (sizeof(readers_count) * 8 - 1);
  static inline constexpr readers_count readers_count_mask = ~write_entered_mask;

 private:
  boost::fibers::timed_mutex m_access{/* */};
  readers_count m_readers{0};
  condition_variable m_stateGate{/* */};
  condition_variable m_noReadersGate{/* */};

  [[nodiscard]] inline bool hasPendingWrite() const {
    return (m_readers & write_entered_mask) > 0;
  }
  [[nodiscard]] inline bool hasNotPendingWrite() const {
    return (m_readers & write_entered_mask) == 0;
  }
  [[nodiscard]] inline bool hasPendingRead() const {
    return (m_readers & readers_count_mask) > 0;
  }
  [[nodiscard]] inline bool hasNotPendingRead() const {
    return (m_readers & readers_count_mask) == 0;
  }

  [[nodiscard]] inline bool readersCountWillOverflow() const {
    return (m_readers & readers_count_mask) == readers_count_mask;
  }
  [[nodiscard]] inline bool readersCountWillNotOverflow() const {
    return (m_readers & readers_count_mask) < readers_count_mask;
  }

  inline void enterWriteState() {
    m_readers |= write_entered_mask;
  }
  inline void leaveWriteState() {
    m_readers &= ~write_entered_mask;
  }

  inline readers_count readersCount() const {
    return m_readers & readers_count_mask;
  }
  inline void setReadersCount(readers_count count) {
    m_readers &= ~readers_count_mask;
    m_readers |= count;
  }

  inline void enterReadState() {
    setReadersCount((m_readers & readers_count_mask) + 1);
  }
  inline void leaveReadState() {
    setReadersCount((m_readers & readers_count_mask) - 1);
  }

 public:
  inline ~SharedMutex() {
    [[maybe_unused]] unique_lock lock{m_access};
  }

  template <typename _ClockT, typename _DurationT>
  [[nodiscard]] bool try_lock_until(const std::chrono::time_point<_ClockT, _DurationT>& limit) {
    if (!m_access.try_lock_until(limit)) {
      return false;
    }
    unique_lock lock{m_access, std::adopt_lock};

    if (hasPendingWrite()) {
      while (true) {
        const auto status = m_stateGate.wait_until(lock, limit);
        if (hasNotPendingWrite()) {
          break;
        }
        if (status == boost::fibers::cv_status::timeout) {
          return false;
        }
      }
    }

    enterWriteState();

    if (hasPendingRead()) {
      while (true) {
        const auto status = m_noReadersGate.wait_until(lock, limit);
        if (hasNotPendingRead()) {
          break;
        }
        if (status == boost::fibers::cv_status::timeout) {
          leaveWriteState();
          return false;
        }
      }
    }

    return true;
  }

  template <typename _RepT, typename _PeriodT>
  [[nodiscard]] bool try_lock_for(const std::chrono::duration<_RepT, _PeriodT>& duration) {
    return try_lock_until(std::chrono::steady_clock::now() + duration);
  }

  void lock() {
    unique_lock lock{m_access};
    while (hasPendingWrite()) {
      m_stateGate.wait(lock);
    }
    enterWriteState();
    while (hasPendingRead()) {
      m_noReadersGate.wait(lock);
    }
  }

  void unlock() {
    [[maybe_unused]] unique_lock lock{m_access};
    m_readers = 0;
    m_stateGate.notify_all();
  }

  [[nodiscard]] bool try_lock() {
    if (!m_access.try_lock()) {
      return false;
    }
    [[maybe_unused]] unique_lock lock{m_access, std::adopt_lock};
    if (hasPendingRead() || hasPendingWrite()) {
      return false;
    }
    enterWriteState();
    return true;
  }

  template <typename _ClockT, typename _DurationT>
  [[nodiscard]] bool try_lock_shared_until(const std::chrono::time_point<_ClockT, _DurationT>& limit) {
    if (!m_access.try_lock_until(limit)) {
      return false;
    }
    unique_lock lock{m_access, std::adopt_lock};

    while (true) {
      if (hasPendingWrite()) {
        const auto status = m_stateGate.wait_until(lock, limit);
        if (status == boost::fibers::cv_status::timeout) {
          return false;
        }
      }

      if (readersCountWillOverflow()) {
        const auto status = m_stateGate.wait_until(lock, limit);
        if (status == boost::fibers::cv_status::timeout) {
          return false;
        }
        if (hasNotPendingWrite()) {
          break;
        }
      }
    }

    enterReadState();
    return true;
  }

  template <typename _RepT, typename _PeriodT>
  [[nodiscard]] bool try_lock_shared_for(const std::chrono::duration<_RepT, _PeriodT>& duration) {
    return try_lock_shared_until(std::chrono::steady_clock::now() + duration);
  }

  void lock_shared() {
    unique_lock lock{m_access};
    while (true) {
      if (hasPendingWrite()) {
        m_stateGate.wait(lock);
      }

      if (readersCountWillOverflow()) {
        m_stateGate.wait(lock);
        if (hasNotPendingWrite()) {
          break;
        }
      }
    }

    enterReadState();
  }

  [[nodiscard]] bool try_lock_shared() {
    if (!m_access.try_lock()) {
      return false;
    }
    [[maybe_unused]] unique_lock lock{m_access, std::adopt_lock};
    if (hasPendingWrite() || readersCountWillOverflow()) {
      return false;
    }
    enterReadState();
    return true;
  }

  void unlock_shared() {
    [[maybe_unused]] unique_lock lock{m_access};
    if (hasNotPendingWrite()) {
      if (readersCountWillOverflow()) {
        leaveReadState();
        m_stateGate.notify_one();
      }
    } else {
      leaveReadState();
      if (readersCount() == 0) {
        m_noReadersGate.notify_one();
      }
    }
  }
};

}  // namespace Async
}  // namespace Xi
