// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include "Xi/Async/Boost.hpp"

namespace Xi {
namespace Async {

using Mutex = Boost::Mutex;

}
}  // namespace Xi
