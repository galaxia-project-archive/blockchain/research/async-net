// Copyright (c) 2019 by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include <cinttypes>

#include <gmock/gmock.h>

#include <Xi/Async/Async.hpp>

#define XI_UNIT_TEST_SUITE Async_Event

TEST(XI_UNIT_TEST_SUITE, Notify) {
  using namespace Xi;
  using namespace Xi::Async;

  Event event{/* */};
  event.notify();
  EXPECT_EQ(event.wait(), DecidingWaitResult::Success);
  event.cancel();
  EXPECT_EQ(event.wait(), DecidingWaitResult::Success);
}

TEST(XI_UNIT_TEST_SUITE, Cancel) {
  using namespace Xi;
  using namespace Xi::Async;

  Event event{/* */};
  event.cancel();
  EXPECT_EQ(event.wait(), DecidingWaitResult::Cancelled);
  event.notify();
  EXPECT_EQ(event.wait(), DecidingWaitResult::Cancelled);
}

TEST(XI_UNIT_TEST_SUITE, Timeout) {
  using namespace Xi;
  using namespace Xi::Async;

  {
    Event event{/* */};
    event.notifyAfter(std::chrono::milliseconds{2});
    auto res = event.waitFor(std::chrono::milliseconds{1});
    EXPECT_EQ(res, WaitResult::Timeout);
    EXPECT_EQ(event.waitFor(std::chrono::milliseconds{2}), WaitResult::Success);
  }

  {
    Event event{/* */};
    event.cancelAfter(std::chrono::milliseconds{2});
    auto res = event.waitFor(std::chrono::milliseconds{1});
    EXPECT_EQ(res, WaitResult::Timeout);
    EXPECT_EQ(event.waitFor(std::chrono::milliseconds{2}), WaitResult::Cancelled);
  }
}

TEST(XI_UNIT_TEST_SUITE, OnNotifyDetach) {
  using namespace Xi;
  using namespace Xi::Async;

  Event first{/* */};
  Event second{/* */};
  first.onNotify([second]() mutable { second.cancel(); }).detach();
  first.notifyAfter(std::chrono::milliseconds{2});
  EXPECT_EQ(second.wait(), DecidingWaitResult::Cancelled);
}

TEST(XI_UNIT_TEST_SUITE, OnCancelDetach) {
  using namespace Xi;
  using namespace Xi::Async;

  Event first{/* */};
  Event second{/* */};
  first.onCancel([second]() mutable { second.notify(); }).detach();
  first.cancelAfter(std::chrono::milliseconds{2});
  EXPECT_EQ(second.wait(), DecidingWaitResult::Success);
}

TEST(XI_UNIT_TEST_SUITE, IfAnyNotifiedSuccess) {
  using namespace Xi;
  using namespace Xi::Async;

  Event first{/* */};
  Event second{/* */};
  auto any = ifAnyNotified({first, second});
  first.cancelAfter(std::chrono::milliseconds{8});
  second.notifyAfter(std::chrono::milliseconds{2});

  EXPECT_EQ(any.wait(), DecidingWaitResult::Success);
  EXPECT_EQ(first.waitFor(std::chrono::milliseconds{0}), WaitResult::Timeout);
}

TEST(XI_UNIT_TEST_SUITE, IfAnyNotifiedCancelled) {
  using namespace Xi;
  using namespace Xi::Async;

  Event first{/* */};
  Event second{/* */};
  auto any = ifAnyNotified({first, second});
  first.cancelAfter(std::chrono::milliseconds{5});
  second.cancelAfter(std::chrono::milliseconds{2});

  EXPECT_EQ(any.wait(), DecidingWaitResult::Cancelled);
}

TEST(XI_UNIT_TEST_SUITE, IfAnyNotifiedEmpty) {
  using namespace Xi;
  using namespace Xi::Async;

  auto any = ifAnyNotified({});
  EXPECT_EQ(any.wait(), DecidingWaitResult::Cancelled);
}

TEST(XI_UNIT_TEST_SUITE, IfAnyNotifiedTimeout) {
  using namespace Xi;
  using namespace Xi::Async;

  Event first{/* */};
  Event second{/* */};
  auto any = ifAnyNotified({first, second});
  first.cancelAfter(std::chrono::milliseconds{5});
  second.cancelAfter(std::chrono::milliseconds{5});

  EXPECT_EQ(any.waitFor(std::chrono::milliseconds{2}), WaitResult::Timeout);
}

TEST(XI_UNIT_TEST_SUITE, IfAllNotified) {
  using namespace Xi;
  using namespace Xi::Async;

  Event first{/* */};
  Event second{/* */};
  auto any = ifAllNotified({first, second});
  first.notifyAfter(std::chrono::milliseconds{5});
  second.notifyAfter(std::chrono::milliseconds{5});

  EXPECT_EQ(any.waitFor(std::chrono::milliseconds{10}), WaitResult::Success);
}

TEST(XI_UNIT_TEST_SUITE, IfAllNotifiedOneCancelled) {
  using namespace Xi;
  using namespace Xi::Async;

  Event first{/* */};
  Event second{/* */};
  auto any = ifAllNotified({first, second});
  first.notifyAfter(std::chrono::milliseconds{5});
  second.cancelAfter(std::chrono::milliseconds{5});

  EXPECT_EQ(any.waitFor(std::chrono::milliseconds{10}), WaitResult::Cancelled);
}
