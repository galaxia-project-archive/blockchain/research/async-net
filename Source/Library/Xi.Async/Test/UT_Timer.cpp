﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include <vector>

#include <gmock/gmock.h>

#include <Xi/Async/Async.hpp>

#define XI_UNIT_TEST_SUITE Async_Timer

TEST(XI_UNIT_TEST_SUITE, Once) {
  using namespace Xi::Async;

  Event getNotified{/* */};
  [[maybe_unused]] auto timer =
      Timer::once(std::chrono::milliseconds{20}, [getNotified]() mutable { getNotified.notify(); });
  EXPECT_EQ(getNotified.wait(), DecidingWaitResult::Success);
}

TEST(XI_UNIT_TEST_SUITE, Repeat) {
  using namespace Xi;
  using namespace Xi::Async;

  int countOnMe = 0;
  {
    [[maybe_unused]] auto timer =
        Timer::repeat(std::chrono::milliseconds{25}, [&countOnMe]() mutable { countOnMe += 1; });
    sleepFor(Chrono::MilliSeconds{110});
  }
  sleepFor(Chrono::MilliSeconds{60});
  EXPECT_EQ(countOnMe, 4);
}
