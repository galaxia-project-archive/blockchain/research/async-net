﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include <string>
#include <iostream>

#include <gmock/gmock.h>

#include <Xi/Extern/Push.hh>
#include <boost/asio/steady_timer.hpp>
#include <boost/asio/dispatch.hpp>
#include <Xi/Extern/Pop.hh>

#include <Xi/Async/Async.hpp>

#define XI_UNIT_TEST_SUITE Async_Yield

TEST(XI_UNIT_TEST_SUITE, StorageConstructor) {
  using namespace Xi::Async;

  int first = 0;
  std::string second{};
  auto storage = yield(first, second);

  EXPECT_EQ(std::get<0>(storage.arguments), std::addressof(first));
  EXPECT_EQ(std::get<1>(storage.arguments), std::addressof(second));
}

TEST(XI_UNIT_TEST_SUITE, TimerAsyncInvoke) {
  using namespace Xi;
  using namespace Xi::Async;

  const size_t size = 10000;

  std::vector<boost::asio::steady_timer> timers;
  timers.reserve(size);
  std::vector<Future<boost::system::error_code>> waiters;
  waiters.reserve(size);

  for (size_t i = 0; i < size; ++i) {
    timers.emplace_back(this_executor::sharedIo());
    timers.back().expires_after(std::chrono::milliseconds{10});
    waiters.emplace_back(Async::invoke(
        Launch::Post,
        [](boost::asio::steady_timer& timer) {
          boost::system::error_code ec{};
          timer.async_wait(yield(ec));
          return ec;
        },
        std::ref(timers.back())));
  }

  for (auto& timer : timers) {
    timer.cancel();
  }
  for (auto& waiter : waiters) {
    EXPECT_FALSE(waiter.get().failed());
  }
}
