﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Async/Loop.hpp"

#include <thread>
#include <type_traits>
#include <limits>
#include <future>
#include <vector>
#include <cassert>

#include <Xi/Extern/Push.hh>
#include <boost/asio.hpp>
#include <boost/fiber/algo/round_robin.hpp>
#include <Xi/Extern/Pop.hh>

#include <Xi/ErrorModel.hh>

#include "Xi/Async/Scheduler/Executor.hpp"

#include "LoopGuard.hpp"
#include "scheduler/RoundRobin.hpp"

namespace Xi {
namespace Async {

namespace {
static thread_local Loop* this_loop = nullptr;

void runExecutor(Loop* loop, LoopGuard& guard, Boost::IoContext& sharedIo) {
  assert(this_loop == nullptr);
  this_loop = loop;
  auto exec = std::make_shared<Scheduler::Executor>(sharedIo);
  guard.add(exec->localIo());
  boost::fibers::use_scheduling_algorithm<Scheduler::RoundRobin>(exec);
  (*exec)();
  boost::fibers::use_scheduling_algorithm<boost::fibers::algo::round_robin>();
  this_loop = nullptr;
}
}  // namespace

Loop& Loop::current() {
  XI_EXCEPTIONAL_IF(NotInitializedError, this_loop == nullptr);
  return *this_loop;
}

Loop::Loop() : m_guard{new LoopGuard} {
  /* */
}

Loop::~Loop() {
  /* */
}

void Loop::run(std::uint32_t numThreads) {
  if (numThreads == 0) {
    numThreads = static_cast<decltype(numThreads)>(std::thread::hardware_concurrency());
  }
  XI_EXCEPTIONAL_IF(RuntimeError, numThreads == 0)

  Boost::IoContext sharedIo{static_cast<int>(numThreads)};
  m_guard->add(sharedIo);
  std::vector<std::future<void>> worker{};
  worker.reserve(numThreads - 1);

  for (uint32_t i = 1; i < numThreads; ++i) {
    worker.emplace_back(std::async(std::launch::async, runExecutor, this, std::ref(*m_guard), std::ref(sharedIo)));
  }
  runExecutor(this, *m_guard, sharedIo);
  for (auto& exec : worker) {
    exec.wait();
  }
}

void Loop::shutdown() {
  m_guard.reset();
}

}  // namespace Async
}  // namespace Xi
