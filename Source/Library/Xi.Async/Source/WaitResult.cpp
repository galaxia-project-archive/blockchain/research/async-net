﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Async/WaitResult.hpp"

#include <type_traits>

#include <Xi/ErrorModel.hh>

#include "Xi/Async/Boost.hpp"

namespace Xi::Async {

static_assert(
    static_cast<std::underlying_type_t<Boost::ConditionVariableStatus>>(Boost::ConditionVariableStatus::timeout) ==
        static_cast<std::underlying_type_t<Boost::ConditionVariableStatus>>(WaitResult::Timeout),
    "must match because they are internally casted without any check");
static_assert(
    static_cast<std::underlying_type_t<Boost::ConditionVariableStatus>>(Boost::ConditionVariableStatus::no_timeout) ==
        static_cast<std::underlying_type_t<Boost::ConditionVariableStatus>>(WaitResult::Success),
    "must match because they are internally casted without any check");

bool isSuccess(WaitResult res) {
  switch (res) {
    case WaitResult::Success:
      return true;
    case WaitResult::Timeout:
    case WaitResult::Cancelled:
      return false;
  }
  XI_EXCEPTIONAL(InvalidEnumValueError)
}

bool isSuccess(DecidingWaitResult res) {
  switch (res) {
    case DecidingWaitResult::Success:
      return true;
    case DecidingWaitResult::Cancelled:
      return false;
  }
  XI_EXCEPTIONAL(InvalidEnumValueError)
}

bool isCancelled(WaitResult res) {
  switch (res) {
    case WaitResult::Cancelled:
      return true;
    case WaitResult::Timeout:
    case WaitResult::Success:
      return false;
  }
  XI_EXCEPTIONAL(InvalidEnumValueError)
}

bool isCancelled(DecidingWaitResult res) {
  switch (res) {
    case DecidingWaitResult::Cancelled:
      return true;
    case DecidingWaitResult::Success:
      return false;
  }
  XI_EXCEPTIONAL(InvalidEnumValueError)
}

bool isTimeout(WaitResult res) {
  switch (res) {
    case WaitResult::Timeout:
      return true;
    case WaitResult::Success:
    case WaitResult::Cancelled:
      return false;
  }
  XI_EXCEPTIONAL(InvalidEnumValueError)
}

}  // namespace Xi::Async
