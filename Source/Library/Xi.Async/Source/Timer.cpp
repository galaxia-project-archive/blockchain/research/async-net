﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Async/Timer.hpp"

#include "Xi/Async/Scheduler/Executor.hpp"

namespace Xi::Async {

std::shared_ptr<Timer> Timer::once(const std::chrono::milliseconds& duration, Callback callback) {
  std::shared_ptr<Timer> reval{new Timer{std::move(callback), duration}};
  reval->m_repeat = false;
  reval->start();
  return reval;
}

std::shared_ptr<Timer> Timer::repeat(const std::chrono::milliseconds& duration, Callback callback) {
  std::shared_ptr<Timer> reval{new Timer{std::move(callback), duration}};
  reval->m_repeat = true;
  reval->start();
  return reval;
}

Timer::~Timer() {
  if (m_state.load(std::memory_order_consume) == State::Running) {
    cancel();
  }
}

Timer::State Timer::state() const {
  return m_state.load(std::memory_order_consume);
}

void Timer::cancel() {
  m_timer.cancel();
  m_state.store(State::Cancelled, std::memory_order_release);
}

void Timer::pause() {
  m_timer.cancel();
  m_state.store(State::Paused, std::memory_order_release);
}

void Timer::resume() {
  if (state() == State::Paused) {
    m_state.store(State::Running, std::memory_order_release);
    m_timer.expires_from_now(boost::posix_time::milliseconds{m_duration.count()});
    m_timer.async_wait(std::bind(&Timer::invoke, shared_from_this(), std::placeholders::_1));
  }
}

Timer::Timer(Callback cb, const Duration& dur)
    : m_callback{std::move(cb)},
      m_duration{dur},
      m_timer{this_executor::localIo()},
      m_state{State::Running},
      m_repeat{false} {
}

void Timer::invoke(std::weak_ptr<Timer> timer, boost::system::error_code ec) {
  auto this_ = timer.lock();
  if (!this_) {
    return;
  }

  if (ec == boost::system::errc::operation_canceled) {
    this_->m_state.store(State::Finished, std::memory_order_release);
    return;
  }
  if (this_->state() == State::Running) {
    this_->m_callback();
    if (this_->m_repeat) {
      this_->start();
    } else {
      this_->m_state.store(State::Finished, std::memory_order_release);
    }
  }
}

void Timer::start() {
  m_timer.expires_from_now(
      boost::posix_time::milliseconds{Chrono::duration_cast<Chrono::MilliSeconds>(m_duration).count()});
  m_timer.async_wait(std::bind(&Timer::invoke, std::weak_ptr<Timer>{shared_from_this()}, std::placeholders::_1));
}

}  // namespace Xi::Async
