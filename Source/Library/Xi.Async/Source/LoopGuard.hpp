// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <vector>

#include <Xi/Global.hh>

#include "Xi/Async/Boost.hpp"

namespace Xi {
namespace Async {

XI_DECLARE_SMART_POINTER_CLASS(LoopGuard)

class LoopGuard final {
 public:
  explicit LoopGuard();
  XI_DELETE_COPY(LoopGuard)
  XI_DELETE_MOVE(LoopGuard)
  ~LoopGuard();

  void add(Boost::IoContext& context);
  void shutdown();

 private:
  struct Impl;
  std::unique_ptr<Impl> m_impl;
};

}  // namespace Async
}  // namespace Xi
