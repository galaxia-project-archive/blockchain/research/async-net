﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Async/Scheduler/Executor.hpp"

#include <Xi/ErrorModel.hh>

#include "Xi/Async/Mutex.hpp"

namespace {
static thread_local Xi::Async::Scheduler::Executor *_this_exectuor = nullptr;
}  // namespace

namespace Xi::Async {
namespace Scheduler {

Executor::Executor(Boost::IoContext &sharedIo_) : m_sharedIo{sharedIo_}, m_localIo{1} {
  /* */
}

Executor::~Executor() {
  /* */
}

Boost::IoContext &Executor::sharedIo() {
  return m_sharedIo;
}

const Boost::IoContext &Executor::sharedIo() const {
  return m_sharedIo;
}

Boost::IoContext &Executor::localIo() {
  return m_localIo;
}

const Boost::IoContext &Executor::localIo() const {
  return m_localIo;
}

std::thread::id Executor::thread() const {
  return m_thread;
}

void Executor::operator()() {
  XI_EXCEPTIONAL_IF_NOT(RuntimeError, _this_exectuor == nullptr);
  _this_exectuor = this;
  m_thread = std::this_thread::get_id();
  boost::asio::post(m_localIo, []() { boost::this_fiber::yield(); });

  while (!localIo().stopped() || !sharedIo().stopped()) {
    try {
      size_t executed = 0;
      while (localIo().poll_one()) {
        executed += 1;
      }
      if (sharedIo().poll_one()) {
        executed += 1;
      }
      if (executed > 0) {
        continue;
      }
      boost::this_fiber::yield();
    } catch (std::exception &e) {
      XI_PRINT_EC("io queue threw: %s", e.what());
      XI_UNUSED(e)
    } catch (...) {
      XI_PRINT_EC("io queue threw: UNKNOWN");
    }
  }
  m_thread = std::thread::id{/* */};
  _this_exectuor = nullptr;
}

}  // namespace Scheduler

namespace this_executor {

Boost::IoContext &sharedIo() {
  XI_EXCEPTIONAL_IF(RuntimeError, _this_exectuor == nullptr);
  return _this_exectuor->sharedIo();
}

Boost::IoContext &localIo() {
  XI_EXCEPTIONAL_IF(RuntimeError, _this_exectuor == nullptr);
  return _this_exectuor->localIo();
}

std::thread::id thread() {
  XI_EXCEPTIONAL_IF(RuntimeError, _this_exectuor == nullptr);
  return _this_exectuor->thread();
}

}  // namespace this_executor

}  // namespace Xi::Async
