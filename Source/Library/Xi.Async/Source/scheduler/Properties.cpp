// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Async/Scheduler/Properties.hpp"

#include <cassert>

#include "Xi/Async/Scheduler/Executor.hpp"

namespace Xi::Async {
namespace Scheduler {

Properties& Properties::current() {
  return boost::this_fiber::properties<Properties>();
}

Properties::Properties(Boost::FiberContext* context) : Properties{context, WeakExecutor{/* */}} {
  /* */
}

Properties::Properties(Boost::FiberContext* context, WeakExecutor executor_)
    : Boost::FiberProperties(context), m_executor{executor_} {
  /* */
}

Properties::~Properties() {
  /* */
}

WeakExecutor Properties::executor() {
  return m_executor;
}

WeakConstExecutor Properties::executor() const {
  return m_executor;
}

Event& Properties::cancellation() {
  return m_cancellation;
}

const Event& Properties::cancellation() const {
  return m_cancellation;
}

void Properties::schedule(Boost::FiberContext* context) {
  auto lockedExecutor = executor().lock();
  assert(lockedExecutor);
  if (this_executor::thread() != lockedExecutor->thread()) {
    lockedExecutor->localIo().post([context]() { boost::fibers::context::active()->schedule(context); });
  } else {
    boost::fibers::context::active()->schedule(context);
  }
}

}  // namespace Scheduler

namespace this_fiber {

Event cancellation() {
  return Scheduler::Properties::current().cancellation();
}

Scheduler::WeakExecutor executor() {
  return Scheduler::Properties::current().executor();
}

}  // namespace this_fiber

}  // namespace Xi::Async
