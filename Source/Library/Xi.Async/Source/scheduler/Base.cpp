// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "scheduler/Base.hpp"

#include <utility>

#include <Xi/ErrorModel.hh>

namespace Xi::Async::Scheduler {

Base::~Base() {
  /* */
}

Boost::FiberProperties *Base::new_properties(Boost::FiberContext *context) {
  return new Properties{context, executor()};
}

Base::Base(SharedExecutor executor_) : m_executor{executor_} {
  XI_EXCEPTIONAL_IF(NullArgumentError, executor() == nullptr);
}

SharedExecutor Base::executor() {
  return m_executor;
}

SharedConstExecutor Base::executor() const {
  return m_executor;
}

}  // namespace Xi::Async::Scheduler
