// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "LoopGuard.hpp"

#include <vector>

#include "Xi/Async/Mutex.hpp"

namespace Xi {
namespace Async {

namespace {
using Work = std::unique_ptr<Boost::IoContext::work>;
}

struct LoopGuard::Impl {
  std::vector<Work> work;
  Mutex guard;
};

LoopGuard::LoopGuard() : m_impl{new Impl} {
  /* */
}

LoopGuard::~LoopGuard() {
  shutdown();
}

void LoopGuard::add(Boost::IoContext &context) {
  [[maybe_unused]] auto lck = lock(m_impl->guard);
  m_impl->work.emplace_back(std::make_unique<Boost::IoContext::work>(context));
}

void LoopGuard::shutdown() {
  [[maybe_unused]] auto lck = lock(m_impl->guard);
  m_impl->work.clear();
}

}  // namespace Async
}  // namespace Xi
