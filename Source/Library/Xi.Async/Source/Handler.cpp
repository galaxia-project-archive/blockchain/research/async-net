﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Async/Handler.hpp"

#include <utility>

#include <Xi/ErrorModel.hh>

#include "Xi/Async/Boost.hpp"
#include "Xi/Async/Future.hpp"
#include "Xi/Async/Event.hpp"
#include "Xi/Async/WaitResult.hpp"
#include "Xi/Async/PackagedTask.hpp"
#include "Xi/Async/Invoke.hpp"

namespace Xi::Async {

namespace {
void onForward(const SharedFuture<DecidingWaitResult> &source, Event trigger, const DecidingWaitResult desired) {
  if (source.get() == desired) {
    trigger.notify();
  } else {
    trigger.cancel();
  }
}

void onTrigger(const Handler::Callback &cb, const Event &trigger) {
  if (trigger.wait() == DecidingWaitResult::Success) {
    cb();
  }
}
}  // namespace

struct Handler::_Impl {
  Event trigger;
};

Handler::Handler() : m_impl{new _Impl} {
  /* */
}

Handler::~Handler() {
  cancel();
}

void Handler::cancel() {
  [[maybe_unused]] auto thisLock = lock(m_guard);
  if (m_impl) {
    m_impl->trigger.cancel();
  }
  m_impl.reset();
}

void Handler::detach() {
  boost::fibers::fiber{[](Handler this_) {
                         [[maybe_unused]] auto thisLock = lock(this_.m_guard);
                         if (this_.m_impl) {
                           (void)this_.m_impl->trigger.wait();
                         }
                       },
                       std::move(*this)}
      .detach();
}

Handler::Handler(Handler &&other) {
  [[maybe_unused]] auto otherLock = lock(other.m_guard);
  this->m_impl = std::move(other.m_impl);
  other.m_impl.reset();
}

Handler &Handler::operator=(Handler &&other) {
  if (this == std::addressof(other)) {
    return *this;
  }
  {
    [[maybe_unused]] auto thisLock = lock(m_guard);
    [[maybe_unused]] auto otherLock = lock(other.m_guard);
    this->m_impl = std::move(other.m_impl);
    other.m_impl.reset();
    return *this;
  }
}

Handler makeHandler(Handler::Callback &&cb, SharedFuture<DecidingWaitResult> notifier) {
  return makeHandler(std::move(cb), std::move(notifier), DecidingWaitResult::Success);
}

Handler makeHandler(Handler::Callback &&cb, SharedFuture<DecidingWaitResult> notifier,
                    const DecidingWaitResult desiredState) {
  Handler reval{/* */};
  Async::invoke(onForward, std::move(notifier), reval.m_impl->trigger, desiredState);
  Async::invoke(onTrigger, std::move(cb), reval.m_impl->trigger);
  return reval;
}

}  // namespace Xi::Async
