// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Serialization/Json/JsonError.hpp"

XI_ERROR_CODE_CATEGORY_BEGIN(Xi::Serialization::Json, Json)
XI_ERROR_CODE_DESC(NoValue, "expected value actually no value given")
XI_ERROR_CODE_DESC(Internal, "internal libarary failure")
XI_ERROR_CODE_DESC(TrailingBytes, "json string had tralining charactes, not parsed")

XI_ERROR_CODE_DESC(TypeMissmatchInteger, "type missmatch expected integer")
XI_ERROR_CODE_DESC(TypeMissmatchLongInteger, "type missmatch expected long integer")
XI_ERROR_CODE_DESC(TypeMissmatchUnsignedInteger, "type missmatch expected unisgned integer")
XI_ERROR_CODE_DESC(TypeMissmatchUnsignedLongInteger, "type missmatch expected unisgend long integer")
XI_ERROR_CODE_DESC(TypeMissmatchNumber, "type missmatch expected floating number")
XI_ERROR_CODE_DESC(TypeMissmatchLongNumber, "type missmatch expected long floating number")
XI_ERROR_CODE_DESC(TypeMissmatchString, "type missmatch expected string")
XI_ERROR_CODE_DESC(TypeMissmatchBoolean, "type missmatch expected boolean")
XI_ERROR_CODE_DESC(TypeMissmatchArray, "type missmatch expected array")
XI_ERROR_CODE_DESC(TypeMissmatchObject, "type missmatch expected object")

XI_ERROR_CODE_DESC(IndexOutOfRange, "given index is out of range")
XI_ERROR_CODE_DESC(IntegerOutOfBounds, "integer excceds its internal memory boundary")

XI_ERROR_CODE_DESC(NullTag, "type tag is null")
XI_ERROR_CODE_DESC(FlagOverflow, "too many flags provided")
XI_ERROR_CODE_DESC(DuplicateTag, "flag array contains duplicates")

XI_ERROR_CODE_DESC(SizeMissmatch, "blob size does not match")
XI_ERROR_CODE_CATEGORY_END()
