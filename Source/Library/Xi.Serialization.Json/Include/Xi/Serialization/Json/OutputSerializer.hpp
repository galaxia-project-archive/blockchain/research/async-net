// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <stack>
#include <utility>
#include <sstream>

#include <Xi/Stream/OutputStream.hpp>
#include <Xi/Stream/OStream.hpp>
#include <Xi/Serialization/Serializer.hpp>
#include <Xi/Serialization/OutputSerializer.hpp>

namespace Xi {
namespace Serialization {
namespace Json {
class OutputSerializer final : public Serialization::OutputSerializer {
 public:
  OutputSerializer(Stream::OutputStream& stream);
  ~OutputSerializer() override;

  Format format() const override;

  Result<void> writeInt8(std::int8_t value, const Tag& nameTag) override;
  Result<void> writeUInt8(std::uint8_t value, const Tag& nameTag) override;
  Result<void> writeInt16(std::int16_t value, const Tag& nameTag) override;
  Result<void> writeUInt16(std::uint16_t value, const Tag& nameTag) override;
  Result<void> writeInt32(std::int32_t value, const Tag& nameTag) override;
  Result<void> writeUInt32(std::uint32_t value, const Tag& nameTag) override;
  Result<void> writeInt64(std::int64_t value, const Tag& nameTag) override;
  Result<void> writeUInt64(std::uint64_t value, const Tag& nameTag) override;

  Result<void> writeBoolean(bool value, const Tag& nameTag) override;

  Result<void> writeFloat(float value, const Tag& nameTag) override;
  Result<void> writeDouble(double value, const Tag& nameTag) override;

  Result<void> writeTag(const Tag& value, const Tag& nameTag) override;
  Result<void> writeFlag(const TagVector& flag, const Tag& nameTag) override;

  Result<void> writeString(const std::string_view value, const Tag& nameTag) override;
  Result<void> writeBinary(ConstByteSpan value, const Tag& nameTag) override;

  Result<void> writeBlob(ConstByteSpan value, const Tag& nameTag) override;

  Result<void> beginWriteComplex(const Tag& nameTag) override;
  Result<void> endWriteComplex() override;

  Result<void> beginWriteVector(size_t size, const Tag& nameTag) override;
  Result<void> endWriteVector() override;

  Result<void> beginWriteArray(size_t size, const Tag& nameTag) override;
  Result<void> endWriteArray() override;

  Result<void> writeNull(const Tag& nameTag) override;
  Result<void> writeNotNull(const Tag& nameTag) override;

 private:
  struct _Impl;
  std::unique_ptr<_Impl> m_impl;
};
}  // namespace Json

template <typename _ValueT>
Result<void> toJson(const _ValueT& value, Stream::OutputStream& ostream) {
  Json::OutputSerializer output{ostream};
  Serialization::Serializer ser{output};
  XI_ERROR_PROPAGATE_CATCH(ser(const_cast<_ValueT&>(value), Tag::Null))
  XI_SUCCEED()
}

template <typename _ValueT>
Result<std::string> toJson(const _ValueT& value) {
  std::stringstream reval{};
  Stream::OStream stream{makeDelegatedOwnership<std::ostream>(reval)};
  XI_ERROR_PROPAGATE_CATCH(toJson(value, stream))
  XI_SUCCEED(reval.str());
}

}  // namespace Serialization
}  // namespace Xi
