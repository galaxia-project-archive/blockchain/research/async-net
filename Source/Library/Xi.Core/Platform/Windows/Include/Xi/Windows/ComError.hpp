// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <winerror.h>
#include <system_error>
#include <type_traits>

namespace Xi {
namespace Windows {

enum struct ComError : int {
  Success = S_OK,
  Aborted = E_ABORT,
  AccessDenied = E_ACCESSDENIED,
  Fail = E_FAIL,
  Handle = E_HANDLE,
  InvalidArgument = E_INVALIDARG,
  NoInterface = E_NOINTERFACE,
  NotImplemented = E_NOTIMPL,
  OutOfMemory = E_OUTOFMEMORY,
  InvalidPointer = E_POINTER,
  Unexpected = E_UNEXPECTED,
};

class ComErrorCategory : public std::error_category {
 public:
  using error_category::error_category;

  const char* name() const noexcept override;

  std::string message(int hresult) const override;
  std::error_condition default_error_condition(int hresult) const noexcept override;

 public:
  static ComErrorCategory Instance;
};

inline std::error_code make_error_code(ComError err) {
  return std::error_code(static_cast<int>(err), ComErrorCategory::Instance);
}

ComError toComError(const HRESULT hresult) noexcept;

}  // namespace Windows
}  // namespace Xi

namespace std {
template <>
struct is_error_code_enum<Xi::Windows::ComError> : true_type {
  /* */
};
}  // namespace std
