// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <gmock/gmock.h>

#include <Xi/Global.hh>
#include <Xi/ErrorModel.hh>

namespace Xi {
namespace Testing {
MATCHER(IsSuccess, "Success") {
  XI_UNUSED(result_listener)
  if (!arg.isError()) {
    return true;
  } else {
    return false;
  }
}

MATCHER(IsFailure, "Failure") {
  XI_UNUSED(result_listener)
  return arg.isError();
}
}  // namespace Testing
}  // namespace Xi
