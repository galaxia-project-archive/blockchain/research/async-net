// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/ErrorModel/GlitchError.hpp"

XI_ERROR_CODE_CATEGORY_BEGIN(Xi, Glitch)
XI_ERROR_CODE_DESC(Moved, "result object referenced was moved")
XI_ERROR_CODE_DESC(Exceptional, "exception occured")
XI_ERROR_CODE_DESC(NotInitialized, "uninitalized error/result")
XI_ERROR_CODE_CATEGORY_END()
