﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/ErrorModel/Error.hpp"

#include <stdexcept>
#include <cassert>
#include <sstream>

#include "Xi/ErrorModel/Print.hh"
#include "Xi/ErrorModel/GlitchError.hpp"

namespace Xi {

Error::Error() : m_error{make_error_code(GlitchError::NotInitialized)} {
  /* */
}

Error::Error(std::error_code ec) : m_error{ec} {
  /* */
}

std::string Error::message() const {
  return code().message();
}

std::string Error::category() const {
  return code().category().name();
}

std::string Error::stringify() const {
  std::ostringstream builder{};
  builder << "[";
  builder << category();
  builder << ": ";
  builder << message();
  builder << "]";
  return builder.str();
}

const std::error_code &Error::code() const {
  return m_error;
}

}  // namespace Xi
