// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/String/Replace.hpp"

#include <algorithm>
#include <iterator>

namespace Xi {

std::string replace(std::string_view str, std::string_view toReplace, std::string_view replacement) {
  if (str.empty()) {
    return "";
  }
  if (toReplace.empty() || toReplace.size() > str.size()) {
    return std::string{str.data(), str.size()};
  }

  std::string reval{};
  reval.reserve(str.size());

  size_t pos = 0;
  size_t lastCopy = 0;
  size_t matchPos = 0;

  while (pos < str.size()) {
    if (str[pos] == toReplace[matchPos]) {
      matchPos += 1;
      if (!(matchPos < toReplace.size())) {
        std::copy(str.data() + lastCopy, str.data() + pos + 1 - toReplace.size(), std::back_inserter(reval));
        std::copy(replacement.begin(), replacement.end(), std::back_inserter(reval));
        lastCopy = pos + 1;
        matchPos = 0;
      }
    } else {
      matchPos = 0;
    }

    pos += 1;
  }

  if (lastCopy < pos) {
    std::copy(str.data() + lastCopy, str.data() + pos, std::back_inserter(reval));
  }

  return reval;
}

}  // namespace Xi
