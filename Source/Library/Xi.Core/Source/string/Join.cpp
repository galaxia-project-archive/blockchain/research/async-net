// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/String/Join.hpp"

#include <numeric>
#include <cstring>

namespace Xi {

std::string join(Span<const std::string> strings, std::string_view token) {
  if (strings.empty()) {
    return "";
  } if (strings.size() == 1) {
    return strings.front();
  } else {
    std::string reval{};
    auto size = std::accumulate(strings.begin(), strings.end(), (strings.size() - 1) * token.size(),
                                [](const size_t acc, const auto& istring) { return acc + istring.size(); });
    reval.resize(size);
    for (size_t i = 0, offset = 0; i < strings.size(); ++i) {
      const auto& istring = strings[i];
      std::memcpy(std::addressof(reval[offset]), istring.data(), istring.size() * sizeof(std::string::value_type));
      offset += istring.size() + token.size();
    }
    for (size_t i = 0, offset = 0; i < strings.size() - 1; ++i) {
      offset += strings[i].size();
      std::memcpy(std::addressof(reval[offset]), token.data(), token.size() * sizeof(std::string::value_type));
      offset += token.size();
    }
    return reval;
  }
}

}  // namespace Xi
