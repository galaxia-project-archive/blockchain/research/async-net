// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/String/EndsWith.hpp"

#include <algorithm>

namespace Xi {

bool endsWith(std::string_view str, std::string_view suffix) {
  if (suffix.empty()) {
    return true;
  } if (str.size() < suffix.size()) {
    return false;
  } else {
    return std::mismatch(suffix.rbegin(), suffix.rend(), str.rbegin(), str.rend()).first == suffix.rend();
  }
}

}  // namespace Xi
