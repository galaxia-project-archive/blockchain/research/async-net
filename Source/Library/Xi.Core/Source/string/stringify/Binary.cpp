// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/String/Stringify/Binary.hpp"

#include <sstream>
#include <limits>
#include <type_traits>

namespace Xi {

namespace {
template <typename _ValueT>
std::string toBinaryString(_ValueT value) {
  std::ostringstream builder{};
  if constexpr (std::is_signed_v<_ValueT>) {
    if (value < 0) {
      builder << "-";
      value *= -1;
    }
  }
  builder << "0b";
  for (size_t i = 0; i < sizeof(_ValueT) * 8; ++i) {
    if ((value & (static_cast<_ValueT>(1) << (sizeof(_ValueT) * 8 - i - 1))) > 0) {
      builder << "1";
    } else {
      builder << "0";
    }
  }
  return builder.str();
}
}  // namespace

std::string stringify(const uint8_t value, Stringify::binary_t) {
  return toBinaryString(value);
}

std::string stringify(const uint16_t value, Stringify::binary_t) {
  return toBinaryString(value);
}

std::string stringify(const uint32_t value, Stringify::binary_t) {
  return toBinaryString(value);
}

std::string stringify(const uint64_t value, Stringify::binary_t) {
  return toBinaryString(value);
}

std::string stringify(const int8_t value, Stringify::binary_t) {
  return toBinaryString(value);
}

std::string stringify(const int16_t value, Stringify::binary_t) {
  return toBinaryString(value);
}

std::string stringify(const int32_t value, Stringify::binary_t) {
  return toBinaryString(value);
}

std::string stringify(const int64_t value, Stringify::binary_t) {
  return toBinaryString(value);
}

}  // namespace Xi
