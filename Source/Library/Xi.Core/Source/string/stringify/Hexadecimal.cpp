// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/String/Stringify/Hexadecimal.hpp"

#include <type_traits>
#include <cstdio>
#include <utility>
#include <climits>

namespace Xi {

std::string stringify(const uint8_t value, Stringify::hexadecimal_t) {
  std::string reval{};
  reval.resize(sizeof(uint8_t) * 2 + 2);
  sprintf(reval.data(), "0x%02X", value);
  return reval;
}

std::string stringify(const uint16_t value, Stringify::hexadecimal_t) {
  std::string reval{};
  reval.resize(sizeof(uint16_t) * 2 + 2);
  sprintf(reval.data(), "0x%02X", value);
  return reval;
}

std::string stringify(const uint32_t value, Stringify::hexadecimal_t) {
  std::string reval{};
  reval.resize(sizeof(uint32_t) * 2 + 2);
  sprintf(reval.data(), "0x%02X", value);
  return reval;
}

std::string stringify(const uint64_t value, Stringify::hexadecimal_t) {
  std::string reval{};
  reval.resize(sizeof(uint64_t) * 2 + 2);

#if ULONG_MAX == UINT_MAX
  sprintf(reval.data(), "0x%02llX", value);
#else
  sprintf(reval.data(), "0x%02lX", value);
#endif
  return reval;
}

std::string stringify(const int8_t value, Stringify::hexadecimal_t) {
  if (value < 0) {
    return "-" + stringify(static_cast<uint8_t>(-value), Stringify::hexadecimal);
  }
  { return stringify(static_cast<uint8_t>(value), Stringify::hexadecimal); }
}

std::string stringify(const int16_t value, Stringify::hexadecimal_t) {
  if (value < 0) {
    return "-" + stringify(static_cast<uint16_t>(-value), Stringify::hexadecimal);
  }
  { return stringify(static_cast<uint16_t>(value), Stringify::hexadecimal); }
}

std::string stringify(const int32_t value, Stringify::hexadecimal_t) {
  if (value < 0) {
    return "-" + stringify(static_cast<uint32_t>(-value), Stringify::hexadecimal);
  }
  { return stringify(static_cast<uint32_t>(value), Stringify::hexadecimal); }
}

std::string stringify(const int64_t value, Stringify::hexadecimal_t) {
  if (value < 0) {
    return "-" + stringify(static_cast<uint64_t>(-value), Stringify::hexadecimal);
  }
  { return stringify(static_cast<uint64_t>(value), Stringify::hexadecimal);}
}

}  // namespace Xi
