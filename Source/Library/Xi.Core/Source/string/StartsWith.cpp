// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/String/StartsWith.hpp"

#include <algorithm>

namespace Xi {

bool startsWith(std::string_view str, std::string_view prefix) {
  if (prefix.empty()) {
    return true;
  } if (str.size() < prefix.size()) {
    return false;
  } else {
    return std::mismatch(prefix.begin(), prefix.end(), str.begin(), str.end()).first == prefix.end();
  }
}

}  // namespace Xi
