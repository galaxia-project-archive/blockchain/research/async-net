// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/String/ToUpper.hpp"

#include <memory>
#include <locale>

namespace Xi {

std::string toUpper(std::string_view str) {
  const std::locale locale{/* */};
  std::string reval{};
  reval.resize(str.size());
  for (size_t i = 0; i < str.size(); ++i) {
    reval[i] = static_cast<std::string::value_type>(std::toupper(str[i], locale));
  }
  return reval;
}

std::string::value_type toUpper(const std::string::value_type ch) {
  const std::locale locale{/* */};
  return static_cast<std::string::value_type>(std::toupper(ch, locale));
}

void toUpper(std::string &str, in_place_t) {
  const std::locale locale{/* */};
  for (char &i : str) {
    i = static_cast<std::string::value_type>(std::toupper(i, locale));
  }
}

}  // namespace Xi
