// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/String/Split.hpp"

namespace Xi {

void split(std::string_view str, std::string_view tokens, const std::function<void(std::string_view)>& cb) {
  size_t begin = 0;
  size_t end = begin;

  while (end < str.size()) {
    const bool isToken = tokens.find(str[end]) != std::string_view::npos;
    if (isToken) {
      if (end > begin) {
        cb(str.substr(begin, end - begin));
      }
      begin = end + 1;
    }
    end += 1;
  }

  if (end > begin) {
    cb(str.substr(begin, end - begin));
  }
}

std::vector<std::string_view> split(std::string_view str, std::string_view tokens) {
  std::vector<std::string_view> reval{};
  split(str, tokens, [&reval](auto istr) { reval.emplace_back(istr); });
  return reval;
}

std::vector<std::string> split(std::string_view str, std::string_view tokens, make_copy_t) {
  std::vector<std::string> reval{};
  split(str, tokens, [&reval](auto istr) { reval.emplace_back(istr.data(), istr.size()); });
  return reval;
}

}  // namespace Xi
