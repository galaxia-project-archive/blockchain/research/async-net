// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/String/Parse/Number.hh"

#include <ctype.h>
#include <limits.h>
#include <string.h>
#include <errno.h>
#include <math.h>
#include <locale.h>


int xi_string_parse_number_int8(int8_t *out, const char *string, const char *stringEnd, uint8_t base)
{
  XI_RETURN_EC_IF(isspace(*string), XI_STRING_PARSE_NUMBER_ERROR_LEADING_SPACE);

  char *parseEnd = NULL;
  long number = 0;

  errno = 0;
  number = strtol(string, &parseEnd, base);

  XI_RETURN_EC_IF(string == parseEnd, XI_STRING_PARSE_NUMBER_ERROR_INCONVERTIBLE);

  if ((errno == ERANGE && number == LONG_MAX) || number > SCHAR_MAX) {
    return XI_STRING_PARSE_NUMBER_ERROR_OVERFLOW;
  }
  if ((errno == ERANGE && number == LONG_MIN) || number < SCHAR_MIN) {
    return XI_STRING_PARSE_NUMBER_ERROR_UNDERFLOW;
  } else if (errno == EINVAL) {
    return XI_STRING_PARSE_NUMBER_ERROR_INVALID_CHARACTER;
  } else if (errno) {
    return XI_STRING_PARSE_NUMBER_ERROR_INCONVERTIBLE;
  }
  XI_RETURN_EC_IF_NOT(parseEnd == stringEnd, XI_STRING_PARSE_NUMBER_ERROR_TRAILING_CHARACTERS);

  *out = (int8_t)number;
  XI_RETURN_SC(XI_RETURN_CODE_SUCCESS);
}

int xi_string_parse_number_int16(int16_t *out, const char *string, const char *stringEnd, uint8_t base)
{
  XI_RETURN_EC_IF(isspace(*string), XI_STRING_PARSE_NUMBER_ERROR_LEADING_SPACE);

  char *parseEnd = NULL;
  long number = 0;

  errno = 0;
  number = strtol(string, &parseEnd, base);

  XI_RETURN_EC_IF(string == parseEnd, XI_STRING_PARSE_NUMBER_ERROR_INCONVERTIBLE);

  if ((errno == ERANGE && number == LONG_MAX) || number > SHRT_MAX) {
    return XI_STRING_PARSE_NUMBER_ERROR_OVERFLOW;
  } else if ((errno == ERANGE && number == LONG_MIN) || number < SHRT_MIN) {
      return XI_STRING_PARSE_NUMBER_ERROR_UNDERFLOW;
    }
    else if (errno == EINVAL) {
      return XI_STRING_PARSE_NUMBER_ERROR_INVALID_CHARACTER;
    }
    else if (errno) {
      return XI_STRING_PARSE_NUMBER_ERROR_INCONVERTIBLE;
    }
  XI_RETURN_EC_IF_NOT(parseEnd == stringEnd, XI_STRING_PARSE_NUMBER_ERROR_TRAILING_CHARACTERS);

  *out = (int16_t)number;
  XI_RETURN_SC(XI_RETURN_CODE_SUCCESS);
}

int xi_string_parse_number_int32(int32_t *out, const char *string, const char *stringEnd, uint8_t base)
{
  XI_RETURN_EC_IF(isspace(*string), XI_STRING_PARSE_NUMBER_ERROR_LEADING_SPACE);

  char *parseEnd = NULL;
  long long number = 0;

  errno = 0;
  number = strtoll(string, &parseEnd, base);

  XI_RETURN_EC_IF(string == parseEnd, XI_STRING_PARSE_NUMBER_ERROR_INCONVERTIBLE);

  if ((errno == ERANGE && number == LLONG_MAX) || number > INT_MAX) {
    return XI_STRING_PARSE_NUMBER_ERROR_OVERFLOW;
  } else if ((errno == ERANGE && number == LLONG_MIN) || number < INT_MIN) {
    return XI_STRING_PARSE_NUMBER_ERROR_UNDERFLOW;
  } else if(errno == EINVAL) {
    return XI_STRING_PARSE_NUMBER_ERROR_INVALID_CHARACTER;
  } else if(errno) {
    return XI_STRING_PARSE_NUMBER_ERROR_INCONVERTIBLE;
  }
  XI_RETURN_EC_IF_NOT(parseEnd == stringEnd, XI_STRING_PARSE_NUMBER_ERROR_TRAILING_CHARACTERS);

  *out = (int32_t)number;
  XI_RETURN_SC(XI_RETURN_CODE_SUCCESS);
}

int xi_string_parse_number_int64(int64_t *out, const char *string, const char *stringEnd, uint8_t base)
{
  XI_RETURN_EC_IF(isspace(*string), XI_STRING_PARSE_NUMBER_ERROR_LEADING_SPACE);

  char *parseEnd = NULL;
  long long number = 0;

  errno = 0;
  number = strtoll(string, &parseEnd, base);

  XI_RETURN_EC_IF(string == parseEnd, XI_STRING_PARSE_NUMBER_ERROR_INCONVERTIBLE);

  if (errno == ERANGE && number == LLONG_MAX) {
    return XI_STRING_PARSE_NUMBER_ERROR_OVERFLOW;
  }
  if (errno == ERANGE && number == LLONG_MIN) {
    return XI_STRING_PARSE_NUMBER_ERROR_UNDERFLOW;
  } else if (errno == EINVAL) {
    return XI_STRING_PARSE_NUMBER_ERROR_INVALID_CHARACTER;
  } else if (errno) {
    return XI_STRING_PARSE_NUMBER_ERROR_INCONVERTIBLE;
  }
  XI_RETURN_EC_IF_NOT(parseEnd == stringEnd, XI_STRING_PARSE_NUMBER_ERROR_TRAILING_CHARACTERS);

  *out = (int64_t)number;
  XI_RETURN_SC(XI_RETURN_CODE_SUCCESS);
}

int xi_string_parse_number_uint8(uint8_t *out, const char *string, const char *stringEnd, uint8_t base)
{
  XI_RETURN_EC_IF(isspace(*string), XI_STRING_PARSE_NUMBER_ERROR_LEADING_SPACE);

  char *parseEnd = NULL;
  unsigned long number = 0;

  errno = 0;
  number = strtoul(string, &parseEnd, base);

  XI_RETURN_EC_IF(string == parseEnd, XI_STRING_PARSE_NUMBER_ERROR_INCONVERTIBLE);

  if ((errno == ERANGE && number == ULONG_MAX) || number > UCHAR_MAX) {
    return XI_STRING_PARSE_NUMBER_ERROR_OVERFLOW;
  } else if (errno == ERANGE && number == 0UL) {
      return XI_STRING_PARSE_NUMBER_ERROR_UNDERFLOW;
    }
    else if (errno == EINVAL) {
      return XI_STRING_PARSE_NUMBER_ERROR_INVALID_CHARACTER;
    }
    else if (errno) {
      return XI_STRING_PARSE_NUMBER_ERROR_INCONVERTIBLE;
    }
  XI_RETURN_EC_IF_NOT(parseEnd == stringEnd, XI_STRING_PARSE_NUMBER_ERROR_TRAILING_CHARACTERS);

  *out = (uint8_t)number;
  XI_RETURN_SC(XI_RETURN_CODE_SUCCESS);
}

int xi_string_parse_number_uint16(uint16_t *out, const char *string, const char *stringEnd, uint8_t base)
{
  XI_RETURN_EC_IF(isspace(*string), XI_STRING_PARSE_NUMBER_ERROR_LEADING_SPACE);

  char *parseEnd = NULL;
  unsigned long number = 0;

  errno = 0;
  number = strtoul(string, &parseEnd, base);

  XI_RETURN_EC_IF(string == parseEnd, XI_STRING_PARSE_NUMBER_ERROR_INCONVERTIBLE);

  if ((errno == ERANGE && number == ULONG_MAX) || number > USHRT_MAX) {
    return XI_STRING_PARSE_NUMBER_ERROR_OVERFLOW;
  } else if (errno == ERANGE && number == 0UL) {
    return XI_STRING_PARSE_NUMBER_ERROR_UNDERFLOW;
  } else if(errno == EINVAL) {
    return XI_STRING_PARSE_NUMBER_ERROR_INVALID_CHARACTER;
  } else if(errno) {
    return XI_STRING_PARSE_NUMBER_ERROR_INCONVERTIBLE;
  }
  XI_RETURN_EC_IF_NOT(parseEnd == stringEnd, XI_STRING_PARSE_NUMBER_ERROR_TRAILING_CHARACTERS);

  *out = (uint16_t)number;
  XI_RETURN_SC(XI_RETURN_CODE_SUCCESS);
}

int xi_string_parse_number_uint32(uint32_t *out, const char *string, const char *stringEnd, uint8_t base)
{
  XI_RETURN_EC_IF(isspace(*string), XI_STRING_PARSE_NUMBER_ERROR_LEADING_SPACE);

  char *parseEnd = NULL;
  unsigned long long number = 0;

  errno = 0;
  number = strtoull(string, &parseEnd, base);

  XI_RETURN_EC_IF(string == parseEnd, XI_STRING_PARSE_NUMBER_ERROR_INCONVERTIBLE);

  if ((errno == ERANGE && number == ULLONG_MAX) || number > UINT_MAX) {
    return XI_STRING_PARSE_NUMBER_ERROR_OVERFLOW;
  }
  if (errno == ERANGE && number == 0UL) {
    return XI_STRING_PARSE_NUMBER_ERROR_UNDERFLOW;
  } else if (errno == EINVAL) {
    return XI_STRING_PARSE_NUMBER_ERROR_INVALID_CHARACTER;
  } else if (errno) {
    return XI_STRING_PARSE_NUMBER_ERROR_INCONVERTIBLE;
  }
  XI_RETURN_EC_IF_NOT(parseEnd == stringEnd, XI_STRING_PARSE_NUMBER_ERROR_TRAILING_CHARACTERS);

  *out = (uint32_t)number;
  XI_RETURN_SC(XI_RETURN_CODE_SUCCESS);
}

int xi_string_parse_number_uint64(uint64_t *out, const char *string, const char *stringEnd, uint8_t base)
{
  XI_RETURN_EC_IF(isspace(*string), XI_STRING_PARSE_NUMBER_ERROR_LEADING_SPACE);

  char *parseEnd = NULL;
  unsigned long long number = 0;

  errno = 0;
  number = strtoull(string, &parseEnd, base);

  XI_RETURN_EC_IF(string == parseEnd, XI_STRING_PARSE_NUMBER_ERROR_INCONVERTIBLE);

  if (errno == ERANGE && number == ULLONG_MAX) {
    return XI_STRING_PARSE_NUMBER_ERROR_OVERFLOW;
    } else if (errno == ERANGE && number == 0UL) {
      return XI_STRING_PARSE_NUMBER_ERROR_UNDERFLOW;
    }
    else if (errno == EINVAL) {
      return XI_STRING_PARSE_NUMBER_ERROR_INVALID_CHARACTER;
    }
    else if (errno) {
      return XI_STRING_PARSE_NUMBER_ERROR_INCONVERTIBLE;
    }
  XI_RETURN_EC_IF_NOT(parseEnd == stringEnd, XI_STRING_PARSE_NUMBER_ERROR_TRAILING_CHARACTERS);

  *out = (uint64_t)number;
  XI_RETURN_SC(XI_RETURN_CODE_SUCCESS);
}

int xi_string_parse_number_float(float *out, const char *string, const char *stringEnd)
{
  XI_RETURN_EC_IF(isspace(*string), XI_STRING_PARSE_NUMBER_ERROR_LEADING_SPACE);

  char *parseEnd = NULL;
  float number = 0;

  errno = 0;
  number = strtof(string, &parseEnd);

  XI_RETURN_EC_IF(string == parseEnd, XI_STRING_PARSE_NUMBER_ERROR_INCONVERTIBLE);

  if (errno == ERANGE && number >= HUGE_VALF) {
    return XI_STRING_PARSE_NUMBER_ERROR_OVERFLOW;
  } else if (errno == ERANGE && number <= -HUGE_VALF) {
    return XI_STRING_PARSE_NUMBER_ERROR_UNDERFLOW;
  } else if(errno == EINVAL) {
    return XI_STRING_PARSE_NUMBER_ERROR_INVALID_CHARACTER;
  } else if(errno) {
    return XI_STRING_PARSE_NUMBER_ERROR_INCONVERTIBLE;
  }
  XI_RETURN_EC_IF_NOT(parseEnd == stringEnd, XI_STRING_PARSE_NUMBER_ERROR_TRAILING_CHARACTERS);

  *out = number;
  XI_RETURN_SC(XI_RETURN_CODE_SUCCESS);
}

int xi_string_parse_number_double(double *out, const char *string, const char *stringEnd)
{
  XI_RETURN_EC_IF(isspace(*string), XI_STRING_PARSE_NUMBER_ERROR_LEADING_SPACE);

  char *parseEnd = NULL;
  double number = 0;

  errno = 0;
  number = strtod(string, &parseEnd);

  XI_RETURN_EC_IF(string == parseEnd, XI_STRING_PARSE_NUMBER_ERROR_INCONVERTIBLE);

  if (errno == ERANGE && number >= HUGE_VAL) {
    return XI_STRING_PARSE_NUMBER_ERROR_OVERFLOW;
  }
  if (errno == ERANGE && number <= -HUGE_VAL) {
    return XI_STRING_PARSE_NUMBER_ERROR_UNDERFLOW;
  } else if (errno == EINVAL) {
    return XI_STRING_PARSE_NUMBER_ERROR_INVALID_CHARACTER;
  } else if (errno) {
    return XI_STRING_PARSE_NUMBER_ERROR_INCONVERTIBLE;
  }
  XI_RETURN_EC_IF_NOT(parseEnd == stringEnd, XI_STRING_PARSE_NUMBER_ERROR_TRAILING_CHARACTERS);

  *out = number;
  XI_RETURN_SC(XI_RETURN_CODE_SUCCESS);
}

int xi_string_parse_number_long_double(long double *out, const char *string, const char *stringEnd)
{
  XI_RETURN_EC_IF(isspace(*string), XI_STRING_PARSE_NUMBER_ERROR_LEADING_SPACE);

  char *parseEnd = NULL;
  long double number = 0;

  errno = 0;
  number = strtold(string, &parseEnd);

  XI_RETURN_EC_IF(string == parseEnd, XI_STRING_PARSE_NUMBER_ERROR_INCONVERTIBLE);

  if (errno == ERANGE && number >= HUGE_VALL) {
    return XI_STRING_PARSE_NUMBER_ERROR_OVERFLOW;
} else if (errno == ERANGE && number <= -HUGE_VALL) {
    return XI_STRING_PARSE_NUMBER_ERROR_UNDERFLOW;
  } else if(errno == EINVAL) {
    return XI_STRING_PARSE_NUMBER_ERROR_INVALID_CHARACTER;
  } else if(errno) {
    return XI_STRING_PARSE_NUMBER_ERROR_INCONVERTIBLE;
  }
  XI_RETURN_EC_IF_NOT(parseEnd == stringEnd, XI_STRING_PARSE_NUMBER_ERROR_TRAILING_CHARACTERS);

  *out = number;
  XI_RETURN_SC(XI_RETURN_CODE_SUCCESS);
}
