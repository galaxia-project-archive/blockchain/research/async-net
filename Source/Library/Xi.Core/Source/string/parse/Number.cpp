﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/String/Parse/Number.hh"

XI_ERROR_CODE_CATEGORY_BEGIN(Xi, ParseNumber)
XI_ERROR_CODE_DESC(IllFormed, "ill formed number")
XI_ERROR_CODE_DESC(Overflow, "number is too large")
XI_ERROR_CODE_DESC(Underflow, "number is too small")
XI_ERROR_CODE_DESC(LeadingSpace, "number has leading spaces")
XI_ERROR_CODE_DESC(TrailingCharacter, "number has traling characters")
XI_ERROR_CODE_DESC(InvalidCharacter, "number contains invalid characters")
XI_ERROR_CODE_DESC(InvalidLocale, "number locale is invalid")
XI_ERROR_CODE_CATEGORY_END()

namespace Xi {

Result<void> parse(const std::string &str, uint8_t &out, uint8_t base) {
  const auto cstr = str.c_str();
  const auto ec = xi_string_parse_number_uint8(std::addressof(out), cstr, cstr + str.size(), base);
  XI_FAIL_IF_NOT(ec == XI_RETURN_CODE_SUCCESS, static_cast<ParseNumberError>(ec));
  XI_SUCCEED()
}

Result<void> parse(const std::string &str, uint16_t &out, uint8_t base) {
  const auto cstr = str.c_str();
  const auto ec = xi_string_parse_number_uint16(std::addressof(out), cstr, cstr + str.size(), base);
  XI_FAIL_IF_NOT(ec == XI_RETURN_CODE_SUCCESS, static_cast<ParseNumberError>(ec));
  XI_SUCCEED()
}

Result<void> parse(const std::string &str, uint32_t &out, uint8_t base) {
  const auto cstr = str.c_str();
  const auto ec = xi_string_parse_number_uint32(std::addressof(out), cstr, cstr + str.size(), base);
  XI_FAIL_IF_NOT(ec == XI_RETURN_CODE_SUCCESS, static_cast<ParseNumberError>(ec));
  XI_SUCCEED()
}

Result<void> parse(const std::string &str, uint64_t &out, uint8_t base) {
  const auto cstr = str.c_str();
  const auto ec = xi_string_parse_number_uint64(std::addressof(out), cstr, cstr + str.size(), base);
  XI_FAIL_IF_NOT(ec == XI_RETURN_CODE_SUCCESS, static_cast<ParseNumberError>(ec));
  XI_SUCCEED()
}

Result<void> parse(const std::string &str, int8_t &out, uint8_t base) {
  const auto cstr = str.c_str();
  const auto ec = xi_string_parse_number_int8(std::addressof(out), cstr, cstr + str.size(), base);
  XI_FAIL_IF_NOT(ec == XI_RETURN_CODE_SUCCESS, static_cast<ParseNumberError>(ec));
  XI_SUCCEED()
}

Result<void> parse(const std::string &str, int16_t &out, uint8_t base) {
  const auto cstr = str.c_str();
  const auto ec = xi_string_parse_number_int16(std::addressof(out), cstr, cstr + str.size(), base);
  XI_FAIL_IF_NOT(ec == XI_RETURN_CODE_SUCCESS, static_cast<ParseNumberError>(ec));
  XI_SUCCEED()
}

Result<void> parse(const std::string &str, int32_t &out, uint8_t base) {
  const auto cstr = str.c_str();
  const auto ec = xi_string_parse_number_int32(std::addressof(out), cstr, cstr + str.size(), base);
  XI_FAIL_IF_NOT(ec == XI_RETURN_CODE_SUCCESS, static_cast<ParseNumberError>(ec));
  XI_SUCCEED()
}

Result<void> parse(const std::string &str, int64_t &out, uint8_t base) {
  const auto cstr = str.c_str();
  const auto ec = xi_string_parse_number_int64(std::addressof(out), cstr, cstr + str.size(), base);
  XI_FAIL_IF_NOT(ec == XI_RETURN_CODE_SUCCESS, static_cast<ParseNumberError>(ec));
  XI_SUCCEED()
}

Result<void> parse(const std::string &str, float &out) {
  const auto cstr = str.c_str();
  const auto ec = xi_string_parse_number_float(std::addressof(out), cstr, cstr + str.size());
  XI_FAIL_IF_NOT(ec == XI_RETURN_CODE_SUCCESS, static_cast<ParseNumberError>(ec));
  XI_SUCCEED()
}

Result<void> parse(const std::string &str, double &out) {
  const auto cstr = str.c_str();
  const auto ec = xi_string_parse_number_double(std::addressof(out), cstr, cstr + str.size());
  XI_FAIL_IF_NOT(ec == XI_RETURN_CODE_SUCCESS, static_cast<ParseNumberError>(ec));
  XI_SUCCEED()
}

Result<void> parse(const std::string &str, long double &out) {
  const auto cstr = str.c_str();
  const auto ec = xi_string_parse_number_long_double(std::addressof(out), cstr, cstr + str.size());
  XI_FAIL_IF_NOT(ec == XI_RETURN_CODE_SUCCESS, static_cast<ParseNumberError>(ec));
  XI_SUCCEED()
}

}  // namespace Xi
