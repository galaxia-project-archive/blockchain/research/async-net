// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include <gmock/gmock.h>

#include <Xi/String/EndsWith.hpp>

#define XI_TEST_SUITE Xi_Algorithm_String_EndsWith

TEST(XI_TEST_SUITE, EmptyCases) {
  using namespace ::testing;
  using namespace ::Xi;

  EXPECT_TRUE(endsWith("", ""));
  EXPECT_TRUE(endsWith("X", ""));
  EXPECT_FALSE(endsWith("", "C"));
}

TEST(XI_TEST_SUITE, Match) {
  using namespace ::testing;
  using namespace ::Xi;

  EXPECT_TRUE(endsWith("__^", "^"));
  EXPECT_TRUE(endsWith("__^", "_^"));
  EXPECT_TRUE(endsWith("__^", "__^"));
}

TEST(XI_TEST_SUITE, NoMatch) {
  using namespace ::testing;
  using namespace ::Xi;

  EXPECT_FALSE(endsWith("__^", "_"));
  EXPECT_FALSE(endsWith("__^", "^_"));
  EXPECT_FALSE(endsWith("__^", "_^_"));
}
