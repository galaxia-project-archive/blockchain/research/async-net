// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include <gmock/gmock.h>

#include <Xi/String/ToUpper.hpp>

#define XI_TEST_SUITE Xi_Algorithm_String_ToUpper

TEST(XI_TEST_SUITE, Empty) {
  using namespace ::testing;
  using namespace ::Xi;

  EXPECT_THAT(toUpper(""), Eq(""));
}

TEST(XI_TEST_SUITE, InPlace) {
  using namespace ::testing;
  using namespace ::Xi;

  {
    std::string value{"Ab-_-bA"};
    toUpper(value, in_place);
    EXPECT_THAT(value, Eq("AB-_-BA"));
  }
}

TEST(XI_TEST_SUITE, Copy) {
  using namespace ::testing;
  using namespace ::Xi;

  EXPECT_THAT(toUpper("Ab-_-bA"), Eq("AB-_-BA"));
}
