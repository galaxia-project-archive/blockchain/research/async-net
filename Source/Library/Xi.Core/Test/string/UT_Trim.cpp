// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include <gmock/gmock.h>

#include <Xi/String/Trim.hpp>

#define XI_TEST_SUITE Xi_Algorithm_String_Trim

TEST(XI_TEST_SUITE, Empty) {
  using namespace ::testing;
  using namespace ::Xi;

  EXPECT_THAT(trimLeft(""), Eq(""));
  EXPECT_THAT(trimRight(""), Eq(""));
  EXPECT_THAT(trim(""), Eq(""));
}

TEST(XI_TEST_SUITE, Common) {
  using namespace ::testing;
  using namespace ::Xi;

  EXPECT_THAT(trimLeft("    "), Eq(""));
  EXPECT_THAT(trimRight("    "), Eq(""));
  EXPECT_THAT(trimLeft("    _   "), Eq("_   "));
  EXPECT_THAT(trimRight("    _   "), Eq("    _"));
  EXPECT_THAT(trim("       "), Eq(""));
  EXPECT_THAT(trim("   -  -    "), Eq("-  -"));
  EXPECT_THAT(trim("   \0  \0    "), Eq("\0  \0"));
}

TEST(XI_TEST_SUITE, CommonInPlace) {
  using namespace ::testing;
  using namespace ::Xi;

  {
    std::string value{"    "};
    trimLeft(value, in_place);
    EXPECT_THAT(value, Eq(""));
  }
  {
    std::string value{"    "};
    trimRight(value, in_place);
    EXPECT_THAT(value, Eq(""));
  }
  {
    std::string value{"    "};
    trim(value, in_place);
    EXPECT_THAT(value, Eq(""));
  }

  {
    std::string value{"   -   "};
    trimLeft(value, in_place);
    EXPECT_THAT(value, Eq("-   "));
  }
  {
    std::string value{"   -   "};
    trimRight(value, in_place);
    EXPECT_THAT(value, Eq("   -"));
  }
  {
    std::string value{"   -   "};
    trim(value, in_place);
    EXPECT_THAT(value, Eq("-"));
  }

  {
    std::string value{"X X"};
    trim(value, in_place);
    EXPECT_THAT(value, Eq("X X"));
  }
  {
    std::string value{"\0"};
    trim(value, in_place);
    EXPECT_THAT(value, Eq("\0"));
  }
  {
    std::string value{" \0  \0 "};
    trim(value, in_place);
    EXPECT_THAT(value, Eq("\0  \0"));
  }
}
