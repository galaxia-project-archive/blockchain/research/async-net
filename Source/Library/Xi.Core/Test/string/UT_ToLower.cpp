// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include <gmock/gmock.h>

#include <Xi/String/ToLower.hpp>

#define XI_TEST_SUITE Xi_Algorithm_String_ToLower

TEST(XI_TEST_SUITE, Empty) {
  using namespace ::testing;
  using namespace ::Xi;

  EXPECT_THAT(toLower(""), Eq(""));
}

TEST(XI_TEST_SUITE, InPlace) {
  using namespace ::testing;
  using namespace ::Xi;

  {
    std::string value{"Ab-_-bA"};
    toLower(value, in_place);
    EXPECT_THAT(value, Eq("ab-_-ba"));
  }
}

TEST(XI_TEST_SUITE, Copy) {
  using namespace ::testing;
  using namespace ::Xi;

  EXPECT_THAT(toLower("Ab-_-bA"), Eq("ab-_-ba"));
}
