// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include <gmock/gmock.h>

#include <Xi/String/ToString.hpp>

#define XI_TEST_SUITE Xi_Algorithm_String_ToHexadecimal

TEST(XI_TEST_SUITE, Common) {
  using namespace ::testing;
  using namespace ::Xi;

  EXPECT_THAT(toString(static_cast<uint8_t>(0), Stringify::hexadecimal), Eq("0x00"));
  EXPECT_THAT(toString(static_cast<uint8_t>(0xFF), Stringify::hexadecimal), Eq("0xFF"));
  EXPECT_THAT(toString(static_cast<int8_t>(-1), Stringify::hexadecimal), Eq("-0x01"));
  EXPECT_THAT(toString(static_cast<int8_t>(-0x0F), Stringify::hexadecimal), Eq("-0x0F"));
}
