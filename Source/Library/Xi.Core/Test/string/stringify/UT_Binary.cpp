// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include <gmock/gmock.h>

#include <Xi/String/ToString.hpp>

#define XI_TEST_SUITE Xi_Algorithm_String_ToBinary

TEST(XI_TEST_SUITE, Common) {
  using namespace ::testing;
  using namespace ::Xi;

  EXPECT_THAT(toString(static_cast<uint8_t>(0), Stringify::binary), Eq("0b00000000"));
  EXPECT_THAT(toString(static_cast<uint8_t>(0xFF), Stringify::binary), Eq("0b11111111"));
  EXPECT_THAT(toString(static_cast<int8_t>(-1), Stringify::binary), Eq("-0b00000001"));
  EXPECT_THAT(toString(static_cast<int8_t>(-0x0F), Stringify::binary), Eq("-0b00001111"));
}
