// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include <gmock/gmock.h>

#include <Xi/String/Replace.hpp>

#define XI_TEST_SUITE Xi_Algorithm_String_Replace

TEST(XI_TEST_SUITE, EmptyCases) {
  using namespace ::testing;
  using namespace ::Xi;

  EXPECT_THAT(replace("", "", "X"), Eq(""));
  EXPECT_THAT(replace("", "_", "X"), Eq(""));
  EXPECT_THAT(replace("X__X", "", "X"), Eq("X__X"));
}

TEST(XI_TEST_SUITE, NeverMatch) {
  using namespace ::testing;
  using namespace ::Xi;

  EXPECT_THAT(replace("^-.-^", "", "X"), Eq("^-.-^"));
  EXPECT_THAT(replace("^-.-^", "^-.-^_", "X"), Eq("^-.-^"));
  EXPECT_THAT(replace("^-.-^", "^_._^", "X"), Eq("^-.-^"));
  EXPECT_THAT(replace("^-.-^", "^_", "X"), Eq("^-.-^"));
}

TEST(XI_TEST_SUITE, FullMatch) {
  using namespace ::testing;
  using namespace ::Xi;

  EXPECT_THAT(replace("^-.-^", "^-.-^", ""), Eq(""));
  EXPECT_THAT(replace("^-.-^", "^-.-^", "X"), Eq("X"));
  EXPECT_THAT(replace("^-.-^", "^-.-^", "^-.-^"), Eq("^-.-^"));
  EXPECT_THAT(replace("^-.-^", "-", "_"), Eq("^_._^"));
  EXPECT_THAT(replace("^-.-^", "^", "X"), Eq("X-.-X"));
}

TEST(XI_TEST_SUITE, PartialMatch) {
  using namespace ::testing;
  using namespace ::Xi;

  EXPECT_THAT(replace("^-.-^", "-", "_"), Eq("^_._^"));
  EXPECT_THAT(replace("^-.-^", "^", "X"), Eq("X-.-X"));
  EXPECT_THAT(replace("-", "-", "^"), Eq("^"));
  EXPECT_THAT(replace("----", "-", "^."), Eq("^.^.^.^."));
  EXPECT_THAT(replace("----", "--", "^."), Eq("^.^."));
}

TEST(XI_TEST_SUITE, SelfReplacement) {
  using namespace ::testing;
  using namespace ::Xi;

  EXPECT_THAT(replace("--------", "-", "-"), Eq("--------"));
  EXPECT_THAT(replace("--------", "-", "--"), Eq("----------------"));
}
