// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include <gmock/gmock.h>

#include <Xi/String/Split.hpp>

#define XI_TEST_SUITE Xi_Algorithm_String_Split

TEST(XI_TEST_SUITE, Empty) {
  using namespace ::testing;
  using namespace ::Xi;

  {
    auto result = split("", "");
    EXPECT_THAT(result, IsEmpty());
  }

  {
    auto result = split("", "\0");
    EXPECT_THAT(result, IsEmpty());
  }

  {
    auto result = split("\0\n\0", "\0\n");
    EXPECT_THAT(result, IsEmpty());
  }
}

TEST(XI_TEST_SUITE, Single) {
  using namespace ::testing;
  using namespace ::Xi;

  {
    auto result = split("a", "");
    ASSERT_THAT(result, SizeIs(1));
    EXPECT_THAT(result[0], Eq("a"));
  }

  {
    auto result = split("  a\0 ", "bcde");
    ASSERT_THAT(result, SizeIs(1));
    EXPECT_THAT(result[0], Eq("  a\0 "));
  }
}

TEST(XI_TEST_SUITE, Multi) {
  using namespace ::testing;
  using namespace ::Xi;

  {
    auto result = split("abba", "b");
    ASSERT_THAT(result, SizeIs(2));
    EXPECT_THAT(result[0], Eq("a"));
    EXPECT_THAT(result[1], Eq("a"));
  }
}
