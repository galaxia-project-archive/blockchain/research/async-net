// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include <gmock/gmock.h>

#include <Xi/String/StartsWith.hpp>

#define XI_TEST_SUITE Xi_Algorithm_String_StartsWith

TEST(XI_TEST_SUITE, EmptyCases) {
  using namespace ::testing;
  using namespace ::Xi;

  EXPECT_TRUE(startsWith("", ""));
  EXPECT_TRUE(startsWith("X", ""));
  EXPECT_FALSE(startsWith("", "C"));
}

TEST(XI_TEST_SUITE, Match) {
  using namespace ::testing;
  using namespace ::Xi;

  EXPECT_TRUE(startsWith("__^", "_"));
  EXPECT_TRUE(startsWith("__^", "__"));
  EXPECT_TRUE(startsWith("__^", "__^"));
}

TEST(XI_TEST_SUITE, NoMatch) {
  using namespace ::testing;
  using namespace ::Xi;

  EXPECT_FALSE(startsWith("__^", "^"));
  EXPECT_FALSE(startsWith("__^", "_^"));
}
