// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include <gmock/gmock.h>

#include <Xi/String/Join.hpp>

#define XI_TEST_SUITE Xi_Algorithm_String_Join

TEST(XI_TEST_SUITE, EmptyCase) {
  using namespace ::testing;
  using namespace ::Xi;

  {
    std::vector<std::string> value{};
    EXPECT_THAT(join(value, ","), Eq(""));
  }
  {
    std::vector<std::string> value{};
    EXPECT_THAT(join(value, ""), Eq(""));
  }
}

TEST(XI_TEST_SUITE, SingleValue) {
  using namespace ::testing;
  using namespace ::Xi;

  {
    std::vector<std::string> value{""};
    EXPECT_THAT(join(value, ","), Eq(""));
  }
  {
    std::vector<std::string> value{"_"};
    EXPECT_THAT(join(value, ","), Eq("_"));
  }
}

TEST(XI_TEST_SUITE, MultiValue) {
  using namespace ::testing;
  using namespace ::Xi;

  {
    std::vector<std::string> value{"A", "B"};
    EXPECT_THAT(join(value, ","), Eq("A,B"));
  }
  {
    std::vector<std::string> value{",", "^", ","};
    EXPECT_THAT(join(value, ", "), Eq(",, ^, ,"));
  }
}
