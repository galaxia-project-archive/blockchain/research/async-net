// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include <gmock/gmock.h>

#include <Xi/Testing/Result.hpp>
#include <Xi/String/FromString.hpp>

#define XI_TEST_SUITE Xi_Algorithm_String_FromString

TEST(XI_TEST_SUITE, Number) {
  using namespace ::testing;
  using namespace ::Xi;
  using namespace ::Xi::Testing;

  {
    static_assert(has_parse_expression_v<uint8_t>, "");
    auto value = fromString<uint8_t>("1");
    ASSERT_THAT(value, IsSuccess());
    EXPECT_THAT(*value, Eq(1));
  }

  {
    auto value = fromString<uint8_t>("0");
    ASSERT_THAT(value, IsSuccess());
    EXPECT_THAT(*value, Eq(0));
  }

  {
    auto value = fromString<uint8_t>("0xFF");
    ASSERT_THAT(value, IsSuccess());
    EXPECT_THAT(*value, Eq(0xFF));
  }

  {
    auto value = fromString<int8_t>("0xFF");
    ASSERT_THAT(value, IsFailure());
  }

  {
    auto value = fromString<int8_t>("-0xFF");
    ASSERT_THAT(value, IsFailure());
  }

  {
    auto value = fromString<int8_t>("-0x0F");
    ASSERT_THAT(value, IsSuccess());
    EXPECT_THAT(*value, Eq(-0x0F));
  }
}
