// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <Xi/Global.hh>

namespace Xi {

struct make_copy_t {
  /* */
};

static inline constexpr make_copy_t make_copy{/* */};

}  // namespace Xi
