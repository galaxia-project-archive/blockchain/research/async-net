// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include "Xi/TypeTrait/IsDetected.hpp"
#include "Xi/TypeTrait/IsSmartPointer.hpp"
