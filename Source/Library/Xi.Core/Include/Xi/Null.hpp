// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

namespace Xi {

struct Null {
  /* */
};
static inline constexpr Null null{/* */};

}  // namespace Xi
