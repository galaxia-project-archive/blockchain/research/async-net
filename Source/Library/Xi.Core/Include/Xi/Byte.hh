﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#if defined(__cplusplus)
extern "C" {
#endif

#include <inttypes.h>

typedef uint8_t xi_byte_t;

#if defined(__cplusplus)
}
#endif

#if defined(__cplusplus)

#include <cinttypes>
#include <cstddef>
#include <array>
#include <vector>
#include <string>
#include <string_view>

#include "Xi/Span.hpp"

namespace Xi {
using Byte = xi_byte_t;
using ByteVector = std::vector<Byte>;
template <size_t _Size>
using ByteArray = std::array<Byte, _Size>;

XI_DECLARE_SPANS(Byte)

static inline ByteSpan asByteSpan(void* data, size_t size) {
  return ByteSpan{reinterpret_cast<Byte*>(data), size};
}

static inline ConstByteSpan asByteSpan(const void* data, size_t size) {
  return ConstByteSpan{reinterpret_cast<const Byte*>(data), size};
}

static inline ConstByteSpan asConstByteSpan(void* data, size_t size) {
  return ConstByteSpan{reinterpret_cast<const Byte*>(data), size};
}

static inline ConstByteSpan asConstByteSpan(const void* data, size_t size) {
  return ConstByteSpan{reinterpret_cast<const Byte*>(data), size};
}

static inline ByteSpan asByteSpan(std::string& str) {
  return ByteSpan{reinterpret_cast<Byte*>(str.data()), str.size()};
}
static inline ConstByteSpan asByteSpan(const std::string& str) {
  return ConstByteSpan{reinterpret_cast<const Byte*>(str.data()), str.size()};
}
static inline ConstByteSpan asConstByteSpan(std::string& str) {
  return ConstByteSpan{reinterpret_cast<const Byte*>(str.data()), str.size()};
}
static inline ConstByteSpan asConstByteSpan(const std::string& str) {
  return ConstByteSpan{reinterpret_cast<const Byte*>(str.data()), str.size()};
}

static inline ConstByteSpan asByteSpan(std::string_view str) {
  return ConstByteSpan{reinterpret_cast<const Byte*>(str.data()), str.size()};
}
static inline ConstByteSpan asConstByteSpan(std::string_view str) {
  return ConstByteSpan{reinterpret_cast<const Byte*>(str.data()), str.size()};
}

}  // namespace Xi

#endif
