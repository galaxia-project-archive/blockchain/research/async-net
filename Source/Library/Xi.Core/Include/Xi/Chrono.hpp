﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <chrono>

#include "Xi/Chrono/Convert.hpp"

namespace Xi::Chrono {

using MicroSeconds = std::chrono::microseconds;
using MilliSeconds = std::chrono::milliseconds;
using Seconds = std::chrono::seconds;

}  // namespace Xi::Chrono
