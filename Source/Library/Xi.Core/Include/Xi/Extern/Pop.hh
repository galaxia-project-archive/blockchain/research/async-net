﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

/*!
 * Include this header after you included external headers yielding warnings.
 *
 * Note you must have previously included the corresponding Push header.
 */

#include <leathers/pop>

#if defined(_MSC_VER)

#undef NOGDI

#if defined(max)
#undef max
#endif

#if defined(min)
#undef min
#endif

#endif
