// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <memory>
#include <utility>

#include "Xi/Global.hh"
#include "Xi/ErrorModel.hh"

namespace Xi {

template <typename _ValueT>
class Ownership final {
 private:
  struct _Concept {
    virtual ~_Concept() = default;

    virtual const _ValueT& get() const = 0;
    virtual _ValueT& get() = 0;
  };

  template <typename _CovariantT>
  struct _UniqueOwnership final : _Concept {
    std::unique_ptr<_CovariantT> m_data;

    _UniqueOwnership(std::unique_ptr<_CovariantT> data) : m_data{std::move(data)} {
      XI_EXCEPTIONAL_IF_NOT(NullArgumentError, m_data)
    }
    ~_UniqueOwnership() = default;

    virtual const _ValueT& get() const {
      return *this->m_data;
    }
    virtual _ValueT& get() {
      return *this->m_data;
    }
  };

  template <typename _CovariantT>
  struct _DelegatedOwnership final : _Concept {
    _CovariantT& m_data;

    _DelegatedOwnership(_CovariantT& data) : m_data{data} {
      /* */
    }
    ~_DelegatedOwnership() = default;

    virtual const _ValueT& get() const {
      return this->m_data;
    }
    virtual _ValueT& get() {
      return this->m_data;
    }
  };

  template <typename _CovariantT>
  struct _ValueOwnership final : _Concept {
    _CovariantT m_data;

    _ValueOwnership(_CovariantT data) : m_data{std::move(data)} {
      /* */
    }
    ~_ValueOwnership() = default;

    virtual const _ValueT& get() const {
      return this->m_data;
    }
    virtual _ValueT& get() {
      return this->m_data;
    }
  };

 private:
  std::unique_ptr<_Concept> m_impl;

 private:
  explicit Ownership(std::unique_ptr<_Concept> impl) : m_impl{std::move(impl)} {
    /* */
  }

  template <typename _IValueT, typename _CovariantT>
  friend Ownership<_IValueT> makeUniqueOwnership(std::unique_ptr<_CovariantT> data);
  template <typename _IValueT, typename _CovariantT>
  friend Ownership<_IValueT> makeDelegatedOwnership(_CovariantT& data);
  template <typename _IValueT, typename _CovariantT>
  friend Ownership<_IValueT> makeValueOwnership(_CovariantT&& data);

 public:
  XI_DELETE_COPY(Ownership)
  XI_DEFAULT_MOVE(Ownership)
  virtual ~Ownership() = default;

  const _ValueT& get() const {
    return this->m_impl->get();
  }

  _ValueT& get() {
    return this->m_impl->get();
  }
};

template <typename _ValueT, typename _CovariantT = _ValueT>
Ownership<_ValueT> makeUniqueOwnership(std::unique_ptr<_CovariantT> data) {
  return Ownership<_ValueT>{std::unique_ptr<typename Ownership<_ValueT>::_Concept>{
      new typename Ownership<_ValueT>::template _UniqueOwnership<_CovariantT>{std::move(data)}}};
}

template <typename _ValueT, typename _CovariantT = _ValueT>
Ownership<_ValueT> makeDelegatedOwnership(_CovariantT& data) {
  return Ownership<_ValueT>{std::unique_ptr<typename Ownership<_ValueT>::_Concept>{
      new typename Ownership<_ValueT>::template _DelegatedOwnership<_CovariantT>{data}}};
}

template <typename _ValueT, typename _CovariantT = _ValueT>
Ownership<_ValueT> makeValueOwnership(_CovariantT&& data) {
  return Ownership<_ValueT>{std::unique_ptr<typename Ownership<_ValueT>::_Concept>{
      new typename Ownership<_ValueT>::template _ValueOwnership<_CovariantT>{std::move(data)}}};
}

}  // namespace Xi
