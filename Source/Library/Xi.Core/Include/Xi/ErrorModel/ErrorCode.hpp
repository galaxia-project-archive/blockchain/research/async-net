﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <string>
#include <system_error>
#include <type_traits>

#include "Xi/ErrorModel/Error.hpp"

#define XI_ERROR_CODE_BEGIN(NAME)               \
  enum struct [[nodiscard]] NAME##Error : int { \
    Success = 0x0000,
#define XI_ERROR_CODE_VALUE(NAME, VALUE) NAME = VALUE,
#define XI_ERROR_CODE_END(NAME, DESC)                                             \
  }                                                                               \
  ;                                                                               \
  struct NAME##ErrorCategory final : public std::error_category {                 \
    const char *name() const noexcept override {                                  \
      return DESC;                                                                \
    }                                                                             \
    std::string message(int ev) const override;                                   \
                                                                                  \
   private:                                                                       \
    NAME##ErrorCategory() = default;                                              \
                                                                                  \
   public:                                                                        \
    static NAME##ErrorCategory Instance;                                          \
  };                                                                              \
  inline std::error_code make_error_code(NAME##Error err) {                       \
    return std::error_code{static_cast<int>(err), NAME##ErrorCategory::Instance}; \
  }                                                                               \
  inline std::string stringify(NAME##Error err) {                                 \
    return NAME##ErrorCategory::Instance.message(static_cast<int>(err));          \
  }                                                                               \
  inline ::Xi::Error makeError(NAME##Error ec) {                                  \
    return ::Xi::makeError(make_error_code(ec));                                  \
  }                                                                               \
  [[nodiscard]] inline bool isSuccess(NAME##Error ec) {                           \
    return ec == NAME##Error::Success;                                            \
  }                                                                               \
  [[nodiscard]] inline bool isFailure(NAME##Error ec) {                           \
    return !isSuccess(ec);                                                        \
  }

#define XI_ERROR_CODE_CATEGORY_BEGIN(NAMESPACE, NAME)                        \
  NAMESPACE::NAME##ErrorCategory NAMESPACE::NAME##ErrorCategory::Instance{}; \
  std::string NAMESPACE::NAME##ErrorCategory::message(int ev) const {        \
    using error_code_type = NAME##Error;                                     \
    const NAME##Error code = static_cast<error_code_type>(ev);               \
    switch (code) {                                                          \
      case error_code_type::Success:                                         \
        return "success";

#define XI_ERROR_CODE_DESC(VALUE, DESC) \
  case error_code_type::VALUE:          \
    return DESC;

#define XI_ERROR_CODE_CATEGORY_END() \
  }                                  \
  return "unknown";                  \
  }

#define XI_ERROR_CODE_OVERLOADS(NAMESPACE, NAME)                           \
  namespace std {                                                          \
  template <>                                                              \
  struct is_error_code_enum<NAMESPACE::NAME##Error> : public true_type {}; \
  }
