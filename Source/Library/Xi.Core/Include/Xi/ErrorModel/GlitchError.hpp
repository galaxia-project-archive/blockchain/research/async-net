// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <Xi/ErrorModel/ErrorCode.hpp>

namespace Xi {

XI_ERROR_CODE_BEGIN(Glitch)
XI_ERROR_CODE_VALUE(Moved, 0x0001)
XI_ERROR_CODE_VALUE(Exceptional, 0x0002)
XI_ERROR_CODE_VALUE(NotInitialized, 0x0003)
XI_ERROR_CODE_END(Glitch, "GlitchError")

}  // namespace Xi
