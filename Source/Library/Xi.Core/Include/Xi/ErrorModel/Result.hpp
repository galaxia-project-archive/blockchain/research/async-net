﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <memory>
#include <utility>
#include <type_traits>
#include <variant>
#include <optional>
#include <iostream>

#include "Xi/Global.hh"
#include "Xi/ErrorModel/Print.hh"
#include "Xi/ErrorModel/Error.hpp"
#include "Xi/ErrorModel/GlitchError.hpp"
#include "Xi/String/ToString.hpp"
#include "Xi/TypeTrait/IsSmartPointer.hpp"

namespace Xi {

struct result_success_t {};
static inline constexpr result_success_t result_success{};
struct result_failure_t {};
static inline constexpr result_failure_t result_failure{};

/*!
 * \brief The Result class wraps an expected return value to be either the value or an error.
 *
 * This class enables methods to fail and propagate errors if one of their dependencies fail.
 *
 * \attention Whenever you return a result you may never throw an exception \see XI_ERROR_TRY and XI_ERROR CATCH
 *
 * \code{.cpp}
 * Xi::Result<Transaction> Proxy::queryTransaction(const Crypto::Hash& id) {
 *  XI_ERROR_TRY
 *  // Assume we have a load balance that may return a result encoding an error or a valid remote serving the
 *  // transaction.
 *  auto remote = m_loadBalancer.queryTransactionRemote(id).takeOrThrow();
 *  return remote->queryTransaction(id);
 *  XI_ERROR_CATCH
 * }
 * \endcode
 */
template <typename _ValueT = void, typename _ErrorT = Error>
class [[nodiscard]] Result {
 public:
  using error_t = _ErrorT;
  using value_t = std::decay_t<_ValueT>;

  using variant_t = std::variant<error_t, value_t>;
  static inline constexpr std::in_place_index_t<0> error_index{/* */};
  static inline constexpr std::in_place_index_t<1> value_index{/* */};

  static inline constexpr bool is_pointer = std::is_pointer_v<value_t> || is_smart_pointer_v<value_t>;

 private:
  std::variant<error_t, value_t> m_result;

 public:
  explicit Result() : m_result{error_index, error_t{}} {
    /* */
  }
  explicit Result(result_failure_t) : m_result{error_index, error_t{}} {
    /* */
  }
  /* implicit */ Result(const error_t& err) : m_result{error_index, err} {
    /* */
  }

  template <typename... _ArgsT>
  Result(result_success_t, _ArgsT && ... args) : m_result{value_index, std::forward<_ArgsT>(args)...} {
    /* */
  }

  Result(const Result& other) : m_result{other.m_result} {
    static_assert(std::is_copy_assignable_v<value_t> || std::is_copy_constructible_v<value_t>,
                  "In order to copy a result the underyling type must be copy constructible or assignable.");
  }

  Result& operator=(const Result& other) {
    static_assert(std::is_copy_assignable_v<value_t> || std::is_copy_constructible_v<value_t>,
                  "In order to copy a result the underyling type must be copy constructible or assignable.");
    m_result = other.m_result;
  }

  Result(Result && other) : m_result{std::move(other.m_result)} {
    static_assert(std::is_move_assignable_v<value_t> || std::is_move_constructible_v<value_t>,
                  "In order to move a result the underyling type must be move constructible or assignable.");
    other.m_result = variant_t{error_index, makeError(GlitchError::Moved)};
  }

  Result& operator=(Result&& other) {
    static_assert(std::is_move_assignable_v<value_t> || std::is_move_constructible_v<value_t>,
                  "In order to move a result the underyling type must be move constructible or assignable.");
    m_result = std::move(other.m_result);
    other.m_result = variant_t{error_index, makeError(GlitchError::Moved)};
    return *this;
  }

  ~Result() = default;

  bool isError() const {
    return this->m_result.index() == 0;
  }
  bool isValue() const {
    return this->m_result.index() == 1;
  }

  const error_t& error() const {
    return std::get<error_t>(m_result);
  }

  const value_t& value() const {
    return std::get<1>(m_result);
  }
  const value_t& operator*() const {
    return std::get<1>(m_result);
  }
  value_t& value() {
    return std::get<1>(m_result);
  }
  value_t& operator*() {
    return std::get<1>(m_result);
  }

  value_t take() {
    static_assert(std::is_move_constructible<value_t>::value, "You can only take move constructible types.");
    return std::move(std::get<1>(m_result));
  }

  std::conditional_t<is_pointer, const value_t&, const value_t*> operator->() const {
    if constexpr (is_pointer) {
      return std::get<value_t>(this->m_result);
    } else {
      return std::get_if<value_t>(&this->m_result);
    }
  }

  std::conditional_t<is_pointer, value_t&, value_t*> operator->() {
    if constexpr (is_pointer) {
      return std::get<value_t>(this->m_result);
    } else {
      return std::get_if<value_t>(&this->m_result);
    }
  }

  // Implicit Conversion
  template <typename _OtherValueT, std::enable_if_t<std::is_convertible_v<value_t, _OtherValueT>, int> = 0>
  operator Result<_OtherValueT, error_t>() const {
    if (this->isError()) {
      return this->error();
    } else {
      return Result<_OtherValueT>{result_success, this->value()};
    }
  }

  std::string stringify() const {
    if (this->isError()) {
      return toString(this->error());
    } else {
      return toString(this->value());
    }
  }
};

template <typename _ErrorT>
class [[nodiscard]] Result<void, _ErrorT> {
 private:
  std::optional<_ErrorT> m_error;

 public:
  explicit Result(result_failure_t = result_failure) : m_error{_ErrorT{/* */}} {
    /* */
  }
  explicit Result(result_success_t) : m_error{std::nullopt} {
    /* */
  }
  /* implicit */ Result(const _ErrorT& err) : m_error{err} {
  }
  XI_DEFAULT_COPY(Result)
  XI_DEFAULT_MOVE(Result)
  ~Result() = default;

  bool isError() const {
    return m_error.has_value();
  }

  const _ErrorT& error() const {
    return *m_error;
  }

  std::string stringify() const {
    if (this->isError()) {
      return toString(this->error());
    } else {
      return "success";
    }
  }
};

template <typename _ValueT, std::enable_if_t<std::is_move_constructible_v<_ValueT>, int> = 0>
inline Result<std::decay_t<_ValueT>> makeSuccess(_ValueT&& val) {
  return Result<std::decay_t<_ValueT>>{result_success, typename Result<_ValueT>::value_t{std::forward<_ValueT>(val)}};
}

template <typename _ValueT, std::enable_if_t<std::is_copy_constructible_v<_ValueT>, int> = 0>
inline Result<std::decay_t<_ValueT>> makeSuccess(const _ValueT& val) {
  return Result<std::decay_t<_ValueT>>{result_success, val};
}

// clang-format off
template <
  typename _ValueT,
  typename _Arg0,
  typename... _ArgsT,
  std::enable_if_t<
      !std::is_same_v<std::decay_t<_ValueT>, std::decay_t<_Arg0>>
  , int> = 0
>
// clang-format on
inline Result<std::decay_t<_ValueT>> makeSuccess(_Arg0&& arg, _ArgsT&&... args) {
  return Result<std::decay_t<_ValueT>>{result_success, std::forward<_Arg0>(arg), std::forward<_ArgsT>(args)...};
}

inline Result<void> makeSuccess() {
  return Result<void>{result_success};
}

template <typename... _ValueT>
inline Error makeFailure(_ValueT&&... args) {
  return makeError(std::forward<_ValueT>(args)...);
}

template <typename _ValueT>
[[nodiscard]] inline bool isSuccess(const Result<_ValueT>& ec) {
  return !ec.isError();
}
template <typename _ValueT>
[[nodiscard]] inline bool isFailure(const Result<_ValueT>& ec) {
  return ec.isError();
}

}  // namespace Xi

#define XI_ERROR_TRY       \
  using ::Xi::makeSuccess; \
  using ::Xi::makeFailure; \
  try {
#define XI_ERROR_CATCH                          \
  }                                             \
  catch (...) {                                 \
    return makeError(GlitchError::Exceptional); \
  }

#define XI_ERROR_PROPAGATE(VAR) XI_RETURN_EC_IF(VAR.isError(), VAR.error());
#define XI_ERROR_CODE_PROPAGATE(VAR) \
  if (VAR) {                         \
    return ::Xi::makeError(VAR);     \
  }

#define XI_ERROR_PROPAGATE_CATCH(STMT) \
  {                                    \
    const auto __ec = STMT;            \
    XI_ERROR_PROPAGATE(__ec)           \
  }

#define XI_FAIL(X) return ::Xi::makeFailure(X);
#define XI_FAIL_IF(COND, X)                                                                               \
  if (COND) {                                                                                             \
    XI_PRINT_EC("[%s:%i] condition (%s) satisfied returning error: %s\n", __FILE__, __LINE__, #COND, #X); \
    XI_FAIL(X)                                                                                            \
  }
#define XI_FAIL_IF_NOT(COND, X) XI_FAIL_IF(!(COND), X)

#define XI_SUCCEED(...) return makeSuccess(__VA_ARGS__);
#define XI_SUCCEED_IF(COND, ...) \
  if (COND) {                    \
    XI_SUCCEED(__VA_ARGS__)      \
  }
#define XI_SUCCEED_IF_NOT(COND, ...) XI_SUCCEED_IF(!(COND), __VA_ARGS__)
