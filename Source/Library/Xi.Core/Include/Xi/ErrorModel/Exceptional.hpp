﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <stdexcept>
#include <utility>
#include <type_traits>
#include <string>
#include <cassert>

#include <Xi/Extern/Push.hh>
#include <fmt/format.h>
#include <Xi/Extern/Pop.hh>

#include "Xi/Global.hh"
#include "Xi/ErrorModel/Print.hh"

namespace Xi {
namespace Impl {
struct exceptional_macro_invocation_t {
  /* */
};
static inline constexpr exceptional_macro_invocation_t exceptional_macro_invocation{/* */};

template <typename _ExceptionT>
[[noreturn]] inline void exceptional(exceptional_macro_invocation_t) {
  static_assert(std::is_base_of<std::exception, _ExceptionT>::value, "Only exceptions can be exceptional.");
  throw _ExceptionT{};
}

template <typename _ExceptionT, typename... _Ts>
[[noreturn]] inline void exceptional(exceptional_macro_invocation_t, const char* s, _Ts&&... args) {
  static_assert(std::is_base_of<std::exception, _ExceptionT>::value, "Only exceptions can be exceptional.");
  throw _ExceptionT{fmt::format(s, std::forward<_Ts>(args)...)};
}

template <typename _ExceptionT>
[[noreturn]] inline void exceptional(exceptional_macro_invocation_t, const std::string& s) {
  static_assert(std::is_base_of<std::exception, _ExceptionT>::value, "Only exceptions can be exceptional.");
  throw _ExceptionT{s};
}

template <typename _ExceptionT>
inline void exceptional_if(exceptional_macro_invocation_t, bool cond) {
  static_assert(std::is_base_of<std::exception, _ExceptionT>::value, "Only exceptions can be exceptional.");
  if (cond) {
    exceptional<_ExceptionT>();
  }
}

template <typename _ExceptionT, typename... _Ts>
inline void exceptional_if(exceptional_macro_invocation_t, bool cond, const char* s, _Ts&&... args) {
  static_assert(std::is_base_of<std::exception, _ExceptionT>::value, "Only exceptions can be exceptional.");
  if (cond) {
    exceptional<_ExceptionT>(fmt::format(s, std::forward<_Ts>(args)...));
  }
}

template <typename _ExceptionT>
inline void exceptional_if(exceptional_macro_invocation_t, bool cond, const std::string& s) {
  static_assert(std::is_base_of<std::exception, _ExceptionT>::value, "Only exceptions can be exceptional.");
  if (cond) {
    exceptional<_ExceptionT>(s);
  }
}

template <typename _ExceptionT>
inline void exceptional_if_not(exceptional_macro_invocation_t, bool cond) {
  static_assert(std::is_base_of<std::exception, _ExceptionT>::value, "Only exceptions can be exceptional.");
  if (!cond) {
    exceptional<_ExceptionT>();
  }
}

template <typename _ExceptionT, typename... _Ts>
inline void exceptional_if_not(exceptional_macro_invocation_t, bool cond, const char* s, _Ts&&... args) {
  static_assert(std::is_base_of<std::exception, _ExceptionT>::value, "Only exceptions can be exceptional.");
  if (!cond) {
    exceptional<_ExceptionT>(fmt::format(s, std::forward<_Ts>(args)...));
  }
}

template <typename _ExceptionT>
inline void exceptional_if_not(exceptional_macro_invocation_t, bool cond, const std::string& s) {
  static_assert(std::is_base_of<std::exception, _ExceptionT>::value, "Only exceptions can be exceptional.");
  if (!cond) {
    exceptional<_ExceptionT>(s);
  }
}

}  // namespace Impl
}  // namespace Xi

#define XI_EXCEPTIONAL(ERROR_TYPE, ...) \
  ::Xi::Impl::exceptional<ERROR_TYPE>(::Xi::Impl::exceptional_macro_invocation __VA_OPT__(, ) __VA_ARGS__);
#define XI_EXCEPTIONAL_IF(ERROR_TYPE, COND, ...)                                                                  \
  if (COND) {                                                                                                     \
    assert(false);                                                                                                \
    XI_PRINT_EC("[%s:%i] condition (%s) failed throwin exception: %s\n", __FILE__, __LINE__, #COND, #ERROR_TYPE); \
    XI_EXCEPTIONAL(ERROR_TYPE __VA_OPT__(, ) __VA_ARGS__)                                                         \
  }
#define XI_EXCEPTIONAL_IF_NOT(ERROR_TYPE, COND, ...) XI_EXCEPTIONAL_IF(ERROR_TYPE, (!(COND))__VA_OPT__(, ) __VA_ARGS__)

#define XI_DECLARE_EXCEPTIONAL_CATEGORY(CAT)      \
  class CAT##Exception : public std::exception {  \
   private:                                       \
    std::string m;                                \
                                                  \
   protected:                                     \
    CAT##Exception(const char* s) : m{s} {        \
    }                                             \
    CAT##Exception(const std::string& s) : m{s} { \
    }                                             \
                                                  \
   public:                                        \
    virtual ~CAT##Exception() = default;          \
                                                  \
    const char* what() const noexcept {           \
      return m.c_str();                           \
    }                                             \
  };

#define XI_DECLARE_EXCEPTIONAL_INSTANCE(X, MSG, CAT)     \
  class X##Error : public CAT##Exception {               \
   public:                                               \
    X##Error() : CAT##Exception(MSG) {                   \
    }                                                    \
    X##Error(const char* s) : CAT##Exception(s) {        \
    }                                                    \
    X##Error(const std::string& s) : CAT##Exception(s) { \
    }                                                    \
  };
