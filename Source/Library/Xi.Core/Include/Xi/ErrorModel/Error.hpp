﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <system_error>

namespace Xi {
/*!
 * \brief The Error class wraps an arbitrary error.
 *
 * \note An instance of an error is always an error. For conditionally return an error or nothing or a return value
 * \see Xi::Result .
 *
 * Sometimes caller are not able to handle any kind of error and may dont want to deal with them in any way except
 * returning immedietly. This often results into boolean returns or exceptions. The Error class provides an alternative
 * such that errors can be propagated backwards without loosing information about the error that actually occured.
 */
class [[nodiscard]] Error {
 public:
  /*!
   * \brief Error creates an not initialiezd error.
   *
   * Even if an error is not initialized properly it is considered as an error. This constructor is necessary to use
   * errors in many STL parts as they assume/require types to be default constructible.
   */
  explicit Error();
  explicit Error(std::error_code ec);
  explicit Error(const Error&) = default;
  Error& operator=(const Error&) = default;
  explicit Error(Error &&) = default;
  Error& operator=(Error&&) = default;
  ~Error() = default;

  /*!
   * \brief message yields a short description of the error.
   *
   * \todo may replace this with a contextual error message provider. That way we wont have to log errors until
   * we really want to and have necessary data stored to identify its source. Ie. a transaction validation fails
   * due to too low mixins. Then the provider could store max/min and actual mixin to provide a meaningfull error
   * message.
   */
  std::string message() const;

  /*!
   * \brief category Queries the error code category name.
   * \return A string representation of the category identifier.
   */
  std::string category() const;

  /// Returns a small string representation of this error.
  std::string stringify() const;

  /*!
   * \brief errorCode the underyling error code encoding.
   */
  const std::error_code& code() const;

 private:
  std::error_code m_error;
};

inline Error makeError(std::error_code ec) {
  return Error{ec};
}

inline Error makeError(const std::errc sysError) {
  return makeError(make_error_code(sysError));
}

}  // namespace Xi
