﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include "Xi/Global/Concatenate.hh"
#include "Xi/Global/NArgs.hh"
#include "Xi/Global/Padding.hh"
#include "Xi/Global/Unused.hh"
#include "Xi/Global/Boolean.hh"
#include "Xi/Global/ThreadLocal.hh"

#if defined(__cplusplus)

#include "Xi/Global/Constructor.hpp"
#include "Xi/Global/SmartPointer.hpp"

#endif  // defined(__cplusplus)
