// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <string>
#include <string_view>

#include <Xi/Overload.hpp>

namespace Xi {

[[nodiscard]] std::string toUpper(std::string_view str);
[[nodiscard]] std::string::value_type toUpper(const std::string::value_type ch);
void toUpper(std::string& str, in_place_t);

}  // namespace Xi
