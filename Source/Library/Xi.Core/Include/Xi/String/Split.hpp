// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <vector>
#include <string>
#include <string_view>
#include <functional>

#include "Xi/Overload.hpp"

namespace Xi {

void split(std::string_view str, std::string_view tokens, const std::function<void(std::string_view)>& cb);
[[nodiscard]] std::vector<std::string_view> split(std::string_view str, std::string_view tokens);
[[nodiscard]] std::vector<std::string> split(std::string_view str, std::string_view tokens, make_copy_t);

}  // namespace Xi
