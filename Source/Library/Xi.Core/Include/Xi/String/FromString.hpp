// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <string>
#include <utility>
#include <cinttypes>
#include <type_traits>

#include "Xi/ErrorModel.hh"
#include "Xi/TypeTrait/IsDetected.hpp"

#include "Xi/String/Parse/Parse.hpp"

namespace Xi {

template <typename _ValueT, typename... _Ts, std::enable_if_t<has_parse_member_expression_v<_ValueT, _Ts...>, int> = 0>
Result<_ValueT> fromString(const std::string& str, _Ts&&... args) {
  return _ValueT::parse(str, std::forward<_Ts>(args)...);
}

template <typename _ValueT, typename... _Ts, std::enable_if_t<has_parse_expression_v<_ValueT, _Ts...>, int> = 0>
Result<_ValueT> fromString(const std::string& str, _Ts&&... args) {
  _ValueT value;
  XI_ERROR_PROPAGATE_CATCH(parse(str, value, std::forward<_Ts>(args)...))
  if constexpr (std::is_compound_v<_ValueT>) {
    XI_SUCCEED(std::move(value))
  } else {
    XI_SUCCEED(value)
  }
}

}  // namespace Xi
