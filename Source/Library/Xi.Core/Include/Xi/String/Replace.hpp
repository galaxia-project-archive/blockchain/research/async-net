// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <string>
#include <string_view>

namespace Xi {

[[nodiscard]] std::string replace(std::string_view str, std::string_view toReplace, std::string_view replacement);

}  // namespace Xi
