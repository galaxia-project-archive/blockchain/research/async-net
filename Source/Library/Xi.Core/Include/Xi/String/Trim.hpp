// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <string>
#include <string_view>

#include "Xi/Overload.hpp"

namespace Xi {

[[nodiscard]] std::string trimLeft(std::string_view str);
void trimLeft(std::string& str, in_place_t);

[[nodiscard]] std::string trimRight(std::string_view str);
void trimRight(std::string& str, in_place_t);

[[nodiscard]] std::string trim(std::string_view str);
void trim(std::string& str, in_place_t);

}  // namespace Xi
