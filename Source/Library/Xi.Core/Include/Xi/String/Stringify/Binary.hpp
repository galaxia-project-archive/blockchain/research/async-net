// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <cinttypes>
#include <string>

namespace Xi {
namespace Stringify {

struct binary_t {
  /* */
};

static inline constexpr binary_t binary{/* */};

}  // namespace Stringify

std::string stringify(const uint8_t value, Stringify::binary_t);
std::string stringify(const uint16_t value, Stringify::binary_t);
std::string stringify(const uint32_t value, Stringify::binary_t);
std::string stringify(const uint64_t value, Stringify::binary_t);
std::string stringify(const int8_t value, Stringify::binary_t);
std::string stringify(const int16_t value, Stringify::binary_t);
std::string stringify(const int32_t value, Stringify::binary_t);
std::string stringify(const int64_t value, Stringify::binary_t);

}  // namespace Xi
