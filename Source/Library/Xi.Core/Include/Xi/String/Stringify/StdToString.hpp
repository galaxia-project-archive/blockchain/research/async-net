// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <string>
#include <type_traits>

#include "Xi/TypeTrait/IsDetected.hpp"

namespace Xi {

template <typename _ValueT>
using std_to_string_expression_t = decltype(std::to_string(std::declval<const _ValueT&>()));
template <typename _ValueT>
static inline constexpr bool has_std_to_string_expression_v = is_detected_v<std_to_string_expression_t, _ValueT>;

}  // namespace Xi
