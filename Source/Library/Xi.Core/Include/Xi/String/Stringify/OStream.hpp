// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <type_traits>
#include <string>
#include <sstream>
#include <utility>

#include "Xi/TypeTrait/IsDetected.hpp"

#include "Xi/String/Stringify/Trait.hpp"

namespace Xi {

template <typename _ValueT>
using ostringstream_expression_t = decltype(std::declval<std::ostream&>() << std::declval<const _ValueT&>());
template <typename _ValueT>
static inline constexpr bool has_ostringstream_expression_v = is_detected_v<ostringstream_expression_t, _ValueT>;

}  // namespace Xi

template <typename _ValueT, std::enable_if_t<Xi::has_stringify_expression_v<_ValueT>, int> = 0>
inline std::ostream& operator<<(std::ostream& stream, const _ValueT& value) {
  return stream << stringify(value);
}

template <typename _ValueT, std::enable_if_t<Xi::has_stringify_member_expression_v<_ValueT>, int> = 0>
inline std::ostream& operator<<(std::ostream& stream, const _ValueT& value) {
  return stream << value.stringify();
}
