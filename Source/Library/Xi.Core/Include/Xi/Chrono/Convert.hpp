﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <chrono>
#include <type_traits>

namespace Xi {
namespace Chrono {

template <typename _TargetClockT, typename _ClockT, typename _DurationT>
typename _TargetClockT::time_point clock_cast(std::chrono::time_point<_ClockT, _DurationT> const& time) {
  if constexpr (std::is_same_v<typename _TargetClockT::time_point, std::chrono::time_point<_ClockT, _DurationT>>) {
    return time;
  } else {
    return _TargetClockT::now() + (time - _ClockT::now());
  }
}

using std::chrono::duration_cast;

}  // namespace Chrono
}  // namespace Xi
