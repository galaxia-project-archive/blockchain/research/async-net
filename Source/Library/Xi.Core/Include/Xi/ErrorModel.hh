// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include "Xi/ErrorModel/Print.hh"
#include "Xi/ErrorModel/Return.hh"

#if defined(__cplusplus)

#include "Xi/ErrorModel/Error.hpp"
#include "Xi/ErrorModel/ErrorCode.hpp"
#include "Xi/ErrorModel/Exceptional.hpp"
#include "Xi/ErrorModel/Exceptions.hpp"
#include "Xi/ErrorModel/Result.hpp"
#include "Xi/ErrorModel/GlitchError.hpp"

#endif
