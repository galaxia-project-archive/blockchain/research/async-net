// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#define XI_IMPL_CONCATENATE_(A, B) A##B
#define XI_CONCATENATE(A, B) XI_IMPL_CONCATENATE_(A, B)
