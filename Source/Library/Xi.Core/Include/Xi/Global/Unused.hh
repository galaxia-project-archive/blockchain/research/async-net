// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include "Xi/Global/Concatenate.hh"

/*!
 * \def XI_UNUSED Marks a variable unused intentionally.
 */
#define XI_UNUSED(VALUE) (void)VALUE;

#if defined(__cplusplus)
/*!
 * \def XI_UNUSED_REVAL Marks a return value as intentionally unused. Mainly used for RAII objects.
 */
#define XI_UNUSED_REVAL(STMT) [[maybe_unused]] auto XI_CONCATENATE(_XI_UNUSED_REVAL_, __LINE__) = STMT;
#endif
