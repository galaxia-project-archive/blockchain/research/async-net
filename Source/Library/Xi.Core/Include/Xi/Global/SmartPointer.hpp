// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <memory>

#define XI_DECLARE_SMART_POINTER(TYPE)                   \
  using Unique##TYPE = std::unique_ptr<TYPE>;            \
  using Shared##TYPE = std::shared_ptr<TYPE>;            \
  using Weak##TYPE = std::weak_ptr<TYPE>;                \
  using SharedConst##TYPE = std::shared_ptr<const TYPE>; \
  using WeakConst##TYPE = std::weak_ptr<const TYPE>;

#define XI_DECLARE_SMART_POINTER_CLASS(TYPE) \
  class TYPE;                                \
  XI_DECLARE_SMART_POINTER(TYPE)

#define XI_DECLARE_SMART_POINTER_STRUCT(TYPE) \
  struct TYPE;                                \
  XI_DECLARE_SMART_POINTER(TYPE)
