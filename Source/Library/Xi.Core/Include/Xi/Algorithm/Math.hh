// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#if defined(__cplusplus)

#include <cinttypes>
#include <vector>
#include <algorithm>
#include <type_traits>

#if defined(__GNUC__) || defined(__clang__)
#include <cstdio>
#else
#include <limits>
#endif

#include <Xi/Span.hpp>
#include <Xi/ErrorModel.hh>

namespace Xi {

/*!
 * \brief pow computes the to the power of function (base^exponent)
 * \param base the base of the formula
 * \param exponent the exponent of the formula
 * \return base^exponent, 1 if exponent is 0
 */
template <typename _IntegerT>
static inline constexpr _IntegerT pow(_IntegerT base, _IntegerT exponent) {
  static_assert(std::is_integral_v<_IntegerT>, "pow is only supported for integral types");
  static_assert(std::is_unsigned_v<_IntegerT>, "pow is only supported of unsigned integral types");
  return exponent == 0 ? 1 : base * pow<_IntegerT>(base, exponent - 1);
}

template <typename _IntegerT>
[[nodiscard]] bool hasAdditionOverflow(const _IntegerT lhs, const _IntegerT rhs, _IntegerT* res) {
#if defined(__GNUC__) || defined(__clang__)
  return __builtin_add_overflow(lhs, rhs, res);
#else
  if (lhs + rhs < lhs) {
    return true;
  } else {
    *res = lhs + rhs;
    return false;
  }
#endif
}

template <typename _IntegerT>
static inline constexpr _IntegerT log2(_IntegerT n) {
  return (n > 1) ? 1 + log2(n >> 1) : 0;
}

template <typename _IntegerT>
static inline constexpr bool isMultipleOf(_IntegerT target, _IntegerT scalar) {
  return (target == 0 || scalar == 0) ? (target == 0 && scalar == 0) : ((target / scalar) * scalar) == target;
}

template <typename _IntegerT>
static inline constexpr bool isPowerOf2(_IntegerT n) {
  return !(n == 0) && !(n & (n - 1));
}

template <typename _IntegerT>
static inline _IntegerT median(ConstSpan<_IntegerT> values) {
  XI_EXCEPTIONAL_IF(NullArgumentError, values.empty())
  XI_RETURN_SC_IF(values.size() == 1, values[0]);
  std::vector<_IntegerT> sorted{};
  sorted.reserve(values.size());
  std::copy(values.begin(), values.end(), std::back_inserter(sorted));
  std::sort(begin(sorted), end(sorted));
  if ((sorted.size() & 1) > 0) {
    return sorted[sorted.size() / 2];
  } else {
    return (sorted[sorted.size() / 2 - 1] + sorted[sorted.size() / 2]) / 2;
  }
}

template <typename _IntegerT>
static inline _IntegerT median(const std::vector<_IntegerT>& values) {
  return median<_IntegerT>(ConstSpan<_IntegerT>{values});
}

}  // namespace Xi

#endif
