// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <type_traits>
#include <memory>

namespace Xi {

template <typename _ValueT>
struct is_unique_pointer : std::false_type {};
template <typename _ValueT>
struct is_unique_pointer<std::unique_ptr<_ValueT>> : std::true_type {};
template <typename _ValueT>
struct is_unique_pointer<std::unique_ptr<const _ValueT>> : std::true_type {};
template <typename _ValueT>
inline constexpr bool is_unique_pointer_v = is_unique_pointer<_ValueT>::value;

template <typename _ValueT>
struct is_shared_pointer : std::false_type {};
template <typename _ValueT>
struct is_shared_pointer<std::shared_ptr<_ValueT>> : std::true_type {};
template <typename _ValueT>
struct is_shared_pointer<std::shared_ptr<const _ValueT>> : std::true_type {};
template <typename _ValueT>
inline constexpr bool is_shared_pointer_v = is_shared_pointer<_ValueT>::value;

template <typename _ValueT>
struct is_weak_pointer : std::false_type {};
template <typename _ValueT>
struct is_weak_pointer<std::weak_ptr<_ValueT>> : std::true_type {};
template <typename _ValueT>
struct is_weak_pointer<std::weak_ptr<const _ValueT>> : std::true_type {};
template <typename _ValueT>
inline constexpr bool is_weak_pointer_v = is_weak_pointer<_ValueT>::value;

template <typename _ValueT>
struct is_smart_pointer
    : std::conditional_t<is_unique_pointer_v<_ValueT> || is_shared_pointer_v<_ValueT> || is_weak_pointer_v<_ValueT>,
                         std::true_type, std::false_type> {};
template <typename _ValueT>
inline constexpr bool is_smart_pointer_v = is_smart_pointer<_ValueT>::value;

static_assert(is_smart_pointer_v<std::unique_ptr<int>>, "");
static_assert(is_smart_pointer_v<std::unique_ptr<const int>>, "");
static_assert(is_smart_pointer_v<std::shared_ptr<int>>, "");
static_assert(is_smart_pointer_v<std::shared_ptr<const int>>, "");
static_assert(is_smart_pointer_v<std::weak_ptr<int>>, "");
static_assert(is_smart_pointer_v<std::weak_ptr<const int>>, "");

}  // namespace Xi
