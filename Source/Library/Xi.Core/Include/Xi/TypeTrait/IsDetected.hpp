// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include "Xi/Extern/Push.hh"
#include <boost/type_traits/is_detected.hpp>
#include "Xi/Extern/Pop.hh"

namespace Xi {

using boost::is_detected;
using boost::is_detected_v;

}  // namespace Xi
