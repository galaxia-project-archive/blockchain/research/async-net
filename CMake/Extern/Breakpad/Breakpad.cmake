# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_EXTERN_BREAKPAD)
  return()
endif()
set(CMAKE_XI_EXTERN_BREAKPAD TRUE)

add_subdirectory(${CMAKE_CURRENT_LIST_DIR})
