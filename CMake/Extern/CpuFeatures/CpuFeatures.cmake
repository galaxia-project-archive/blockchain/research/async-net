# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_EXTERN_CPU_FEATURES)
  return()
endif()
set(CMAKE_XI_EXTERN_CPU_FEATURES TRUE)

add_subdirectory(${XI_ROOT_DIR}/Extern/CpuFeatures EXCLUDE_FROM_ALL)

mark_as_advanced(
  BUILD_TESTING
  BUILD_SHARED_LIBS
  BUILD_PIC
)

add_library(cpufeatures::cpufeatures ALIAS cpu_features)
