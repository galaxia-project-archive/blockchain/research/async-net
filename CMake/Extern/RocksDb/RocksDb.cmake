# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_EXTERN_ROCKSDB)
  return()
endif()
set(CMAKE_XI_EXTERN_ROCKSDB TRUE)

xi_include(Package/Lz4)
xi_include(Package/Zlib)
xi_include(Package/BZip2)

set(WITH_LZ4 ON CACHE INTERNAL "" FORCE)
set(WITH_ZLIB ON CACHE INTERNAL "" FORCE)
set(WITH_BZ2 ON CACHE INTERNAL "" FORCE)
set(FAIL_ON_WARNINGS OFF CACHE INTERNAL "" FORCE)
set(ROCKSDB_LITE OFF CACHE INTERNAL "" FORCE)
set(WITH_TESTS OFF CACHE INTERNAL "" FORCE)
set(WITH_TOOLS OFF CACHE INTERNAL "" FORCE)

add_subdirectory(${XI_ROOT_DIR}/Extern/RocksDb EXCLUDE_FROM_ALL)

add_library(rocksdb::rocksdb ALIAS rocksdb)
