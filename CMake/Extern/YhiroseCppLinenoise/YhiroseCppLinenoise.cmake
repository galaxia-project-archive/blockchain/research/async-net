# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_EXTERN_YHIROSE_CPP_LINENOISE)
  return()
endif()
set(CMAKE_XI_EXTERN_YHIROSE_CPP_LINENOISE TRUE)

add_library(cpp-linenoise INTERFACE IMPORTED GLOBAL)
target_include_directories(cpp-linenoise INTERFACE ${XI_ROOT_DIR}/Extern/YhiroseCppLinenoise)
add_library(cpp-linenoise::cpp-linenoise ALIAS cpp-linenoise)
