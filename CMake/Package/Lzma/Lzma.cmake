# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_PACKAGE_LZMA)
    return()
endif()
set(CMAKE_XI_PACKAGE_LZMA TRUE)

hunter_add_package(lzma)
find_package(lzma CONFIG REQUIRED)
