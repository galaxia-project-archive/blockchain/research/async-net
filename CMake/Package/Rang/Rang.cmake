# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_PACKAGE_RANG)
  return()
endif()
set(CMAKE_XI_PACKAGE_RANG TRUE)

hunter_add_package(rang)
find_package(rang CONFIG REQUIRED)

mark_as_advanced(
  rang_DIR
)
