# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

hunter_config(
  BZip2

  VERSION
    "1.0.6-p4"
)
