# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_PACKAGE_BZIP2)
  return()
endif()
set(CMAKE_XI_PACKAGE_BZIP2 TRUE)

hunter_add_package(BZip2)
find_package(BZip2 CONFIG REQUIRED)
