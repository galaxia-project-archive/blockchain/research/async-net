# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

hunter_config(
  Async++

  VERSION
    "0.0.3-hunter"
)
