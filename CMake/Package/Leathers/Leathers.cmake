# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_PACKAGE_LEATHERS)
  return()
endif()
set(CMAKE_XI_PACKAGE_LEATHERS TRUE)

xi_include(Package/Boost)

hunter_add_package(Leathers)
find_package(Leathers CONFIG REQUIRED)

mark_as_advanced(sugar_DIR Leathers_DIR)
