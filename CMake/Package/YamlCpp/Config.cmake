# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

hunter_config(
  yaml-cpp

  VERSION
    "0.6.2-0f9a586-p1"
)
