# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

hunter_config(
  rocksdb

  VERSION
    "5.14.2"
)
