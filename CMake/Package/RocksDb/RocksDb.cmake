# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_PACKAGE_ROCKSDB)
  return()
endif()
set(CMAKE_XI_PACKAGE_ROCKSDB TRUE)

hunter_add_package(rocksdb)
find_package(RocksDB CONFIG REQUIRED)
