# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

hunter_config(
  RapidJSON

  VERSION
    "1.1.0-66eb606-p0"
)
