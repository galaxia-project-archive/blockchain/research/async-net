# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_PACKAGE_LZ4)
  return()
endif()
set(CMAKE_XI_PACKAGE_LZ4 TRUE)

hunter_add_package(lz4)
find_package(lz4 CONFIG REQUIRED)

mark_as_advanced(
  lz4_DIR
)
