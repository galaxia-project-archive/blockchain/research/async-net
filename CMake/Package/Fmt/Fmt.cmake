# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_PACKAGE_FMT)
    return()
endif()
set(CMAKE_XI_PACKAGE_FMT TRUE)

hunter_add_package(fmt)
find_package(fmt CONFIG REQUIRED)

mark_as_advanced(fmt_DIR)
