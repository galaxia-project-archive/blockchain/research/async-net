# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(MSVC)
  hunter_config(
    OpenSSL
  
    VERSION
      "1.1.1c"
  
    CMAKE_ARGS
      ASM_SUPPORT=ON
  )
else()
  hunter_config(
    OpenSSL
  
    VERSION
      "1.1.1c"
  )
endif()