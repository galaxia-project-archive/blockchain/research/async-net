# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_PACKAGE_OPENSSL)
  return()
endif()
set(CMAKE_XI_PACKAGE_OPENSSL TRUE)

xi_include(Xi/Log)
xi_include(Xi/Compiler/Classify/Linkage)

if(WIN32)
  find_package(Perl QUIET)
  if(NOT PERL_FOUND)
    xi_fatal("The package manager requires perl to build openssl on windows. Please intall a recent version.")
  endif()
endif()

hunter_add_package(OpenSSL)
find_package(OpenSSL REQUIRED)

if(WIN32 AND XI_COMPILER_STATIC)
  foreach(ssl_lib OpenSSL::Crypto OpenSSL::SSL)
    target_link_libraries(
      ${ssl_lib}

      INTERFACE
        WS2_32.LIB
        GDI32.LIB
        ADVAPI32.LIB
        CRYPT32.LIB
        USER32.LIB
    )
  endforeach()
endif()

mark_as_advanced(
  LIB_EAY_DEBUG
  LIB_EAY_RELEASE

  SSL_EAY_DEBUG
  SSL_EAY_RELEASE
)
