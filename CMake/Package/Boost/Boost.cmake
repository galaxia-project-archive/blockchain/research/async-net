# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_PACKAGE_BOOST)
  return()
endif()
set(CMAKE_XI_PACKAGE_BOOST TRUE)

hunter_add_package(Boost)

find_package(Boost)

mark_as_advanced(BZip2_DIR)

set(Boost_VERSION ${Boost_VERSION} CACHE INTERNAL "Boost Version" FORCE)
mark_as_advanced(Boost_VERSION)
