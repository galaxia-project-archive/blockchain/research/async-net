# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_PACKAGE_BOOST_FIBER)
  return()
endif()
set(CMAKE_XI_PACKAGE_BOOST_FIBER TRUE)

xi_include(Package/Boost/Context)

hunter_add_package(
  Boost

  COMPONENTS
    fiber
)

find_package(Boost CONFIG REQUIRED fiber context)

target_link_libraries(
    Boost::fiber

    INTERFACE
        Boost::context
)
