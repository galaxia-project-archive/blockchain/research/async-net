# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_PACKAGE_BOOST_DATETIME)
  return()
endif()
set(CMAKE_XI_PACKAGE_BOOST_DATETIME TRUE)

hunter_add_package(
  Boost

  COMPONENTS
    date_time
)

find_package(Boost CONFIG REQUIRED date_time)