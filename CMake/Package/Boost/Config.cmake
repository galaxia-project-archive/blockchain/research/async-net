# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

hunter_config(
    Boost

    VERSION
      "1.70.0-p0"

    CMAKE_ARGS
      IOSTREAMS_NO_BZIP2=0
      IOSTREAMS_NO_ZLIB=0
)
