# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_PACKAGE_BOOST_IOSTREAMS)
  return()
endif()
set(CMAKE_XI_PACKAGE_BOOST_IOSTREAMS TRUE)

xi_include(Package/Zlib)
xi_include(Package/BZip2)

hunter_add_package(
  Boost

  COMPONENTS
    iostreams
)

find_package(Boost CONFIG REQUIRED iostreams)
