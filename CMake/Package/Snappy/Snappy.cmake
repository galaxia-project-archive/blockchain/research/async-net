# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_PACKAGE_SNAPPY)
  return()
endif()
set(CMAKE_XI_PACKAGE_SNAPPY TRUE)

hunter_add_package(sleef)
find_package(sleef CONFIG REQUIRED)
