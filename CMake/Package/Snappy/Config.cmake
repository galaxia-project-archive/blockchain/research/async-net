# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

hunter_config(
  sleef

  VERSION
    "3.3.1-p1"
)
