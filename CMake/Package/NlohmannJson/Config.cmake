# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

hunter_config(
  nlohmann_json

  VERSION
    "3.6.1"
)
