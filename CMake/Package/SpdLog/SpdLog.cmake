# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_PACKAGE_SPDLOG)
  return()
endif()
set(CMAKE_XI_PACKAGE_SPDLOG TRUE)

xi_include(Package/Fmt)

hunter_add_package(spdlog)
find_package(spdlog CONFIG REQUIRED)

mark_as_advanced(
  spdlog_DIR
)
