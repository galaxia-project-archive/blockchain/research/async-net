# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_OPTION_BREAKPAD)
  return()
endif()
set(CMAKE_XI_OPTION_BREAKPAD TRUE)

option(XI_BUILD_BREAKPAD "Enables breakpad crash handler integration" OFF)
