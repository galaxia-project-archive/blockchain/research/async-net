# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_OPTION_INSTALL)
  return()
endif()
set(CMAKE_XI_OPTION_INSTALL TRUE)

option(XI_INSTALL "Integrates install targets." ON)
