# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_OPTION_BUILD_UNITTEST)
    return()
endif()
set(CMAKE_XI_OPTION_BUILD_UNITTEST TRUE)

option(XI_BUILD_UNITTEST "Enables unit tests for the framework" OFF)