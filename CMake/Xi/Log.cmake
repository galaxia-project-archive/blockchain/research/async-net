# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_LOG)
    return()
endif()
set(CMAKE_XI_LOG TRUE)

option(XI_CMAKE_DEBUG "Enables verbose output for cmake debugging purposes." OFF)

set(XI_LOG_PREFIX "[XI] " CACHE INTERNAL "" FORCE)
set(XI_LOG_INDENT " -- " CACHE INTERNAL "" FORCE)

# xi_log_format(
#   <msg>               << message to be displayed
#   [...]               << items to be listed
# )
# Formats log messages for all aliases
macro(xi_log_format out_var header)
    set(${out_var} ${header})
    foreach(line ${ARGN})
        set(${out_var} "${${out_var}}\n${XI_LOG_INDENT}${line}")
    endforeach()
endmacro() # xi_log_format

# xi_status(
#   <msg>               << message to be displayed
#   [...]               << items to be listed
# )
function(xi_status)
    xi_log_format(log_message ${ARGN})
    message(STATUS ${XI_LOG_PREFIX} ${log_message})
endfunction() # xi_status

if(XI_CMAKE_DEBUG)
    function(xi_debug)
        xi_log_format(log_message ${ARGN})
        message(STATUS ${XI_LOG_PREFIX} "[DEBUG] " ${log_message})
    endfunction() # xi_debug
else()
    function(xi_debug)
        # empty on purpose
    endfunction() # xi_debug
endif()

# xi_warning(
#   <msg>               << message to be displayed
#   [...]               << items to be listed
# )
function(xi_warning)
    xi_log_format(log_message ${ARGN})
    message(WARNING ${XI_LOG_PREFIX} ${log_message})
endfunction() # xi_warning

# xi_error(
#   <msg>               << message to be displayed
#   [...]               << items to be listed
# )
function(xi_error)
    xi_log_format(log_message ${ARGN})
    message(SEND_ERROR ${XI_LOG_PREFIX} ${log_message})
endfunction() # xi_error

# xi_fatal(
#   <msg>               << message to be displayed
#   [...]               << items to be listed
# )
function(xi_fatal)
    xi_log_format(log_message ${ARGN})
    message(FATAL_ERROR ${XI_LOG_PREFIX} ${log_message})
endfunction() # xi_fatal
