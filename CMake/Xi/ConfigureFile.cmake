# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_CONFIGURE_FILE)
    return()
endif()
set(CMAKE_XI_CONFIGURE_FILE TRUE)

xi_include(Xi/Log)

macro(xi_configure_file input output)
    if(EXISTS ${output})
        set(temp_output ${output}.temp)
        configure_file(${input} ${temp_output} ${ARGN})
        file(SHA256 ${temp_output} temp_hash)
        file(SHA256 ${output} out_hash)
        if(NOT temp_hash STREQUAL out_hash)
            file(RENAME ${temp_output} ${output})
            xi_status("Configuration file updated." ${output})
        else()
            file(REMOVE ${temp_output})
        endif()
    else()
        configure_file(${input} ${output} ${ARGN})
        xi_status("Configuration file initialized." ${output})
    endif()
endmacro()
