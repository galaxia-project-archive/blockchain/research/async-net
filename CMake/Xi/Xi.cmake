# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_XI)
    return()
endif()
set(CMAKE_XI_XI TRUE)

set(XI_ROOT_DIR "${CMAKE_CURRENT_SOURCE_DIR}")
set(XI_CMAKE_SOURCE_DIR "${XI_ROOT_DIR}/CMake" CACHE INTERNAL "" FORCE)
include(${XI_CMAKE_SOURCE_DIR}/Xi/Include.cmake)

xi_include(Hunter)
xi_include(Xi/Log)

xi_debug(
  "CMake initialized."
  "XI_ROOT_DIR=${XI_ROOT_DIR}"
  "XI_CMAKE_SOURCE_DIR=${XI_CMAKE_SOURCE_DIR}"
)

set(XI_PROJECT_NAME "Xi")
set(XI_PROJECT_COMPANY "Michael Herwig")
set(XI_PROJECT_DESCRIPTION "An evolving framework for blockchain-based applications.")
set(XI_PROJECT_HOMEPAGE "TBD")
set(XI_PROJECT_COPYRIGHT "Copyright 2018-present Michael Herwig <michael.herwig@hotmail.de>")

file(READ "${CMAKE_SOURCE_DIR}/VERSION" version_content)
string(REPLACE "\n" ";" version_content "${version_content}")
list(GET version_content 0 XI_PROJECT_VERSION)

xi_status(
  "Project Info"
    "Name             ${XI_PROJECT_NAME}"
    "Company          ${XI_PROJECT_COMPANY}"
    "Description      ${XI_PROJECT_DESCRIPTION}"
    "Homepage         ${XI_PROJECT_HOMEPAGE}"
    "Copyright        ${XI_PROJECT_COPYRIGHT}"
    "Version          ${XI_PROJECT_VERSION}"
)
