# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_VERSION)
  return()
endif()
set(CMAKE_XI_VERSION TRUE)

xi_include(Xi/Make/Version)
xi_make_version(${XI_ROOT_DIR}/VERSION Version.Project XI_VERSION)
