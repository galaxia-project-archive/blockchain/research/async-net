# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_MAKE_UNITTEST)
    return()
endif()
set(CMAKE_XI_MAKE_UNITTEST TRUE)

xi_include(Xi/Log)
xi_include(Xi/Make/SetProperties)
xi_include(Xi/Make/SetCompilerFlags)
xi_include(Xi/Option/BuildUnitTest)

# xi_make_unit_test(
#   <source_dir>                    < The library source directory
#   <lib_name>                      < Library target name
#   <target>                        < Target to link against the test executable (ie. the library to test)
#   [TEST_LIBRARY <lib>]            < Overrides the default GMock::main library usage
#   [TEST_LIBRARIES <lib>...]       < Additional libraries linked to the test suite
# )
# Creates a <lib_name>.UnitTest target linked against gmock including a
# main function running all tests specified in source files contained by
# <source_dir>/Tests.

if(XI_BUILD_UNITTEST)
    xi_debug("unit tests enabled (XI_BUILD_UNITTEST=ON)...")
    enable_testing()
    include(CMakeParseArguments)

    macro(xi_make_unit_test source_dir lib_name target)
        cmake_parse_arguments(
            XI_MAKE_UNIT_TESTS
            ""
            "TEST_LIBRARY"
            "TEST_LIBRARIES"
            ${ARGN}
        )

        set(test_name "UnitTest.${lib_name}")
        set(test_dir "${source_dir}/Test")
        xi_make_file_search(test_files ${test_dir})
        source_group(TREE ${test_dir} FILES ${test_files})
        xi_make_set_compiler_flags(${test_files})

        if(NOT XI_MAKE_UNIT_TESTS_TEST_LIBRARY)
            set(XI_MAKE_UNIT_TESTS_TEST_LIBRARY GMock::main)
        endif()

        if(test_files)
            xi_include(Package/GTest)
            add_executable(${test_name} ${test_files})
            xi_make_set_properties(${test_name})
            target_link_libraries(
                ${test_name}

                PRIVATE
                    ${target}
                    ${XI_MAKE_UNIT_TESTS_TEST_LIBRARIES}
                    ${XI_MAKE_UNIT_TESTS_TEST_LIBRARY}
            )
            target_include_directories(
                ${test_name}

                PRIVATE
                  ${test_dir}
            )
            get_target_property(target_deps ${target} LINK_LIBRARIES)
            foreach(testing_lib ${target_deps} ${target})
                if(TARGET "${testing_lib}::Testing")
                    target_link_libraries(
                        ${test_name}

                        PRIVATE
                            "${testing_lib}::Testing"
                    )
                endif()
            endforeach()

            add_test(NAME ${test_name} COMMAND ${test_name})
        else()
            # No tests available
        endif()
    endmacro()
else()
    xi_status("UnitTests are disabled (XI_BUILD_UNITTEST=OFF), skipping...")
    macro(xi_make_unit_test)
        # Dummy
    endmacro()
endif()
