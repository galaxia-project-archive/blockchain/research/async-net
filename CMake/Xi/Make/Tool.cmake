# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_MAKE_TOOL)
  return()
endif()
set(CMAKE_XI_MAKE_TOOL TRUE)

include(CMakeParseArguments)

xi_include(Xi/Log)
xi_include(Xi/Make/FileSearch)
xi_include(Xi/Make/SetProperties)
xi_include(Xi/Make/SetCompilerFlags)

set(XI_SOURCE_TOOL_ROOT "${XI_ROOT_DIR}/Source/Tool" CACHE INTERNAL "root source dir of all tool sources")

# xi_make_tool(
#   <tool_name>                           < Name of the tool (dots are used to identify namespacing)
#   [LIBRARIES <lib>...]                  < Libraries to link
# )
function(xi_make_tool tool_name_)
  cmake_parse_arguments(
    XI_MAKE_TOOL
      ""
      ""
      "LIBRARIES"

    ${ARGN}
  )

  set(tool_root "${XI_SOURCE_TOOL_ROOT}/${tool_name_}")
  set(tool_name "Tool.${tool_name_}")
  string(REPLACE "." "::" tool_namespace ${tool_name_})

  xi_debug(
    "Setting up tool: ${tool_name_}"
      "Name=${tool_name}"
      "Namespace=${tool_namespace}"
      "Root=${tool_root}"
  )

  set(tool_source_dir "${tool_root}/Source")
  xi_make_file_search(tool_source_files "${tool_source_dir}" HEADERS SOURCES)
  xi_make_set_compiler_flags(${tool_source_files})

  add_executable(${tool_name} ${tool_source_files})
  xi_make_set_properties(${tool_name})

  target_include_directories(
    ${tool_name}

    PRIVATE
      ${tool_source_dir}
  )

  target_link_libraries(
    ${tool_name}

    PRIVATE
      ${XI_MAKE_TOOL_LIBRARIES}
  )

  add_executable(${tool_namespace} ALIAS ${tool_name})
endfunction()
