# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_MAKE_APPLICATION)
  return()
endif()
set(CMAKE_XI_MAKE_APPLICATION TRUE)

xi_include(Xi/Log)
xi_include(Xi/Version)
xi_include(Xi/Option/Install)
xi_include(Xi/ConfigureFile)
xi_include(Xi/Compiler/Classify/Identifier)
xi_include(Xi/Make/FileSearch)
xi_include(Xi/Make/SetProperties)
xi_include(Xi/Make/SetCompilerFlags)

include(CMakeParseArguments)

set(XI_APPLICATION_ROOT_DIR "${XI_ROOT_DIR}/Source/Application" CACHE INTERNAL "")
set(XI_APPLICATION_DESC_FILE "${CMAKE_CURRENT_LIST_DIR}/Description.rc.in" CACHE INTERNAL "")

# xi_make_application(
#   <name>                            < The name of the application (namespaces are indicates as dots)
#   [LIBRARIES <lib>...]              < Libraries to link against
# )
#
# This function creates a new 'Application.<name>' target to built an application located at './Source/Application'
# (relative to the project root directory). By convention the application directory must have the following
# structure.
# ./Source/Application/<name>               < The application root directory
#   - Source/                               < Source files of the application (Headers + Sources)
#   - Asset/                                < Application assets
#     - Thumbnail.ico                       < Thumbnail application icon used for windows binaries
# The application itself will be built with lowercase and dashes (ie. Xi.Miner -> xi-miner). Additionally the
# application name should not be used in cmake itself. If you need to call the app from cmake consider calling the
# exported namespace application (ie. Xi.Miner -> Xi::Miner).
function(xi_make_application app_name_)
  set(app_root_dir "${XI_APPLICATION_ROOT_DIR}/${app_name_}")
  set(app_asset_dir "${app_root_dir}/Asset")
  set(app_source_dir "${app_root_dir}/Source")
  set(app_target_name "Application.${app_name_}")
  string(REPLACE "." "::" app_target_namespace ${app_name_})
  string(REPLACE "." "-" app_binary_name ${app_name_})
  string(TOLOWER ${app_binary_name} app_binary_name)

  set(app_include_dirs "${app_source_dir}")

  cmake_parse_arguments(
    XI_MAKE_APPLICATION
      ""
      ""
      "LIBRARIES"

    ${ARGN}
  )

  xi_make_file_search(app_source_files ${app_source_dir} HEADERS SOURCES)
  xi_make_set_compiler_flags(${app_source_files})

  if(XI_COMPILER_MSVC)
    set(thumbnail_source_file "${app_asset_dir}/Thumbnail.ico")
    if(EXISTS "${thumbnail_source_file}")
      list(APPEND app_source_files "${thumbnail_source_file}")
      list(APPEND app_include_dirs ${app_asset_dir})
    else()
      xi_fatal("Missing thumbnail icon '${thumbnail_source_file}' for '${app_name_}'.")
    endif()

    set(description_bin_dir ${CMAKE_CURRENT_BINARY_DIR}/description/${app_name_})
    set(description_file ${description_bin_dir}/Description.rc)

    set(XI_APP_NAME "${app_binary_name}")

    file(MAKE_DIRECTORY ${description_bin_dir})
    xi_configure_file(${XI_APPLICATION_DESC_FILE} ${description_file})
    list(APPEND app_source_files ${description_file})
  endif()

  add_executable(${app_target_name} ${app_source_files})
  xi_make_set_properties(${app_target_name})

  target_include_directories(
    ${app_target_name}

    PRIVATE
      ${app_include_dirs}
  )

  target_link_libraries(
    ${app_target_name}

    PRIVATE
      Version::Project

      ${XI_MAKE_APPLICATION_LIBRARIES}
  )

  set_target_properties(
    ${app_target_name}

    PROPERTIES
      OUTPUT_NAME "${app_binary_name}"
  )

  add_executable(${app_target_namespace} ALIAS ${app_target_name})

  xi_debug(
    "Application '${app_name_}':"
      "Target: ${app_target_name}"
      "Namespace: ${app_target_namespace}"
      "Source: ${app_source_dir}"
      "Binary: ${app_binary_name}"
  )

  if(XI_INSTALL)
    install(
      TARGETS
        ${app_target_name}

      PERMISSIONS
        OWNER_EXECUTE OWNER_READ
        GROUP_EXECUTE GROUP_READ
        WORLD_EXECUTE WORLD_READ

      RUNTIME
        DESTINATION bin
    )
  endif()
endfunction()
