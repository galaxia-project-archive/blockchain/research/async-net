﻿# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_MAKE_LIBRARY)
    return()
endif()
set(CMAKE_XI_MAKE_LIBRARY TRUE)

include(CMakeParseArguments)

xi_include(Xi/ConfigureFile)
xi_include(Xi/Compiler)
xi_include(Xi/Compiler/Classify/Linkage)
xi_include(Xi/Compiler/Classify/Platform)
xi_include(Xi/Make/Placeholder)
xi_include(Xi/Make/UnitTest)
xi_include(Xi/Make/FileSearch)
xi_include(Xi/Make/CodeDoc)
xi_include(Xi/Make/SetProperties)
xi_include(Xi/Make/SetCompilerFlags)

if(XI_BUILD_UNITTEST)
  xi_include(Package/GTest)
endif()

option(XI_LIBRARY_USE_PLACEHOLDER "Adds a placeholder cpp file for interface libraries." OFF)

set(XI_LIBRARY_ROOT "${XI_ROOT_DIR}/Source/Library" CACHE INTERNAL "root source dir of all internal libraries")
set(XI_LEGACY_ROOT "${XI_ROOT_DIR}/Source/Library/Legacy" CACHE INTERNAL "root source dir of all legacy libraries")
set(XI_LIBRARY_API_TEMPLATE "${CMAKE_CURRENT_LIST_DIR}/Api.hh.in" CACHE INTERNAL "")
set(XI_GENERATED_FILE_NOTICE "// This file is generated, you may not persist any changes here." CACHE INTERNAL "")
# xi_make_library(
#   <lib_name>                          < Library name (dots are used to identify namespacing)
#   [LEGACY]                            < Indicates a legacy library
#   [SOURCE_DIR <dir>]                  < Overrides canoncial source directory
#   [PUBLIC_LIBRARIES <lib>...]         < Public dependencies
#   [PRIVATE_LIBRARIES <lib>...]        < Private dependencies
#   [...]                               < Forwarded to xi_make_unit_test
# )
#
# This script sets up the library Xi::SOURCE_DIR_ROOT::SOURCE_DIR_CHILD::... Please always use
# the namespaced version of the library as its more error prone in cmake scripts.
#
# Each library needs to have a canonicial form with the following structure.
#   - ./Include             < Interface include files
#   - ./Source              < Impelementation files and interfaces being used internally.
#   - ./Testing             < Testing library with mocks and matchers to be used to test this library.
#   - ./Tests               < Unit-Test source/include files.
#   - ./Doc                 < Additional documentation files, like images diagarams and so on.
#
# If unit tests are enabled and unit test files are present a unit test target is created including
# all unit test files in the directory. The main method is automiatically linked, thus you do not
# need to take care of it.
#
# If a library has no source files it is created as an inteface library and sources may not show
# up, depending on your IDE. Further an interface library may not have any private dependency.
function(xi_make_library lib_name_)
    cmake_parse_arguments(
        XI_MAKE_LIBRARY
            "LEGACY"
            "TEST_LIBRARY;SOURCE_DIR"
            "PUBLIC_LIBRARIES;PRIVATE_LIBRARIES;TEST_LIBRARIES"
            ${ARGN}
    )

    set(lib_name ${lib_name_})
    if(DEFINED XI_MAKE_LIBRARY_SOURCE_DIR)
      set(source_dir "${XI_MAKE_LIBRARY_SOURCE_DIR}")
    else()
      set(source_dir "${XI_LIBRARY_ROOT}/${lib_name}")
    endif()
    string(REPLACE "." "::" lib_namespace ${lib_name})

    string(REPLACE "::" ";" lib_namespace_list ${lib_namespace})
    set(lib_name "Library.${lib_name}")

    xi_debug(
      "Setting up library: ${lib_name_}"
        "Name=${lib_name}"
        "Namespace=${lib_namespace}"
        "Source=${source_dir}"
    )

    # Some configuration processing variables.
    string(REPLACE "." "_" XI_MAKE_LIBRARY_CURRENT_MACRO_PREFIX ${lib_name})
    string(TOUPPER ${XI_MAKE_LIBRARY_CURRENT_MACRO_PREFIX} XI_MAKE_LIBRARY_CURRENT_MACRO_PREFIX)

    if(XI_COMPILER_SHARED)
        set(XI_MAKE_LIBRARY_CURRENT_IS_SHARED 1)
        set(XI_MAKE_LIBRARY_CURRENT_IS_STATIC 0)
    else()
        set(XI_MAKE_LIBRARY_CURRENT_IS_SHARED 0)
        set(XI_MAKE_LIBRARY_CURRENT_IS_STATIC 1)
    endif()

    set(lib_include_dir "${source_dir}/Include")
    set(lib_source_dir "${source_dir}/Source")

    set(lib_public_include_dirs "")
    if(EXISTS ${lib_include_dir})
      list(APPEND lib_public_include_dirs ${lib_include_dir})
    endif()

    set(lib_private_include_dirs "")
    if(EXISTS ${lib_source_dir})
      list(APPEND lib_private_include_dirs ${lib_source_dir})
    endif()

    set(lib_namespace_path "")
    foreach(namespace_dir ${lib_namespace_list})
        set(lib_namespace_path "${lib_namespace_path}/${namespace_dir}")
    endforeach()

    xi_make_file_search(lib_include_files ${lib_include_dir} HEADERS)
    source_group(TREE ${lib_include_dir} PREFIX Include FILES ${lib_include_files})
    xi_make_file_search(lib_source_files ${lib_source_dir} HEADERS SOURCES)
    source_group(TREE ${lib_source_dir} PREFIX Source FILES ${lib_source_files})


    # --------------------------------------------------------------------------------------------------------------- #
    # BEGIN                                                                                                           #
    # Platform Dependent Code                                                                                         #
    # --------------------------------------------------------------------------------------------------------------- #
    foreach(platform ${XI_COMPILER_CLASSIFY_SUPPORTED_PLATFORMS})
      if(NOT ${XI_COMPILER_PLATFORM_${platform}})
        continue()
      endif()
      set(platform_name ${XI_COMPILER_PLATFORM_${platform}_NAME})
      set(platform_source_dir "${source_dir}/Platform/${platform_name}")
      set(platform_include_dir "${platform_source_dir}/Include")
      set(platform_source_dir "${platform_source_dir}/Source")

      xi_make_file_search(platform_include_files ${platform_include_dir} HEADERS)
      source_group(TREE ${platform_include_dir} PREFIX Include FILES ${platform_include_files})
      xi_make_file_search(platform_source_files ${platform_source_dir} HEADERS SOURCES)
      source_group(TREE ${platform_source_dir} PREFIX Source FILES ${platform_source_files})

      if(EXISTS ${platform_source_dir})
        list(APPEND lib_private_include_dirs ${platform_source_dir})
      endif()

      if(EXISTS ${platform_include_dir})
        list(APPEND lib_public_include_dirs ${platform_include_dir})
      endif()

      list(APPEND lib_source_files ${platform_source_files})
      list(APPEND lib_include_files ${platform_include_files})
    endforeach()
    # --------------------------------------------------------------------------------------------------------------- #
    # Platform Dependent Code                                                                                         #
    # END                                                                                                             #
    # --------------------------------------------------------------------------------------------------------------- #

    # --------------------------------------------------------------------------------------------------------------- #
    # BEGIN                                                                                                           #
    # Library Configuration Files                                                                                     #
    # --------------------------------------------------------------------------------------------------------------- #
    set(lib_config_include_dir ${CMAKE_CURRENT_BINARY_DIR}/config/${lib_name})
    set(lib_config_header_out_dir ${lib_config_include_dir}/${lib_namespace_path}/Config)
    list(APPEND lib_public_include_dirs "${lib_config_include_dir}")

    set(lib_config_file ${source_dir}/Config.hh.in)
    set(lib_config_out_file ${lib_config_header_out_dir}/Config.hh)
    source_group(Generated FILES lib_config_out_file)

    if(EXISTS ${lib_config_file})
        file(MAKE_DIRECTORY ${lib_config_header_out_dir})
        xi_debug("Configuration file '${lib_config_file}' detected.")
        list(APPEND lib_include_files ${lib_config_out_file})
        xi_configure_file(${lib_config_file} ${lib_config_out_file} @ONLY)
    endif()
    # --------------------------------------------------------------------------------------------------------------- #
    # Library Configuration Files                                                                                     #
    # END                                                                                                             #
    # --------------------------------------------------------------------------------------------------------------- #

    if(NOT lib_source_files AND NOT lib_include_files)
      xi_fatal("${lib_name} does not have any source file.")
    endif()

    if(NOT lib_source_files AND XI_LIBRARY_USE_PLACEHOLDER)
      xi_debug("Embedding placeholder for interface library.")
      set(lib_source_files "${XI_MAKE_PLACEHOLDER}")
    endif()

    xi_make_set_compiler_flags(${lib_include_files} ${lib_source_files})

    if(lib_source_files)
        add_library(${lib_name} ${lib_include_files} ${lib_source_files})

        target_include_directories(
            ${lib_name}

            PUBLIC
                ${lib_public_include_dirs}

            PRIVATE
                ${lib_private_include_dirs}
        )

        target_link_libraries(
            ${lib_name}

            PUBLIC
                ${XI_MAKE_LIBRARY_PUBLIC_LIBRARIES}

            PRIVATE
                ${XI_MAKE_LIBRARY_PRIVATE_LIBRARIES}
        )

        set_source_files_properties(
            ${lib_source_files}

            PROPERTIES
                COMPILE_DEFINITIONS "${XI_MAKE_LIBRARY_CURRENT_MACRO_PREFIX}_SOURCE"
        )

        xi_make_set_properties(${lib_name})
    else()
        add_library(${lib_name} INTERFACE)
        target_sources(${lib_name} INTERFACE ${lib_include_files})
        target_include_directories(
            ${lib_name}

            INTERFACE
                ${lib_public_include_dirs}
        )
        if(PRIVATE_LIBRARIES)
            xi_fatal("Interface libraries may not have any private dependencies.")
        endif()
        target_link_libraries(
            ${lib_name}

            INTERFACE
                ${PUBLIC_LIBRARIES}
        )
    endif()

    add_library(${lib_namespace} ALIAS ${lib_name})

    if(IS_DIRECTORY "${source_dir}/Testing" AND NOT DEFINED XI_MAKE_LIBRARY_SOURCE_DIR)
      if(XI_BUILD_UNITTEST)
        xi_make_library(
          "${lib_name_}.Testing"

          SOURCE_DIR
            "${source_dir}/Testing"

          PUBLIC_LIBRARIES
            GMock::gmock
            ${lib_namespace}
        )
      endif()
    endif()

    xi_make_unit_test(
        ${source_dir} ${lib_name_} ${lib_namespace}
        ${ARGN}
    )
#    xi_make_code_doc(${source_dir} ${lib_name})

endfunction()
