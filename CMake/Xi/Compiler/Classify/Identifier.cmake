# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_COMPILER_CLASSIFY_IDENTIFIER)
    return()
endif()
set(CMAKE_XI_COMPILER_CLASSIFY_IDENTIFIER TRUE)

xi_include(Xi/Log)

if(NOT CMAKE_C_COMPILER_ID STREQUAL CMAKE_CXX_COMPILER_ID)
    xi_fatal("C and CXX compiler do not match.")
endif()


set(
    XI_COMPILER_CLASSIFY_SUPPORTED_IDENTIFIERS
        MSVC
        GCC
        CLANG

    CACHE INTERNAL "" FORCE
)

foreach(compiler ${XI_COMPILER_CLASSIFY_SUPPORTED_IDENTIFIERS})
    set(XI_COMPILER_${compiler} OFF CACHE INTERNAL "" FORCE)
endforeach()

if(CMAKE_C_COMPILER_ID STREQUAL "MSVC")
    set(XI_COMPILER_MSVC ON CACHE INTERNAL "" FORCE)
    set(XI_COMPILER_ID "MSVC" CACHE INTERNAL "" FORCE)
elseif(CMAKE_C_COMPILER_ID STREQUAL "GNU")
    set(XI_COMPILER_GCC ON CACHE INTERNAL "" FORCE)
    set(XI_COMPILER_ID "GCC" CACHE INTERNAL "" FORCE)
elseif(CMAKE_C_COMPILER_ID MATCHES "^(Apple)?Clang$")
    set(XI_COMPILER_CLANG ON CACHE INTERNAL "" FORCE)
    set(XI_COMPILER_ID "CLANG" CACHE INTERNAL "" FORCE)
else()
    xi_fatal("Unsupported compiler id: ${CMAKE_C_COMPILER_ID}")
endif()

