# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_COMPILER_CLASSIFY)
    return()
endif()
set(CMAKE_XI_COMPILER_CLASSIFY TRUE)

xi_include(Xi/Compiler/Classify/Identifier)
xi_include(Xi/Compiler/Classify/Linkage)
xi_include(Xi/Compiler/Classify/Endianess)
xi_include(Xi/Compiler/Classify/Platform)

xi_include(Xi/Log)
xi_status(
    "Classify"
    "Identifier: ${XI_COMPILER_ID}"
    "Linkage: ${XI_COMPILER_LINK_TYPE}"
    "Endianess: ${XI_COMPILER_ENDIANESS}"
    "Platform: ${XI_COMPILER_PLATFORM_ID}"
)
