# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_COMPILER_ADD_FLAG)
  return()
endif()
set(CMAKE_XI_COMPILER_ADD_FLAG TRUE)

xi_include(Xi/Log)

include(CMakeParseArguments)

# xi_compiler_flag_add(
#   [<flag>...]                 < Flags to add for C and CXX source files.
#   [C <flag>...]               < Flags to add for C source files.
#   [CXX <flag>...]             < Flags to add for CXX source files.
# )
macro(xi_compiler_flag_add)
  cmake_parse_arguments(
    COMPILER_FLAG_ADD
      ""
      ""
      "C;CXX"

    ${ARGN}
  )

  foreach(flag ${COMPILER_FLAG_ADD_UNPARSED_ARGUMENTS})
    if(DEFINED XI_C_FLAGS)
      set(XI_C_FLAGS "${XI_C_FLAGS} ${flag}")
    else()
      set(XI_C_FLAGS "${flag}")
    endif()
    if(DEFINED XI_CXX_FLAGS)
      set(XI_CXX_FLAGS "${XI_CXX_FLAGS} ${flag}")
    else()
      set(XI_CXX_FLAGS "${flag}")
    endif()
  endforeach()

  foreach(flag ${COMPILER_FLAG_ADD_C})
    if(DEFINED XI_C_FLAGS)
      set(XI_C_FLAGS "${XI_C_FLAGS} ${flag}")
    else()
      set(XI_C_FLAGS "${flag}")
    endif()
  endforeach()

  foreach(flag ${COMPILER_FLAG_ADD_CXX})
    if(DEFINED XI_CXX_FLAGS)
      set(XI_CXX_FLAGS "${XI_CXX_FLAGS} ${flag}")
    else()
      set(XI_CXX_FLAGS "${flag}")
    endif()
  endforeach()
endmacro()


