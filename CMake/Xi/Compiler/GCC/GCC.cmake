# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_COMPILER_GCC)
    return()
endif()
set(CMAKE_XI_COMPILER_GCC TRUE)

xi_include(Xi/Option/TreatWarningAsError)
xi_include(Xi/Option/Breakpad)
xi_include(Xi/Compiler/Flag/Add)
xi_include(Xi/Compiler/Check)

xi_compiler_check(
  REQUIRED 7.3 
  UPGRADE 8.1
  RECOMMENDED 8.3
)

xi_compiler_flag_add("-Wall -Wextra")

if(XI_TREAT_WARNING_AS_ERROR)
  xi_compiler_flag_add("-Werror")
endif()

if(XI_BUILD_BREAKPAD)
  if(CMAKE_BUILD_TYPE MATCHES RELEASE OR CMAKE_BUILD_TYPE MATCHES MINSIZEREL)
    xi_compiler_flag_add("-g -fno-omit-frame-pointer")
  endif()
endif()
