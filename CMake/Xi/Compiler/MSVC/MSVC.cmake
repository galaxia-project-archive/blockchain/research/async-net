# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_COMPILER_MSVC)
    return()
endif()
set(CMAKE_XI_COMPILER_MSVC TRUE)

xi_include(Xi/Log)
xi_include(Xi/Option/TreatWarningAsError)
xi_include(Xi/Option/Breakpad)
xi_include(Xi/Compiler/Flag/Add)
xi_include(Xi/Compiler/Check)
xi_include(Xi/Compiler/MSVC/Sdk)
xi_include(Xi/Compiler/MSVC/Parallel)

xi_compiler_check(
  REQUIRED 19.10
  UPGRADE 19.15
  RECOMMENDED 19.23
)

xi_compiler_flag_add("/FS")
xi_compiler_flag_add("/W4")
xi_compiler_flag_add("/D_CRT_SECURE_NO_WARNINGS")

# Required by the legacy logging interface
# REPLACE
xi_compiler_flag_add("/wd4239")

if(XI_TREAT_WARNING_AS_ERROR)
  xi_compiler_flag_add("/WX")
endif()

if(XI_BUILD_BREAKPAD)
  if(CMAKE_BUILD_TYPE MATCHES RELEASE OR CMAKE_BUILD_TYPE MATCHES MINSIZEREL)
    xi_compiler_flag_add("/DEBUG")
  endif()
endif()
