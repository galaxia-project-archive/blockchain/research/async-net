# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_COMPILER_MSVC_PARALLEL)
    return()
endif()
set(CMAKE_XI_COMPILER_MSVC_PARALLEL TRUE)

option(XI_COMPILER_PARALLEL_BUILD OFF "enables parallel compiler execution")
set(XI_COMPILER_PARALLEL_BUILD_THREADS "-1" CACHE STRING
        "maximum number of threads to use for parallel compilation (<=0 -> max)")

xi_include(Xi/Log)
xi_include(Xi/Compiler/Flag/Add)

if(XI_COMPILER_PARALLEL_BUILD)
  if(XI_COMPILER_PARALLEL_BUILD_THREADS LESS_EQUAL "0")
    xi_status("Enabling parallel compilation with maximum threads.")
    xi_compiler_flag_add("/MP")
  else()
    xi_status("Enabling parallel compilation with ${XI_PARALLEL_BUILD_THREADS} threads.")
    xi_compiler_flag_add("/MP${XI_PARALLEL_BUILD_THREADS}")
  endif()
endif()
