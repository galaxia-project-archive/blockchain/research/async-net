# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_COMPILER_CHECK)
  return()
endif()
set(CMAKE_XI_COMPILER_CHECK TRUE)

include(CMakeParseArguments)

xi_include(Xi/Log)

# xi_compiler_check(
#   REQUIRED <version>                < Version required at least, if not fullfilled an fatal error is reported.
#   UPGRADE <version>                 < Lower versions are marked deprecated and new releases may drop support.
#   RECOMMENDED <version>             < This version is testet at most and recommended to use
# )
function(xi_compiler_check)
  cmake_parse_arguments(
    XI_COMPILER_CHECK
      ""
      "REQUIRED;UPGRADE;RECOMMENDED"
      ""

    ${ARGN}
  )

  if(NOT XI_COMPILER_CHECK_REQUIRED)
    xi_fatal("REQUIRED is not an optional parameter for xi_compiler_check")
  endif()

  if(CMAKE_C_COMPILER_VERSION VERSION_LESS ${XI_COMPILER_CHECK_REQUIRED})
    xi_fatal(
      "Your c compiler is outdated and not supported."
        "Compiler           ${CMAKE_C_COMPILER_ID}"
        "Current Version    ${CMAKE_C_COMPILER_VERSION}"
        "Required Version   ${XI_COMPILER_CHECK_REQUIRED}"
    )
  endif()

  if(CMAKE_CXX_COMPILER_VERSION VERSION_LESS ${XI_COMPILER_CHECK_REQUIRED})
    xi_fatal(
      "Your cxx compiler is outdated and not supported."
      "Compiler           ${CMAKE_CXX_COMPILER_ID}"
      "Current Version    ${CMAKE_CXX_COMPILER_VERSION}"
      "Required Version   ${XI_COMPILER_CHECK_REQUIRED}"
    )
  endif()

  if(XI_COMPILER_CHECK_UPGRADE)
    if(CMAKE_C_COMPILER_VERSION VERSION_LESS ${XI_COMPILER_CHECK_UPGRADE})
      xi_warning(
        "Your c compiler is outdated and will not be supported in upcoming versions."
        "Compiler           ${CMAKE_C_COMPILER_ID}"
        "Current Version    ${CMAKE_C_COMPILER_VERSION}"
        "Upgrade Version    ${XI_COMPILER_CHECK_UPGRADE}"
      )
    endif()

    if(CMAKE_CXX_COMPILER_VERSION VERSION_LESS ${XI_COMPILER_CHECK_UPGRADE})
      xi_warning(
        "Your cxx compiler is outdated and will not be supported in upcoming versions."
        "Compiler           ${CMAKE_CXX_COMPILER_ID}"
        "Current Version    ${CMAKE_CXX_COMPILER_VERSION}"
        "Upgrade Version    ${XI_COMPILER_CHECK_UPGRADE}"
      )
    endif()
  endif()

  if(XI_COMPILER_CHECK_RECOMMENDED)
    if(CMAKE_C_COMPILER_VERSION VERSION_LESS ${XI_COMPILER_CHECK_RECOMMENDED})
      xi_warning(
        "Your c compiler is outdated, a newer version is recommended."
        "Compiler              ${CMAKE_C_COMPILER_ID}"
        "Current Version       ${CMAKE_C_COMPILER_VERSION}"
        "Recommended Version   ${XI_COMPILER_CHECK_RECOMMENDED}"
      )
    endif()

    if(CMAKE_CXX_COMPILER_VERSION VERSION_LESS ${XI_COMPILER_CHECK_RECOMMENDED})
      xi_warning(
        "Your cxx compiler is outdated, a newer version is recommended"
        "Compiler              ${CMAKE_CXX_COMPILER_ID}"
        "Current Version       ${CMAKE_CXX_COMPILER_VERSION}"
        "Recommended Version   ${XI_COMPILER_CHECK_RECOMMENDED}"
      )
    endif()
  endif()
endfunction()
