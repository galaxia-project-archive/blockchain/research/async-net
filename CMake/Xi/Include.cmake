# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_INCLUDE)
    return()
endif()
set(CMAKE_XI_INCLUDE TRUE)

include(CMakeParseArguments)

# xi_include(
#   [GLOB_RECURSE]          < Module path is a glob expression and all subfiles that matches should be included.
#   <module>...             < Module paths to be included (ie. Xi/Log, Package/GTest)
# )
# Includes a module file based on its path. Paths like Hunter/Hunter can be shortened as Hunter.
macro(xi_include)
  cmake_parse_arguments(
    XI_INCLUDE
      "GLOB_RECURSE"
      ""
      ""

    ${ARGN}
  )

  if(XI_INCLUDE_GLOB_RECURSE)
    set(xi_include_patterns "")
    foreach(xi_include_pattern ${XI_INCLUDE_UNPARSED_ARGUMENTS})
      list(APPEND xi_include_patterns "${XI_CMAKE_SOURCE_DIR}/${xi_include_pattern}")
    endforeach()
    file(
      GLOB_RECURSE xi_include_modules
      RELATIVE "${XI_CMAKE_SOURCE_DIR}"
      ${xi_include_patterns}
    )
  else()
    set(xi_include_modules ${XI_INCLUDE_UNPARSED_ARGUMENTS})
  endif()

  foreach(xi_include_module ${xi_include_modules})
    set(xi_include_file_to_include ${XI_CMAKE_SOURCE_DIR}/${xi_include_module})
    if(IS_DIRECTORY ${xi_include_file_to_include})
      get_filename_component(xi_include_module_name "${xi_include_file_to_include}" NAME)
      set(xi_include_file_to_include "${xi_include_file_to_include}/${xi_include_module_name}")
    endif()
    if(NOT EXISTS "${xi_include_file_to_include}")
      set(xi_include_file_to_include "${xi_include_file_to_include}.cmake")
    endif()
    include(${xi_include_file_to_include})
  endforeach()
endmacro()
