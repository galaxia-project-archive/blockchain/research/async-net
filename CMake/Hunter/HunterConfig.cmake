# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

set(PACKAGE_ROOT_DIR ${CMAKE_CURRENT_LIST_DIR}/../Package)

file(GLOB package_dirs LIST_DIRECTORIES ON ${PACKAGE_ROOT_DIR}/*)
foreach(package_dir ${package_dirs})
    set(package_config_file ${package_dir}/Config.cmake)
    if(EXISTS ${package_config_file})
        include(${package_config_file})
    endif()
endforeach()
