# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_HUNTER_HUNTER)
    return()
endif()
set(CMAKE_XI_HUNTER_HUNTER TRUE)

set(HUNTER_CONFIGURATION_TYPES "${CMAKE_BUILD_TYPE}" CACHE INTERNAL "")
xi_include(Hunter/HunterGate)

# https://github.com/ruslo/hunter/releases
HunterGate(
    URL "https://github.com/ruslo/hunter/archive/v0.23.214.tar.gz"
    SHA1 "e14bc153a7f16d6a5eeec845fb0283c8fad8c358"
    FILEPATH "${CMAKE_CURRENT_LIST_DIR}/HunterConfig.cmake"
)
