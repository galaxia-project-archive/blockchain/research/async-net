# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_LIBRARY_Core)
  return()
endif()
set(CMAKE_XI_LIBRARY_Core TRUE)

xi_include(Xi/Make/Library)

xi_make_library(Xi.TypeSafe)
