# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_LIBRARY_CRYPTO_HASH)
  return()
endif()
set(CMAKE_XI_LIBRARY_CRYPTO_HASH TRUE)

xi_include(Xi/Make/Library)
xi_include(Package/OpenSsl)
xi_include(Source/Library/Xi/Core)
xi_include(Source/Library/Xi/Log)
xi_include(Source/Library/Xi/Memory)
xi_include(Source/Library/Xi/Endianess)
xi_include(Source/Library/Xi/Encoding)

xi_include(Xi/Option/BuildUnitTest.cmake)
if(XI_BUILD_UNITTEST)
  xi_include(Source/Library/Xi/Crypto/Random)
endif()

xi_make_library(
  Xi.Crypto.Hash

  PUBLIC_LIBRARIES
    Xi::Core
    Xi::Memory
    Xi::Encoding

  PRIVATE_LIBRARIES
    Xi::Log
    Xi::Endianess
    OpenSSL::Crypto

  TEST_LIBRARIES
    Xi::Crypto::Random
)
