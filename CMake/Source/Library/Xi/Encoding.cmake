# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_LIBRARY_ENCODING)
  return()
endif()
set(CMAKE_XI_LIBRARY_ENCODING TRUE)

xi_include(Xi/Make/Library)
xi_include(Source/Library/Xi/Core)
xi_include(Source/Library/Xi/Stream)

xi_make_library(
  Xi.Encoding

  PUBLIC_LIBRARIES
    Xi::Core
    Xi::Stream
)
