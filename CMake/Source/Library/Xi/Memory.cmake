# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_LIBRARY_MEMORY)
  return()
endif()
set(CMAKE_XI_LIBRARY_MEMORY TRUE)

xi_include(Xi/Make/Library)
xi_include(Source/Library/Xi/Core)
xi_include(Source/Library/Xi/TypeSafe)

xi_make_library(
  Xi.Memory

  PUBLIC_LIBRARIES
    Xi::Core
    Xi::TypeSafe
)
