# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_SOURCE_LIBRARY_XI_ASYNC)
  return()
endif()
set(CMAKE_XI_SOURCE_LIBRARY_XI_ASYNC TRUE)

xi_include(Xi/Make/Library)

xi_include(Package/Boost)
xi_include(Package/Boost/Fiber)

xi_include(Source/Library/Xi/Core)

xi_make_library(
  Xi.Async

  PUBLIC_LIBRARIES
    Xi::Core

    Boost::boost
    Boost::fiber

  TEST_LIBRARY
    Xi::Async::Testing
)

