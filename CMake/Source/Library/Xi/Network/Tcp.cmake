# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_SOURCE_LIBRARY_XI_NETWORK_TCP)
  return()
endif()
set(CMAKE_XI_SOURCE_LIBRARY_XI_NETWORK_TCP TRUE)

xi_include(Xi/Make/Library)

xi_include(Source/Library/Xi/Core)
xi_include(Source/Library/Xi/Async)
xi_include(Source/Library/Xi/Log)
xi_include(Source/Library/Xi/Stream)
xi_include(Source/Library/Xi/Serialization)
xi_include(Source/Library/Xi/Ssl)
xi_include(Source/Library/Xi/Network)

xi_make_library(
  Xi.Network.Tcp

  PUBLIC_LIBRARIES
    Xi::Core
    Xi::Async
    Xi::Stream
    Xi::Serialization
    Xi::Ssl
    Xi::Network

  PRIVATE_LIBRARIES
    Xi::Log

  TEST_LIBRARY
    Xi::Async::Testing
)
