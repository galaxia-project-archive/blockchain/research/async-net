# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_LIBRARY_SERIALIZATION_SERIALIZATION)
  return()
endif()
set(CMAKE_XI_LIBRARY_SERIALIZATION_SERIALIZATION TRUE)

xi_include(Xi/Make/Library)
xi_include(Xi/Make/UnitTest)
xi_include(Source/Library/Xi/Core)
xi_include(Source/Library/Xi/Memory)

if(XI_BUILD_UNITTEST)
  xi_include(Source/Library/Xi/Serialization/Binary)
  xi_include(Source/Library/Xi/Serialization/Json)
  xi_include(Source/Library/Xi/Serialization/Yaml)
endif()

xi_make_library(
  Xi.Serialization

  PUBLIC_LIBRARIES
    Xi::Core
    Xi::Memory

  TEST_LIBRARIES
    Xi::Serialization::Binary
    Xi::Serialization::Json
    Xi::Serialization::Yaml
)
