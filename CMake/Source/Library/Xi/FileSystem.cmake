# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_LIBRARY_FILESYSTEM)
  return()
endif()
set(CMAKE_XI_LIBRARY_FILESYSTEM TRUE)

xi_include(Xi/Make/Library)
xi_include(Source/Library/Xi/Core)
xi_include(Source/Library/Xi/TypeSafe)
xi_include(Source/Library/Xi/Stream)
xi_include(Source/Library/Xi/Memory)
xi_include(Source/Library/Xi/Serialization)

xi_make_library(
  Xi.FileSystem

  PUBLIC_LIBRARIES
    Xi::Core
    Xi::Stream
    Xi::TypeSafe
    Xi::Memory
    Xi::Serialization
)

if(XI_COMPILER_GCC)
  if(CMAKE_CXX_COMPILER_VERSION VERSION_LESS 9)
    target_link_libraries(
      Library.Xi.FileSystem

      PUBLIC
        stdc++fs
    )
  endif()
endif()
