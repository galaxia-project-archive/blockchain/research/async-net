# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_LIBRARY_STREAM)
  return()
endif()
set(CMAKE_XI_LIBRARY_STREAM TRUE)

xi_include(Xi/Make/Library)
xi_include(Source/Library/Xi/Core)
xi_include(Source/Library/Xi/Async)

xi_make_library(
  Xi.Stream

  PUBLIC_LIBRARIES
    Xi::Core
    Xi::Async

  TEST_LIBRARY
    Xi::Async::Testing
)
