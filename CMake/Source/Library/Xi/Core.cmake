# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_LIBRARY_XI)
  return()
endif()
set(CMAKE_XI_LIBRARY_XI TRUE)

xi_include(Xi/Make/Library)

xi_include(Package/Leathers)
xi_include(Package/Boost)
xi_include(Package/Boost/FileSystem)
xi_include(Package/Boost/Thread)
xi_include(Package/Fmt)

xi_make_library(
  Xi.Core

  PUBLIC_LIBRARIES
    Leathers::leathers
    fmt::fmt
    Boost::boost
    Boost::filesystem
    Boost::thread
)
