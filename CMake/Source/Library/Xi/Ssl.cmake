# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_SOURCE_LIBRARY_XI_SSL)
  return()
endif()
set(CMAKE_XI_SOURCE_LIBRARY_XI_SSL TRUE)

xi_include(Xi/Make/Library)

xi_include(Package/OpenSsl)

xi_include(Source/Library/Xi/Core)
xi_include(Source/Library/Xi/Log)
xi_include(Source/Library/Xi/Async)
xi_include(Source/Library/Xi/FileSystem)
xi_include(Source/Library/Xi/Serialization)

xi_make_library(
  Xi.Ssl

  PUBLIC_LIBRARIES
    Xi::Core
    Xi::Async
    Xi::FileSystem
    Xi::Serialization

    OpenSSL::Crypto
    OpenSSL::SSL

  PRIVATE_LIBRARIES
    Xi::Log
)
