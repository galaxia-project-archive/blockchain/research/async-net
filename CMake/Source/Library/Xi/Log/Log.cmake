# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_LIBRARY_LOG)
  return()
endif()
set(CMAKE_XI_LIBRARY_LOG TRUE)

xi_include(Xi/Make/Library)
xi_include(Package/Fmt)
xi_include(Package/SpdLog)
xi_include(Source/Library/Xi/Core)
xi_include(Source/Library/Xi/Async)

xi_make_library(
  Xi.Log

  PUBLIC_LIBRARIES
    Xi::Core
    Xi::Async

    fmt::fmt

  PRIVATE_LIBRARIES
    spdlog::spdlog

  TEST_LIBRARY
    Xi::Async::Testing
)
