# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_SOURCE_LIBRARY_XI_VERSIONINFO)
  return()
endif()
set(CMAKE_XI_SOURCE_LIBRARY_XI_VERSIONINFO TRUE)

set(
  XI_RELEASE_CHANNEL "clutter"
  CACHE STRING "Release channel this instance is built for [clutter|edge|beta|release]"
)

string(TOUPPER "${XI_RELEASE_CHANNEL}" XI_RELEASE_CHANNEL_UPPER)

xi_include(Xi/Make/Library)
xi_include(Xi/Version)
xi_include(Source/Library/Xi/Core)
xi_include(Source/Library/Xi/Serialization)

xi_make_library(
  Xi.VersionInfo

  PUBLIC_LIBRARIES
    Xi::Core
    Xi::Serialization

    Version::Project
)
