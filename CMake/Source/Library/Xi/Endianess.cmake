# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_LIBRARY_ENDIANESS)
  return()
endif()
set(CMAKE_XI_LIBRARY_ENDIANESS TRUE)

xi_include(Xi/Make/Library)
xi_include(Xi/Compiler/Classify/Endianess)

xi_make_library(Xi.Endianess)
