#!/bin/bash

START=$(date +%s.%N)
for i in {1..100}; do
   (echo >/dev/tcp/127.0.01/22868) &>/dev/null
   # (echo >/dev/tcp/127.0.01/22869) &>/dev/null
done
END=$(date +%s.%N)
DIFF=$(echo "$END - $START" | bc)
echo $DIFF
