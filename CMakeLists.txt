﻿# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

cmake_minimum_required(VERSION 3.14.0)

include(CMake/Xi/Xi.cmake)

cmake_policy(SET CMP0048 NEW)
project(
  ${XI_PROJECT_NAME}
  
  DESCRIPTION
    ${XI_PROJECT_DESCRIPTION}

  HOMEPAGE_URL
    ${XI_PROJECT_HOMEPAGE}

  VERSION
    ${XI_PROJECT_VERSION}

  LANGUAGES 
    C 
    CXX
)

xi_include(
  GLOB_RECURSE
    Source/Library/*
)
